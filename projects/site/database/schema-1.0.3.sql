USE pizzburgdb;
DROP TABLE IF EXISTS pizzburgdb.SitePages ;

CREATE TABLE IF NOT EXISTS pizzburgdb.SitePages (
  pageID INT UNSIGNED NOT NULL AUTO_INCREMENT,
  pageName VARCHAR(32) NOT NULL ,
  pageContent VARCHAR(65535) NOT NULL ,
  UNIQUE INDEX pageName_UNIQUE (pageName ASC) ,
  PRIMARY KEY (pageID) )
ENGINE = InnoDB;

INSERT INTO pizzburgdb.SitePages (pageName, pageContent)
  VALUES ('delivery', '
<b>Как сделать заказ?</b><br><br>
<img src="./site_files/0.jpg">
<b>20-47-107</b><br>
<img src="./site_files/1.gif">
<b>633030847</b><br>
<br>

<b>Часы работы</b><br>
ВС - ЧТ - с 10 до 00<br>
ПТ, СБ - с 10 до 02<br><br>

<b>Оплата</b><br>
Оплата заказа производится наличными курьеру<br><br>

<b>Бесплатная доставка при сумме заказа от 200 рублей:</b><br>
Центр<br>
Парковый<br>
Заостровка<br>
ДКЖ<br>
Пермь 1<br>
район ПГУ<br>
Балатово<br>
Нагорный<br>
ул. Чкалова<br>
район ЦКР<br>
ул.Мильчакова<br>
Гознак<br>
<br>

<b>Бесплатная доставка при сумме заказа от 300 рублей:</b><br>
Мотовилиха<br>
Садовый<br>
Юбилейный<br>
Южный<br>
Рабочий поселок<br>
Цирк<br>
ул. Дружбы<br>
Авторадио<br>
ул. 1905 года<br>
Висим<br>
Архиерейка<br>
Крохалева<br>
Камская долина<br>
Бахаревка<br>
Владимирский<br>
Велта<br>
Липовая гора<br>
<br>

<b>Бесплатная доставка при сумме заказа от 400 рублей:</b><br>
Пролетарка<br>
Комплекс ПГТУ<br>
Кондратово<br>
Верхние Муллы<br>
Запруд<br>
Вышка-1<br>
Верхняя Курья<br>
Песьянка<br>

<br>

<b>Бесплатная доставка при сумме заказа от 600 рублей:</b><br>
Вышка-2<br>
Закамск<br>
Акулова<br>
Большое Савино<br>
Нижняя Курья<br>
Гамово<br>
Осенцы<br>
<br>

<b>Бесплатная доставка при сумме заказе от 800 рублей:</b><br>
КамГЭС<br>
Кислотные дачи<br>
Левшино<br>
Гайва<br>
Ферма<br>
Фролы<br>
Лобаново<br>
Култаево<br>
Нижние Муллы<br>
<br>

<b>Бесплатная доставка при сумме заказа от 1000 рублей:</b><br>
Крым<br>
Заозерье<br>
<br>

<b>Бесплатная доставка при сумме заказа от 1500 рублей:</b><br>
Новые Ляды<br>
Голованово<br>
Краснокамск<br>
<br>
');