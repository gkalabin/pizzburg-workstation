package ru.pizzburg.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Handles and retrieves requests to static pages
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Controller
@Lazy(false)
public class StaticPageController {
    @Autowired
    private MessageSource messageSource;

	protected final Log logger = LogFactory.getLog(getClass());

	@RequestMapping(value = "/delivery", method = RequestMethod.GET)
	public String getDeliveryPage(ModelMap model) {
        model.put("pageTitle", messageSource.getMessage("title-delivery", null, null));
        model.put("pageContent", "omnomnom");
		return "static";
	}
}
