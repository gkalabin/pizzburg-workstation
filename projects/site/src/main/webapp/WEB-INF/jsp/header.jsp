<%@page contentType="text/html" pageEncoding="UTF-8" %>
<table width="100%" cellspacing="0" cellpadding="0" class="content">
    <tbody>
    <tr> <!-- HEADER -->
        <td colspan="3"><img class="header" src="${pageContext.request.contextPath}/resources/images/header.png"></td>
    </tr>
    <!-- MENU -->
    <tr>
        <td class="promotionsCaption">Новости</td>
        <td class="separatorMenu">&nbsp;</td>
        <td class="navigationMenu">
            <nobr>
                <span class="menuSpring">&nbsp;</span>
                <a href="${pageContext.request.contextPath}/pizza">Пицца</a>
                <span class="menuSpring">&nbsp;</span>
                <a href="${pageContext.request.contextPath}/rolls">Роллы</a>
                <span class="menuSpring">&nbsp;</span>
                <a href="${pageContext.request.contextPath}/salads">Салаты</a>
                <span class="menuSpring">&nbsp;</span>
                <a href="${pageContext.request.contextPath}/desserts">Десерты</a>
                <span class="menuSpring">&nbsp;</span>
                <a href="${pageContext.request.contextPath}/drinks">Напитки</a>
                <span class="menuSpring">&nbsp;</span>
            </nobr>
        </td>
    </tr>

    <tr>
        <td class="promotionsCaptionBottom">&nbsp;</td>
        <td class="separatorMenuBottom">&nbsp;</td>
        <td class="navigationMenuBottom">&nbsp;</td>
    </tr>

    <tr>
        <td valign="top"> <!-- PROMOTIONS -->
            <ul class="promotions">
                <li class="promotionItem">
                    <div class="promotionTitle">
                        <b><img src="${pageContext.request.contextPath}/resources/images/pizza.gif"> Третья пицца в
                            подарок! </b>
                    </div>
					<span class="promotionCaption">
                        При заказе двух пицц - третья "пицца недели" или напиток в подарок! Подробности у оператора
                    </span>

                </li>

                <li class="promotionItem">
                    <span class="promotionCaption">Внимание: скидки и акции не суммируются</span>
                </li>
            </ul>
        </td>

        <td class="separatorContent">&nbsp;</td>
        <td valign="top" colspan="4">
            <div class="contentHeader">
                <nobr>
                    <span class="contentHeaderSpring">&nbsp;</span>

                    <a href="${pageContext.request.contextPath}/delivery">Условия доставки</a>
                    <span class="contentHeaderSpring">&nbsp;</span>

                    <a href="${pageContext.request.contextPath}/job">Вакансии</a>
                    <span class="contentHeaderSpring">&nbsp;</span>

                    <a href="${pageContext.request.contextPath}/about">О компании</a>
                    <span class="contentHeaderSpring">&nbsp;</span>

                </nobr>
            </div>

            <div class="contentData">