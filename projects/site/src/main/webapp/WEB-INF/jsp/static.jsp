<%--
    Document   : template for static pages
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <title>${pageTitle}</title>
    <fmt:message key="meta-description"/>
    <fmt:message key="meta-keywords"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/styles.css">
</head>
<body>
<%@ include file="/WEB-INF/jsp/header.jsp" %>
<fmt:message key="meta-description"/>
 ${pageContent}
<%@ include file="/WEB-INF/jsp/footer.jsp" %>
</body>
</html>