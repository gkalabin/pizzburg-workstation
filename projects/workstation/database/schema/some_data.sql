-- MySQL dump 10.13  Distrib 5.5.24, for Linux (x86_64)
--
-- Host: localhost    Database: pizzburgdb
-- ------------------------------------------------------
-- Server version	5.5.24-log
--
-- Dumping data for table `DiscountValues`
--

USE pizzburgdb;

LOCK TABLES `DiscountValues` WRITE;
/*!40000 ALTER TABLE `DiscountValues` DISABLE KEYS */;
INSERT INTO `DiscountValues` VALUES (1000,2),(2000,5),(5000,12);
/*!40000 ALTER TABLE `DiscountValues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Ingredients`
--

LOCK TABLES `Ingredients` WRITE;
/*!40000 ALTER TABLE `Ingredients` DISABLE KEYS */;
INSERT INTO `Ingredients` VALUES (1,'Мясо',200,'','кг',0),(2,'Рыба',150,'','кг',0),(3,'Тесто',50,'','кг',0),(4,'Соус',300,'','литр',0);
/*!40000 ALTER TABLE `Ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ProductGroups`
--

LOCK TABLES `ProductGroups` WRITE;
/*!40000 ALTER TABLE `ProductGroups` DISABLE KEYS */;
INSERT INTO `ProductGroups` VALUES (1,'Пицца',''),(2,'Подарок','');
/*!40000 ALTER TABLE `ProductGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Products`
--

LOCK TABLES `Products` WRITE;
/*!40000 ALTER TABLE `Products` DISABLE KEYS */;
INSERT INTO `Products` VALUES (1,'Пицца с мясом',''),(2,'Пицца с рыбой','');
/*!40000 ALTER TABLE `Products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ProductIngredients`
--

LOCK TABLES `ProductIngredients` WRITE;
/*!40000 ALTER TABLE `ProductIngredients` DISABLE KEYS */;
INSERT INTO `ProductIngredients` VALUES (1,1,0.1),(1,3,0.2),(1,4,0.04),(2,2,0.1),(2,3,0.2),(2,4,0.04);
/*!40000 ALTER TABLE `ProductIngredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ProductsGroups`
--

LOCK TABLES `ProductsGroups` WRITE;
/*!40000 ALTER TABLE `ProductsGroups` DISABLE KEYS */;
INSERT INTO `ProductsGroups` VALUES (1,1,200),(2,1,150),(1,2,0),(2,2,0);
/*!40000 ALTER TABLE `ProductsGroups` ENABLE KEYS */;
UNLOCK TABLES;

-- Dump completed on 2012-05-27  1:46:19
