-- For some reason OrdersProducts word gone from schema. In the dump from 13.06.12
-- fk_OrdersProducts_ProductGroups1 became fk__ProductGroups1

ALTER TABLE OrdersProducts DROP FOREIGN KEY fk_OrdersProducts_ProductGroups1;
ALTER TABLE OrdersProducts ADD CONSTRAINT fk_OrdersProducts_ProductGroups1
  FOREIGN KEY (groupID )
  REFERENCES pizzburgdb.ProductGroups (groupID )
  ON DELETE CASCADE
  ON UPDATE CASCADE;

-- ALTER TABLE OrdersProducts DROP FOREIGN KEY fk__ProductGroups1;
-- ALTER TABLE OrdersProducts ADD CONSTRAINT fk_OrdersProducts_ProductGroups1
--   FOREIGN KEY (groupID )
--   REFERENCES pizzburgdb.ProductGroups (groupID )
--   ON DELETE CASCADE
--   ON UPDATE CASCADE;