ALTER TABLE Orders ADD COLUMN personsCount INT UNSIGNED DEFAULT 1;
ALTER TABLE Orders ADD COLUMN comment VARCHAR(300) NULL;


-- ---------------------------------------------------------------------------------------------------------------------
-- triggers ------------------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------
DROP TRIGGER IF EXISTS `BI_OrdersProducts` ;
DROP TRIGGER IF EXISTS `BD_OrdersProducts` ;
DROP TRIGGER IF EXISTS `BU_OrdersProducts` ;
DROP TRIGGER IF EXISTS `BI_Orders` ;
DROP TRIGGER IF EXISTS `BU_Orders` ;
DROP TRIGGER IF EXISTS `BD_Orders` ;
DROP PROCEDURE IF EXISTS logDebug;

-- ---------------------------------------------------------------------------------------------------------------------
-- orders-products triggers --------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------
DELIMITER |
CREATE TRIGGER `BI_OrdersProducts` BEFORE INSERT ON `OrdersProducts` FOR EACH ROW 
begin
SET @price = (SELECT `price` FROM pizzburgdb.ProductsGroups WHERE productID = NEW.productID AND groupID= NEW.groupID);
SET @orderOldSum = (SELECT `cost` FROM pizzburgdb.Orders WHERE orderID = NEW.orderID);
SET @orderStatus = (SELECT `orderStatus` FROM pizzburgdb.Orders WHERE orderID = NEW.orderID);
SET @alternateDiscountCard = (SELECT `alternateDiscountCard` FROM pizzburgdb.Orders WHERE orderID = NEW.orderID);

-- update sum of order
SET @orderSum = @price * NEW.count;
UPDATE pizzburgdb.Orders SET `cost`=(@orderOldSum + @orderSum) WHERE orderID=NEW.orderID;

IF (@alternateDiscountCard IS NOT NULL)
THEN
  SET @clientPhone = (SELECT `phone` FROM pizzburgdb.Clients WHERE discountCardNumber = @alternateDiscountCard);
ELSE
  SET @clientPhone = (SELECT `clientPhone` FROM pizzburgdb.Orders WHERE orderID = NEW.orderID);
END IF;

-- update sum on client's account
SELECT `ordersSum` INTO @clientSum FROM pizzburgdb.Clients WHERE `phone`=@clientPhone;
UPDATE pizzburgdb.Clients SET `ordersSum`=`ordersSum` + @orderSum WHERE `phone`=@clientPhone;

-- update discount of order
SET @discount = 0;
SELECT `discount` INTO @discount FROM DiscountValues WHERE sumFrom<=(@clientSum + @orderSum) ORDER BY `sumFrom` DESC LIMIT 1;

SET @alternateDiscount = (SELECT `alternateDiscount` FROM pizzburgdb.Orders WHERE orderID = NEW.orderID);
IF (@alternateDiscount IS NOT NULL AND @alternateDiscount > @discount)
THEN
  SET @discount = @alternateDiscount;
END IF;

UPDATE pizzburgdb.Orders SET `discount`=@discount WHERE orderID=NEW.orderID;

IF (@orderStatus > 1) THEN
BEGIN
    CALL pizzburg_writeProductToStock(NEW.productID, NEW.count);
END; END IF;

CALL pizzburg_assignCardIfNeeded(NEW.orderID);
end;
|



DELIMITER |
CREATE TRIGGER `BD_OrdersProducts` BEFORE DELETE ON `OrdersProducts` FOR EACH ROW 
BEGIN
SET @price = (SELECT `price` FROM pizzburgdb.ProductsGroups WHERE productID = OLD.productID AND groupID= OLD.groupID);
SET @orderOldSum = (SELECT `cost` FROM pizzburgdb.Orders WHERE orderID = OLD.orderID);
SET @orderStatus = (SELECT `orderStatus` FROM pizzburgdb.Orders WHERE orderID = OLD.orderID);
SET @alternateDiscountCard = (SELECT `alternateDiscountCard` FROM pizzburgdb.Orders WHERE orderID = OLD.orderID);

-- update sum of order
SET @orderSum = @price * OLD.count;
UPDATE pizzburgdb.Orders SET `cost`=(@orderOldSum - @orderSum) WHERE orderID=OLD.orderID;

IF (@alternateDiscountCard IS NOT NULL)
THEN
  SET @clientPhone = (SELECT `phone` FROM pizzburgdb.Clients WHERE discountCardNumber = @alternateDiscountCard);
ELSE
  SET @clientPhone = (SELECT `clientPhone` FROM pizzburgdb.Orders WHERE orderID = OLD.orderID);
END IF;

-- update sum on client's account
SELECT `ordersSum` INTO @clientSum FROM pizzburgdb.Clients WHERE `phone`=@clientPhone;
UPDATE pizzburgdb.Clients SET `ordersSum`=`ordersSum` - @orderSum WHERE `phone`=@clientPhone;

-- update discount of order
SET @discount = 0;
SELECT `discount` INTO @discount FROM DiscountValues WHERE sumFrom<=(@clientSum - @orderSum) ORDER BY `sumFrom` DESC LIMIT 1;

SET @alternateDiscount = (SELECT `alternateDiscount` FROM pizzburgdb.Orders WHERE orderID = OLD.orderID);
IF (@alternateDiscount IS NOT NULL AND @alternateDiscount > @discount)
THEN
  SET @discount = @alternateDiscount;
END IF;

UPDATE pizzburgdb.Orders SET `discount`=@discount WHERE orderID=OLD.orderID;

IF (@orderStatus > 1) THEN
BEGIN
    CALL pizzburg_revertProductFromStock(OLD.productID, OLD.count);
END; END IF;

CALL pizzburg_assignCardIfNeeded(OLD.orderID);
end;
|


DELIMITER |
CREATE TRIGGER `BU_OrdersProducts` BEFORE UPDATE ON `OrdersProducts` FOR EACH ROW 
BEGIN
SET @price = (SELECT `price` FROM pizzburgdb.ProductsGroups WHERE productID = NEW.productID AND groupID= NEW.groupID);
SET @orderOldSum = (SELECT `cost` FROM pizzburgdb.Orders WHERE orderID = NEW.orderID);
SET @orderStatus = (SELECT `orderStatus` FROM pizzburgdb.Orders WHERE orderID = NEW.orderID);
SET @alternateDiscountCard = (SELECT `alternateDiscountCard` FROM pizzburgdb.Orders WHERE orderID = NEW.orderID);

-- update sum of order
SET @orderSum = @price * (CAST(NEW.count AS SIGNED)- CAST(OLD.count AS SIGNED));
UPDATE pizzburgdb.Orders SET `cost`=(@orderOldSum + @orderSum) WHERE orderID=NEW.orderID;

IF (@alternateDiscountCard IS NOT NULL)
THEN
  SET @clientPhone = (SELECT `phone` FROM pizzburgdb.Clients WHERE discountCardNumber = @alternateDiscountCard);
ELSE
  SET @clientPhone = (SELECT `clientPhone` FROM pizzburgdb.Orders WHERE orderID = NEW.orderID);
END IF;

-- update sum on client's account
SELECT `ordersSum` INTO @clientSum FROM pizzburgdb.Clients WHERE `phone`=@clientPhone;
UPDATE pizzburgdb.Clients SET `ordersSum`=`ordersSum` + @orderSum WHERE `phone`=@clientPhone;

-- update discount of order
SET @discount = 0;
SELECT `discount` INTO @discount FROM DiscountValues WHERE sumFrom<=(@clientSum + @orderSum) ORDER BY `sumFrom` DESC LIMIT 1;

SET @alternateDiscount = (SELECT `alternateDiscount` FROM pizzburgdb.Orders WHERE orderID = NEW.orderID);
IF (@alternateDiscount IS NOT NULL AND @alternateDiscount > @discount)
THEN
  SET @discount = @alternateDiscount;
END IF;

UPDATE pizzburgdb.Orders SET `discount`=@discount WHERE orderID=NEW.orderID;

IF (@orderStatus > 1) THEN
BEGIN
    CALL pizzburg_revertProductFromStock(OLD.productID, OLD.count);
    CALL pizzburg_writeProductToStock(NEW.productID, NEW.count);
END; END IF;

CALL pizzburg_assignCardIfNeeded(NEW.orderID);
end;
|


-- ---------------------------------------------------------------------------------------------------------------------
-- orders triggers -----------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------
DELIMITER |
CREATE TRIGGER `BI_Orders` BEFORE INSERT ON `Orders` FOR EACH ROW
BEGIN
    SET NEW.cost = 0;
    SET NEW.discount = 0;
END;
|

DELIMITER |
CREATE TRIGGER `BU_Orders` BEFORE UPDATE ON `Orders` FOR EACH ROW
BEGIN
    IF (OLD.orderStatus = 1 AND NEW.orderStatus > 1) THEN
    BEGIN
        call pizzburg_writeStock(OLD.orderID);
    END; END IF;
    IF (OLD.orderStatus > 1 AND NEW.orderStatus = 1) THEN
    BEGIN
        call pizzburg_revertStock(OLD.orderID);
    END; END IF;

    IF (OLD.orderStatus != 5 AND NEW.orderStatus = 5) THEN
    BEGIN
      SET @alternateDiscountCard = (SELECT `alternateDiscountCard` FROM pizzburgdb.Orders WHERE orderID = OLD.orderID);
      -- get client card number

      -- update sum on client's account
      IF (@alternateDiscountCard IS NOT NULL)
      THEN
        SET @clientPhone = (SELECT `phone` FROM pizzburgdb.Clients WHERE discountCardNumber = @alternateDiscountCard);
      ELSE
        SET @clientPhone = (SELECT OLD.clientPhone);
      END IF;

      -- update sum on client's account
      SELECT `ordersSum` INTO @clientSum FROM pizzburgdb.Clients WHERE `phone`=@clientPhone;
      UPDATE pizzburgdb.Clients SET `ordersSum`=(@clientSum - OLD.cost) WHERE `phone`=@clientPhone;
    END; END IF;

    IF (OLD.alternateDiscountCard IS NULL AND NEW.alternateDiscountCard IS NOT NULL) THEN
    BEGIN
        UPDATE Clients SET ordersSum = ordersSum - OLD.cost WHERE phone=OLD.clientPhone;
        UPDATE Clients SET ordersSum = ordersSum + NEW.cost WHERE discountCardNumber = NEW.alternateDiscountCard;
    END; END IF;

    IF (NEW.alternateDiscountCard IS NULL AND OLD.alternateDiscountCard IS NOT NULL) THEN
    BEGIN
        UPDATE Clients SET ordersSum = ordersSum + NEW.cost WHERE phone=NEW.clientPhone;
        UPDATE Clients SET ordersSum = ordersSum - OLD.cost WHERE discountCardNumber = OLD.alternateDiscountCard;
    END; END IF;

    IF (NEW.alternateDiscountCard IS NOT NULL)
    THEN
      SET @clientSum = (SELECT ordersSum FROM Clients WHERE discountCardNumber = NEW.alternateDiscountCard);
    ELSE
      SET @clientSum = (SELECT ordersSum FROM Clients WHERE phone=NEW.clientPhone);
    END IF;

    SET @discount = 0;
    SELECT `discount` INTO @discount FROM DiscountValues WHERE sumFrom<=@clientSum ORDER BY `sumFrom` DESC LIMIT 1;

    IF (NEW.alternateDiscount IS NOT NULL AND NEW.alternateDiscount > @discount)
    THEN
      SET @discount = NEW.alternateDiscount;
    END IF;

    SET NEW.discount = @discount;
END;
|

DELIMITER |
CREATE TRIGGER `BD_Orders` BEFORE DELETE ON `Orders` FOR EACH ROW
BEGIN
SET @alternateDiscountCard = (SELECT `alternateDiscountCard` FROM pizzburgdb.Orders WHERE orderID = OLD.orderID);
-- get client card number

-- update sum on client's account
IF (@alternateDiscountCard IS NOT NULL)
THEN
  SET @clientPhone = (SELECT `phone` FROM pizzburgdb.Clients WHERE discountCardNumber = @alternateDiscountCard);
ELSE
  SET @clientPhone = (SELECT OLD.clientPhone);
END IF;

-- update sum on client's account
SELECT `ordersSum` INTO @clientSum FROM pizzburgdb.Clients WHERE `phone`=@clientPhone;
UPDATE pizzburgdb.Clients SET `ordersSum`=(@clientSum - OLD.cost) WHERE `phone`=@clientPhone;

IF (OLD.orderStatus > 1)
THEN
  CALL pizzburg_revertStock(OLD.orderID);
END IF;


CALL pizzburg_assignCardIfNeeded(OLD.orderID);
END;
|
DELIMITER ;
-- ---------------------------------------------------------------------------------------------------------------------
-- procedures ----------------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------

-- ---------------------------------------------------------------------------------------------------------------------
-- product in stock ----------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS pizzburg_writeProductToStock;
DROP PROCEDURE IF EXISTS pizzburg_revertProductFromStock;


DELIMITER //

CREATE PROCEDURE pizzburg_writeProductToStock(_product_id INT, _count INT)
BEGIN
	UPDATE Ingredients
    SET Ingredients.balance = Ingredients.balance - 
        (SELECT count*_count FROM ProductIngredients WHERE productID = _product_id AND ingredientID=Ingredients.ingredientID) 
    WHERE Ingredients.ingredientID IN (SELECT ingredientID FROM ProductIngredients WHERE productID = _product_id);
END //

CREATE PROCEDURE pizzburg_revertProductFromStock(_product_id INT, _count INT)
BEGIN
	UPDATE Ingredients
    SET Ingredients.balance = Ingredients.balance + 
        (SELECT count*_count FROM ProductIngredients WHERE productID = _product_id AND ingredientID=Ingredients.ingredientID) 
    WHERE Ingredients.ingredientID IN (SELECT ingredientID FROM ProductIngredients WHERE productID = _product_id);
END //

DELIMITER ;


-- ---------------------------------------------------------------------------------------------------------------------
-- functions -----------------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS pizzburg_assignCardIfNeeded;

DELIMITER //

CREATE PROCEDURE pizzburg_assignCardIfNeeded(_order_id INT)
BEGIN
  SET @clientPhone = (SELECT clientPhone FROM Orders WHERE orderID=_order_id);
  SET @orderCardNumber = (SELECT alternateDiscountCard FROM Orders WHERE orderID=_order_id);
  SET @clientCardNumber = (SELECT discountCardNumber FROM Clients WHERE phone=@clientPhone);
  SET @clientSum = (SELECT ordersSum FROM Clients WHERE phone=@clientPhone);
  SET @hasDiscount = (SELECT COUNT(*) FROM DiscountValues WHERE sumFrom <= @clientSum);

  IF (@orderCardNumber IS NULL AND @clientCardNumber IS NULL AND @hasDiscount > 0) THEN
  BEGIN
    INSERT INTO DiscountCards () VALUES ();
    SET @newCardNumber = (SELECT last_insert_id());
    UPDATE Clients SET discountCardNumber =@newCardNumber WHERE phone = @clientPhone;
  END; END IF;
END //

DELIMITER ;

DROP TABLE IF EXISTS `pizzburgdb`.`Logs` ;