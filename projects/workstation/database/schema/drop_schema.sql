USE pizzburgdb;

DROP TRIGGER IF EXISTS `BI_OrdersProducts` ;
DROP TRIGGER IF EXISTS `BD_OrdersProducts` ;
DROP TRIGGER IF EXISTS `BU_OrdersProducts` ;
DROP TRIGGER IF EXISTS `BI_Orders` ;
DROP TRIGGER IF EXISTS `BU_Orders` ;
DROP TRIGGER IF EXISTS `BD_Orders` ;
DROP PROCEDURE IF EXISTS logDebug;

SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE  IF EXISTS Orders;
DROP TABLE  IF EXISTS Clients;
DROP TABLE  IF EXISTS DiscountCards;
DROP TABLE  IF EXISTS DiscountValues;
DROP TABLE  IF EXISTS Ingredients;
DROP TABLE  IF EXISTS Logs;
DROP TABLE  IF EXISTS OrderStatuses;
DROP TABLE  IF EXISTS OrdersProducts;
DROP TABLE  IF EXISTS ProductGroups;
DROP TABLE  IF EXISTS ProductIngredients;
DROP TABLE  IF EXISTS Products;
DROP TABLE  IF EXISTS ProductsGroups;
DROP TABLE  IF EXISTS Roles;
DROP TABLE  IF EXISTS Streets;
DROP TABLE  IF EXISTS WorkstationUsers;
SET FOREIGN_KEY_CHECKS = 1;

DROP SCHEMA IF EXISTS `pizzburgdb` ;


DROP USER `pizzburg_db_user`@'%';
DROP USER `pizzburg_db_user`@'127.0.0.1';
DROP USER `pizzburg_db_user`@'localhost';