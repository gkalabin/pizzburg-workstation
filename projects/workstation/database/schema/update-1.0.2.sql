-- remove duplicates on Product-Ingredients table

USE pizzburgdb;

CREATE TEMPORARY TABLE ProductIngredientsTmp AS SELECT DISTINCT * FROM ProductIngredients;
DELETE FROM ProductIngredients;
INSERT INTO ProductIngredients(productID,ingredientID,count) SELECT productID,ingredientID,count FROM ProductIngredientsTmp;
ALTER TABLE ProductIngredients
  ADD PRIMARY KEY ProductsIngredientsPk (productID, ingredientID);

CREATE TEMPORARY TABLE ProductsGroupsTmp AS SELECT DISTINCT * FROM ProductsGroups;
DELETE FROM ProductsGroups;
INSERT INTO ProductsGroups(productID,groupID,price) SELECT productID,groupID,price FROM ProductsGroupsTmp;
ALTER TABLE ProductsGroups
  ADD PRIMARY KEY ProductsGroupsPk (productID, groupID);

CREATE TEMPORARY TABLE OrdersProductsTmp AS SELECT DISTINCT * FROM OrdersProducts;
DELETE FROM OrdersProducts;
INSERT INTO OrdersProducts(orderID,productID,groupID,count) SELECT orderID,productID,groupID,count FROM OrdersProductsTmp;
ALTER TABLE OrdersProducts
  ADD PRIMARY KEY OrdersProductsPk (orderID,productID,groupID);