#!/usr/bin/perl
use strict;
use warnings;

use encoding 'utf-8';
binmode(STDIN, ':encoding(utf-8)');
binmode(STDOUT, ':encoding(utf-8)');

my $filename_in = "streets_line.txt";
my $filename_out = "streets.sql";
my $dbname = "pizzburgdb";

open my $fh_in, "<:encoding(utf-8)", $filename_in or croak $!;
open my $fh_out, ">:encoding(utf-8)", $filename_out or croak $!;
print "files opened\n";
print $fh_out "INSERT INTO `pizzburgdb`.`Streets` (`title`, `type`) VALUES ";

my $line_number = 0;
while (my $line = <$fh_in>) {
    chomp ($line);
    $line_number++;
    print $fh_out "," if $line_number != 1;
    print $fh_out "\n\t('$line', 1)";
}
print $fh_out ";";
print "Processed $line_number lines.\n";
close $fh_in;
close $fh_out;
