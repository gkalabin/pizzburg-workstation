#!/usr/bin/perl
use strict;
use warnings;

use encoding 'utf-8';
binmode(STDIN, ':encoding(utf-8)');
binmode(STDOUT, ':encoding(utf-8)');

my $filename_in = "streets_raw.txt";
my $filename_out = "streets_line.txt";
open my $fh_in, "<:encoding(utf-8)", $filename_in or croak $!;
open my $fh_out, ">:encoding(utf-8)", $filename_out or croak $!;
print "file opened\n";

my $line_number = 0;
my $items = 0;
while (my $line = <$fh_in>) {
    $line_number++;
    $line =~ s/^\s//x;
    $line =~ s/\s$//x;
    if ($line =~ m/^(\s*|.)$/x) { next; }
    
    my $street_type = "";
    if ($line =~ m/([\w\d\-\,()\s]+)\s(\w+)\.\s*(\(.*\))*/x) {
        if ($2 eq "ул") {
            $street_type = "Улица ";
        } elsif ($2 eq "пер") {
            $street_type = "Переулок ";
        } elsif ($2 eq "пр") {
            $street_type = "Проспект ";
        } elsif ($2 eq "бульв" || $2 eq "б") {
            $street_type = "Бульвар ";
        } elsif ($2 eq "ш") {
            $street_type = "Шоссе ";
        } elsif ($2 eq "пос") {
            $street_type = "Посёлок ";
        } else {
            print "Unknown street type \"$2\" in line $line_number\n";
        }
        $line = $1;
        $line .= " $3" if defined($3);
    } else {
        print "Not found street type in line \"$line\" in line $line_number\n";
    }
    print $fh_out ($street_type.$line."\n");
    $items++;
}
print "Processed $line_number lines. Words count $items.\n";
close $fh_in;
close $fh_out;
