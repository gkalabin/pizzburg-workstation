package ru.pizzburg.utils;

import org.displaytag.properties.SortOrderEnum;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class Converter {

    public static SortOrderEnum parseSortOrder(String order) {
        if ("asc".equals(order)) {
            return SortOrderEnum.ASCENDING;
        } else if ("desc".equals(order)) {
            return SortOrderEnum.DESCENDING;
        } else {
            return null;
        }
    }

    public static String getStringSortOrder(SortOrderEnum order) {
        if (SortOrderEnum.ASCENDING.equals(order)) {
            return "ASC";
        } else if (SortOrderEnum.DESCENDING.equals(order)) {
            return "DESC";
        } else {
            return null;
        }
    }

    public static int longToInt(long l) {
        if (l < Integer.MIN_VALUE) {
            return Integer.MIN_VALUE;
        }
        if (l > Integer.MAX_VALUE) {
            return Integer.MAX_VALUE;
        }
        return (int) l;
    }

    private Converter() { }
}
