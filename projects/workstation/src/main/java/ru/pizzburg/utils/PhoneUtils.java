package ru.pizzburg.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class PhoneUtils {
    protected final static Log logger = LogFactory.getLog("PhoneUtils");
    public static final int PHONE_FULL_DIGITS_COUNT = 11;
    public static final int PHONE_LOCAL_DIGITS_COUNT = 7;
    public static final String PHONE_LOCAL_PREFIX = "7342";
    public static final String PHONE_FORMAT = "+%s-%s-%s-%s";

    public static String getPhoneNumberFormatted(long phoneDigits) {
        String digits = String.valueOf(phoneDigits);
        if (digits.length() > PHONE_FULL_DIGITS_COUNT) {
            logger.warn("more digits in phone number: " + phoneDigits);
            return null;
        } else if (digits.length() < PHONE_FULL_DIGITS_COUNT) {
            logger.warn("less digits in phone number: " + phoneDigits);
            final int digitsLength = digits.length();
            for (int i = 0; i < PHONE_FULL_DIGITS_COUNT - digitsLength; i++) {
                digits = "0" + digits;
            }
        }
        return String.format(PHONE_FORMAT, digits.substring(0, 1), digits.substring(1, 4), digits.substring(4, 7), digits.substring(7));
    }

    public static boolean isPhoneValid(String phone) {
        String digits = phone.replaceAll("\\D", "");
        int length = digits.length();
        return length == PHONE_FULL_DIGITS_COUNT || length == PHONE_LOCAL_DIGITS_COUNT;
    }

    public static long parsePhone(String phone) {
        if (phone == null) {
            logger.warn("phone parsing: null argument");
            return -1;
        }
        String digits = phone.replaceAll("\\D", "");
        int length = digits.length();
        switch (length) {
            case PHONE_FULL_DIGITS_COUNT:
                return digits.startsWith("8") ?
                        Long.parseLong("7" + digits.substring(1)) :
                        Long.parseLong(digits);
            case PHONE_LOCAL_DIGITS_COUNT:
                return Long.parseLong(PHONE_LOCAL_PREFIX + digits);
        }
        // in case of incorrect phone return -1
        logger.error("Invalid phone parsing: " + phone);
        return -1;
    }
}
