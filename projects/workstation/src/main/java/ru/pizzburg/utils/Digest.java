package ru.pizzburg.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class Digest {
    private static Log logger = LogFactory.getLog("Digest");

    public static String getSha1Hash(String s) {
        MessageDigest crypt;
        String res = null;
        try {
            crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(s.getBytes("utf8"));
            res = new BigInteger(1, crypt.digest()).toString(16);
            for (int i = res.length(); i < 40; i++) {
                res = "0" + res;
            }
        } catch (NoSuchAlgorithmException e) {
            logger.error("fatal error: algorithm not found", e);
        } catch (UnsupportedEncodingException e) {
            logger.error("fatal error: encoding", e);
        }
        return res;
    }
}
