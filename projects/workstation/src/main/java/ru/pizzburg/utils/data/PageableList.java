package ru.pizzburg.utils.data;

import java.util.List;
import org.displaytag.pagination.PaginatedList;
import org.displaytag.properties.SortOrderEnum;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class PageableList<T> implements PaginatedList {

    private List<T> data;
    private int listSize;
    private PaginationParams paginationParams;
    private SortingParams sortingParams;

    public PageableList() {
    }

    public PageableList(List<T> initialData, int listSize) {
        this.data = initialData;
        this.listSize = listSize;
    }

    public void setPaginationParams(PaginationParams paginationParams) {
        this.paginationParams = paginationParams;
    }

    public void setSortingParams(SortingParams sortingParams) {
        this.sortingParams = sortingParams;
    }

    @Override
    public List<T> getList() {
        return data;
    }

    @Override
    public int getPageNumber() {
        return paginationParams.getPageNumber();
    }

    @Override
    public int getObjectsPerPage() {
        return paginationParams.getObjectsPerPage();
    }

    @Override
    public int getFullListSize() {
        return listSize;
    }

    @Override
    public String getSortCriterion() {
        return sortingParams.getSortCriterion();
    }

    @Override
    public SortOrderEnum getSortDirection() {
        return sortingParams.getSortDirection();
    }

    @Override
    public String getSearchId() {
        return null;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public void setListSize(int listSize) {
        this.listSize = listSize;
    }

    public PaginationParams getPaginationParams() {
        return paginationParams;
    }

    public SortingParams getSortingParams() {
        return sortingParams;
    }
}
