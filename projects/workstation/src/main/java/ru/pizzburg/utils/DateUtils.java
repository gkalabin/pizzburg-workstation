package ru.pizzburg.utils;

import org.apache.commons.logging.LogFactory;
import ru.pizzburg.constants.Const;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class DateUtils {
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(Const.DATE_FORMAT_STRING);
    public static final SimpleDateFormat OUT_DATE_TIME_FORMAT = new SimpleDateFormat(Const.OUT_DATE_TIME_FORMAT_STRING);
    public static final SimpleDateFormat IN_DATE_TIME_FORMAT = new SimpleDateFormat(Const.IN_DATE_TIME_FORMAT_STRING);
    public static final Date LEFT_DATE_BOUND;
    public static final Date RIGHT_DATE_BOUND;

    static {
        Date left = new Date();
        Date right = new Date();
        try {
            left = ru.pizzburg.utils.DateUtils.DATE_FORMAT.parse("1.1.1970");
            right = ru.pizzburg.utils.DateUtils.DATE_FORMAT.parse("1.1.2500");
        } catch (ParseException ex) {
            LogFactory.getLog("Pizzburg").fatal("Formatters class can't init", ex);
        }
        LEFT_DATE_BOUND = left;
        RIGHT_DATE_BOUND = right;
    }

    private DateUtils() {}

    public static Date getHourStart() {
        Calendar now = Calendar.getInstance();
        Calendar dayStart = new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH), now.get(Calendar.HOUR_OF_DAY), 0);
        return dayStart.getTime();
    }

    public static Date getDayStart() {
        Calendar now = Calendar.getInstance();
        Calendar dayStart = new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
        return dayStart.getTime();
    }

    public static Date getWeekStart() {
        Calendar cal = Calendar.getInstance();
        cal.clear(Calendar.MILLISECOND);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MINUTE);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        return cal.getTime();
    }

    public static Date getMonthStart() {
        Calendar now = Calendar.getInstance();
        Calendar dayStart = new GregorianCalendar(now.get(Calendar.YEAR), now.get(Calendar.MONTH), 1);
        return dayStart.getTime();
    }

    public static Date getYearStart() {
        Calendar now = Calendar.getInstance();
        Calendar dayStart = new GregorianCalendar(now.get(Calendar.YEAR), Calendar.JANUARY, 1);
        return dayStart.getTime();
    }

    public static float getDaysCount(Date from, Date to) {
        return (float) (to.getTime() - from.getTime()) / (1000 * 60 * 60 * 24);
    }

    public static String getDateFormatted(Date d) {
        return DATE_FORMAT.format(d);
    }

    public static String getDateFormatted() {
        return getDateFormatted(new Date());
    }

    public static String getDateTimeFormatted(Date date) {
        return OUT_DATE_TIME_FORMAT.format(date);
    }

    public static boolean isDateValid(String date) {
        try {
            Date d = DATE_FORMAT.parse(date);
            if (d.after(RIGHT_DATE_BOUND) || d.before(LEFT_DATE_BOUND)) {
                return false;
            }
        } catch (ParseException ex) {
            return false;
        }
        return true;
    }

    public static Date parseDate(String date) {
        try {
            return DATE_FORMAT.parse(date);
        } catch (ParseException ex) {
            return null;
        }
    }
}
