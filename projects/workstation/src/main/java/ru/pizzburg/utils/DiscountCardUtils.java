package ru.pizzburg.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class DiscountCardUtils {
    protected final static Log logger = LogFactory.getLog("DiscountCardUtils");
    public static final int DISCOUNT_CARD_DIGITS_COUNT = 5;
    public static final String DISCOUNT_CARD_NUMBER_FORMAT = "%s";

    public static boolean isDiscountCardNumberValid(String cardNumber) {
        if (cardNumber == null) {
            return false;
        }
        String digits = cardNumber.replaceAll("\\D", "");
        long number = 0;
        try {
            number = Long.valueOf(digits);
        } catch (NumberFormatException ex) {
            return false;
        }
        return digits.length() == DISCOUNT_CARD_DIGITS_COUNT && number != 0;
    }

    public static long getDiscountCardNumber(String cardNumberStr) {
        if (cardNumberStr == null) {
            return -1;
        }
        String digits = cardNumberStr.replaceAll("\\D", "");
        if (digits.length() > DISCOUNT_CARD_DIGITS_COUNT) {
            return -1;
        }
        long val = -1;
        try {
            val = Long.parseLong(digits);
        } catch (NumberFormatException ex) {
            logger.warn("can't parse discount card " + cardNumberStr);
        }
        return val;
    }

    public static String getDiscountCardNumberFormatted(long cardNumber) {
        String digits = String.valueOf(cardNumber);
        if (digits.length() > DISCOUNT_CARD_DIGITS_COUNT) {
            logger.warn("more digits in card number: " + cardNumber);
            return null;
        } else if (digits.length() < DISCOUNT_CARD_DIGITS_COUNT) {
            final int digitsLength = digits.length();
            for (int i = 0; i < DISCOUNT_CARD_DIGITS_COUNT - digitsLength; i++) {
                digits = "0" + digits;
            }
        }
        return String.format(DISCOUNT_CARD_NUMBER_FORMAT, digits);
    }
}
