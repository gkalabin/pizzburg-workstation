package ru.pizzburg.utils.data;

import org.displaytag.properties.SortOrderEnum;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class SortingParams {

    private final String sortCriterion;
    private final SortOrderEnum sortDirection;

    public String getSortCriterion() {
        return sortCriterion;
    }

    public SortOrderEnum getSortDirection() {
        return sortDirection;
    }

    public SortingParams(String sortCriterion, SortOrderEnum sortDirection) {
        this.sortCriterion = sortCriterion;
        this.sortDirection = sortDirection;
    }

    @Override
    public String toString() {
        return "SortingParams{" + "sortCriterion=" + sortCriterion + ", sortDirection=" + sortDirection + '}';
    }
}
