package ru.pizzburg.utils;

import org.apache.commons.logging.LogFactory;
import ru.pizzburg.web.domain.WorkstationUser;

import java.text.ParseException;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class StringUtils {

    private static final Pattern EMPTY_STRING_PATTERN = Pattern.compile("\\s");

    public static boolean isStringEmptyOrWhitespaces(String s) {
        return s == null || s.isEmpty() || EMPTY_STRING_PATTERN.matcher(s).matches();
    }
}
