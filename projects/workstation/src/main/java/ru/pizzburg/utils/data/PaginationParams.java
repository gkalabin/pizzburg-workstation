package ru.pizzburg.utils.data;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class PaginationParams {

    private static final int OBJECTS_PER_PAGE_DEFAULT = 50;
    private final int pageNumber;
    private final int objectsPerPage;

    public PaginationParams(int pageNumber, int objectsPerPage) {
        this.pageNumber = pageNumber;
        this.objectsPerPage = objectsPerPage;
    }

    public PaginationParams() {
        this.pageNumber = 1;
        this.objectsPerPage = OBJECTS_PER_PAGE_DEFAULT;
    }

    public int getObjectsPerPage() {
        return objectsPerPage;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    @Override
    public String toString() {
        return "PaginationParams{" + "pageNumber=" + pageNumber + ", objectsPerPage=" + objectsPerPage + '}';
    }
}
