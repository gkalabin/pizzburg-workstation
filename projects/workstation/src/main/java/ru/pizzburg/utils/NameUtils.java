package ru.pizzburg.utils;

import ru.pizzburg.web.domain.WorkstationUser;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class NameUtils {
    public static String getFullNameFormatted(String name, String surname, String middleName) {
        StringBuilder res = new StringBuilder();
        boolean isNotEmpty = false;
        if (surname != null && !surname.isEmpty()) {
            res.append(surname);
            isNotEmpty = true;
        }
        if (name != null && !name.isEmpty()) {
            if (isNotEmpty) {
                res.append(" ");
            }
            res.append(name);
            isNotEmpty = true;
        }
        if (middleName != null && !middleName.isEmpty()) {
            if (isNotEmpty) {
                res.append(" ");
            }
            res.append(middleName);
        }
        return res.toString();
    }

    public static String getFullNameFormatted(WorkstationUser user) {
        if (user == null) {
            return null;
        }
        return getFullNameFormatted(user.getName(), user.getSurname(), user.getMiddleName());
    }
}
