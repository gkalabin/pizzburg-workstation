package ru.pizzburg.web.service.products;

import org.displaytag.properties.SortOrderEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.pizzburg.utils.data.PageableList;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.web.domain.Product;
import ru.pizzburg.web.domain.ProductGroup;
import ru.pizzburg.web.domain.ProductInGroup;
import ru.pizzburg.web.repo.IngredientDao;
import ru.pizzburg.web.repo.ProductDao;
import ru.pizzburg.web.repo.ProductGroupDao;

import java.util.List;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Service
@Lazy(false)
public class ProductsManager {

    private static final PaginationParams DEFAULT_PAGINATION_PARAMS;
    private static final SortingParams DEFAULT_SORTING_PARAMS;
    private static final int MAX_OBJECTS_PER_PAGE = 100;
    @Autowired
    private ProductDao productDao;
    @Autowired
    private IngredientDao ingredientDao;
    @Autowired
    private ProductGroupDao productGroupDao;

    static {
        DEFAULT_PAGINATION_PARAMS = new PaginationParams();
        DEFAULT_SORTING_PARAMS = new SortingParams("title", SortOrderEnum.ASCENDING);
    }

    public List<Product> getProducts() {
        List<Product> products = productDao.getProducts();
        if (products == null) {
            return null;
        }
//        for (Product p : products) {
//            p.setIngredients(ingredientDao.getIngredientsForProduct(p));
//        }
        return products;
    }

    public PageableList<Product> getProducts(PaginationParams pp, SortingParams sp) {
        if (pp == null) {
            pp = DEFAULT_PAGINATION_PARAMS;
        } else {
            int page = pp.getPageNumber() <= 0 ? DEFAULT_PAGINATION_PARAMS.getPageNumber() : pp.getPageNumber();
            int results = pp.getObjectsPerPage() <= 0 || pp.getObjectsPerPage() > MAX_OBJECTS_PER_PAGE
                    ? DEFAULT_PAGINATION_PARAMS.getObjectsPerPage() : pp.getObjectsPerPage();
            pp = new PaginationParams(page, results);
        }
        if (sp == null) {
            sp = DEFAULT_SORTING_PARAMS;
        }
        final PageableList<Product> productList = productDao.getProducts(pp, sp);
        if (productList.getList() == null) {
            return null;
        }
        for (Product p : productList.getList()) {
            p.setIngredients(ingredientDao.getIngredientsForProduct(p));
            p.setGroups(productGroupDao.getGroupsForProduct(p));
        }
        return productList;
    }

    public String addProduct(Product product) {
        return productDao.addProduct(product);
    }

    public Product getProduct(int id) {
        Product product = productDao.getProduct(id);
        if (product == null) {
            return null;
        }
        product.setIngredients(ingredientDao.getIngredientsForProduct(product));
        product.setGroups(productGroupDao.getGroupsForProduct(product));
        return product;
    }

    public String updateProduct(Product product) {
        return productDao.updateProduct(product,
                ingredientDao.getIngredientsForProduct(product),
                productGroupDao.getGroupsForProduct(product));
    }

    public String removeProduct(int id) {
        return productDao.removeProduct(id);
    }

    public List<ProductInGroup> getProductsForGroupId(int groupId) {
        ProductGroup group = new ProductGroup();
        group.setId(groupId);
        return productDao.getProductsForGroup(group);
    }

    public float getProductPrice(int productId, int groupId) {
        return productDao.getProductPrice(productId, groupId);
    }
}
