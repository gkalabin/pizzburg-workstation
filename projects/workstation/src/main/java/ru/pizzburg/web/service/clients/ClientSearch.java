package ru.pizzburg.web.service.clients;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class ClientSearch {

    private String phone = "";
    private String address = "";
    private String name = "";
    private String surname = "";
    private String middleName = "";
    private String cardNumber = "";
    private String sumFrom = "";
    private String sumTo = "";
    private ClientFilter clientFilter;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public ClientFilter getClientFilter() {
        return clientFilter;
    }

    public void setClientFilter(ClientFilter clientFilter) {
        this.clientFilter = clientFilter;
    }

    public String getSumFrom() {
        return sumFrom;
    }

    public void setSumFrom(String sumFrom) {
        this.sumFrom = sumFrom;
    }

    public String getSumTo() {
        return sumTo;
    }

    public void setSumTo(String sumTo) {
        this.sumTo = sumTo;
    }

    @Override
    public String toString() {
        return "ClientSearch{" + "phone=" + phone + ", address=" + address + ", name=" + name + ", surname=" + surname + ", middleName=" + middleName + ", cardNumber=" + cardNumber + ", sumFrom=" + sumFrom + ", sumTo=" + sumTo + ", clientFilter=" + clientFilter + '}';
    }
}
