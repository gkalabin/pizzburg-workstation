package ru.pizzburg.web.domain;

import java.util.Date;

import ru.pizzburg.utils.DiscountCardUtils;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class DiscountCard {

    private long id;
    private Date emissionDate;

    public Date getEmissionDate() {
        return emissionDate;
    }

    public void setEmissionDate(Date emissionDate) {
        this.emissionDate = emissionDate;
    }

    public String getEmissionDateFormatted() {
        if (emissionDate == null) {
            return null;
        }
        return ru.pizzburg.utils.DateUtils.getDateFormatted(emissionDate);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIdFormatted() {
        return DiscountCardUtils.getDiscountCardNumberFormatted(id);
    }

    @Override
    public String toString() {
        return "DiscountCard{" +
                "id=" + id +
                ", emissionDate=" + emissionDate +
                '}';
    }
}
