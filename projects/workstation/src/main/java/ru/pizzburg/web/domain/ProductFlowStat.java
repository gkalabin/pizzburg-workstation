package ru.pizzburg.web.domain;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class ProductFlowStat {

    private Product product;
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProductFlowStat other = (ProductFlowStat) obj;
        if (this.product != other.product && (this.product == null || !this.product.equals(other.product))) {
            return false;
        }
        if (this.count != other.count) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.product != null ? this.product.hashCode() : 0);
        hash = 97 * hash + this.count;
        return hash;
    }

    @Override
    public String toString() {
        return "ProductFlowStat{" + "product=" + product + ", count=" + count + '}';
    }
}
