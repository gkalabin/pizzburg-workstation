package ru.pizzburg.web.service.clients;

import org.springframework.context.annotation.Lazy;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.pizzburg.constants.FieldLength;
import ru.pizzburg.utils.StringUtils;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Lazy(false)
public class ClientSearchValidator implements Validator {

    @Override
    public boolean supports(Class clazz) {
        return ClientSearch.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        ClientSearch cs = (ClientSearch) obj;
        checkFieldLength(cs, errors);
        cs.setClientFilter(new ClientFilter());

        if (!StringUtils.isStringEmptyOrWhitespaces(cs.getName())) {
            cs.getClientFilter().setName(cs.getName().replace("*", "%").replace("?", "_"));
            cs.getClientFilter().setIsEmpty(false);
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(cs.getSurname())) {
            cs.getClientFilter().setSurname(cs.getSurname().replace("*", "%").replace("?", "_"));
            cs.getClientFilter().setIsEmpty(false);
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(cs.getMiddleName())) {
            cs.getClientFilter().setMiddleName(cs.getMiddleName().replace("*", "%").replace("?", "_"));
            cs.getClientFilter().setIsEmpty(false);
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(cs.getAddress())) {
            cs.getClientFilter().setAddress(cs.getAddress().replace("*", "%").replace("?", "_"));
            cs.getClientFilter().setIsEmpty(false);
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(cs.getPhone())) {
            String phone = cs.getPhone().replaceAll("[^\\d*?]", "").replace("*", "%").replace("?", "_");
            cs.getClientFilter().setPhone(phone);
            cs.getClientFilter().setIsEmpty(false);
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(cs.getCardNumber())) {
            String cardNumber = cs.getCardNumber().replaceAll("[^\\d*?]", "").replace("*", "%").replace("?", "_");
            cs.getClientFilter().setCardNumber(cardNumber);
            cs.getClientFilter().setIsEmpty(false);
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(cs.getSumFrom())) {
            try {
                cs.setSumFrom(cs.getSumFrom().replace(",", "."));
                int sumFrom = Integer.valueOf(cs.getSumFrom());
                if (sumFrom <= 0) {
                    errors.rejectValue("sumFrom", "error.negative-sum", "Sum must be > 0");
                } else {
                    cs.getClientFilter().setSumFrom(sumFrom);
                    cs.getClientFilter().setIsEmpty(false);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("sumFrom", "typeMismatch.int", "Not convertable to number");
            }
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(cs.getSumTo())) {
            try {
                cs.setSumTo(cs.getSumTo().replace(",", "."));
                int sumTo = Integer.valueOf(cs.getSumTo());
                if (sumTo <= 0) {
                    errors.rejectValue("sumTo", "error.negative-sum", "Sum must be > 0");
                } else {
                    cs.getClientFilter().setSumTo(sumTo);
                    cs.getClientFilter().setIsEmpty(false);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("sumTo", "typeMismatch.int", "Not convertable to number");
            }
        }

        if (errors.hasErrors()) {
            cs.setClientFilter(new ClientFilter());
        }
    }

    private void checkFieldLength(ClientSearch ai, Errors errors) {
        if (ai.getName().length() > FieldLength.CLIENT_NAME) {
            errors.rejectValue("name", "error.too-long-value",
                    new Object[]{FieldLength.CLIENT_NAME}, "Max length is " + FieldLength.CLIENT_NAME);
        }
        if (ai.getSurname().length() > FieldLength.CLIENT_SURNAME) {
            errors.rejectValue("surname", "error.too-long-value",
                    new Object[]{FieldLength.CLIENT_SURNAME}, "Max length is " + FieldLength.CLIENT_SURNAME);
        }
        if (ai.getMiddleName().length() > FieldLength.CLIENT_MIDDLE_NAME) {
            errors.rejectValue("middleName", "error.too-long-value",
                    new Object[]{FieldLength.CLIENT_MIDDLE_NAME}, "Max length is " + FieldLength.CLIENT_MIDDLE_NAME);
        }
        if (ai.getAddress().length() > FieldLength.CLIENT_DEFAULT_ADDRESS) {
            errors.rejectValue("address", "error.too-long-value",
                    new Object[]{FieldLength.CLIENT_DEFAULT_ADDRESS}, "Max length is " + FieldLength.CLIENT_DEFAULT_ADDRESS);
        }
    }
}