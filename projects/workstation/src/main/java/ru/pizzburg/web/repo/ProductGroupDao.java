package ru.pizzburg.web.repo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.pizzburg.constants.DatabaseErrors;
import ru.pizzburg.web.domain.GroupInProduct;
import ru.pizzburg.web.domain.Product;
import ru.pizzburg.web.domain.ProductGroup;
import ru.pizzburg.web.domain.ProductInGroup;
import ru.pizzburg.web.repo.mapper.GroupInProductMapper;
import ru.pizzburg.web.repo.mapper.ProductGroupMapper;

import javax.sql.DataSource;
import java.util.List;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Repository
@Lazy(false)
public class ProductGroupDao extends JdbcTemplate {
    protected final Log logger = LogFactory.getLog(getClass());

    @Override
    @Autowired
    public void setDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    public List<ProductGroup> getGroups() {
        return query("SELECT * FROM pizzburgdb.ProductGroups ORDER BY title ASC", new ProductGroupMapper());
    }

    public String addGroup(ProductGroup g) {
        if (queryForInt("SELECT COUNT(*) FROM pizzburgdb.ProductGroups WHERE title=?", g.getTitle()) != 0) {
            return DatabaseErrors.PRODUCT_GROUP_EXISTS;
        }
        try {
            update("INSERT INTO pizzburgdb.ProductGroups (title, description) VALUES (?,?)", g.getTitle(), g.getDescription());
            int id = queryForInt("SELECT last_insert_id()");
            for (ProductInGroup pig : g.getProducts()) {
                update("INSERT INTO pizzburgdb.ProductsGroups (productID, groupID, price) VALUES (?,?,?)",
                        pig.getProduct().getId(), id, pig.getPrice());
            }
        } catch (DataAccessException ex) {
            logger.error("add product group fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return null;
    }

    public ProductGroup getGroup(int id) {
        List<ProductGroup> result = query("SELECT * FROM pizzburgdb.ProductGroups WHERE groupID=?", new Object[]{id}, new ProductGroupMapper());
        if (result.isEmpty()) {
            logger.warn("Access to product group which doesn't exist");
            return null;
        } else if (result.size() != 1) {
            logger.warn("database schema error: Not unique result from query. Get " + result.size() + " product groups");
            return result.get(0);
        } else {
            return result.get(0);
        }
    }

    public String updateGroup(ProductGroup g, List<ProductInGroup> initialProducts) {
        if (queryForInt("SELECT COUNT(*) FROM pizzburgdb.ProductGroups WHERE title=? AND groupID != ?", g.getTitle(), g.getId()) != 0) {
            return DatabaseErrors.PRODUCT_GROUP_EXISTS;
        }
        int result = 0;
        try {
            result = update("UPDATE pizzburgdb.ProductGroups SET title=?, description=? WHERE groupID=?",
                    g.getTitle(), g.getDescription(), g.getId());

            if (result == 0) {
                return DatabaseErrors.PRODUCT_GROUP_NOT_EXISTS;
            }

            // adding new and updating modified products
            for (ProductInGroup pigNew : g.getProducts()) {
                boolean contains = false;
                boolean changed = false;
                for (ProductInGroup pigOld : initialProducts) {
                    if (pigNew.getProduct().getId() == pigOld.getProduct().getId()) {
                        contains = true;
                        if (pigNew.getPrice() != pigOld.getPrice()) {
                            changed = true;
                        }
                        break;
                    }
                }
                if (contains && changed) {
                    update("UPDATE pizzburgdb.ProductsGroups SET price=? WHERE groupID=? AND productID=?",
                            pigNew.getPrice(), g.getId(), pigNew.getProduct().getId());
                } else if (!contains) {
                    update("INSERT INTO pizzburgdb.ProductsGroups (productID, groupID, price) VALUES (?,?,?)",
                            pigNew.getProduct().getId(), g.getId(), pigNew.getPrice());
                }
            }

            // delete removed ingredients
            for (ProductInGroup pigOld : initialProducts) {
                boolean removed = true;
                for (ProductInGroup pigNew : g.getProducts()) {
                    if (pigNew.getProduct().getId() == pigOld.getProduct().getId()) {
                        removed = false;
                        break;
                    }
                }
                if (removed) {
                    update("DELETE FROM pizzburgdb.ProductsGroups WHERE groupID=? AND productID=?", g.getId(), pigOld.getProduct().getId());
                }
            }
        } catch (DataAccessException ex) {
            logger.error("update product group fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return result > 0 ? null : DatabaseErrors.PRODUCT_GROUP_NOT_EXISTS;
    }

    public String removeGroup(int id) {
        int result = 0;
        try {
            result = update("DELETE FROM pizzburgdb.ProductGroups WHERE groupID=?", id);
        } catch (DataAccessException ex) {
            logger.error("delete product group fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return result == 1 ? null : DatabaseErrors.PRODUCT_GROUP_NOT_EXISTS;
    }

    public List<GroupInProduct> getGroupsForProduct(Product p) {
        if (p == null) {
            logger.warn("null product passed. return null");
            return null;
        }
        return query("SELECT * FROM pizzburgdb.ProductGroups "
                + "LEFT JOIN pizzburgdb.ProductsGroups ON ProductsGroups.groupID = ProductGroups.groupID "
                + "WHERE productID = ?", new Object[]{p.getId()}, new GroupInProductMapper());
    }
}
