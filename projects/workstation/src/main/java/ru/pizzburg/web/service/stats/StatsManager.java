package ru.pizzburg.web.service.stats;

import java.util.Date;
import org.displaytag.properties.SortOrderEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.pizzburg.utils.data.PageableList;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.constants.SortStrings;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.web.domain.*;
import ru.pizzburg.web.repo.StatsDao;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Service
@Lazy(false)
public class StatsManager {

    private static final PaginationParams DEFAULT_PAGINATION_PARAMS;
    private static final SortingParams DEFAULT_INGREDIENT_FLOW_SORTING_PARAMS;
    private static final SortingParams DEFAULT_PRODUCT_FLOW_SORTING_PARAMS;
    private static final SortingParams DEFAULT_OPERATORS_SORTING_PARAMS;
    private static final SortingParams DEFAULT_CARRIERS_SORTING_PARAMS;
    private static final int MAX_OBJECTS_PER_PAGE = 100;
    @Autowired
    private StatsDao statsDao;

    static {
        DEFAULT_PAGINATION_PARAMS = new PaginationParams();
        DEFAULT_INGREDIENT_FLOW_SORTING_PARAMS = new SortingParams(SortStrings.STATS_INGREDIENT_SORT_CRITERION, SortOrderEnum.ASCENDING);
        DEFAULT_PRODUCT_FLOW_SORTING_PARAMS = new SortingParams(SortStrings.STATS_PRODUCT_SORT_CRITERION, SortOrderEnum.ASCENDING);
        DEFAULT_OPERATORS_SORTING_PARAMS = new SortingParams("orderID", SortOrderEnum.ASCENDING);
        DEFAULT_CARRIERS_SORTING_PARAMS = new SortingParams("orderID", SortOrderEnum.ASCENDING);
    }

    public PageableList<IngredientFlowStat> getIngredientFlow(Date from, Date to, PaginationParams pp, SortingParams sp) {
        pp = setDefaultPaginationParamsIfNeeded(pp);
        if (sp == null) {
            sp = DEFAULT_INGREDIENT_FLOW_SORTING_PARAMS;
        }
        return statsDao.getIngredientsFlow(from, to, pp, sp);
    }

    public PageableList<ProductFlowStat> getProductFlow(Date from, Date to, PaginationParams pp, SortingParams sp) {
        pp = setDefaultPaginationParamsIfNeeded(pp);
        if (sp == null) {
            sp = DEFAULT_PRODUCT_FLOW_SORTING_PARAMS;
        }
        return statsDao.getProductsFlow(from, to, pp, sp);
    }

    public PageableList<OperatorStat> getOperatorStats(Date from, Date to, WorkstationUser operator,
                                                       PaginationParams pp, SortingParams sp) {
        pp = setDefaultPaginationParamsIfNeeded(pp);
        if (sp == null) {
            sp = DEFAULT_OPERATORS_SORTING_PARAMS;
        }
        return statsDao.getOperatorStats(from, to,operator,pp,sp);
    }


    public PageableList<Order> getCarrierStats(Date from, Date to, WorkstationUser carrier, PaginationParams pp, SortingParams sp) {
        pp = setDefaultPaginationParamsIfNeeded(pp);
        if (sp == null) {
            sp = DEFAULT_CARRIERS_SORTING_PARAMS;
        }
        return statsDao.getCarrierStats(from, to,carrier,pp,sp);
    }

    private PaginationParams setDefaultPaginationParamsIfNeeded(PaginationParams pp) {
        PaginationParams res;
        if (pp == null) {
            res = DEFAULT_PAGINATION_PARAMS;
        } else {
            int page = pp.getPageNumber() <= 0 ? DEFAULT_PAGINATION_PARAMS.getPageNumber() : pp.getPageNumber();
            int results = pp.getObjectsPerPage() <= 0 || pp.getObjectsPerPage() > MAX_OBJECTS_PER_PAGE
                    ? DEFAULT_PAGINATION_PARAMS.getObjectsPerPage() : pp.getObjectsPerPage();
            res = new PaginationParams(page, results);
        }
        return res;
    }

    public int getCarrierShippedOrdersCount(Date from, Date to, WorkstationUser carrier) {
        return statsDao.getCarrierOrdersCount(from, to, carrier, Status.SHIPPED);
    }
    public int getCarrierPaidOrdersCount(Date from, Date to, WorkstationUser carrier) {
        return statsDao.getCarrierOrdersCount(from, to, carrier, Status.PAID);
    }
    public int getCarrierCancelledOrdersCount(Date from, Date to, WorkstationUser carrier) {
        return statsDao.getCarrierOrdersCount(from, to, carrier, Status.CANCELLED);
    }

    public double getOrdersCost(Date from, Date to) {
        return statsDao.getOrdersCost(from, to);
    }

    public double getOrdersPrice(Date from, Date to) {
        return statsDao.getOrdersPrice(from, to);
    }

    public int getOrdersCount(Date from, Date to) {
        return statsDao.getOrdersCount(from, to);
    }
}
