package ru.pizzburg.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.pizzburg.web.domain.Status;
import ru.pizzburg.web.service.orders.FormStatus;
import ru.pizzburg.web.service.orders.FormStatusValidator;
import ru.pizzburg.web.service.orders.OrdersManager;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Controller
@Lazy(false)
public class StatusesController {
    private static final String STATUSES_VIEW_NAME = "statuses";
    protected final Log logger = LogFactory.getLog(getClass());
    @Autowired
    private FormStatusValidator formStatusValidator;
    @Autowired
    private OrdersManager ordersManager;
    @Autowired
    private MessageSource messageSource;

	@Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/orders/statuses", method = RequestMethod.GET)
    public String getStatusesPage(ModelMap model) {
        model.addAttribute("statuses", ordersManager.getStatuses());
        return STATUSES_VIEW_NAME;
    }

	@Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/orders/statuses/edit/{id}", method = RequestMethod.GET)
    public String getStatusEditForm(ModelMap model, @PathVariable int id, RedirectAttributes redirectAttributes) {
        Status s = ordersManager.getStatus(id);
        if (s == null) {
            logger.warn("Null status request");
            redirectAttributes.addFlashAttribute("errorMessage", messageSource.getMessage("error.status-not-exist", null, null));
            return "redirect:/orders/statuses";
        }
        FormStatus fs = new FormStatus();
        fs.setTitle(s.getTitle());
        fs.setDescription(s.getDescription());
        fs.setId(s.getId());
        model.addAttribute("formStatus", fs);
        model.addAttribute("statuses", ordersManager.getStatuses());
        return STATUSES_VIEW_NAME;
    }

	@Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/orders/statuses/edit/{id}", method = RequestMethod.POST)
    public String editStatus(ModelMap model, @ModelAttribute("formStatus") FormStatus editStatus,
                             BindingResult result, RedirectAttributes redirectAttributes) {
        formStatusValidator.validate(editStatus, result);

        if (!result.hasErrors()) {
            //form success
            final Status status = new Status();
            status.setTitle(editStatus.getTitle());
            status.setDescription(editStatus.getDescription());
            status.setId(editStatus.getId());

            final String updateStatusResult = ordersManager.updateStatus(status);
            if (updateStatusResult != null) {
                model.addAttribute("errorMessage", messageSource.getMessage(updateStatusResult, null, null));
                model.addAttribute("statuses", ordersManager.getStatuses());
                return STATUSES_VIEW_NAME;
            } else {
                redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.status-successfully-updated", null, null));
                return "redirect:/orders/statuses";
            }
        } else {
            // form contains errors

            // status title lost. reset it!
            Status s = ordersManager.getStatus(editStatus.getId());
            if (s == null) {
                logger.warn("Status edition: removed while editing");
                return "redirect:/orders/statuses";
            }
            editStatus.setTitle(s.getTitle());

            model.addAttribute("statuses", ordersManager.getStatuses());
            model.addAttribute("formStatus", editStatus);
            return STATUSES_VIEW_NAME;
        }
    }
}
