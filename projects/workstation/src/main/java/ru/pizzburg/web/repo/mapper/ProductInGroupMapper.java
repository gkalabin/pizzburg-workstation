package ru.pizzburg.web.repo.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import ru.pizzburg.web.domain.Product;
import ru.pizzburg.web.domain.ProductInGroup;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class ProductInGroupMapper implements ParameterizedRowMapper<ProductInGroup> {

    @Override
    public ProductInGroup mapRow(ResultSet rs, int rowNum) throws SQLException {
        ProductInGroup pig = new ProductInGroup();
        pig.setProduct(new Product());
        pig.getProduct().setId(rs.getInt("productID"));
        pig.getProduct().setTitle(rs.getString("title"));
        pig.getProduct().setDescription(rs.getString("description"));
        pig.setPrice(rs.getFloat("price"));
        return pig;
    }
}