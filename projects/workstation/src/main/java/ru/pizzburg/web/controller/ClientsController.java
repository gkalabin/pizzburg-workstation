package ru.pizzburg.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.pizzburg.constants.SortStrings;
import ru.pizzburg.utils.Converter;
import ru.pizzburg.utils.Digest;
import ru.pizzburg.utils.PhoneUtils;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.web.domain.Client;
import ru.pizzburg.web.domain.DiscountCard;
import ru.pizzburg.web.service.clients.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Controller
@SessionAttributes({"formClient", "clientSearch", "clientFilter"})
@Lazy(false)
public class ClientsController {

    private static final String CLIENTS_VIEW_NAME = "clients";
    public static final String RESULT_COUNT_COOKIE_SUFFIX = "_clients_resultCount";
    protected final Log logger = LogFactory.getLog(getClass());
    @Autowired
    private ClientsManager clientsManager;
    @Autowired
    private FormClientValidator formClientValidator;
    @Autowired
    private ClientSearchValidator clientSearchValidator;
    @Autowired
    private MessageSource messageSource;

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/clients/search", method = RequestMethod.GET)
    public String searchPage(HttpServletRequest request, ModelMap model, Principal principal) {
        ClientSearch cs = (ClientSearch) model.get("clientSearch");
        if (cs == null) {
            cs = new ClientSearch();
            model.addAttribute("clientSearch", cs);
        }

        FormClient fc = (FormClient) model.get("formClient");
        if (fc == null) {
            fc = new FormClient();
            model.addAttribute("formClient", fc);
        }

        model.addAttribute("clients", clientsManager.getClients(parsePagingParams(request, principal),
                parseSortingParams(request), (ClientFilter) model.get("clientFilter")));
        return CLIENTS_VIEW_NAME;
    }

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/clients/search/cancel", method = RequestMethod.GET)
    public String cancelSearch(ModelMap model) {
        model.addAttribute("clientSearch", new ClientSearch());
        model.addAttribute("clientFilter", new ClientFilter());

        FormClient fc = (FormClient) model.get("formClient");
        if (fc == null) {
            fc = new FormClient();
            model.addAttribute("formClient", fc);
        }
        return "redirect:/clients/";
    }

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/clients/search", method = RequestMethod.POST)
    public String search(HttpServletRequest request, ModelMap model, @ModelAttribute("clientSearch") ClientSearch cs,
                         BindingResult result, Principal principal) {
        clientSearchValidator.validate(cs, result);

        if (cs == null) {
            model.addAttribute("clientSearch", new ClientSearch());
        }

        FormClient fc = (FormClient) model.get("formClient");
        if (fc == null) {
            fc = new FormClient();
            model.addAttribute("formClient", fc);
        }

        ClientFilter filter = cs.getClientFilter();
        model.addAttribute("clientFilter", filter);
        if (result.hasErrors()) {
            model.addAttribute("clients", clientsManager.getClients(parsePagingParams(request, principal),
                    parseSortingParams(request), filter));
            return CLIENTS_VIEW_NAME;
        } else {
            return "redirect:/clients/";
        }
    }

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    public String getPage(HttpServletRequest request, ModelMap model, Principal principal) {
        ClientSearch cs = (ClientSearch) model.get("clientSearch");
        if (cs == null) {
            cs = new ClientSearch();
            model.addAttribute("clientSearch", cs);
        }

        FormClient fc = new FormClient();
        model.addAttribute("formClient", fc);
        model.addAttribute("clients", clientsManager.getClients(parsePagingParams(request, principal),
                parseSortingParams(request), (ClientFilter) model.get("clientFilter")));
        return CLIENTS_VIEW_NAME;
    }

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/clients", method = RequestMethod.POST)
    public String addClient(HttpServletRequest request, ModelMap model, @ModelAttribute("formClient") FormClient addClient,
                            BindingResult result, RedirectAttributes redirectAttributes, Principal principal) {
        formClientValidator.validate(addClient, result);

        if (!result.hasErrors()) {
            //form success

            Client c = new Client();
            c.setPhone(PhoneUtils.parsePhone(addClient.getPhoneStr()));
            c.setName(addClient.getName());
            c.setSurname(addClient.getSurname());
            c.setMiddleName(addClient.getMiddleName());
            c.setDefaultAddress(addClient.getDefaultAddress());
            c.setOrdersSum(addClient.getOrdersSum());
            if (addClient.isWithCard()) {
                DiscountCard dc = new DiscountCard();
                dc.setId(addClient.getCard().getIdLond());
                dc.setEmissionDate(addClient.getCard().getEmissionDateDate());
                c.setCard(dc);
            }
            final String additionResult = clientsManager.addClient(c);
            if (additionResult != null) {
                model.addAttribute("errorMessage", messageSource.getMessage(additionResult, null, null));
                model.addAttribute("clients", clientsManager.getClients(parsePagingParams(request, principal),
                        parseSortingParams(request), (ClientFilter) model.get("clientFilter")));
                return CLIENTS_VIEW_NAME;
            } else {
                redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.client-successfully-added", null, null));
                redirectAttributes.addFlashAttribute("clientSearch", model.get("clientSearch"));
                return "redirect:/" + CLIENTS_VIEW_NAME;
            }
        } else {
            model.addAttribute("clients", clientsManager.getClients(parsePagingParams(request, principal),
                    parseSortingParams(request), (ClientFilter) model.get("clientFilter")));
            return CLIENTS_VIEW_NAME;
        }
    }

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/clients/edit/{phone}", method = RequestMethod.GET)
    public String getEditForm(HttpServletRequest request, ModelMap model, @PathVariable long phone,
                              RedirectAttributes redirectAttributes, Principal principal) {
        ClientSearch cs = (ClientSearch) model.get("clientSearch");
        if (cs == null) {
            cs = new ClientSearch();
            model.addAttribute("clientSearch", cs);
        }

        Client c = clientsManager.getClient(phone);
        if (c == null) {
            logger.warn("Null client request");
            redirectAttributes.addFlashAttribute("errorMessage", messageSource.getMessage("error.client-not-exist", null, null));
            return "redirect:/" + CLIENTS_VIEW_NAME;
        }
        FormClient fc = new FormClient();
        fc.setPhoneStr(PhoneUtils.getPhoneNumberFormatted(c.getPhone()));
        fc.setName(c.getName());
        fc.setSurname(c.getSurname());
        fc.setMiddleName(c.getMiddleName());
        fc.setOrdersSumStr(String.valueOf(c.getOrdersSum()));
        // TODO: deprecated
        fc.setDefaultAddress(c.getDefaultAddress());
        if (c.getCard() != null) {
            fc.setWithCard(true);
            fc.getCard().setId(c.getCard().getIdFormatted());
            fc.getCard().setEmissionDate(ru.pizzburg.utils.DateUtils.getDateFormatted(c.getCard().getEmissionDate()));
        }
        model.addAttribute("formClient", fc);
        model.addAttribute("clients", clientsManager.getClients(parsePagingParams(request, principal),
                parseSortingParams(request), (ClientFilter) model.get("clientFilter")));
        return CLIENTS_VIEW_NAME;
    }

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/clients/edit/{phone}", method = RequestMethod.POST)
    public String editClient(HttpServletRequest request, ModelMap model, @PathVariable long phone, @ModelAttribute("formClient") FormClient editClient,
                             BindingResult result, RedirectAttributes redirectAttributes, Principal principal) {
        if (model.get("formClient") == null) {
            model.addAttribute("formClient", new FormClient());
        }

        formClientValidator.validate(editClient, result);

        if (!result.hasErrors()) {
            //form success
            Client c = new Client();
            c.setPhone(PhoneUtils.parsePhone(editClient.getPhoneStr()));
            c.setName(editClient.getName());
            c.setSurname(editClient.getSurname());
            c.setMiddleName(editClient.getMiddleName());
            c.setDefaultAddress(editClient.getDefaultAddress());
            c.setOrdersSum(editClient.getOrdersSum());
            if (editClient.isWithCard()) {
                DiscountCard dc = new DiscountCard();
                dc.setId(editClient.getCard().getIdLond());
                dc.setEmissionDate(editClient.getCard().getEmissionDateDate());
                c.setCard(dc);
            }
            String updatingResult = clientsManager.updateClient(c, phone);
            if (updatingResult != null) {
                model.addAttribute("errorMessage", messageSource.getMessage(updatingResult, null, null));
                model.addAttribute("clients", clientsManager.getClients(parsePagingParams(request, principal),
                        parseSortingParams(request), (ClientFilter) model.get("clientFilter")));
                return CLIENTS_VIEW_NAME;
            } else {
                redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.client-successfully-updated", null, null));
                redirectAttributes.addFlashAttribute("clientSearch", model.get("clientSearch"));
                return "redirect:/" + CLIENTS_VIEW_NAME;
            }
        } else {
            model.addAttribute("clients", clientsManager.getClients(parsePagingParams(request, principal),
                    parseSortingParams(request), (ClientFilter) model.get("clientFilter")));
            return CLIENTS_VIEW_NAME;
        }
    }

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/clients/remove/{phone}", method = RequestMethod.GET)
    public String removeIngredient(HttpServletRequest request, ModelMap model, @PathVariable long phone,
                                   RedirectAttributes redirectAttributes, Principal principal) {
        if (model.get("formClient") == null) {
            model.addAttribute("formClient", new FormClient());
        }

        String removeResult = clientsManager.removeClient(phone);
        if (removeResult != null) {
            model.addAttribute("errorMessage", messageSource.getMessage(removeResult, null, null));
            model.addAttribute("clients", clientsManager.getClients(parsePagingParams(request, principal),
                    parseSortingParams(request), (ClientFilter) model.get("clientFilter")));
            return CLIENTS_VIEW_NAME;
        } else {
            redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.client-successfully-removed", null, null));
            return "redirect:/" + CLIENTS_VIEW_NAME;
        }
    }

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(method = RequestMethod.GET, value = "/clients/addDiscountCard")
    protected String appendCardToClient(@ModelAttribute("formClient") FormClient formClient) {
        formClient.setWithCard(true);
        formClient.getCard().setEmissionDate(ru.pizzburg.utils.DateUtils.getDateFormatted());
        return "empty";
    }

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(method = RequestMethod.GET, value = "/clients/removeDiscountCard")
    protected String removeCardFromClient(@ModelAttribute("formClient") FormClient formClient) {
        formClient.setWithCard(false);
        return "empty";
    }

    private SortingParams parseSortingParams(HttpServletRequest request) {
        String sort = request.getParameter("sort");
        String dir = request.getParameter("dir");
        if (sort == null || dir == null || !SortStrings.isDirectionCorrect(dir)
                || !SortStrings.isClientCorrect(sort)) {
            return null;
        }
        return new SortingParams(sort, Converter.parseSortOrder(dir));
    }

    private PaginationParams parsePagingParams(HttpServletRequest request, Principal principal) {
        String pageStr = request.getParameter("page");
        String resultsStr = request.getParameter("results");
        String cookieName = Digest.getSha1Hash(principal.getName() + RESULT_COUNT_COOKIE_SUFFIX);
        for (Cookie c : request.getCookies()) {
            if (cookieName.equals(c.getName())) {
                resultsStr = c.getValue();
                break;
            }
        }
        if (pageStr == null && resultsStr == null) {
            return null;
        }
        int page = -1;
        if (pageStr != null) {
            try {
                page = Integer.parseInt(pageStr);
            } catch (NumberFormatException ex) {
                logger.info("invalid paging params (clients page): " + pageStr);
            }
        }
        int results = -1;
        if (resultsStr != null) {
            try {
                results = Integer.parseInt(resultsStr);
            } catch (NumberFormatException ex) {
                logger.info("invalid paging params (clients page): " + pageStr);
            }
        }
        return new PaginationParams(page, results);
    }
}
