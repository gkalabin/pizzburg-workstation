package ru.pizzburg.web.service.orders;

import java.util.List;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class OrderSearch {

    private String orderId;
    private String phone;
    private String card;
    private String address;
    private String registratorLogin;
    private String registratorName;
    private String registratorSurname;
    private boolean registratorMe;
    private String dateFrom;
    private String dateTo;
    private List<Integer> status;
    private String costFrom;
    private String costTo;
    private String discountFrom;
    private String discountTo;
    private OrderFilter orderFilter;

    public String getCostFrom() {
        return costFrom;
    }

    public void setCostFrom(String costFrom) {
        this.costFrom = costFrom;
    }

    public String getCostTo() {
        return costTo;
    }

    public void setCostTo(String costTo) {
        this.costTo = costTo;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRegistratorName() {
        return registratorName;
    }

    public void setRegistratorName(String registratorName) {
        this.registratorName = registratorName;
    }

    public String getRegistratorSurname() {
        return registratorSurname;
    }

    public void setRegistratorSurname(String registratorSurname) {
        this.registratorSurname = registratorSurname;
    }

    public List<Integer> getStatus() {
        return status;
    }

    public void setStatus(List<Integer> status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRegistratorLogin() {
        return registratorLogin;
    }

    public void setRegistratorLogin(String registratorLogin) {
        this.registratorLogin = registratorLogin;
        if (orderFilter!= null) {
            orderFilter.setRegistratorLogin(registratorLogin);
        }
    }

    public OrderFilter getOrderFilter() {
        if (orderFilter == null) {
            return new OrderFilter();
        }
        return orderFilter;
    }

    public void setOrderFilter(OrderFilter orderFilter) {
        this.orderFilter = orderFilter;
    }

    public boolean isRegistratorMe() {
        return registratorMe;
    }

    public void setRegistratorMe(boolean registratorMe) {
        this.registratorMe = registratorMe;
    }

    public String getDiscountFrom() {
        return discountFrom;
    }

    public void setDiscountFrom(String discountFrom) {
        this.discountFrom = discountFrom;
    }


    public String getDiscountTo() {
        return discountTo;
    }

    public void setDiscountTo(String discountTo) {
        this.discountTo = discountTo;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "OrderSearch{" +
                "orderId='" + orderId + '\'' +
                ", phone='" + phone + '\'' +
                ", card='" + card + '\'' +
                ", address='" + address + '\'' +
                ", registratorLogin='" + registratorLogin + '\'' +
                ", registratorName='" + registratorName + '\'' +
                ", registratorSurname='" + registratorSurname + '\'' +
                ", registratorMe=" + registratorMe +
                ", dateFrom='" + dateFrom + '\'' +
                ", dateTo='" + dateTo + '\'' +
                ", status=" + status +
                ", costFrom='" + costFrom + '\'' +
                ", costTo='" + costTo + '\'' +
                ", discountFrom='" + discountFrom + '\'' +
                ", discountTo='" + discountTo + '\'' +
                ", orderFilter=" + orderFilter +
                '}';
    }
}
