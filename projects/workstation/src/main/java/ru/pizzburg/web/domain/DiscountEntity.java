package ru.pizzburg.web.domain;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class DiscountEntity {

    private int sumFrom;
    private int sumTo;
    private float discount;

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public int getSumFrom() {
        return sumFrom;
    }

    public void setSumFrom(int sumFrom) {
        this.sumFrom = sumFrom;
    }

    public int getSumTo() {
        return sumTo;
    }

    public void setSumTo(int sumTo) {
        this.sumTo = sumTo;
    }

    @Override
    public String toString() {
        return "DiscountEntity{" + "fromSum=" + sumFrom + ", toSum=" + sumTo + ", discount=" + discount + '}';
    }
}
