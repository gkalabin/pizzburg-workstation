package ru.pizzburg.web.repo.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import ru.pizzburg.web.domain.Ingredient;
import ru.pizzburg.web.domain.IngredientFlowStat;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class IngredientFlowMapper implements ParameterizedRowMapper<IngredientFlowStat> {

    @Override
    public IngredientFlowStat mapRow(ResultSet rs, int rowNum) throws SQLException {
        IngredientFlowStat stat = new IngredientFlowStat();
        stat.setIngredient(new Ingredient());
        stat.getIngredient().setTitle(rs.getString("title"));
        stat.getIngredient().setUnits(rs.getString("units"));
        stat.getIngredient().setId(rs.getInt("ingredientID"));
        stat.getIngredient().setPrice(rs.getFloat("cost"));
        stat.getIngredient().setDescription(rs.getString("description"));
        stat.setCount(rs.getFloat("count"));
        stat.setCost(rs.getFloat("cost"));
        return stat;
    }
}
