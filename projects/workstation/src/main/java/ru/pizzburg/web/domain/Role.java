package ru.pizzburg.web.domain;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class Role {

    public static final Role ROLE_CARRIER = new Role();
    public static final Role ROLE_OPERATOR = new Role();

    private int id;
    private String name;
    private String description;

    static {
        ROLE_CARRIER.setId(1);
        ROLE_CARRIER.setName("ROLE_CARRIER");
        ROLE_OPERATOR.setId(2);
        ROLE_OPERATOR.setName("ROLE_OPERATOR");
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Role{" + "id=" + id + ", name=" + name + ", description=" + description + '}';
    }
}
