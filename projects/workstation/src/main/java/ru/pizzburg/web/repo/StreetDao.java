package ru.pizzburg.web.repo;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Repository
@Lazy(false)
public class StreetDao extends JdbcTemplate {
    @Override
    @Autowired
    public void setDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    public List<String> getStreetsList(String startsWith, int maxRows) {
        return query("SELECT title FROM pizzburgdb.Streets WHERE title LIKE ? LIMIT ?",
                new Object[]{"%"+startsWith + "%", maxRows}, new SingleColumnRowMapper<String>());
    }
}
