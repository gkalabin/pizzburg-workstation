package ru.pizzburg.web.domain;

import java.util.List;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class Product {

    private int id;
    private String title;
    private String description;
    private List<IngredientInProduct> ingredients;
    private List<GroupInProduct> groups;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<IngredientInProduct> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<IngredientInProduct> ingredients) {
        this.ingredients = ingredients;
    }

    public List<GroupInProduct> getGroups() {
        return groups;
    }

    public void setGroups(List<GroupInProduct> groups) {
        this.groups = groups;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", title=" + title + ", description=" + description + ", ingredients=" + ingredients + ", groups=" + groups + '}';
    }
}
