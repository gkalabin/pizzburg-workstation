package ru.pizzburg.web.repo.mapper;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import ru.pizzburg.web.domain.OperatorStat;
import ru.pizzburg.web.domain.Order;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class OperatorStatMapper implements ParameterizedRowMapper<OperatorStat> {

    @Override
    public OperatorStat mapRow(ResultSet rs, int rowNum) throws SQLException {
        OperatorStat stat = new OperatorStat();
        Order order = new Order();
        stat.setOrder(order);
        order.setId(rs.getInt("orderID"));
        order.setRegisterDate(rs.getTimestamp("registerDate"));
        // not needed
//        order.setShipmentAddress(rs.getString("shipmentAddress"));
//        order.setCost(rs.getFloat("cost"));
        order.setDiscount(rs.getFloat("discount"));
//        float discount = rs.getFloat("alternateDiscount");
//        if (discount != 0) {
//            order.setAlternateDiscount(discount);
//        }
//        long card = rs.getLong("alternateDiscountCard");
//        if (card != 0) {
//            DiscountCard dc = new DiscountCard();
//            dc.setId(card);
//            order.setAlternateDiscountCard(dc);
//        }

        stat.setCost(rs.getFloat("orderCost"));
        stat.setPrice(rs.getFloat("orderPrice"));
        return stat;
    }
}