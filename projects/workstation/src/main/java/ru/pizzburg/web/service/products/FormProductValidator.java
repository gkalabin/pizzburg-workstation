package ru.pizzburg.web.service.products;

import org.springframework.context.annotation.Lazy;
import org.springframework.util.AutoPopulatingList;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.pizzburg.constants.FieldLength;
import ru.pizzburg.web.domain.GroupInProduct;
import ru.pizzburg.web.domain.IngredientInProduct;
import ru.pizzburg.web.domain.ProductGroup;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Lazy(false)
public class FormProductValidator implements Validator {

    @Override
    public boolean supports(Class clazz) {
        return FormProduct.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "error.empty-title", "Empty title");
        FormProduct fp = (FormProduct) obj;
        checkFieldLength(fp, errors);
        processIngredients(fp, errors);
        processGroups(fp, errors);
    }

    private void processIngredients(FormProduct fp, Errors errors) {
        AutoPopulatingList<IngredientFormEntry> ingredientStr = fp.getIngredientsStr();
        AutoPopulatingList<IngredientFormEntry> ingredientStrNew = new AutoPopulatingList<IngredientFormEntry>(IngredientFormEntry.class);
        ingredientStrNew.addAll(ingredientStr);
        ingredientStr.clear();
        fp.getIngredients().clear();

        // remove removed items
        for (int i = 0; i < ingredientStrNew.size(); i++) {
            if (!fp.getDeletedIngredients().contains(i)) {
                final IngredientFormEntry ingredient = ingredientStrNew.get(i);
                ingredientStr.add(ingredient);
            }
        }
        fp.getDeletedIngredients().clear();

        for (int i = 0; i < ingredientStr.size(); i++) {
            try {
                int id = Integer.parseInt(ingredientStr.get(i).getId());
                if (id < 0) {
                    errors.rejectValue("ingredientsStr[" + i + "]", "error.ingredient-negative-id", "Ingredient error");
                } else {
                    fp.getIngredients().add(new IngredientInProduct());
                    fp.getIngredients().get(i).getIngredient().setId(id);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("ingredientsStr[" + i + "]", "error.ingredient-not-int-id", "Ingredient error");
                continue;
            }
            try {
                ingredientStr.get(i).setCount(ingredientStr.get(i).getCount().replace(",", "."));
                float count = Float.parseFloat(ingredientStr.get(i).getCount());
                if (count <= 0) {
                    errors.rejectValue("ingredientsStr[" + i + "]", "error.negative-count", "Count must be >0");
                } else {
                    // TODO: index out of bounds!
                    fp.getIngredients().get(i).setCount(count);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("ingredientsStr[" + i + "]", "typeMismatch.float", "Count must be a number");
            }
        }
        for (int i = 0; i < ingredientStr.size(); i++) {
            for (int j = i + 1; j < ingredientStr.size(); j++) {
                if (ingredientStr.get(i).getId().equals(ingredientStr.get(j).getId()) && !ingredientStr.get(i).getId().isEmpty()) {
                    if (errors.getFieldErrorCount("ingredientsStr[" + i + "]") == 0) {
                        errors.rejectValue("ingredientsStr[" + i + "]", "error.not-unique-ingredient", "Duplicate ingredient");
                    }
                    if (errors.getFieldErrorCount("ingredientsStr[" + j + "]") == 0) {
                        errors.rejectValue("ingredientsStr[" + j + "]", "error.not-unique-ingredient", "Duplicate ingredient");
                    }
                }
            }
        }
    }

    private void processGroups(FormProduct fp, Errors errors) {
        AutoPopulatingList<GroupFormEntry> groupsStr = fp.getGroupsStr();
        AutoPopulatingList<GroupFormEntry> groupsStrNew = new AutoPopulatingList<GroupFormEntry>(GroupFormEntry.class);
        groupsStrNew.addAll(groupsStr);
        groupsStr.clear();
        fp.getGroups().clear();

        // remove removed items
        for (int i = 0; i < groupsStrNew.size(); i++) {
            if (!fp.getDeletedGroups().contains(i)) {
                final GroupFormEntry group = groupsStrNew.get(i);
                groupsStr.add(group);
            }
        }
        fp.getDeletedGroups().clear();

        for (int i = 0; i < groupsStr.size(); i++) {
            try {
                int id = Integer.parseInt(groupsStr.get(i).getId());
                if (id < 0) {
                    errors.rejectValue("groupsStr[" + i + "]", "error.group-negative-id", "Group error");
                } else {
                    final GroupInProduct groupInProduct = new GroupInProduct();
                    groupInProduct.setGroup(new ProductGroup());
                    groupInProduct.getGroup().setId(id);
                    fp.getGroups().add(groupInProduct);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("groupsStr[" + i + "]", "error.group-not-int-id", "Group error");
                continue;
            }
            try {
                groupsStr.get(i).setPrice(groupsStr.get(i).getPrice().replace(",", "."));
                float price = Float.parseFloat(groupsStr.get(i).getPrice());
                if (price < 0) {
                    errors.rejectValue("groupsStr[" + i + "]", "error.negative-price", "Price must be >0");
                } else {
                    // TODO: index out of bounds!
                    fp.getGroups().get(i).setPrice(price);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("groupsStr[" + i + "]", "typeMismatch.float", "Count must be a number");
            }
        }
        for (int i = 0; i < groupsStr.size(); i++) {
            for (int j = i + 1; j < groupsStr.size(); j++) {
                if (groupsStr.get(i).getId().equals(groupsStr.get(j).getId()) && !groupsStr.get(i).getId().isEmpty()) {
                    if (errors.getFieldErrorCount("groupsStr[" + i + "]") == 0) {
                        errors.rejectValue("groupsStr[" + i + "]", "error.not-unique-group", "Duplicate group");
                    }
                    if (errors.getFieldErrorCount("groupsStr[" + j + "]") == 0) {
                        errors.rejectValue("groupsStr[" + j + "]", "error.not-unique-group", "Duplicate group");
                    }
                }
            }
        }
    }

    private void checkFieldLength(FormProduct fp, Errors errors) {
        if (fp.getTitle().length() > FieldLength.PRODUCT_TITLE) {
            errors.rejectValue("title", "error.too-long-value",
                    new Object[]{FieldLength.PRODUCT_TITLE}, "Max length is " + FieldLength.PRODUCT_TITLE);
        }
        if (fp.getDescription().length() > FieldLength.PRODUCT_DESCRIPTION) {
            errors.rejectValue("description", "error.too-long-value",
                    new Object[]{FieldLength.PRODUCT_DESCRIPTION}, "Max length is " + FieldLength.PRODUCT_DESCRIPTION);
        }
    }
}
