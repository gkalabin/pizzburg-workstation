package ru.pizzburg.web.service.ingredients;

import org.springframework.context.annotation.Lazy;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.pizzburg.constants.FieldLength;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Lazy(false)
public class FormIngredientValidator implements Validator {

    @Override
    public boolean supports(Class clazz) {
        return FormIngredient.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "error.empty-title", "Empty title");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "error.empty-price", "Empty price");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "balance", "error.empty-balance", "Empty balance");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "units", "error.empty-units", "Empty units");
        FormIngredient fi = (FormIngredient) obj;
        checkFieldLength(fi, errors);
        try {
            fi.setPriceStr(fi.getPriceStr().replace(",", "."));
            float price = Float.valueOf(fi.getPriceStr());
            if (price <= 0) {
                errors.rejectValue("priceStr", "error.negative-price", "Price must be > 0");
            } else {
                fi.setPrice(price);
            }
        } catch (NumberFormatException ex) {
            errors.rejectValue("priceStr", "typeMismatch.float", "Not convertible to number");
        }
        try {
            fi.setBalanceStr(fi.getBalanceStr().replace(",", "."));
            fi.setBalance(Float.valueOf(fi.getBalanceStr()));
        } catch (NumberFormatException ex) {
            errors.rejectValue("balanceStr", "typeMismatch.float", "Not convertible to number");
        }
    }

    private void checkFieldLength(FormIngredient ai, Errors errors) {
        if (ai.getTitle().length() > FieldLength.INGREDIENT_TITLE) {
            errors.rejectValue("title", "error.too-long-value",
                    new Object[]{FieldLength.INGREDIENT_TITLE}, "Max length is " + FieldLength.INGREDIENT_TITLE);
        }
        if (ai.getDescription().length() > FieldLength.INGREDIENT_DESCRIPTION) {
            errors.rejectValue("description", "error.too-long-value",
                    new Object[]{FieldLength.INGREDIENT_DESCRIPTION}, "Max length is " + FieldLength.INGREDIENT_DESCRIPTION);
        }
        if (ai.getUnits().length() > FieldLength.INGREDIENT_UNITS) {
            errors.rejectValue("units", "error.too-long-value",
                    new Object[]{FieldLength.INGREDIENT_UNITS}, "Max length is " + FieldLength.INGREDIENT_UNITS);
        }
    }

    public void validateBalance(Object obj, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "balance", "error.empty-balance", "Empty balance");
        FormIngredient fi = (FormIngredient) obj;
        try {
            fi.setBalanceStr(fi.getBalanceStr().replace(",", "."));
            fi.setBalance(Float.valueOf(fi.getBalanceStr()));
        } catch (NumberFormatException ex) {
            errors.rejectValue("balanceStr", "typeMismatch.float", "Not convertible to number");
        }
    }
}