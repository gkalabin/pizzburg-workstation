package ru.pizzburg.web.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * POJO representing Order entity
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class Order implements Serializable {

    private long id;
    private Date registerDate;
    private Client client;
    private String shipmentAddress;
    private Status status;
    private int personsCount;
    private String comment;
    private WorkstationUser registrator;
    private WorkstationUser carrier;
    private List<ProductInOrder> products;
    private float cost;
    private float discount;
    private float alternateDiscount = -1;
    private DiscountCard alternateDiscountCard;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public WorkstationUser getRegistrator() {
        return registrator;
    }

    public void setRegistrator(WorkstationUser registrator) {
        this.registrator = registrator;
    }

    public String getShipmentAddress() {
        return shipmentAddress;
    }

    public void setShipmentAddress(String shipmentAddress) {
        this.shipmentAddress = shipmentAddress;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<ProductInOrder> getProducts() {
        return products;
    }

    public void setProducts(List<ProductInOrder> products) {
        this.products = products;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public String getRegisterDateFormatted() {
        return ru.pizzburg.utils.DateUtils.getDateTimeFormatted(registerDate);
    }

    public String getRegisterDateOnly() {
        return ru.pizzburg.utils.DateUtils.getDateFormatted(registerDate);
    }

    public WorkstationUser getCarrier() {
        return carrier;
    }

    public void setCarrier(WorkstationUser carrier) {
        this.carrier = carrier;
    }

    public float getAlternateDiscount() {
        return alternateDiscount;
    }

    public void setAlternateDiscount(float alternateDiscount) {
        this.alternateDiscount = alternateDiscount;
    }

    public DiscountCard getAlternateDiscountCard() {
        return alternateDiscountCard;
    }

    public void setAlternateDiscountCard(DiscountCard alternateDiscountCard) {
        this.alternateDiscountCard = alternateDiscountCard;
    }

    public int getPersonsCount() {
        return personsCount;
    }

    public void setPersonsCount(int personsCount) {
        this.personsCount = personsCount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", registerDate=" + registerDate +
                ", client=" + client +
                ", shipmentAddress='" + shipmentAddress + '\'' +
                ", status=" + status +
                ", personsCount=" + personsCount +
                ", comment='" + comment + '\'' +
                ", registrator=" + registrator +
                ", carrier=" + carrier +
                ", products=" + products +
                ", cost=" + cost +
                ", discount=" + discount +
                ", alternateDiscount=" + alternateDiscount +
                ", alternateDiscountCard=" + alternateDiscountCard +
                '}';
    }
}
