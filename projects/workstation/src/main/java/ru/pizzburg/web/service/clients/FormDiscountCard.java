package ru.pizzburg.web.service.clients;

import java.util.Date;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class FormDiscountCard {

    private String id;
    private String emissionDate = ru.pizzburg.utils.DateUtils.getDateFormatted();
    private long idLond;
    private Date emissionDateDate;

    public String getEmissionDate() {
        return emissionDate;
    }

    public void setEmissionDate(String emissionDate) {
        this.emissionDate = emissionDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getEmissionDateDate() {
        return emissionDateDate;
    }

    public void setEmissionDateDate(Date emissionDateDate) {
        this.emissionDateDate = emissionDateDate;
    }

    public long getIdLond() {
        return idLond;
    }

    public void setIdLond(long idLond) {
        this.idLond = idLond;
    }

    @Override
    public String toString() {
        return "FormDiscountCard{" + "id=" + id + ", emissionDate=" + emissionDate + ", idLond=" + idLond + ", emissionDateDate=" + emissionDateDate + '}';
    }
}
