package ru.pizzburg.web.repo.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import ru.pizzburg.web.domain.DiscountEntity;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class DiscountEntryMapper implements ParameterizedRowMapper<DiscountEntity> {

    @Override
    public DiscountEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
        DiscountEntity discount = new DiscountEntity();
        discount.setSumFrom(rs.getInt("sumFrom"));
        discount.setDiscount(rs.getFloat("discount"));
        return discount;
    }
}