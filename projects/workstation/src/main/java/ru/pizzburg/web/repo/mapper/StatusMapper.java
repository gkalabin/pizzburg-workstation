package ru.pizzburg.web.repo.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import ru.pizzburg.web.domain.Status;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class StatusMapper implements ParameterizedRowMapper<Status> {

    @Override
    public Status mapRow(ResultSet rs, int rowNum) throws SQLException {
        Status status = new Status();
        status.setId(rs.getInt("statusID"));
        status.setTitle(rs.getString("title"));
        status.setDescription(rs.getString("description"));
        return status;
    }
}