package ru.pizzburg.web.repo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.stereotype.Repository;
import ru.pizzburg.constants.SortStrings;
import ru.pizzburg.utils.*;
import ru.pizzburg.utils.data.PageableList;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.web.domain.*;
import ru.pizzburg.web.repo.mapper.IngredientFlowMapper;
import ru.pizzburg.web.repo.mapper.OperatorStatMapper;
import ru.pizzburg.web.repo.mapper.OrderMapper;
import ru.pizzburg.web.repo.mapper.ProductFlowMapper;

import javax.sql.DataSource;
import java.util.Date;
import java.util.List;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Repository
@Lazy(false)
public class StatsDao extends JdbcTemplate {

    private final Log logger = LogFactory.getLog(getClass());

    @Override
    @Autowired
    public void setDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    public PageableList<IngredientFlowStat> getIngredientsFlow(Date from, Date to, PaginationParams pp, SortingParams sp) {
        PageableList<IngredientFlowStat> result = new PageableList<IngredientFlowStat>();

        result.setListSize(
                queryForInt("SELECT COUNT(DISTINCT Ingredients.ingredientID) FROM "
                        + "Ingredients LEFT JOIN pizzburgdb.ProductIngredients ON Ingredients.ingredientID = ProductIngredients.ingredientID, "
                        + "(SELECT Products.*, SUM(OrdersProducts.count) AS prodCount FROM pizzburgdb.Products LEFT JOIN pizzburgdb.OrdersProducts ON Products.productID = OrdersProducts.productID "
                        + "LEFT JOIN pizzburgdb.Orders ON Orders.orderID = OrdersProducts.orderID WHERE (registerDate BETWEEN ? AND ?) AND Orders.orderStatus >= ? GROUP BY productID) prods "
                        + "WHERE prods.productID = ProductIngredients.productID", from, to, Status.PROCESSING.getId())
        );

        String query = "SELECT "
                + "Ingredients.*, SUM(prods.prodCount*ProductIngredients.count) AS count, "
                + "SUM(prods.prodCount*ProductIngredients.count)*Ingredients.price AS cost FROM "
                + "Ingredients LEFT JOIN pizzburgdb.ProductIngredients ON Ingredients.ingredientID = ProductIngredients.ingredientID, "
                + "(SELECT Products.*, SUM(OrdersProducts.count) AS prodCount FROM pizzburgdb.Products LEFT JOIN pizzburgdb.OrdersProducts ON Products.productID = OrdersProducts.productID "
                + "LEFT JOIN pizzburgdb.Orders ON Orders.orderID = OrdersProducts.orderID WHERE (registerDate BETWEEN ? AND ?) AND Orders.orderStatus >= ? GROUP BY productID) prods "
                + "WHERE prods.productID = ProductIngredients.productID "
                + "GROUP BY Ingredients.ingredientID ORDER BY %s %s LIMIT ?, ?";
        if (SortStrings.STATS_INGREDIENT_SORT_CRITERION.equalsIgnoreCase(sp.getSortCriterion())) {
            query = String.format(query, "Ingredients.title", Converter.getStringSortOrder(sp.getSortDirection()));
        } else {
            query = String.format(query, sp.getSortCriterion(), Converter.getStringSortOrder(sp.getSortDirection()));
        }
        final int fromRec = (pp.getPageNumber() - 1) * pp.getObjectsPerPage();
        List<IngredientFlowStat> ingredientsFlow = query(query,
                new Object[]{from, to, Status.PROCESSING.getId(), fromRec, pp.getObjectsPerPage()},
                new IngredientFlowMapper());
        result.setData(ingredientsFlow);
        result.setPaginationParams(pp);
        result.setSortingParams(sp);
        return result;
    }

    public PageableList<ProductFlowStat> getProductsFlow(Date from, Date to, PaginationParams pp, SortingParams sp) {
        PageableList<ProductFlowStat> result = new PageableList<ProductFlowStat>();

        result.setListSize(
                queryForInt("SELECT COUNT(DISTINCT Products.productID) FROM pizzburgdb.Products LEFT JOIN pizzburgdb.OrdersProducts ON Products.productID = OrdersProducts.productID "
                        + "LEFT JOIN pizzburgdb.Orders ON Orders.orderID = OrdersProducts.orderID WHERE (registerDate BETWEEN ? AND ?) AND Orders.orderStatus >= ?",
                        from, to, Status.PROCESSING.getId())
        );

        String query = "SELECT Products.*, SUM(OrdersProducts.count) AS count FROM pizzburgdb.Products LEFT JOIN pizzburgdb.OrdersProducts ON Products.productID = OrdersProducts.productID "
                + "LEFT JOIN pizzburgdb.Orders ON Orders.orderID = OrdersProducts.orderID WHERE (registerDate BETWEEN ? AND ?) AND Orders.orderStatus >= ? GROUP BY Products.productID ORDER BY %s %s LIMIT ?, ?";
        if (SortStrings.STATS_PRODUCT_SORT_CRITERION.equalsIgnoreCase(sp.getSortCriterion())) {
            query = String.format(query, "Products.title", Converter.getStringSortOrder(sp.getSortDirection()));
        } else {
            query = String.format(query, sp.getSortCriterion(), Converter.getStringSortOrder(sp.getSortDirection()));
        }
        final int fromRec = (pp.getPageNumber() - 1) * pp.getObjectsPerPage();
        List<ProductFlowStat> productsFlow = query(query,
                new Object[]{from, to, Status.PROCESSING.getId(), fromRec, pp.getObjectsPerPage()},
                new ProductFlowMapper());
        result.setData(productsFlow);
        result.setPaginationParams(pp);
        result.setSortingParams(sp);
        return result;
    }

    public PageableList<OperatorStat> getOperatorStats(Date from, Date to, WorkstationUser operator, PaginationParams pp, SortingParams sp) {
        PageableList<OperatorStat> result = new PageableList<OperatorStat>();

        List<Long> sizeQuery = query("SELECT COUNT(*) FROM pizzburgdb.OrdersStats "
                + "WHERE (registerDate BETWEEN ? AND ?) AND registrator=?",
                new Object[]{from, to, operator.getId()},
                new SingleColumnRowMapper<Long>());

        result.setListSize(sizeQuery == null || sizeQuery.isEmpty() ? 0 : Converter.longToInt(sizeQuery.get(0)));

        String query = "SELECT * FROM pizzburgdb.OrdersStats "
                + "WHERE (registerDate BETWEEN ? AND ?) AND registrator=? ORDER BY %s %s LIMIT ?, ?";
        query = String.format(query, sp.getSortCriterion(), Converter.getStringSortOrder(sp.getSortDirection()));
        final int fromRec = (pp.getPageNumber() - 1) * pp.getObjectsPerPage();
        List<OperatorStat> stats = query(query,
                new Object[]{from, to, operator.getId(), fromRec, pp.getObjectsPerPage()},
                new OperatorStatMapper());
        result.setData(stats);
        result.setPaginationParams(pp);
        result.setSortingParams(sp);
        return result;
    }

    public PageableList<Order> getCarrierStats(Date from, Date to, WorkstationUser carrier, PaginationParams pp, SortingParams sp) {
        PageableList<Order> result = new PageableList<Order>();

        List<Long> sizeQuery = query("SELECT COUNT(*) FROM pizzburgdb.Orders "
                + "WHERE (registerDate BETWEEN ? AND ?) AND carrier=?",
                new Object[]{from, to, carrier.getId()},
                new SingleColumnRowMapper<Long>());

        result.setListSize(sizeQuery == null || sizeQuery.isEmpty() ? 0 : Converter.longToInt(sizeQuery.get(0)));

        String query = "SELECT * FROM pizzburgdb.Orders "
                + "WHERE (registerDate BETWEEN ? AND ?) AND carrier=? ORDER BY %s %s LIMIT ?, ?";
        query = String.format(query, sp.getSortCriterion(), Converter.getStringSortOrder(sp.getSortDirection()));
        final int fromRec = (pp.getPageNumber() - 1) * pp.getObjectsPerPage();
        List<Order> stats = query(query,
                new Object[]{from, to, carrier.getId(), fromRec, pp.getObjectsPerPage()},
                new OrderMapper());
        result.setData(stats);
        result.setPaginationParams(pp);
        result.setSortingParams(sp);
        return result;
    }

    public int getCarrierOrdersCount(Date from, Date to, WorkstationUser carrier, Status status) {
        List<Long> sizeQuery = query("SELECT COUNT(*) FROM pizzburgdb.Orders "
                + "WHERE (registerDate BETWEEN ? AND ?) AND carrier=? AND orderStatus = ?",
                new Object[]{from, to, carrier.getId(), status.getId()},
                new SingleColumnRowMapper<Long>());
        return sizeQuery == null || sizeQuery.isEmpty() ? 0 : Converter.longToInt(sizeQuery.get(0));
    }

    public double getOrdersCost(Date from, Date to) {
        List<Double> queryResult = query("SELECT SUM(cost*(100-discount)/100) FROM pizzburgdb.Orders WHERE registerDate BETWEEN ? AND ? " +
                "AND orderStatus != ?",
                new Object[]{from, to, Status.CANCELLED.getId()},
                new SingleColumnRowMapper<Double>());
        if (queryResult != null && queryResult.size() > 1) {
            logger.warn("getOrdersCost: not unique result");
        }
        return queryResult == null || queryResult.isEmpty() || queryResult.get(0) == null ? 0 : queryResult.get(0);
    }

    public double getOrdersPrice(Date from, Date to) {
        List<Double> queryResult = query("SELECT SUM(Ingredients.price*IngredientsCountsForOrders.ingredientCount) " +
                "FROM pizzburgdb.IngredientsCountsForOrders " +
                "LEFT JOIN pizzburgdb.Ingredients ON Ingredients.ingredientID = IngredientsCountsForOrders.ingredientID " +
                "LEFT JOIN pizzburgdb.Orders ON Orders.orderID = IngredientsCountsForOrders.orderID " +
                "WHERE Orders.registerDate BETWEEN ? AND ?",
                new Object[]{from, to},
                new SingleColumnRowMapper<Double>());
        if (queryResult != null && queryResult.size() > 1) {
            logger.warn("getOrdersPrice: not unique result");
        }
        return queryResult == null || queryResult.isEmpty() || queryResult.get(0) == null ? 0 : queryResult.get(0);
    }

    public int getOrdersCount(Date from, Date to) {
        return queryForInt("SELECT COUNT(*) FROM pizzburgdb.Orders WHERE registerDate BETWEEN ? AND ?", from, to);
    }
}
