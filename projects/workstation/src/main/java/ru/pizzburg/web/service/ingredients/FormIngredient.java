package ru.pizzburg.web.service.ingredients;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class FormIngredient {

    private int id;
    private String title;
    private String description;
    private String priceStr;
    private String balanceStr;
    private String units;
    private float price;
    private float balance;

    public String getPriceStr() {
        return priceStr;
    }

    public void setPriceStr(String price) {
        this.priceStr = price;
    }

    public String getBalanceStr() {
        return balanceStr;
    }

    public void setBalanceStr(String balanceStr) {
        this.balanceStr = balanceStr;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "FormIngredient{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", priceStr='" + priceStr + '\'' +
                ", balanceStr='" + balanceStr + '\'' +
                ", units='" + units + '\'' +
                ", price=" + price +
                ", balance=" + balance +
                '}';
    }
}
