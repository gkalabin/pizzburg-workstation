package ru.pizzburg.web.service.groups;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class ProductFormEntry {

    private String id = "";
    private String price = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "ProductFormEntry{" + "id=" + id + ", price=" + price + '}';
    }
}
