package ru.pizzburg.web.service.orders;

import org.springframework.context.annotation.Lazy;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.pizzburg.constants.Const;
import ru.pizzburg.utils.DateUtils;
import ru.pizzburg.constants.FieldLength;
import ru.pizzburg.utils.StringUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Lazy(false)
public class OrderSearchValidator implements Validator {

    @Override
    public boolean supports(Class clazz) {
        return OrderSearch.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        OrderSearch os = (OrderSearch) obj;
        checkFieldLength(os, errors);
        os.setOrderFilter(new OrderFilter());

        if (os.isRegistratorMe()) {
            os.setRegistratorLogin("");
            os.setRegistratorName("");
            os.setRegistratorSurname("");
            os.getOrderFilter().setIsEmpty(false);
        }

        if (!StringUtils.isStringEmptyOrWhitespaces(os.getAddress())) {
            os.getOrderFilter().setAddress(os.getAddress().replace("*", "%").replace("?", "_"));
            os.getOrderFilter().setIsEmpty(false);
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(os.getRegistratorLogin()) && !os.isRegistratorMe()) {
            os.getOrderFilter().setRegistratorLogin(os.getRegistratorLogin().replace("*", "%").replace("?", "_"));
            os.getOrderFilter().setIsEmpty(false);
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(os.getRegistratorName()) && !os.isRegistratorMe()) {
            os.getOrderFilter().setRegistratorName(os.getRegistratorName().replace("*", "%").replace("?", "_"));
            os.getOrderFilter().setIsEmpty(false);
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(os.getRegistratorSurname()) && !os.isRegistratorMe()) {
            os.getOrderFilter().setRegistratorSurname(os.getRegistratorSurname().replace("*", "%").replace("?", "_"));
            os.getOrderFilter().setIsEmpty(false);
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(os.getPhone())) {
            String phone = os.getPhone().replaceAll("[^\\d*?]", "").replace("*", "%").replace("?", "_");
            os.getOrderFilter().setPhone(phone);
            os.getOrderFilter().setIsEmpty(false);
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(os.getCard())) {
            String card = os.getCard().replaceAll("[^\\d*?]", "").replace("*", "%").replace("?", "_");
            os.getOrderFilter().setCard(card);
            os.getOrderFilter().setIsEmpty(false);
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(os.getOrderId())) {
            try {
                int id = Integer.parseInt(os.getOrderId());
                os.getOrderFilter().setOrderId(id);
                os.getOrderFilter().setIsEmpty(false);
            } catch (NumberFormatException ex) {
                errors.rejectValue("orderId", "typeMismatch.int", "Not convertable to number");
            }
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(os.getDateFrom())) {
            try {
                Date from = DateUtils.IN_DATE_TIME_FORMAT.parse(os.getDateFrom());
                os.getOrderFilter().setDateFrom(from);
                os.getOrderFilter().setIsEmpty(false);
            } catch (ParseException ex) {
                errors.rejectValue("dateFrom", "error.invalid-date-time-format", new Object[]{Const.IN_DATE_TIME_FORMAT_STRING}, "Invalid datetime format");
            }
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(os.getDateTo())) {
            try {
                Date to = DateUtils.IN_DATE_TIME_FORMAT.parse(os.getDateTo());
                os.getOrderFilter().setDateTo(to);
                os.getOrderFilter().setIsEmpty(false);
            } catch (ParseException ex) {
                errors.rejectValue("dateTo", "error.invalid-date-time-format", new Object[]{Const.IN_DATE_TIME_FORMAT_STRING}, "Invalid datetime format");
            }
        }

        if (!StringUtils.isStringEmptyOrWhitespaces(os.getCostFrom())) {
            try {
                os.setCostFrom(os.getCostFrom().replace(",", "."));
                float cost = Float.valueOf(os.getCostFrom());
                if (cost <= 0) {
                    errors.rejectValue("costFrom", "error.negative-cost", "Cost must be > 0");
                } else {
                    os.getOrderFilter().setCostFrom(cost);
                    os.getOrderFilter().setIsEmpty(false);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("costFrom", "typeMismatch.float", "Not convertable to number");
            }
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(os.getCostTo())) {
            try {
                os.setCostTo(os.getCostTo().replace(",", "."));
                float cost = Float.valueOf(os.getCostTo());
                if (cost <= 0) {
                    errors.rejectValue("costTo", "error.negative-cost", "Cost must be > 0");
                } else {
                    os.getOrderFilter().setCostTo(cost);
                    os.getOrderFilter().setIsEmpty(false);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("costTo", "typeMismatch.float", "Not convertable to number");
            }
        }

        if (!StringUtils.isStringEmptyOrWhitespaces(os.getDiscountFrom())) {
            try {
                os.setDiscountFrom(os.getDiscountFrom().replace(",", "."));
                float discount = Float.valueOf(os.getDiscountFrom());
                if (discount <= 0) {
                    errors.rejectValue("discountFrom", "error.negative-discount", "Discount must be > 0");
                } else {
                    os.getOrderFilter().setDiscountFrom(discount);
                    os.getOrderFilter().setIsEmpty(false);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("discountFrom", "typeMismatch.float", "Not convertable to number");
            }
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(os.getDiscountTo())) {
            try {
                os.setDiscountTo(os.getDiscountTo().replace(",", "."));
                float discount = Float.valueOf(os.getDiscountTo());
                if (discount <= 0) {
                    errors.rejectValue("discountTo", "error.negative-discount", "Discount must be > 0");
                } else {
                    os.getOrderFilter().setDiscountTo(discount);
                    os.getOrderFilter().setIsEmpty(false);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("discountTo", "typeMismatch.float", "Not convertable to number");
            }
        }

        os.getOrderFilter().setStatuses(new ArrayList<Integer>());
        if (os.getStatus() != null) {
            os.getOrderFilter().getStatuses().addAll(os.getStatus());
            os.getOrderFilter().setIsEmpty(false);
        }

        if (errors.hasErrors()) {
            os.setOrderFilter(new OrderFilter());
        }
    }

    private void checkFieldLength(OrderSearch os, Errors errors) {
        if (os.getAddress() != null && os.getAddress().length() > FieldLength.CLIENT_DEFAULT_ADDRESS) {
            errors.rejectValue("address", "error.too-long-value",
                    new Object[]{FieldLength.CLIENT_DEFAULT_ADDRESS}, "Max length is " + FieldLength.CLIENT_DEFAULT_ADDRESS);
        }
        if (os.getRegistratorLogin() != null && os.getRegistratorLogin().length() > FieldLength.ADMIN_LOGIN) {
            errors.rejectValue("registratorLogin", "error.too-long-value",
                    new Object[]{FieldLength.ADMIN_LOGIN}, "Max length is " + FieldLength.ADMIN_LOGIN);
        }
        if (os.getRegistratorName() != null && os.getRegistratorName().length() > FieldLength.ADMIN_NAME) {
            errors.rejectValue("registratorName", "error.too-long-value",
                    new Object[]{FieldLength.ADMIN_NAME}, "Max length is " + FieldLength.ADMIN_NAME);
        }
        if (os.getRegistratorSurname() != null && os.getRegistratorSurname().length() > FieldLength.ADMIN_SURNAME) {
            errors.rejectValue("registratorSurname", "error.too-long-value",
                    new Object[]{FieldLength.ADMIN_SURNAME}, "Max length is " + FieldLength.ADMIN_SURNAME);
        }
    }
}