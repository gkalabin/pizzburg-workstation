package ru.pizzburg.web.repo.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import ru.pizzburg.web.domain.Product;
import ru.pizzburg.web.domain.ProductFlowStat;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class ProductFlowMapper implements ParameterizedRowMapper<ProductFlowStat> {

    @Override
    public ProductFlowStat mapRow(ResultSet rs, int rowNum) throws SQLException {
        ProductFlowStat product = new ProductFlowStat();
        product.setProduct(new Product());
        product.getProduct().setId(rs.getInt("productID"));
        product.getProduct().setTitle(rs.getString("title"));
        product.getProduct().setDescription(rs.getString("description"));
        product.setCount(rs.getInt("count"));
        return product;
    }
}