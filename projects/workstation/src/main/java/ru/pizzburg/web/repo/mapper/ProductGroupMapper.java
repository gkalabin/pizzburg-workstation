package ru.pizzburg.web.repo.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import ru.pizzburg.web.domain.ProductGroup;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class ProductGroupMapper implements ParameterizedRowMapper<ProductGroup> {

    @Override
    public ProductGroup mapRow(ResultSet rs, int rowNum) throws SQLException {
        ProductGroup group = new ProductGroup();
        group.setId(rs.getInt("groupID"));
        group.setTitle(rs.getString("title"));
        group.setDescription(rs.getString("description"));
        return group;
    }
}