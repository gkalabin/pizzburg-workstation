package ru.pizzburg.web.repo.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import ru.pizzburg.web.domain.Client;
import ru.pizzburg.web.domain.DiscountCard;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class ClientMapper implements ParameterizedRowMapper<Client> {

    @Override
    public Client mapRow(ResultSet rs, int rowNum) throws SQLException {
        Client client = new Client();
        client.setPhone(rs.getLong("phone"));
        client.setName(rs.getString("name"));
        client.setSurname(rs.getString("surname"));
        client.setMiddleName(rs.getString("middleName"));
        client.setDefaultAddress(rs.getString("defaultAddress"));
        client.setOrdersSum(rs.getFloat("ordersSum"));
        final long cardId = rs.getLong("discountCardNumber");
        if (cardId != 0) {
            DiscountCard card = new DiscountCard();
            card.setId(cardId);
            card.setEmissionDate(rs.getDate("emissionDate"));
            client.setCard(card);
        }
        return client;
    }
}