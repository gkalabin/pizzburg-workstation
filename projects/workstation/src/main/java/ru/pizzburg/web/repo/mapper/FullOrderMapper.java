package ru.pizzburg.web.repo.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import ru.pizzburg.web.domain.*;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class FullOrderMapper implements ParameterizedRowMapper<Order> {

    @Override
    public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
        Order order = new Order();
        order.setId(rs.getInt("orderID"));
        order.setRegisterDate(rs.getTimestamp("registerDate"));
        order.setShipmentAddress(rs.getString("shipmentAddress"));
        order.setPersonsCount(rs.getInt("personsCount"));
        order.setComment(rs.getString("comment"));
        if (rs.getInt("orderStatus") == Status.CANCELLED.getId()) {
            order.setCost(0);
        } else {
            order.setCost(rs.getFloat("cost"));
        }
        order.setDiscount(rs.getFloat("discount"));

        float discount = rs.getFloat("alternateDiscount");
        if (discount != 0) {
            order.setAlternateDiscount(discount);
        }
        long card = rs.getLong("alternateDiscountCard");
        if (card != 0) {
            DiscountCard dc = new DiscountCard();
            dc.setId(card);
            order.setAlternateDiscountCard(dc);
        }

        order.setClient(new Client());
        order.getClient().setPhone(rs.getLong("clientPhone"));

        order.setStatus(new Status());
        order.getStatus().setId(rs.getInt("orderStatus"));
        order.getStatus().setTitle(rs.getString("title"));

        order.setRegistrator(new WorkstationUser());
        order.getRegistrator().setId(rs.getInt("registrator"));
        order.getRegistrator().setName(rs.getString("registratorName"));
        order.getRegistrator().setSurname(rs.getString("registratorSurname"));
        order.getRegistrator().setLogin(rs.getString("registratorLogin"));

        order.setCarrier(new WorkstationUser());
        order.getCarrier().setId(rs.getInt("carrier"));
        order.getCarrier().setName(rs.getString("carrierName"));
        order.getCarrier().setSurname(rs.getString("carrierSurname"));
        return order;
    }
}
