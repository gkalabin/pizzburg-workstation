package ru.pizzburg.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.pizzburg.constants.SortStrings;
import ru.pizzburg.utils.Converter;
import ru.pizzburg.utils.Digest;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.web.domain.GroupInProduct;
import ru.pizzburg.web.domain.IngredientInProduct;
import ru.pizzburg.web.domain.Product;
import ru.pizzburg.web.service.groups.ProductGroupsManager;
import ru.pizzburg.web.service.ingredients.IngredientsManager;
import ru.pizzburg.web.service.products.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Controller
@SessionAttributes("formProduct")
@Lazy(false)
public class ProductsController {

    private static final String PRODUCTS_VIEW_NAME = "products";
    public static final String RESULT_COUNT_COOKIE_SUFFIX = "_products_resultCount";

    protected final Log logger = LogFactory.getLog(getClass());
    @Autowired
    private ProductsManager productsManager;
    @Autowired
    private IngredientsManager ingredientsManager;
    @Autowired
    private ProductGroupsManager groupsManager;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private FormProductValidator formProductValidator;

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public String getPage(HttpServletRequest request, ModelMap model, Principal principal) {
        FormProduct fp = new FormProduct();
        model.addAttribute("formProduct", fp);
        model.addAttribute("products", productsManager.getProducts(parsePagingParams(request, principal), parseSortingParams(request)));
        model.addAttribute("allIngredients", ingredientsManager.getIngredients());
        model.addAttribute("allGroups", groupsManager.getGroups());
        return PRODUCTS_VIEW_NAME;
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/products", method = RequestMethod.POST)
    public String addProduct(HttpServletRequest request, ModelMap model, @ModelAttribute("formProduct") FormProduct addProduct,
                             BindingResult result, SessionStatus status, RedirectAttributes redirectAttributes,
                             Principal principal) {
        formProductValidator.validate(addProduct, result);

        if (!result.hasErrors()) {
            //form success
            final Product product = new Product();
            product.setTitle(addProduct.getTitle());
            product.setDescription(addProduct.getDescription());
            product.setIngredients(addProduct.getIngredients());
            product.setGroups(addProduct.getGroups());

            String additionResult = productsManager.addProduct(product);
            if (additionResult != null) {
                model.addAttribute("errorMessage", messageSource.getMessage(additionResult, null, null));
                model.addAttribute("products", productsManager.getProducts(parsePagingParams(request, principal), parseSortingParams(request)));
                model.addAttribute("allIngredients", ingredientsManager.getIngredients());
                model.addAttribute("allGroups", groupsManager.getGroups());
                return PRODUCTS_VIEW_NAME;
            } else {
                redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.product-successfully-added", null, null));
                status.setComplete();
                return "redirect:/" + PRODUCTS_VIEW_NAME;
            }
        } else {
            model.addAttribute("products", productsManager.getProducts(parsePagingParams(request, principal), parseSortingParams(request)));
            model.addAttribute("allIngredients", ingredientsManager.getIngredients());
            model.addAttribute("allGroups", groupsManager.getGroups());
            return PRODUCTS_VIEW_NAME;
        }
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/products/edit/{id}", method = RequestMethod.POST)
    public String updateProduct(HttpServletRequest request, ModelMap model, @ModelAttribute("formProduct") FormProduct editProduct,
                                BindingResult result, SessionStatus status, RedirectAttributes redirectAttributes,
                                Principal principal) {
        formProductValidator.validate(editProduct, result);

        if (!result.hasErrors()) {
            //form success
            final Product product = new Product();
            product.setTitle(editProduct.getTitle());
            product.setDescription(editProduct.getDescription());
            product.setIngredients(editProduct.getIngredients());
            product.setGroups(editProduct.getGroups());
            product.setId(editProduct.getId());

            String updateResult = productsManager.updateProduct(product);
            if (updateResult != null) {
                model.addAttribute("errorMessage", messageSource.getMessage(updateResult, null, null));
                model.addAttribute("products", productsManager.getProducts(parsePagingParams(request, principal), parseSortingParams(request)));
                model.addAttribute("allIngredients", ingredientsManager.getIngredients());
                model.addAttribute("allGroups", groupsManager.getGroups());
                return PRODUCTS_VIEW_NAME;
            } else {
                redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.product-successfully-updated", null, null));
                status.setComplete();
                return "redirect:/" + PRODUCTS_VIEW_NAME;
            }
        } else {
            model.addAttribute("products", productsManager.getProducts(parsePagingParams(request, principal), parseSortingParams(request)));
            model.addAttribute("allIngredients", ingredientsManager.getIngredients());
            model.addAttribute("allGroups", groupsManager.getGroups());
            return PRODUCTS_VIEW_NAME;
        }
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/products/edit/{id}", method = RequestMethod.GET)
    public String getEditForm(HttpServletRequest request, ModelMap model, @PathVariable int id,
                              RedirectAttributes redirectAttributes, Principal principal) {
        Product p = productsManager.getProduct(id);
        if (p == null) {
            logger.warn("Null product request");
            redirectAttributes.addFlashAttribute("errorMessage", messageSource.getMessage("error.product-not-exist", null, null));
            return "redirect:/" + PRODUCTS_VIEW_NAME;
        }
        FormProduct fp = new FormProduct();
        fp.setId(p.getId());
        fp.setDescription(p.getDescription());
        fp.setTitle(p.getTitle());
        for (IngredientInProduct iip : p.getIngredients()) {
            IngredientFormEntry ife = new IngredientFormEntry();
            ife.setCount(String.valueOf(iip.getCount()));
            ife.setId(String.valueOf(iip.getIngredient().getId()));
            fp.getIngredientsStr().add(ife);
        }
        for (GroupInProduct gip : p.getGroups()) {
            GroupFormEntry gfe = new GroupFormEntry();
            gfe.setPrice(String.valueOf(gip.getPrice()));
            gfe.setId(String.valueOf(gip.getGroup().getId()));
            fp.getGroupsStr().add(gfe);
        }
        model.addAttribute("formProduct", fp);
        model.addAttribute("products", productsManager.getProducts(parsePagingParams(request, principal), parseSortingParams(request)));
        model.addAttribute("allIngredients", ingredientsManager.getIngredients());
        model.addAttribute("allGroups", groupsManager.getGroups());
        return PRODUCTS_VIEW_NAME;
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/products/remove/{id}", method = RequestMethod.GET)
    public String removeProduct(HttpServletRequest request, ModelMap model, @PathVariable int id,
                                RedirectAttributes redirectAttributes, Principal principal) {
        if (model.get("formProduct") == null) {
            model.addAttribute("formProduct", new FormProduct());
        }

        String removeResult = productsManager.removeProduct(id);
        if (removeResult != null) {
            model.addAttribute("errorMessage", messageSource.getMessage(removeResult, null, null));
            model.addAttribute("products", productsManager.getProducts(parsePagingParams(request, principal), parseSortingParams(request)));
            model.addAttribute("allIngredients", ingredientsManager.getIngredients());
            model.addAttribute("allGroups", groupsManager.getGroups());
            return PRODUCTS_VIEW_NAME;
        } else {
            redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.product-successfully-removed", null, null));
            return "redirect:/" + PRODUCTS_VIEW_NAME;
        }
    }

    /**
     * <p>Called when the user clicks the add button</p>
     */
    @Secured({"ROLE_ADMIN"})
    @RequestMapping(method = RequestMethod.GET, value = "/products/addIngredient")
    protected String appendIngredientField(@RequestParam Integer fieldId, ModelMap model, @ModelAttribute("formProduct") FormProduct addProduct) {
        model.addAttribute("ingredientNumber", fieldId);
        model.addAttribute("allIngredients", ingredientsManager.getIngredients());

        return "addIngredientToProduct";
    }

    /**
     * <p>Called when the user clicks the delete button</p>
     */
    @Secured({"ROLE_ADMIN"})
    @RequestMapping(method = RequestMethod.GET, value = "/products/deleteIngredient")
    protected String removeIngredientField(@RequestParam Integer fieldId, @ModelAttribute("formProduct") FormProduct addProduct) {
        addProduct.getDeletedIngredients().add(fieldId);
        // TODO: not elegant
        return "empty";
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(method = RequestMethod.GET, value = "/products/addGroup")
    protected String appendGroupField(@RequestParam Integer fieldId, ModelMap model) {
        model.addAttribute("groupNumber", fieldId);
        model.addAttribute("allGroups", groupsManager.getGroups());
        return "addGroupToProduct";
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(method = RequestMethod.GET, value = "/products/deleteGroup")
    protected String removeGroupField(@RequestParam Integer fieldId, @ModelAttribute("formProduct") FormProduct product) {
        product.getDeletedGroups().add(fieldId);
        // TODO: not elegant
        return "empty";
    }

    private SortingParams parseSortingParams(HttpServletRequest request) {
        String sort = request.getParameter("sort");
        String dir = request.getParameter("dir");
        if (sort == null || dir == null || !SortStrings.isDirectionCorrect(dir)
                || !SortStrings.isProductCorrect(sort)) {
            return null;
        }
        return new SortingParams(sort, Converter.parseSortOrder(dir));
    }

    private PaginationParams parsePagingParams(HttpServletRequest request, Principal principal) {
        String pageStr = request.getParameter("page");
        String resultsStr = request.getParameter("results");
        String cookieName = Digest.getSha1Hash(principal.getName() + RESULT_COUNT_COOKIE_SUFFIX);
        for (Cookie c : request.getCookies()) {
            if (cookieName.equals(c.getName())) {
                resultsStr = c.getValue();
                break;
            }
        }
        if (pageStr == null && resultsStr == null) {
            return null;
        }
        int page = -1;
        if (pageStr != null) {
            try {
                page = Integer.parseInt(pageStr);
            } catch (NumberFormatException ex) {
                logger.info("invalid paging params (products page): " + pageStr);
            }
        }
        int results = -1;
        if (resultsStr != null) {
            try {
                results = Integer.parseInt(resultsStr);
            } catch (NumberFormatException ex) {
                logger.info("invalid paging params (products page): " + pageStr);
            }
        }
        return new PaginationParams(page, results);
    }
}
