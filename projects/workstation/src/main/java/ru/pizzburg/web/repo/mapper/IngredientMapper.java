package ru.pizzburg.web.repo.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import ru.pizzburg.web.domain.Ingredient;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class IngredientMapper implements ParameterizedRowMapper<Ingredient> {

    @Override
    public Ingredient mapRow(ResultSet rs, int rowNum) throws SQLException {
        Ingredient ingredient = new Ingredient();
        ingredient.setId(rs.getInt("IngredientID"));
        ingredient.setTitle(rs.getString("title"));
        ingredient.setPrice(rs.getFloat("price"));
        ingredient.setDescription(rs.getString("description"));
        ingredient.setUnits(rs.getString("units"));
        ingredient.setBalance(rs.getFloat("balance"));
        return ingredient;
    }
}