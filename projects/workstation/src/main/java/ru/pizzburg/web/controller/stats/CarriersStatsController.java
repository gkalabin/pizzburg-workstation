package ru.pizzburg.web.controller.stats;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import ru.pizzburg.constants.SortStrings;
import ru.pizzburg.utils.Converter;
import ru.pizzburg.utils.Digest;
import ru.pizzburg.utils.NameUtils;
import ru.pizzburg.utils.data.PageableList;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.web.domain.Order;
import ru.pizzburg.web.domain.Role;
import ru.pizzburg.web.domain.WorkstationUser;
import ru.pizzburg.web.service.admin.AdminManager;
import ru.pizzburg.web.service.stats.FormEmployeeStats;
import ru.pizzburg.web.service.stats.FormEmployeeStatsValidator;
import ru.pizzburg.web.service.stats.StatsManager;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Date;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Controller
@SessionAttributes({"formCarriersStats"})
@Lazy(false)
public class CarriersStatsController {

    private static final String CARRIERS_VIEW_NAME = "stats-carriers";
    public static final String RESULT_COUNT_COOKIE_SUFFIX = "_stats_carriers_resultCount";
    protected final Log logger = LogFactory.getLog(getClass());
    @Autowired
    private StatsManager statsManager;
    @Autowired
    private AdminManager adminManager;
    @Autowired
    private FormEmployeeStatsValidator formEmployeeStatsValidator;

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/stats/carriers", method = RequestMethod.GET)
    public String getCarriersStats(ModelMap model, HttpServletRequest request, Principal principal) {
        FormEmployeeStats formCarriersStats = (FormEmployeeStats) model.get("formCarriersStats");
        if (formCarriersStats == null) {
            formCarriersStats = new FormEmployeeStats();
        }

        model.addAttribute("formCarriersStats", formCarriersStats);
        model.addAttribute("carriers", adminManager.getCarriers());

        Date fromDate = formCarriersStats.getFromDate();
        Date toDate = formCarriersStats.getToDate();
        int carrierId = formCarriersStats.getEmployeeId();
        if (fromDate != null && toDate != null &&
                carrierId > 0) {
            WorkstationUser carrier = adminManager.getUser(formCarriersStats.getEmployeeId());

            if (carrier != null && carrier.getRole() != null
                    && carrier.getRole().getId() == Role.ROLE_CARRIER.getId()) {
                model.addAttribute("from", ru.pizzburg.utils.DateUtils.OUT_DATE_TIME_FORMAT.format(fromDate));
                model.addAttribute("to", ru.pizzburg.utils.DateUtils.OUT_DATE_TIME_FORMAT.format(toDate));
                model.addAttribute("carrier", NameUtils.getFullNameFormatted(carrier));
                model.addAttribute("shipped", statsManager.getCarrierShippedOrdersCount(fromDate, toDate, carrier));
                model.addAttribute("paid", statsManager.getCarrierPaidOrdersCount(fromDate, toDate, carrier));
                model.addAttribute("cancelled", statsManager.getCarrierCancelledOrdersCount(fromDate, toDate, carrier));
                PageableList<Order> carrierStats = statsManager.getCarrierStats(fromDate, toDate, carrier,
                        parsePagingParams(request, principal), parseOperatorsSortingParams(request));
                model.addAttribute("stats", carrierStats);
            }
        }

        return CARRIERS_VIEW_NAME;
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/stats/carriers/**", method = RequestMethod.POST)
    public String getOperatorsCustomStats(ModelMap model, HttpServletRequest request,
                                          @ModelAttribute("formCarriersStats") FormEmployeeStats formCarriersStats,
                                          BindingResult result, Principal principal) {
        formEmployeeStatsValidator.validate(formCarriersStats, result);
        model.addAttribute("carriers", adminManager.getCarriers());

        if (!result.hasErrors()) {
            Date fromDate = formCarriersStats.getFromDate();
            Date toDate = formCarriersStats.getToDate();
            int carrierId = formCarriersStats.getEmployeeId();
            if (fromDate != null && toDate != null &&
                    carrierId > 0) {
                WorkstationUser carrier = adminManager.getUser(formCarriersStats.getEmployeeId());

                if (carrier != null && carrier.getRole() != null
                        && carrier.getRole().getId() == Role.ROLE_CARRIER.getId()) {
                    model.addAttribute("from", ru.pizzburg.utils.DateUtils.OUT_DATE_TIME_FORMAT.format(fromDate));
                    model.addAttribute("to", ru.pizzburg.utils.DateUtils.OUT_DATE_TIME_FORMAT.format(toDate));
                    model.addAttribute("carrier", NameUtils.getFullNameFormatted(carrier));
                    model.addAttribute("shipped", statsManager.getCarrierShippedOrdersCount(fromDate, toDate, carrier));
                    model.addAttribute("paid", statsManager.getCarrierPaidOrdersCount(fromDate, toDate, carrier));
                    model.addAttribute("cancelled", statsManager.getCarrierCancelledOrdersCount(fromDate, toDate, carrier));
                    PageableList<Order> carrierStats = statsManager.getCarrierStats(fromDate, toDate, carrier,
                            parsePagingParams(request, principal), parseOperatorsSortingParams(request));
                    model.addAttribute("stats", carrierStats);
                }
            }
            return CARRIERS_VIEW_NAME;
        } else {
            return CARRIERS_VIEW_NAME;
        }
    }

    private SortingParams parseOperatorsSortingParams(HttpServletRequest request) {
        String sort = request.getParameter("sort");
        String dir = request.getParameter("dir");
        if (sort == null || dir == null || !SortStrings.isDirectionCorrect(dir)
                || !SortStrings.isStatsCarrierCorrect(sort)) {
            return null;
        }
        return new SortingParams(sort, Converter.parseSortOrder(dir));
    }

    private PaginationParams parsePagingParams(HttpServletRequest request, Principal principal) {
        String pageStr = request.getParameter("page");
        String resultsStr = request.getParameter("results");
        String cookieName = Digest.getSha1Hash(principal.getName() + RESULT_COUNT_COOKIE_SUFFIX);
        for (Cookie c : request.getCookies()) {
            if (cookieName.equals(c.getName())) {
                resultsStr = c.getValue();
                break;
            }
        }
        if (pageStr == null && resultsStr == null) {
            return null;
        }
        int page = -1;
        if (pageStr != null) {
            try {
                page = Integer.parseInt(pageStr);
            } catch (NumberFormatException ex) {
                logger.info("invalid paging params (carriers stats): " + pageStr);
            }
        }
        int results = -1;
        if (resultsStr != null) {
            try {
                results = Integer.parseInt(resultsStr);
            } catch (NumberFormatException ex) {
                logger.info("invalid paging params (carriers stats): " + pageStr);
            }
        }
        return new PaginationParams(page, results);
    }
}