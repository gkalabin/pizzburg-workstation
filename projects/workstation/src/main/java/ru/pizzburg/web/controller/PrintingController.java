package ru.pizzburg.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.pizzburg.web.domain.Order;
import ru.pizzburg.web.service.orders.OrdersManager;


/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Controller
@Lazy(false)
public class PrintingController {
    private static final String PRINT_ORDERS_VIEW_NAME = "orders-printable";

    @Autowired
    private OrdersManager ordersManager;
    @Autowired
    private MessageSource messageSource;

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/orders/print/{id}", method = RequestMethod.GET)
    public String updateOrder(ModelMap model, @PathVariable int id) {
        Order order = ordersManager.getOrder(id);
        model.put("order", order);
        if (order == null) {
            return PRINT_ORDERS_VIEW_NAME;
        }
        if (order.getAlternateDiscountCard() != null) {
            model.put("discountReason",
                    String.format(messageSource.getMessage("info.reason-alternate-card", null, null),
                            order.getAlternateDiscountCard().getIdFormatted()));
        } else if (order.getAlternateDiscount() > 0) {
            model.put("discountReason",messageSource.getMessage("info.reason-custom-discount", null, null));
        } else if (order.getClient().getCard() != null) {
            model.put("discountReason",messageSource.getMessage("info.reason-client-card", null, null));
        } else {
            model.put("discountReason",messageSource.getMessage("info.no-reason", null, null));
        }
        return PRINT_ORDERS_VIEW_NAME;
    }
}
