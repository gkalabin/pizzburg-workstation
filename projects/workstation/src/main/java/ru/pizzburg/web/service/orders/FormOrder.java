package ru.pizzburg.web.service.orders;

import org.springframework.util.AutoPopulatingList;
import ru.pizzburg.web.domain.DiscountCard;
import ru.pizzburg.web.domain.ProductInOrder;
import ru.pizzburg.web.domain.Status;
import ru.pizzburg.web.domain.WorkstationUser;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class FormOrder {

    private long id = -1;
    private String clientName = "";
    private String clientPhoneStr;
    private String shipmentAddress;
    private String carrierIdStr;
    private WorkstationUser carrier;
    private String alternateDiscount;
    private float alternateDiscountFloat = -1;
    private String alternateDiscountCard;
    private DiscountCard alternateDiscountCardObject;
    private String statusIdStr;
    private Status status;
    private String comment;
    private String personsCountStr = "1";
    private int personsCount;
    private AutoPopulatingList<ProductFormEntry> productsStr = new AutoPopulatingList<ProductFormEntry>(ProductFormEntry.class);
    private List<ProductInOrder> products = new ArrayList<ProductInOrder>();
    private List<Integer> deletedProducts = new ArrayList<Integer>();

    public String getClientPhoneStr() {
        return clientPhoneStr;
    }

    public void setClientPhoneStr(String clientPhoneStr) {
        this.clientPhoneStr = clientPhoneStr;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getShipmentAddress() {
        return shipmentAddress;
    }

    public void setShipmentAddress(String shipmentAddress) {
        this.shipmentAddress = shipmentAddress;
    }

    public String getStatusIdStr() {
        return statusIdStr;
    }

    public void setStatusIdStr(String statusIdStr) {
        this.statusIdStr = statusIdStr;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<Integer> getDeletedProducts() {
        return deletedProducts;
    }

    public void setDeletedProducts(List<Integer> deletedProducts) {
        this.deletedProducts = deletedProducts;
    }

    public List<ProductInOrder> getProducts() {
        return products;
    }

    public void setProducts(List<ProductInOrder> products) {
        this.products = products;
    }

    public AutoPopulatingList<ProductFormEntry> getProductsStr() {
        return productsStr;
    }

    public void setProductsStr(AutoPopulatingList<ProductFormEntry> productsStr) {
        this.productsStr = productsStr;
    }

    public String getCarrierIdStr() {
        return carrierIdStr;
    }

    public void setCarrierIdStr(String carrierIdStr) {
        this.carrierIdStr = carrierIdStr;
    }

    public String getAlternateDiscount() {
        return alternateDiscount;
    }

    public void setAlternateDiscount(String alternateDiscount) {
        this.alternateDiscount = alternateDiscount;
    }

    public String getAlternateDiscountCard() {
        return alternateDiscountCard;
    }

    public void setAlternateDiscountCard(String alternateDiscountCard) {
        this.alternateDiscountCard = alternateDiscountCard;
    }


    public WorkstationUser getCarrier() {
        return carrier;
    }

    public void setCarrier(WorkstationUser carrier) {
        this.carrier = carrier;
    }

    public float getAlternateDiscountFloat() {
        return alternateDiscountFloat;
    }

    public void setAlternateDiscountFloat(float alternateDiscountFloat) {
        this.alternateDiscountFloat = alternateDiscountFloat;
    }

    public DiscountCard getAlternateDiscountCardObject() {
        return alternateDiscountCardObject;
    }

    public void setAlternateDiscountCardObject(DiscountCard alternateDiscountCardObject) {
        this.alternateDiscountCardObject = alternateDiscountCardObject;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPersonsCountStr() {
        return personsCountStr;
    }

    public void setPersonsCountStr(String personsCountStr) {
        this.personsCountStr = personsCountStr;
    }

    public int getPersonsCount() {
        return personsCount;
    }

    public void setPersonsCount(int personsCount) {
        this.personsCount = personsCount;
    }

    @Override
    public String toString() {
        return "FormOrder{" +
                "id=" + id +
                ", clientName='" + clientName + '\'' +
                ", clientPhoneStr='" + clientPhoneStr + '\'' +
                ", shipmentAddress='" + shipmentAddress + '\'' +
                ", carrierIdStr='" + carrierIdStr + '\'' +
                ", carrier=" + carrier +
                ", alternateDiscount='" + alternateDiscount + '\'' +
                ", alternateDiscountFloat=" + alternateDiscountFloat +
                ", alternateDiscountCard='" + alternateDiscountCard + '\'' +
                ", alternateDiscountCardObject=" + alternateDiscountCardObject +
                ", statusIdStr='" + statusIdStr + '\'' +
                ", status=" + status +
                ", comment='" + comment + '\'' +
                ", personsCountStr='" + personsCountStr + '\'' +
                ", personsCount=" + personsCount +
                ", productsStr=" + productsStr +
                ", products=" + products +
                ", deletedProducts=" + deletedProducts +
                '}';
    }
}
