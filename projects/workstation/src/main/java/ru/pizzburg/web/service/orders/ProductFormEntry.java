package ru.pizzburg.web.service.orders;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class ProductFormEntry {

    private String productId = "";
    private String groupId = "";
    private String count = "1";

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @Override
    public String toString() {
        return "ProductFormEntry{" + "id=" + productId + ", groupId=" + groupId + ", count=" + count + '}';
    }
}
