package ru.pizzburg.web.repo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.stereotype.Repository;
import ru.pizzburg.constants.DatabaseErrors;
import ru.pizzburg.web.domain.DiscountEntity;
import ru.pizzburg.web.repo.mapper.DiscountEntryMapper;

import javax.sql.DataSource;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Repository
@Lazy(false)
public class DiscountDao extends JdbcTemplate {
    protected final Log logger = LogFactory.getLog(getClass());

    @Override
    @Autowired
    public void setDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    public List<DiscountEntity> getDiscounts() {
        return query("SELECT * FROM pizzburgdb.DiscountValues ORDER BY sumFrom ASC",
                new ResultSetExtractor<List<DiscountEntity>>() {

                    @Override
                    public List<DiscountEntity> extractData(ResultSet rs) throws SQLException, DataAccessException {
                        List<DiscountEntity> result = new LinkedList<DiscountEntity>();
                        DiscountEntity d1 = new DiscountEntity(), d2 = new DiscountEntity();
                        while (rs.next()) {
                            d2.setSumFrom(rs.getInt("sumFrom"));
                            d2.setDiscount(rs.getFloat("discount"));
                            d1.setSumTo(d2.getSumFrom());
                            result.add(d1);
                            d1 = d2;
                            d2 = new DiscountEntity();
                        }
                        d1.setSumTo(Integer.MAX_VALUE);
                        result.add(d1);
                        return result;
                    }
                });
    }

    public String addDiscount(DiscountEntity d) {
        if (queryForInt("SELECT COUNT(*) FROM pizzburgdb.DiscountValues WHERE sumFrom=?", d.getSumFrom()) != 0) {
            return DatabaseErrors.DISCOUNT_EXISTS;
        }
        try {
            update("INSERT INTO pizzburgdb.DiscountValues (sumFrom, discount) VALUES (?,?)", d.getSumFrom(), d.getDiscount());
        } catch (DataAccessException ex) {
            logger.error("add discount fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return null;
    }

    public DiscountEntity getDiscount(int sum) {
        List<DiscountEntity> result = query("SELECT * FROM pizzburgdb.DiscountValues WHERE sumFrom=?", new Object[]{sum}, new DiscountEntryMapper());
        if (result.isEmpty()) {
            logger.warn("Access to discount which doesn't exist");
            return null;
        } else if (result.size() != 1) {
            logger.warn("database schema error: Not unique result from query. Get " + result.size() + " discounts");
            return result.get(0);
        } else {
            return result.get(0);
        }
    }

    public String updateDiscount(DiscountEntity d, int initialSum) {
        if (initialSum != d.getSumFrom()) {
            if (queryForInt("SELECT COUNT(*) FROM pizzburgdb.DiscountValues WHERE sumFrom=?", d.getSumFrom()) != 0) {
                return DatabaseErrors.DISCOUNT_EXISTS;
            }
        }
        int result = 0;
        try {
            result = update("UPDATE pizzburgdb.DiscountValues SET sumFrom=?, discount=? WHERE sumFrom=?",
                    d.getSumFrom(), d.getDiscount(), initialSum);
        } catch (DataAccessException ex) {
            logger.error("update discount fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return result > 0?null:DatabaseErrors.DISCOUNT_NOT_EXISTS;
    }

    public String removeDiscount(int sum) {
        int result = 0;
        try {
            result = update("DELETE FROM pizzburgdb.DiscountValues WHERE sumFrom=?", sum);
        } catch (DataAccessException ex) {
            logger.error("delete discount fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return result == 1 ? null : DatabaseErrors.DISCOUNT_NOT_EXISTS;
    }

    public float getSumForCard(long id) {
        int exists = queryForInt("SELECT COUNT(*) FROM DiscountCards WHERE cardNumber=?",id);
        if (exists == 0) {
            return 0;
        }
        List<Float> result = query("SELECT ordersSum FROM pizzburgdb.Clients WHERE discountCardNumber=?", new Object[]{id}, new SingleColumnRowMapper<Float>());
        if (result.isEmpty()) {
            logger.warn("Access to sum which doesn't exist for card="+id);
            return 0;
        } else if (result.size() != 1) {
            logger.warn("database schema error: Not unique result from query. Get " + result.size() + " sums for card="+id);
            return result.get(0);
        } else {
            return result.get(0);
        }
    }
}
