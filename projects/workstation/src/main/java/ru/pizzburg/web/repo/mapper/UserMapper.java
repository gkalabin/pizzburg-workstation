package ru.pizzburg.web.repo.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import ru.pizzburg.web.domain.Role;
import ru.pizzburg.web.domain.WorkstationUser;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class UserMapper implements ParameterizedRowMapper<WorkstationUser> {

    @Override
    public WorkstationUser mapRow(ResultSet rs, int rowNum) throws SQLException {
        WorkstationUser user = new WorkstationUser();
        user.setId(rs.getInt("userID"));
        user.setName(rs.getString("name"));
        user.setSurname(rs.getString("surname"));
        user.setMiddleName(rs.getString("middleName"));
        user.setLogin(rs.getString("login"));
        user.setRole(new Role());
        user.getRole().setId(rs.getInt("roleID"));
        return user;
    }
}