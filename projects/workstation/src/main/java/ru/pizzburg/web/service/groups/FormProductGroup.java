package ru.pizzburg.web.service.groups;

import java.util.ArrayList;
import java.util.List;
import org.springframework.util.AutoPopulatingList;
import ru.pizzburg.web.domain.ProductInGroup;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class FormProductGroup {

    private int id;
    private String title;
    private String description;
    private AutoPopulatingList<ProductFormEntry> productsStr = new AutoPopulatingList<ProductFormEntry>(ProductFormEntry.class);
    private List<ProductInGroup> products = new ArrayList<ProductInGroup>();
    private List<Integer> deletedProducts = new ArrayList<Integer>();

    public List<Integer> getDeletedProducts() {
        return deletedProducts;
    }

    public void setDeletedProducts(List<Integer> deletedProducts) {
        this.deletedProducts = deletedProducts;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<ProductInGroup> getProducts() {
        return products;
    }

    public void setProducts(List<ProductInGroup> products) {
        this.products = products;
    }

    public AutoPopulatingList<ProductFormEntry> getProductsStr() {
        return productsStr;
    }

    public void setProductsStr(AutoPopulatingList<ProductFormEntry> productsStr) {
        this.productsStr = productsStr;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "FormProductGroup{" + "id=" + id + ", title=" + title + ", description=" + description + ", productsStr=" + productsStr + ", products=" + products + ", deletedProducts=" + deletedProducts + '}';
    }
}
