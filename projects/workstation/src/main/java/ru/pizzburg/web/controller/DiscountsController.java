package ru.pizzburg.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.pizzburg.web.domain.DiscountEntity;
import ru.pizzburg.web.service.discount.DiscountManager;
import ru.pizzburg.web.service.discount.FormDiscount;
import ru.pizzburg.web.service.discount.FormDiscountValidator;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Controller
@Lazy(false)
public class DiscountsController {

    private static final String DISCOUNTS_VIEW_NAME = "discounts";
    protected final Log logger = LogFactory.getLog(getClass());
    @Autowired
    private DiscountManager discountManager;
    @Autowired
    private FormDiscountValidator formDiscountValidator;
    @Autowired
    private MessageSource messageSource;

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/discounts", method = RequestMethod.GET)
    public String getPage(ModelMap model) {
        FormDiscount fd = new FormDiscount();
        model.addAttribute("formDiscount", fd);
        model.addAttribute("discounts", discountManager.getDiscounts());
        return DISCOUNTS_VIEW_NAME;
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/discounts", method = RequestMethod.POST)
    public String addDiscount(ModelMap model, @ModelAttribute("formDiscount") FormDiscount discount,
                              BindingResult result, RedirectAttributes redirectAttributes) {
        formDiscountValidator.validate(discount, result);

        if (!result.hasErrors()) {
            //form success
            DiscountEntity entry = new DiscountEntity();
            entry.setSumFrom(discount.getSumFrom());
            entry.setDiscount(discount.getDiscount());

            String additionResult = discountManager.addDiscount(entry);
            if (additionResult != null) {
                model.addAttribute("errorMessage", messageSource.getMessage(additionResult, null, null));
                model.addAttribute("discounts", discountManager.getDiscounts());
                return DISCOUNTS_VIEW_NAME;
            } else {
                redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.discount-successfully-added", null, null));
                return "redirect:/discounts/";
            }
        } else {
            model.addAttribute("discounts", discountManager.getDiscounts());
            return DISCOUNTS_VIEW_NAME;
        }
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/discounts/edit/{sum}", method = RequestMethod.GET)
    public String getEditForm(ModelMap model, @PathVariable int sum, RedirectAttributes redirectAttributes) {
        DiscountEntity entry = discountManager.getDiscount(sum);
        if (entry == null) {
            logger.warn("Null discount request");
            redirectAttributes.addFlashAttribute("errorMessage", messageSource.getMessage("error.discount-not-exist", null, null));
            return "redirect:/discounts/";
        }
        FormDiscount form = new FormDiscount();
        form.setSumFromStr(String.valueOf(entry.getSumFrom()));
        form.setDiscountStr(String.valueOf(entry.getDiscount()));
        model.addAttribute("formDiscount", form);
        model.addAttribute("discounts", discountManager.getDiscounts());
        return DISCOUNTS_VIEW_NAME;
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/discounts/edit/{sum}", method = RequestMethod.POST)
    public String editDiscount(ModelMap model, @PathVariable int sum, @ModelAttribute("formDiscount") FormDiscount form,
                               BindingResult result, RedirectAttributes redirectAttributes) {
        formDiscountValidator.validate(form, result);

        if (!result.hasErrors()) {
            //form success
            DiscountEntity entry = new DiscountEntity();
            entry.setSumFrom(form.getSumFrom());
            entry.setDiscount(form.getDiscount());

            String updateResult = discountManager.updateDiscount(entry, sum);
            if (updateResult != null) {
                model.addAttribute("errorMessage", messageSource.getMessage(updateResult, null, null));
                model.addAttribute("discounts", discountManager.getDiscounts());
                return DISCOUNTS_VIEW_NAME;
            } else {
                redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.discount-successfully-updated", null, null));
                return "redirect:/discounts/";
            }
        } else {
            model.addAttribute("discounts", discountManager.getDiscounts());
            return DISCOUNTS_VIEW_NAME;
        }
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/discounts/remove/{sum}", method = RequestMethod.GET)
    public String removeDiscount(ModelMap model, @PathVariable int sum, RedirectAttributes redirectAttributes) {
        if (model.get("formDiscount") == null) {
            model.addAttribute("formDiscount", new FormDiscount());
        }

        String removeResult = discountManager.removeDiscount(sum);
        if (removeResult != null) {
            model.addAttribute("errorMessage", messageSource.getMessage(removeResult, null, null));
            model.addAttribute("discounts", discountManager.getDiscounts());
            return DISCOUNTS_VIEW_NAME;
        } else {
            redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.discount-successfully-removed", null, null));
            return "redirect:/discounts/";
        }
    }
}
