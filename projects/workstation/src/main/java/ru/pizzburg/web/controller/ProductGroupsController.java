package ru.pizzburg.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.pizzburg.web.domain.ProductGroup;
import ru.pizzburg.web.domain.ProductInGroup;
import ru.pizzburg.web.service.groups.FormProductGroup;
import ru.pizzburg.web.service.groups.FormProductGroupValidator;
import ru.pizzburg.web.service.groups.ProductFormEntry;
import ru.pizzburg.web.service.groups.ProductGroupsManager;
import ru.pizzburg.web.service.products.ProductsManager;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Controller
@SessionAttributes("formProductGroup")
@Lazy(false)
public class ProductGroupsController {

    private static final String PRODUCT_GROUPS_VIEW_NAME = "productGroups";
    protected final Log logger = LogFactory.getLog(getClass());
    @Autowired
    private ProductsManager productsManager;
    @Autowired
    private ProductGroupsManager groupsManager;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private FormProductGroupValidator formProductGroupValidator;

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/groups", method = RequestMethod.GET)
    public String getPage(ModelMap model) {
        FormProductGroup fpg = new FormProductGroup();
        model.addAttribute("formProductGroup", fpg);
        model.addAttribute("groups", groupsManager.getGroups());
        model.addAttribute("allProducts", productsManager.getProducts());
        return PRODUCT_GROUPS_VIEW_NAME;
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/groups", method = RequestMethod.POST)
    public String addGroup(ModelMap model, @ModelAttribute("formProductGroup") FormProductGroup group,
                           BindingResult result, SessionStatus status, RedirectAttributes redirectAttributes) {
        formProductGroupValidator.validate(group, result);

        if (!result.hasErrors()) {
            //form success
            ProductGroup g = new ProductGroup();
            g.setTitle(group.getTitle());
            g.setDescription(group.getDescription());
            g.setProducts(group.getProducts());

            String additionResult = groupsManager.addGroup(g);
            if (additionResult != null) {
                model.addAttribute("errorMessage", messageSource.getMessage(additionResult, null, null));
                model.addAttribute("groups", groupsManager.getGroups());
                model.addAttribute("allProducts", productsManager.getProducts());
                return PRODUCT_GROUPS_VIEW_NAME;
            } else {
                redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.group-successfully-added", null, null));
                status.setComplete();
                return "redirect:/groups/";
            }
        } else {
            model.addAttribute("groups", groupsManager.getGroups());
            model.addAttribute("allProducts", productsManager.getProducts());
            return PRODUCT_GROUPS_VIEW_NAME;
        }
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/groups/edit/{id}", method = RequestMethod.POST)
    public String updateGroup(ModelMap model, @ModelAttribute("formProductGroup") FormProductGroup group,
                              BindingResult result, SessionStatus status, RedirectAttributes redirectAttributes) {
        formProductGroupValidator.validate(group, result);

        if (!result.hasErrors()) {
            //form success
            ProductGroup g = new ProductGroup();
            g.setTitle(group.getTitle());
            g.setDescription(group.getDescription());
            g.setProducts(group.getProducts());
            g.setId(group.getId());

            String updateResult = groupsManager.updateGroup(g);
            if (updateResult != null) {
                model.addAttribute("errorMessage", messageSource.getMessage(updateResult, null, null));
                model.addAttribute("groups", groupsManager.getGroups());
                model.addAttribute("allProducts", productsManager.getProducts());
                return PRODUCT_GROUPS_VIEW_NAME;
            } else {
                redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.group-successfully-updated", null, null));
                status.setComplete();
                return "redirect:/groups/";
            }
        } else {
            model.addAttribute("groups", groupsManager.getGroups());
            model.addAttribute("allProducts", productsManager.getProducts());
            return PRODUCT_GROUPS_VIEW_NAME;
        }
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/groups/edit/{id}", method = RequestMethod.GET)
    public String getEditForm(ModelMap model, @PathVariable int id, RedirectAttributes redirectAttributes) {
        ProductGroup g = groupsManager.getGroup(id);
        if (g == null) {
            logger.warn("Null group request");
            redirectAttributes.addFlashAttribute("errorMessage", messageSource.getMessage("error.group-not-exist", null, null));
            return "redirect:/groups/";
        }
        FormProductGroup fpg = new FormProductGroup();
        fpg.setId(g.getId());
        fpg.setDescription(g.getDescription());
        fpg.setTitle(g.getTitle());
        for (ProductInGroup pig : g.getProducts()) {
            ProductFormEntry pfe = new ProductFormEntry();
            pfe.setId(String.valueOf(pig.getProduct().getId()));
            pfe.setPrice(String.valueOf(pig.getPrice()));
            fpg.getProductsStr().add(pfe);
        }
        model.addAttribute("formProductGroup", fpg);
        model.addAttribute("groups", groupsManager.getGroups());
        model.addAttribute("allProducts", productsManager.getProducts());
        return PRODUCT_GROUPS_VIEW_NAME;
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/groups/remove/{id}", method = RequestMethod.GET)
    public String removeGroup(ModelMap model, @PathVariable int id, RedirectAttributes redirectAttributes) {
        if (model.get("formProductGroup") == null) {
            model.addAttribute("formProductGroup", new FormProductGroup());
        }

        String removeResult = groupsManager.removeGroup(id);
        if (removeResult != null) {
            model.addAttribute("errorMessage", messageSource.getMessage(removeResult, null, null));
            model.addAttribute("groups", groupsManager.getGroups());
            model.addAttribute("allProducts", productsManager.getProducts());
            return PRODUCT_GROUPS_VIEW_NAME;
        } else {
            redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.group-successfully-removed", null, null));
            return "redirect:/groups/";
        }
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(method = RequestMethod.GET, value = "/groups/addProduct")
    protected String appendProductField(@RequestParam Integer fieldId, ModelMap model) {
        model.addAttribute("productNumber", fieldId);
        model.addAttribute("allProducts", productsManager.getProducts());

        return "addProductToGroup";
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(method = RequestMethod.GET, value = "/groups/deleteProduct")
    protected String removeProductField(@RequestParam Integer fieldId, @ModelAttribute("formProductGroup") FormProductGroup group) {
        group.getDeletedProducts().add(fieldId);
        // TODO: not elegant
        return "empty";
    }
}
