package ru.pizzburg.web.controller.stats;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.pizzburg.web.service.FormDateTimeRange;
import ru.pizzburg.web.service.FormDateTimeRangeValidator;
import ru.pizzburg.web.service.stats.StatsManager;

import java.util.Date;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Controller
@Lazy(false)
public class StatsController {

    private static final String STATISTICS_VIEW_NAME = "stats";
    protected final Log logger = LogFactory.getLog(getClass());
    @Autowired
    private StatsManager statsManager;
    @Autowired
    private FormDateTimeRangeValidator formDateTimeRangeValidator;

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/stats", method = RequestMethod.GET)
    public String getStatsPage(ModelMap model) {
        FormDateTimeRange formDateTime = new FormDateTimeRange();
        model.addAttribute("formDate", formDateTime);
        return statsHandler(model, ru.pizzburg.utils.DateUtils.getDayStart(), new Date());
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/stats/**", method = RequestMethod.POST)
    public String getStatsCustom(ModelMap model, @ModelAttribute("formDate") FormDateTimeRange formDateTime,
                                 BindingResult result) {
        formDateTimeRangeValidator.validate(formDateTime, result);
        if (!result.hasErrors()) {
            return statsHandler(model, formDateTime.getFromDate(), formDateTime.getToDate());
        } else {
            return statsHandler(model, ru.pizzburg.utils.DateUtils.getDayStart(), new Date());
        }
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/stats/hour", method = RequestMethod.GET)
    public String getStatsHour(ModelMap model) {
        FormDateTimeRange formDateTime = new FormDateTimeRange();
        model.addAttribute("formDate", formDateTime);
        return statsHandler(model, ru.pizzburg.utils.DateUtils.getHourStart(), new Date());
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/stats/day", method = RequestMethod.GET)
    public String getStatsDay(ModelMap model) {
        FormDateTimeRange formDateTime = new FormDateTimeRange();
        model.addAttribute("formDate", formDateTime);
        return statsHandler(model, ru.pizzburg.utils.DateUtils.getDayStart(), new Date());
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/stats/week", method = RequestMethod.GET)
    public String getStatsWeek(ModelMap model) {
        FormDateTimeRange formDateTime = new FormDateTimeRange();
        model.addAttribute("formDate", formDateTime);
        return statsHandler(model, ru.pizzburg.utils.DateUtils.getWeekStart(), new Date());
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/stats/month", method = RequestMethod.GET)
    public String getStatsMonth(ModelMap model) {
        FormDateTimeRange formDateTime = new FormDateTimeRange();
        model.addAttribute("formDate", formDateTime);
        return statsHandler(model, ru.pizzburg.utils.DateUtils.getMonthStart(), new Date());
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/stats/year", method = RequestMethod.GET)
    public String getStatsYear(ModelMap model) {
        FormDateTimeRange formDateTime = new FormDateTimeRange();
        model.addAttribute("formDate", formDateTime);
        return statsHandler(model, ru.pizzburg.utils.DateUtils.getYearStart(), new Date());
    }

    private String statsHandler(ModelMap model, Date from, Date to) {
        model.addAttribute("from", ru.pizzburg.utils.DateUtils.OUT_DATE_TIME_FORMAT.format(from));
        model.addAttribute("to", ru.pizzburg.utils.DateUtils.OUT_DATE_TIME_FORMAT.format(to));
        model.addAttribute("summaryCost", statsManager.getOrdersCost(from,to));
        model.addAttribute("summaryPrice", statsManager.getOrdersPrice(from,to));
        model.addAttribute("ordersCount", statsManager.getOrdersCount(from,to));
        model.addAttribute("daysCount", ru.pizzburg.utils.DateUtils.getDaysCount(from, to));
        return STATISTICS_VIEW_NAME;
    }
}