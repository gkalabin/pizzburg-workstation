package ru.pizzburg.web.controller.stats;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.pizzburg.constants.SortStrings;
import ru.pizzburg.utils.*;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.web.service.FormDateTimeRange;
import ru.pizzburg.web.service.FormDateTimeRangeValidator;
import ru.pizzburg.web.service.stats.StatsManager;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Date;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Controller
@Lazy(false)
public class FlowStatsController {

	private static final String INGREDIENTS_FLOW_VIEW_NAME = "stats-ingredients-flow";
	private static final String PRODUCTS_FLOW_VIEW_NAME = "stats-products-flow";
    public static final String PRODUCTS_RESULT_COUNT_COOKIE_SUFFIX = "_stats_products_resultCount";
    public static final String INGREDIENTS_RESULT_COUNT_COOKIE_SUFFIX = "_stats_ingredients_resultCount";
    protected final Log logger = LogFactory.getLog(getClass());
	@Autowired
	private StatsManager statsManager;
	@Autowired
	private FormDateTimeRangeValidator formDateTimeRangeValidator;

	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/stats/ingredients-flow", method = RequestMethod.GET)
	public String getIngredientFlowStats(HttpServletRequest request, ModelMap model, Principal principal) {
		FormDateTimeRange formDateTime = new FormDateTimeRange();
		model.addAttribute("formDate", formDateTime);
		return ingredientFlowHandler(request, model, ru.pizzburg.utils.DateUtils.getDayStart(), new Date(), principal);
	}

	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/stats/ingredients-flow/**", method = RequestMethod.POST)
	public String getIngredientFlowCustomStats(HttpServletRequest request, ModelMap model,
                                               @ModelAttribute("formDate") FormDateTimeRange formDateTime,
                                               BindingResult result, Principal principal) {
		formDateTimeRangeValidator.validate(formDateTime, result);
		if (!result.hasErrors()) {
			return ingredientFlowHandler(request, model, formDateTime.getFromDate(), formDateTime.getToDate(), principal);
		} else {
			return ingredientFlowHandler(request, model, ru.pizzburg.utils.DateUtils.getDayStart(), new Date(), principal);
		}
	}

	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/stats/ingredients-flow/hour", method = RequestMethod.GET)
	public String getIngredientFlowStatsHour(HttpServletRequest request, ModelMap model, Principal principal) {
		FormDateTimeRange formDateTime = new FormDateTimeRange();
		model.addAttribute("formDate", formDateTime);
		return ingredientFlowHandler(request, model, ru.pizzburg.utils.DateUtils.getHourStart(), new Date(), principal);
	}

	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/stats/ingredients-flow/day", method = RequestMethod.GET)
	public String getIngredientFlowStatsDay(HttpServletRequest request, ModelMap model, Principal principal) {
		FormDateTimeRange formDateTime = new FormDateTimeRange();
		model.addAttribute("formDate", formDateTime);
		return ingredientFlowHandler(request, model, ru.pizzburg.utils.DateUtils.getDayStart(), new Date(), principal);
	}

	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/stats/ingredients-flow/week", method = RequestMethod.GET)
	public String getIngredientFlowStatsWeek(HttpServletRequest request, ModelMap model, Principal principal) {
		FormDateTimeRange formDateTime = new FormDateTimeRange();
		model.addAttribute("formDate", formDateTime);
		return ingredientFlowHandler(request, model, ru.pizzburg.utils.DateUtils.getWeekStart(), new Date(), principal);
	}

	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/stats/ingredients-flow/month", method = RequestMethod.GET)
	public String getIngredientFlowStatsMonth(HttpServletRequest request, ModelMap model, Principal principal) {
		FormDateTimeRange formDateTime = new FormDateTimeRange();
		model.addAttribute("formDate", formDateTime);
		return ingredientFlowHandler(request, model, ru.pizzburg.utils.DateUtils.getMonthStart(), new Date(), principal);
	}

	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/stats/ingredients-flow/year", method = RequestMethod.GET)
	public String getIngredientFlowStatsYear(HttpServletRequest request, ModelMap model, Principal principal) {
		FormDateTimeRange formDateTime = new FormDateTimeRange();
		model.addAttribute("formDate", formDateTime);
		return ingredientFlowHandler(request, model, ru.pizzburg.utils.DateUtils.getYearStart(), new Date(), principal);
	}

	private String ingredientFlowHandler(HttpServletRequest request, ModelMap model, Date from, Date to, Principal principal) {
		model.addAttribute("from", DateUtils.OUT_DATE_TIME_FORMAT.format(from));
		model.addAttribute("to", DateUtils.OUT_DATE_TIME_FORMAT.format(to));
		model.addAttribute("ingredientsFlow", statsManager.getIngredientFlow(from, to,
				parseIngredientsPagingParams(request, principal), parseIngredientsFlowSortingParams(request)));
		return INGREDIENTS_FLOW_VIEW_NAME;
	}

	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/stats/products-flow", method = RequestMethod.GET)
	public String getProductFlowStats(HttpServletRequest request, ModelMap model, Principal principal) {
		FormDateTimeRange formDateTime = new FormDateTimeRange();
		model.addAttribute("formDate", formDateTime);
		return productFlowHandler(request, model, ru.pizzburg.utils.DateUtils.getDayStart(), new Date(), principal);
	}

	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/stats/products-flow/**", method = RequestMethod.POST)
	public String getProductFlowCustomStats(HttpServletRequest request, ModelMap model,
                                            @ModelAttribute("formDate") FormDateTimeRange formDateTime,
                                            BindingResult result, Principal principal) {
		formDateTimeRangeValidator.validate(formDateTime, result);
		if (!result.hasErrors()) {
			return productFlowHandler(request, model, formDateTime.getFromDate(), formDateTime.getToDate(), principal);
		} else {
			return productFlowHandler(request, model, ru.pizzburg.utils.DateUtils.getDayStart(), new Date(), principal);
		}
	}

	private String productFlowHandler(HttpServletRequest request, ModelMap model, Date from, Date to, Principal principal) {
		model.addAttribute("from", DateUtils.OUT_DATE_TIME_FORMAT.format(from));
		model.addAttribute("to", DateUtils.OUT_DATE_TIME_FORMAT.format(to));
		model.addAttribute("productsFlow", statsManager.getProductFlow(from, to,
				parseProductsPagingParams(request, principal), parseProductsFlowSortingParams(request)));
		return PRODUCTS_FLOW_VIEW_NAME;
	}

	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/stats/products-flow/hour", method = RequestMethod.GET)
	public String getProductFlowStatsHour(HttpServletRequest request, ModelMap model, Principal principal) {
		FormDateTimeRange formDateTime = new FormDateTimeRange();
		model.addAttribute("formDate", formDateTime);
		return productFlowHandler(request, model, ru.pizzburg.utils.DateUtils.getHourStart(), new Date(), principal);
	}

	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/stats/products-flow/day", method = RequestMethod.GET)
	public String getProductFlowStatsDay(HttpServletRequest request, ModelMap model, Principal principal) {
		FormDateTimeRange formDateTime = new FormDateTimeRange();
		model.addAttribute("formDate", formDateTime);
		return productFlowHandler(request, model, ru.pizzburg.utils.DateUtils.getDayStart(), new Date(), principal);
	}

	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/stats/products-flow/week", method = RequestMethod.GET)
	public String getProductFlowStatsWeek(HttpServletRequest request, ModelMap model, Principal principal) {
		FormDateTimeRange formDateTime = new FormDateTimeRange();
		model.addAttribute("formDate", formDateTime);
		return productFlowHandler(request, model, ru.pizzburg.utils.DateUtils.getWeekStart(), new Date(), principal);
	}

	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/stats/products-flow/month", method = RequestMethod.GET)
	public String getProductFlowStatsMonth(HttpServletRequest request, ModelMap model, Principal principal) {
		FormDateTimeRange formDateTime = new FormDateTimeRange();
		model.addAttribute("formDate", formDateTime);
		return productFlowHandler(request, model, ru.pizzburg.utils.DateUtils.getMonthStart(), new Date(), principal);
	}

	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/stats/products-flow/year", method = RequestMethod.GET)
	public String getProductFlowStatsYear(HttpServletRequest request, ModelMap model, Principal principal) {
		FormDateTimeRange formDateTime = new FormDateTimeRange();
		model.addAttribute("formDate", formDateTime);
		return productFlowHandler(request, model, ru.pizzburg.utils.DateUtils.getYearStart(), new Date(), principal);
	}

	private SortingParams parseIngredientsFlowSortingParams(HttpServletRequest request) {
		String sort = request.getParameter("sort");
		String dir = request.getParameter("dir");
		if (sort == null || dir == null || !SortStrings.isDirectionCorrect(dir)
				|| !SortStrings.isStatsIngredientCorrect(sort)) {
			return null;
		}
		return new SortingParams(sort, Converter.parseSortOrder(dir));
	}

	private SortingParams parseProductsFlowSortingParams(HttpServletRequest request) {
		String sort = request.getParameter("sort");
		String dir = request.getParameter("dir");
		if (sort == null || dir == null || !SortStrings.isDirectionCorrect(dir)
				|| !SortStrings.isStatsProductCorrect(sort)) {
			return null;
		}
		return new SortingParams(sort, Converter.parseSortOrder(dir));
	}

    private PaginationParams parsePagingParams(HttpServletRequest request, Principal principal, String resultCountCookieSuffix) {
        String pageStr = request.getParameter("page");
        String resultsStr = request.getParameter("results");
        String cookieName = Digest.getSha1Hash(principal.getName() + resultCountCookieSuffix);
        for (Cookie c: request.getCookies()) {
            if (cookieName.equals(c.getName())) {
                resultsStr = c.getValue();
                break;
            }
        }
        if (pageStr == null && resultsStr == null) {
            return null;
        }
        int page = -1;
        if (pageStr != null) {
            try {
                page = Integer.parseInt(pageStr);
            } catch (NumberFormatException ex) {
                logger.info("invalid paging params (flow stats): " + pageStr);
            }
        }
        int results = -1;
        if (resultsStr != null) {
            try {
                results = Integer.parseInt(resultsStr);
            } catch (NumberFormatException ex) {
                logger.info("invalid paging params (flow stats): " + pageStr);
            }
        }
        return new PaginationParams(page, results);
    }

    private PaginationParams parseProductsPagingParams(HttpServletRequest request, Principal principal) {
        return parsePagingParams(request, principal, PRODUCTS_RESULT_COUNT_COOKIE_SUFFIX);
    }

    private PaginationParams parseIngredientsPagingParams(HttpServletRequest request, Principal principal) {
        return parsePagingParams(request, principal, INGREDIENTS_RESULT_COUNT_COOKIE_SUFFIX);
    }
}