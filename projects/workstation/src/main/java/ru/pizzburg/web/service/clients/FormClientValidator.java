package ru.pizzburg.web.service.clients;

import org.springframework.context.annotation.Lazy;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.pizzburg.constants.Const;
import ru.pizzburg.utils.DateUtils;
import ru.pizzburg.constants.FieldLength;
import ru.pizzburg.utils.DiscountCardUtils;
import ru.pizzburg.utils.PhoneUtils;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Lazy(false)
public class FormClientValidator implements Validator {

    @Override
    public boolean supports(Class clazz) {
        return FormClient.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phoneStr", "error.empty-phone", "Empty phone");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "defaultAddress", "error.empty-address", "Empty address");

        FormClient fc = (FormClient) obj;
        checkFieldLength(fc, errors);
        if (errors.getFieldErrorCount("phoneStr") == 0) {
            if (!PhoneUtils.isPhoneValid(fc.getPhoneStr())) {
                errors.rejectValue("phoneStr", "error.invalid-phone", new Object[]{PhoneUtils.PHONE_FULL_DIGITS_COUNT, PhoneUtils.PHONE_LOCAL_DIGITS_COUNT},
                        "Phone number must contain " + PhoneUtils.PHONE_FULL_DIGITS_COUNT + " or " + PhoneUtils.PHONE_LOCAL_DIGITS_COUNT + " digits");
            }
        }

        try {
            fc.setOrdersSumStr(fc.getOrdersSumStr().replace(",", "."));
            float ordersSum = Float.valueOf(fc.getOrdersSumStr());
            if (ordersSum < 0) {
                errors.rejectValue("ordersSumStr", "error.negative-sum", "Sum must be > 0");
            } else {
                fc.setOrdersSum(ordersSum);
            }
        } catch (NumberFormatException ex) {
            errors.rejectValue("ordersSumStr", "typeMismatch.float", "Not convertible to number");
        }

        if (fc.isWithCard()) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "card.id", "error.empty-card-id", "Empty card number");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "card.emissionDate", "error.empty-emission-date", "Empty emission date");
            if (errors.getFieldErrorCount("card.id") == 0) {
                if (!DiscountCardUtils.isDiscountCardNumberValid(fc.getCard().getId())) {
                    errors.rejectValue("card.id", "error.invalid-card", new Object[]{DiscountCardUtils.DISCOUNT_CARD_DIGITS_COUNT}, "Card number must contain " + DiscountCardUtils.DISCOUNT_CARD_DIGITS_COUNT + " digits");
                } else {
                    fc.getCard().setIdLond(DiscountCardUtils.getDiscountCardNumber(fc.getCard().getId()));
                }
            }
            if (errors.getFieldErrorCount("card.emissionDate") == 0) {
                if (!DateUtils.isDateValid(fc.getCard().getEmissionDate())) {
                    errors.rejectValue("card.emissionDate", "error.date-not-valid",
                            new Object[]{ru.pizzburg.utils.DateUtils.DATE_FORMAT.format(DateUtils.LEFT_DATE_BOUND), ru.pizzburg.utils.DateUtils.DATE_FORMAT.format(DateUtils.RIGHT_DATE_BOUND), ru.pizzburg.utils.DateUtils.DATE_FORMAT.toPattern()},
                            "Date format must be " + Const.DATE_FORMAT_STRING);
                } else {
                    fc.getCard().setEmissionDateDate(DateUtils.parseDate(fc.getCard().getEmissionDate()));
                }
            }
        }
    }

    private void checkFieldLength(FormClient fc, Errors errors) {
        if (fc.getName().length() > FieldLength.CLIENT_NAME) {
            errors.rejectValue("name", "error.too-long-value",
                    new Object[]{FieldLength.CLIENT_NAME}, "Max length is " + FieldLength.CLIENT_NAME);
        }
        if (fc.getSurname().length() > FieldLength.CLIENT_SURNAME) {
            errors.rejectValue("surname", "error.too-long-value",
                    new Object[]{FieldLength.CLIENT_SURNAME}, "Max length is " + FieldLength.CLIENT_SURNAME);
        }
        if (fc.getMiddleName().length() > FieldLength.CLIENT_MIDDLE_NAME) {
            errors.rejectValue("middleName", "error.too-long-value",
                    new Object[]{FieldLength.CLIENT_MIDDLE_NAME}, "Max length is " + FieldLength.CLIENT_MIDDLE_NAME);
        }
        if (fc.getDefaultAddress().length() > FieldLength.CLIENT_DEFAULT_ADDRESS) {
            errors.rejectValue("defaultAddress", "error.too-long-value",
                    new Object[]{FieldLength.CLIENT_DEFAULT_ADDRESS}, "Max length is " + FieldLength.CLIENT_DEFAULT_ADDRESS);
        }
    }
}