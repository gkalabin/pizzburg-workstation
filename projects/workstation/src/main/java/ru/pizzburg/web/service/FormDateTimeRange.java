package ru.pizzburg.web.service;

import java.util.Date;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class FormDateTimeRange {

    private String from = "";
    private String to = "";
    private Date fromDate;
    private Date toDate;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FormDateTimeRange other = (FormDateTimeRange) obj;
        if ((this.from == null) ? (other.from != null) : !this.from.equals(other.from)) {
            return false;
        }
        if ((this.to == null) ? (other.to != null) : !this.to.equals(other.to)) {
            return false;
        }
        if (this.fromDate != other.fromDate && (this.fromDate == null || !this.fromDate.equals(other.fromDate))) {
            return false;
        }
        if (this.toDate != other.toDate && (this.toDate == null || !this.toDate.equals(other.toDate))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.from != null ? this.from.hashCode() : 0);
        hash = 59 * hash + (this.to != null ? this.to.hashCode() : 0);
        hash = 59 * hash + (this.fromDate != null ? this.fromDate.hashCode() : 0);
        hash = 59 * hash + (this.toDate != null ? this.toDate.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "FormDateTimeRange{" + "from=" + from + ", to=" + to + ", fromDate=" + fromDate + ", toDate=" + toDate + '}';
    }
}
