package ru.pizzburg.web.repo.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import ru.pizzburg.web.domain.DiscountCard;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class DiscountCardMapper implements ParameterizedRowMapper<DiscountCard> {

    @Override
    public DiscountCard mapRow(ResultSet rs, int rowNum) throws SQLException {
        DiscountCard discountCard = new DiscountCard();
        discountCard.setId(rs.getLong("cardNumber"));
        discountCard.setEmissionDate(rs.getTimestamp("emissionDate"));
        return discountCard;
    }
}