package ru.pizzburg.web.domain;

/**
 * POJO that describe ingredient flow per day, week or month.
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class IngredientFlowStat {

    private Ingredient ingredient;
    private float count;
    private float cost;

    public float getCount() {
        return count;
    }

    public void setCount(float count) {
        this.count = count;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "IngredientFlowStat{" + "ingredient=" + ingredient + ", count=" + count + ", cost=" + cost + '}';
    }
}