package ru.pizzburg.web.service.orders;

import org.displaytag.properties.SortOrderEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.pizzburg.utils.data.PageableList;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.constants.SortStrings;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.web.domain.Order;
import ru.pizzburg.web.domain.Status;
import ru.pizzburg.web.repo.AdminDao;
import ru.pizzburg.web.repo.ClientDao;
import ru.pizzburg.web.repo.OrderDao;
import ru.pizzburg.web.repo.ProductDao;
import ru.pizzburg.web.service.admin.AdminManager;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Manages all logic connected with orders
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Service
@Lazy(false)
public class OrdersManager {

    private static final PaginationParams DEFAULT_PAGINATION_PARAMS;
    private static final SortingParams DEFAULT_SORTING_PARAMS;
    private static final int MAX_OBJECTS_PER_PAGE = 100;
    @Autowired
    private OrderDao orderDao;
    @Autowired
    private ProductDao productDao;
    @Autowired
    private AdminDao adminDao;
    @Autowired
    private ClientDao clientDao;
    @Autowired
    private AdminManager adminManager;

    static {
        DEFAULT_PAGINATION_PARAMS = new PaginationParams();
        DEFAULT_SORTING_PARAMS = new SortingParams("orderID", SortOrderEnum.DESCENDING);
    }

    public PageableList<Order> getOrders(PaginationParams pp, SortingParams sp, OrderFilter filter) {
        if (pp == null) {
            pp = DEFAULT_PAGINATION_PARAMS;
        } else {
            int page = pp.getPageNumber() <= 0 ? DEFAULT_PAGINATION_PARAMS.getPageNumber() : pp.getPageNumber();
            int results = pp.getObjectsPerPage() <= 0 || pp.getObjectsPerPage() > MAX_OBJECTS_PER_PAGE
                    ? DEFAULT_PAGINATION_PARAMS.getObjectsPerPage() : pp.getObjectsPerPage();
            pp = new PaginationParams(page, results);
        }
        if (sp == null) {
            sp = DEFAULT_SORTING_PARAMS;
        }
        final PageableList<Order> orderList = orderDao.getOrders(pp, sp, filter);
        if (orderList == null || orderList.getList() == null) {
            return null;
        }
        for (Order o : orderList.getList()) {
            o.setProducts(productDao.getProductsForOrder(o));
            if (o.getAlternateDiscountCard() == null)
                o.getClient().setCard(clientDao.getDiscountCard(o.getClient()));
        }

        final SortingParams spFinal = sp;
        if (SortStrings.ORDER_DISCOUNT_CARD_SORT_CRITERION.equalsIgnoreCase(sp.getSortCriterion())) {
            Collections.sort(orderList.getList(), new Comparator<Order>() {
                @Override
                public int compare(Order o1, Order o2) {
                    long card1 = o1.getAlternateDiscountCard() == null ?
                            o1.getClient().getCard() == null ? 0 : o1.getClient().getCard().getId() :
                            o1.getAlternateDiscountCard().getId();
                    long card2 = o2.getAlternateDiscountCard() == null ?
                            o2.getClient().getCard() == null ? 0 : o2.getClient().getCard().getId() :
                            o2.getAlternateDiscountCard().getId();
                    int result = card1 > card2 ? 1 : -1;
                    return spFinal.getSortDirection().equals(SortOrderEnum.ASCENDING) ?
                            result : -result;
                }
            });
        }

        return orderList;
    }

    public String addOrder(Order order) {
        order.setRegistrator(adminDao.getUser(order.getRegistrator().getLogin()));
        return orderDao.addOrder(order);
    }

    public Order getOrder(int id) {
        Order o = orderDao.getOrder(id);
        if (o == null) {
            return null;
        }
        o.setProducts(productDao.getProductsForOrder(o));
        o.setRegistrator(adminManager.getOrderRegistrator(o));
        o.setCarrier(adminManager.getOrderCarrier(o));
        o.setStatus(orderDao.getOrderStatus(o));
        o.setClient(clientDao.getClientForOrder(o));
        return o;
    }

    public String updateOrder(Order order) {
        return orderDao.updateOrder(order, productDao.getProductsForOrder(order));
    }

    public String removeOrder(int id) {
        return orderDao.removeOrder(id);
    }

    public List<Status> getStatuses() {
        return orderDao.getStatuses();
    }

    public Status getStatus(int id) {
        return orderDao.getStatus(id);
    }

    public String updateStatus(Status status) {
        return orderDao.updateStatus(status);
    }

    public float getOrderSum(int orderId) {
        return orderDao.getOrderSum(orderId);
    }
}
