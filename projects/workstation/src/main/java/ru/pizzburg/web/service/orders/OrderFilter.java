package ru.pizzburg.web.service.orders;

import java.util.Date;
import java.util.List;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class OrderFilter {

    private int orderId;
    private String phone;
    private String card;
    private String address;
    private String registratorLogin;
    private String registratorName;
    private String registratorSurname;
    private Date dateFrom;
    private Date dateTo;
    private List<Integer> statuses;
    private float costFrom = -1;
    private float costTo = -1;
    private float discountFrom = -1;
    private float discountTo = -1;
    private boolean isEmpty = true;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float getCostFrom() {
        return costFrom;
    }

    public void setCostFrom(float costFrom) {
        this.costFrom = costFrom;
    }

    public float getCostTo() {
        return costTo;
    }

    public void setCostTo(float costTo) {
        this.costTo = costTo;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRegistratorLogin() {
        return registratorLogin;
    }

    public void setRegistratorLogin(String registratorLogin) {
        this.registratorLogin = registratorLogin;
    }

    public String getRegistratorName() {
        return registratorName;
    }

    public void setRegistratorName(String registratorName) {
        this.registratorName = registratorName;
    }

    public String getRegistratorSurname() {
        return registratorSurname;
    }

    public void setRegistratorSurname(String registratorSurname) {
        this.registratorSurname = registratorSurname;
    }

    public List<Integer> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<Integer> statuses) {
        this.statuses = statuses;
    }

    public float getDiscountFrom() {
        return discountFrom;
    }

    public void setDiscountFrom(float discountFrom) {
        this.discountFrom = discountFrom;
    }

    public float getDiscountTo() {
        return discountTo;
    }

    public void setDiscountTo(float discountTo) {
        this.discountTo = discountTo;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public boolean getIsEmpty() {
        return isEmpty;
    }

    public void setIsEmpty(boolean empty) {
        isEmpty = empty;
    }

    @Override
    public String toString() {
        return "OrderFilter{" +
                "orderId=" + orderId +
                ", phone='" + phone + '\'' +
                ", card='" + card + '\'' +
                ", address='" + address + '\'' +
                ", registratorLogin='" + registratorLogin + '\'' +
                ", registratorName='" + registratorName + '\'' +
                ", registratorSurname='" + registratorSurname + '\'' +
                ", dateFrom=" + dateFrom +
                ", dateTo=" + dateTo +
                ", statuses=" + statuses +
                ", costFrom=" + costFrom +
                ", costTo=" + costTo +
                ", discountFrom=" + discountFrom +
                ", discountTo=" + discountTo +
                ", isEmpty=" + isEmpty +
                '}';
    }
}
