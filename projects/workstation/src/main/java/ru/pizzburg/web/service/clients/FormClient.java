package ru.pizzburg.web.service.clients;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class FormClient {

    private String phoneStr;
    private String surname;
    private String name;
    private String middleName;
    private String defaultAddress;
    private String ordersSumStr = "0";
    private float ordersSum;
    private FormDiscountCard card = new FormDiscountCard();
    private boolean withCard = false;

    public FormDiscountCard getCard() {
        return card;
    }

    public void setCard(FormDiscountCard card) {
        this.card = card;
    }

    public boolean isWithCard() {
        return withCard;
    }

    public void setWithCard(boolean withCard) {
        this.withCard = withCard;
    }

    public String getDefaultAddress() {
        return defaultAddress;
    }

    public void setDefaultAddress(String defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneStr() {
        return phoneStr;
    }

    public void setPhoneStr(String phone) {
        this.phoneStr = phone;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getOrdersSumStr() {
        return ordersSumStr;
    }

    public void setOrdersSumStr(String ordersSumStr) {
        this.ordersSumStr = ordersSumStr;
    }

    public float getOrdersSum() {
        return ordersSum;
    }

    public void setOrdersSum(float ordersSum) {
        this.ordersSum = ordersSum;
    }

    @Override
    public String toString() {
        return "FormClient{" +
                "phoneStr='" + phoneStr + '\'' +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", middleName='" + middleName + '\'' +
                ", defaultAddress='" + defaultAddress + '\'' +
                ", ordersSumStr='" + ordersSumStr + '\'' +
                ", ordersSum=" + ordersSum +
                ", card=" + card +
                ", withCard=" + withCard +
                '}';
    }
}
