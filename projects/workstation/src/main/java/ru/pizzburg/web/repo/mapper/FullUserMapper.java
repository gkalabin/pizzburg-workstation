package ru.pizzburg.web.repo.mapper;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import ru.pizzburg.web.domain.Role;
import ru.pizzburg.web.domain.WorkstationUser;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class FullUserMapper implements ParameterizedRowMapper<WorkstationUser> {

    @Override
    public WorkstationUser mapRow(ResultSet rs, int rowNum) throws SQLException {
        WorkstationUser user = new WorkstationUser();
        user.setId(rs.getInt("userID"));
        user.setName(rs.getString("name"));
        user.setSurname(rs.getString("surname"));
        user.setMiddleName(rs.getString("middleName"));
        user.setLogin(rs.getString("login"));
        user.setPasswordDigest(rs.getString("pass"));
        user.setRole(new Role());
        user.getRole().setId(rs.getInt("roleID"));
        user.getRole().setName(rs.getString("Roles.name"));
        user.getRole().setDescription(rs.getString("Roles.description"));
        return user;
    }
}