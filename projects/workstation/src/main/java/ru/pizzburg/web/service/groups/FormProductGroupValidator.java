package ru.pizzburg.web.service.groups;

import org.springframework.context.annotation.Lazy;
import org.springframework.util.AutoPopulatingList;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.pizzburg.constants.FieldLength;
import ru.pizzburg.web.domain.Product;
import ru.pizzburg.web.domain.ProductInGroup;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Lazy(false)
public class FormProductGroupValidator implements Validator {

    @Override
    public boolean supports(Class clazz) {
        return FormProductGroup.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "error.empty-title", "Empty title");
        FormProductGroup fpg = (FormProductGroup) obj;
        checkFieldLength(fpg, errors);

        AutoPopulatingList<ProductFormEntry> productsStr = fpg.getProductsStr();
        AutoPopulatingList<ProductFormEntry> productsStrNew = new AutoPopulatingList<ProductFormEntry>(ProductFormEntry.class);
        productsStrNew.addAll(productsStr);
        productsStr.clear();
        fpg.getProducts().clear();

        // remove removed items
        for (int i = 0; i < productsStrNew.size(); i++) {
            if (!fpg.getDeletedProducts().contains(i)) {
                final ProductFormEntry product = productsStrNew.get(i);
                productsStr.add(product);
            }
        }
        fpg.getDeletedProducts().clear();

        for (int i = 0; i < productsStr.size(); i++) {
            try {
                int id = Integer.parseInt(productsStr.get(i).getId());
                if (id < 0) {
                    errors.rejectValue("productsStr[" + i + "]", "error.product-negative-id", "Product error");
                } else {
                    final ProductInGroup productInGroup = new ProductInGroup();
                    productInGroup.setProduct(new Product());
                    productInGroup.getProduct().setId(id);
                    fpg.getProducts().add(productInGroup);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("productsStr[" + i + "]", "error.product-not-int-id", "Product error");
                continue;
            }
            try {
                productsStr.get(i).setPrice(productsStr.get(i).getPrice().replace(",", "."));
                float price = Float.parseFloat(productsStr.get(i).getPrice());
                if (price < 0) {
                    errors.rejectValue("productsStr[" + i + "]", "error.negative-price", "Price must be >0");
                } else {
                    fpg.getProducts().get(i).setPrice(price);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("productsStr[" + i + "]", "typeMismatch.float", "Price must be a number");
            }
        }
        for (int i = 0; i < productsStr.size(); i++) {
            for (int j = i + 1; j < productsStr.size(); j++) {
                if (productsStr.get(i).getId().equals(productsStr.get(j).getId()) && !productsStr.get(i).getId().isEmpty()) {
                    if (errors.getFieldErrorCount("productsStr[" + i + "]") == 0) {
                        errors.rejectValue("productsStr[" + i + "]", "error.not-unique-product", "Duplicate product");
                    }
                    if (errors.getFieldErrorCount("productsStr[" + j + "]") == 0) {
                        errors.rejectValue("productsStr[" + j + "]", "error.not-unique-product", "Duplicate product");
                    }
                }
            }
        }
    }

    private void checkFieldLength(FormProductGroup fpg, Errors errors) {
        if (fpg.getTitle().length() > FieldLength.PRODUCT_GROUP_TITLE) {
            errors.rejectValue("title", "error.too-long-value",
                    new Object[]{FieldLength.PRODUCT_GROUP_TITLE}, "Max length is " + FieldLength.PRODUCT_GROUP_TITLE);
        }
        if (fpg.getDescription().length() > FieldLength.PRODUCT_GROUP_DESCRIPTION) {
            errors.rejectValue("description", "error.too-long-value",
                    new Object[]{FieldLength.PRODUCT_GROUP_DESCRIPTION}, "Max length is " + FieldLength.PRODUCT_GROUP_DESCRIPTION);
        }
    }
}
