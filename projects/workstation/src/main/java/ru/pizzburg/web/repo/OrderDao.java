package ru.pizzburg.web.repo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.stereotype.Repository;
import ru.pizzburg.constants.DatabaseErrors;
import ru.pizzburg.constants.SortStrings;
import ru.pizzburg.utils.*;
import ru.pizzburg.utils.DiscountCardUtils;
import ru.pizzburg.utils.PhoneUtils;
import ru.pizzburg.utils.data.PageableList;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.web.domain.Order;
import ru.pizzburg.web.domain.ProductInOrder;
import ru.pizzburg.web.domain.Status;
import ru.pizzburg.web.repo.mapper.FullOrderMapper;
import ru.pizzburg.web.repo.mapper.OrderMapper;
import ru.pizzburg.web.repo.mapper.StatusMapper;
import ru.pizzburg.web.service.orders.OrderFilter;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Repository
@Lazy(false)
public class OrderDao extends JdbcTemplate {
    private static final String FROM_CLAUSE_ORDER_LIST = "Orders.*, OrderStatuses.title, " +
            "RegistratorUser.name AS registratorName, RegistratorUser.surname AS registratorSurname, RegistratorUser.login AS registratorLogin, " +
            "CarrierUser.name AS carrierName, CarrierUser.surname AS carrierSurname";
    private static final String ORDER_DATA_LIST_QUERY = "SELECT %s FROM pizzburgdb.Orders "
            + "LEFT JOIN pizzburgdb.OrderStatuses ON Orders.orderStatus = OrderStatuses.statusID "
            + "LEFT JOIN pizzburgdb.Clients ON Orders.clientPhone = Clients.phone "
            + "LEFT JOIN pizzburgdb.WorkstationUsers AS RegistratorUser ON RegistratorUser.userID = Orders.registrator "
            + "LEFT JOIN pizzburgdb.WorkstationUsers AS CarrierUser ON CarrierUser.userID = Orders.carrier "
            + "LEFT JOIN pizzburgdb.OrdersProducts ON Orders.orderID = OrdersProducts.orderID "
            + "%s %s %s %s %s";
    private static final String ORDER_COUNT_LIST_QUERY = "SELECT COUNT(*) FROM (" + ORDER_DATA_LIST_QUERY + ") AS tmp";
    private static final String LIMIT_CLAUSE = "LIMIT ?, ?";
    private static final String GROUP_BY_CLAUSE = "GROUP BY Orders.orderID";
    private final Log logger = LogFactory.getLog(getClass());

    @Override
    @Autowired
    public void setDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    public PageableList<Order> getOrders(PaginationParams pp, SortingParams sp, OrderFilter filter) {
        PageableList<Order> result = new PageableList<Order>();

        List<Object> filterParams = new ArrayList<Object>();
        String whereClause = getWhereClauseByFilter(filter, filterParams);
        String havingClause = getHavingClauseByFilter(filter, filterParams);
        Object[] dataQueryParams = new Object[filterParams.size() + 2];
        Object[] countQueryParams = new Object[filterParams.size()];
        int idx = 0;
        for (Object o : filterParams) {
            countQueryParams[idx] = o;
            dataQueryParams[idx++] = o;
        }
        dataQueryParams[idx++] = (pp.getPageNumber() - 1) * pp.getObjectsPerPage();
        dataQueryParams[idx] = pp.getObjectsPerPage();

        final String orderClause = getOrderClauseByParams(sp);
        List<Order> orders = query(String.format(ORDER_DATA_LIST_QUERY, FROM_CLAUSE_ORDER_LIST, whereClause, GROUP_BY_CLAUSE, havingClause, orderClause, LIMIT_CLAUSE),
                dataQueryParams, new FullOrderMapper());
        result.setListSize(queryForInt(String.format(ORDER_COUNT_LIST_QUERY, FROM_CLAUSE_ORDER_LIST, whereClause, GROUP_BY_CLAUSE, havingClause, orderClause, ""), countQueryParams));

        result.setData(orders);
        result.setPaginationParams(pp);
        result.setSortingParams(sp);
        return result;
    }

    private String getHavingClauseByFilter(OrderFilter filter, List<Object> params) {
        if (filter == null || filter.getIsEmpty()) {
            return "";
        } else {
            String result = "";
            boolean isEmpty = true;
            if (filter.getCostFrom() > 0) {
                result = "HAVING cost >= ?";
                params.add(filter.getCostFrom());
                isEmpty = false;
            }
            if (filter.getCostTo() > 0) {
                result += (isEmpty ? "HAVING " : " AND ") + "cost <= ?";
                params.add(filter.getCostTo());
            }
            return result;
        }
    }

    private String getWhereClauseByFilter(OrderFilter filter, List<Object> params) {
        if (filter == null || filter.getIsEmpty()) {
            return "";
        } else {
            String result = "";
            boolean isEmpty = true;
            if (filter.getOrderId() >0) {
                result = "WHERE Orders.orderID = ?";
                params.add(filter.getOrderId());
                isEmpty = false;
            }
            if (filter.getRegistratorName() != null) {
                result += (isEmpty ? "WHERE " : " AND ") + "WorkstationUsers.name LIKE ?";
                params.add(filter.getRegistratorName());
                isEmpty = false;
            }
            if (filter.getRegistratorSurname() != null) {
                result += (isEmpty ? "WHERE " : " AND ") + "RegistratorUser.surname LIKE ?";
                params.add(filter.getRegistratorSurname());
                isEmpty = false;
            }
            if (filter.getRegistratorLogin() != null) {
                result += (isEmpty ? "WHERE " : " AND ") + "RegistratorUser.login LIKE ?";
                params.add(filter.getRegistratorLogin());
                isEmpty = false;
            }
            if (filter.getAddress() != null) {
                result += (isEmpty ? "WHERE " : " AND ") + "Orders.shipmentAddress LIKE ?";
                params.add(filter.getAddress());
                isEmpty = false;
            }
            if (filter.getPhone() != null) {
                result += (isEmpty ? "WHERE " : " AND ") + "LPAD(Orders.clientPhone, " + PhoneUtils.PHONE_FULL_DIGITS_COUNT + ", '0') LIKE ?";
                params.add(filter.getPhone());
                isEmpty = false;
            }
            if (filter.getCard() != null) {
                result += (isEmpty ? "WHERE " : " AND ") + "(Orders.alternateDiscountCard IS NOT NULL AND " +
                        "LPAD(Orders.alternateDiscountCard, " + DiscountCardUtils.DISCOUNT_CARD_DIGITS_COUNT + ", '0') LIKE ? OR " +
                        "Orders.alternateDiscountCard IS NULL AND " +
                        "LPAD(Clients.discountCardNumber, " + DiscountCardUtils.DISCOUNT_CARD_DIGITS_COUNT + ", '0') LIKE ?)";
                params.add(filter.getCard());
                params.add(filter.getCard());
                isEmpty = false;
            }
            if (filter.getDateFrom() != null) {
                result += (isEmpty ? "WHERE " : " AND ") + "Orders.registerDate >= ?";
                params.add(filter.getDateFrom());
                isEmpty = false;
            }
            if (filter.getDateTo() != null) {
                result += (isEmpty ? "WHERE " : " AND ") + "Orders.registerDate <= ?";
                params.add(filter.getDateTo());
                isEmpty = false;
            }
            if (filter.getCostFrom() > 0) {
                result += (isEmpty ? "WHERE " : " AND ") + "Orders.cost >= ?";
                params.add(filter.getCostFrom());
                isEmpty = false;
            }
            if (filter.getCostTo() > 0) {
                result += (isEmpty ? "WHERE " : " AND ") + "Orders.cost <= ?";
                params.add(filter.getCostTo());
                isEmpty = false;
            }
            if (filter.getDiscountFrom() > 0) {
                result += (isEmpty ? "WHERE " : " AND ") + "Orders.discount >= ?";
                params.add(filter.getDiscountFrom());
                isEmpty = false;
            }
            if (filter.getDiscountTo() > 0) {
                result += (isEmpty ? "WHERE " : " AND ") + "Orders.discount <= ?";
                params.add(filter.getDiscountTo());
                isEmpty = false;
            }
            if (filter.getStatuses() != null && !filter.getStatuses().isEmpty()) {
                result += (isEmpty ? "WHERE " : " AND ") + "Orders.orderStatus IN (";
                for (int i = 0; i < filter.getStatuses().size(); i++) {
                    result += i == 0 ? "?" : ",?";
                    params.add(filter.getStatuses().get(i));
                }
                result += ")";
                isEmpty = false;
            }
            return result;
        }
    }

    private String getOrderClauseByParams(SortingParams sp) {
        if (SortStrings.ORDER_REGISTRATOR_SORT_CRITERION.equalsIgnoreCase(sp.getSortCriterion())) {
            return String.format("ORDER BY RegistratorUser.name %s, RegistratorUser.surname %s, RegistratorUser.middleName %s",
                    Converter.getStringSortOrder(sp.getSortDirection()),
                    Converter.getStringSortOrder(sp.getSortDirection()), Converter.getStringSortOrder(sp.getSortDirection()));
        } else if (SortStrings.ORDER_CARRIER_SORT_CRITERION.equalsIgnoreCase(sp.getSortCriterion())) {
            return String.format("ORDER BY CarrierUser.name %s, CarrierUser.surname %s, CarrierUser.middleName %s",
                    Converter.getStringSortOrder(sp.getSortDirection()),
                    Converter.getStringSortOrder(sp.getSortDirection()), Converter.getStringSortOrder(sp.getSortDirection()));
        } else if (SortStrings.ORDER_STATUS_SORT_CRITERION.equalsIgnoreCase(sp.getSortCriterion())) {
            return String.format("ORDER BY OrderStatuses.title %s",
                    Converter.getStringSortOrder(sp.getSortDirection()));
        } else if (SortStrings.ORDER_COST_SORT_CRITERION.equalsIgnoreCase(sp.getSortCriterion())) {
            return String.format("ORDER BY Orders.cost*(1-Orders.discount/100) %s",
                    Converter.getStringSortOrder(sp.getSortDirection()));
        } else if (SortStrings.ORDER_DISCOUNT_CARD_SORT_CRITERION.equalsIgnoreCase(sp.getSortCriterion())) {
            // implemented in manager
            return "";
        } else {
            return String.format("ORDER BY Orders.%s %s",
                    sp.getSortCriterion(), Converter.getStringSortOrder(sp.getSortDirection()));
        }
    }

    public String addOrder(Order o) {
        try {

            if (o.getAlternateDiscountCard() != null) {
                int cardExists = queryForInt("SELECT COUNT(*) FROM pizzburgdb.DiscountCards " +
                        "WHERE cardNumber=?", o.getAlternateDiscountCard().getId());
                if (cardExists == 0) {
                    return DatabaseErrors.DISCOUNT_CARD_NOT_FOUND;
                }
            }

            int clientExists = queryForInt("SELECT COUNT(*) FROM pizzburgdb.Clients WHERE phone=?", o.getClient().getPhone());
            if (clientExists == 0) {
                update("INSERT INTO pizzburgdb.Clients (phone, defaultAddress, name) VALUES (?, ?, ?)",
                        o.getClient().getPhone(), o.getShipmentAddress(), o.getClient().getName());
            } else {
                update("UPDATE pizzburgdb.Clients SET name = ? WHERE phone = ?",
                        o.getClient().getName(), o.getClient().getPhone());
            }
            update("INSERT INTO pizzburgdb.Orders (clientPhone, shipmentAddress, registrator, alternateDiscountCard, alternateDiscount, personsCount, comment) " +
                    "VALUES (?,?,?,?,?,?,?)",
                    o.getClient().getPhone(),
                    o.getShipmentAddress(),
                    o.getRegistrator().getId(),
                    (o.getAlternateDiscountCard() != null ? o.getAlternateDiscountCard().getId() : null),
                    (o.getAlternateDiscount() > 0 ? o.getAlternateDiscount() : null),
                    o.getPersonsCount(),
                    o.getComment());
            int id = queryForInt("SELECT last_insert_id()");
            for (ProductInOrder p : o.getProducts()) {
                update("INSERT INTO pizzburgdb.OrdersProducts (orderID, productID, groupID, count) VALUES (?,?,?,?)",
                        id, p.getProduct().getId(), p.getGroup().getId(), p.getCount());
            }
        } catch (DataAccessException ex) {
            logger.error("add order fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return null;
    }

    public Order getOrder(int id) {
        List<Order> result = query("SELECT * FROM pizzburgdb.Orders WHERE orderID=?", new Object[]{id}, new OrderMapper());
        if (result.isEmpty()) {
            logger.warn("Access to order which doesn't exist");
            return null;
        } else if (result.size() != 1) {
            logger.warn("database schema error: Not unique result from query. Get " + result.size() + " orders");
            return result.get(0);
        } else {
            return result.get(0);
        }
    }

    public String updateOrder(Order o, List<ProductInOrder> initialProducts) {
        int result = 0;
        try {

            if (o.getAlternateDiscountCard() != null) {
                int cardExists = queryForInt("SELECT COUNT(*) FROM pizzburgdb.DiscountCards " +
                        "WHERE cardNumber=?", o.getAlternateDiscountCard().getId());
                if (cardExists == 0) {
                    return DatabaseErrors.DISCOUNT_CARD_NOT_FOUND;
                }
            }

            int clientExists = queryForInt("SELECT COUNT(*) FROM pizzburgdb.Clients WHERE phone=?", o.getClient().getPhone());
            if (clientExists == 0) {
                update("INSERT INTO pizzburgdb.Clients (phone, defaultAddress, name) VALUES (?, ?, ?)",
                        o.getClient().getPhone(), o.getShipmentAddress(), o.getClient().getName());
            } else {
                update("UPDATE pizzburgdb.Clients SET name = ? WHERE phone = ?",
                        o.getClient().getName(), o.getClient().getPhone());
            }

            result = update("UPDATE pizzburgdb.Orders " +
                    "SET clientPhone=?, shipmentAddress=?, orderStatus=?, carrier=?, alternateDiscountCard=?, alternateDiscount=?, personsCount=?, comment=? " +
                    "WHERE orderID=?",
                    o.getClient().getPhone(),
                    o.getShipmentAddress(),
                    o.getStatus().getId(),
                    (o.getCarrier() == null ? null : o.getCarrier().getId()),
                    (o.getAlternateDiscountCard() == null ? null : o.getAlternateDiscountCard().getId()),
                    (o.getAlternateDiscount() > 0 ? o.getAlternateDiscount() : null),
                    o.getPersonsCount(),
                    o.getComment(),
                    o.getId());
        } catch (DataAccessException ex) {
            logger.error("update order fails", ex);
            return DatabaseErrors.UNKNOWN;
        }

        if (result == 0) {
            return DatabaseErrors.ORDER_NOT_FOUND;
        }

        // adding new and updating modified products
        for (ProductInOrder pioNew : o.getProducts()) {
            boolean contains = false;
            boolean changed = false;
            for (ProductInOrder pioOld : initialProducts) {
                if (pioNew.getProduct().getId() == pioOld.getProduct().getId()
                        && pioNew.getGroup().getId() == pioOld.getGroup().getId()) {
                    contains = true;
                    if (pioNew.getCount() != pioOld.getCount()) {
                        changed = true;
                    }
                    break;
                }
            }
            if (contains && changed) {
                update("UPDATE pizzburgdb.OrdersProducts SET count=? WHERE productID=? AND orderID=? AND groupID=?",
                        pioNew.getCount(), pioNew.getProduct().getId(), o.getId(), pioNew.getGroup().getId());
            } else if (!contains) {
                update("INSERT INTO pizzburgdb.OrdersProducts (productID, groupID, orderID, count) VALUES (?,?,?,?)",
                        pioNew.getProduct().getId(), pioNew.getGroup().getId(), o.getId(), pioNew.getCount());
            }
        }

        // delete removed ingredients
        for (ProductInOrder pioOld : initialProducts) {
            boolean removed = true;
            for (ProductInOrder pioNew : o.getProducts()) {
                if (pioNew.getProduct().getId() == pioOld.getProduct().getId()) {
                    removed = false;
                    break;
                }
            }
            if (removed) {
                update("DELETE FROM pizzburgdb.OrdersProducts WHERE productID=? AND orderID=? AND groupID=?",
                        pioOld.getProduct().getId(), o.getId(), pioOld.getGroup().getId());
            }
        }

        return null;
    }

    public String removeOrder(int id) {
        int result;
        try {
            result = update("DELETE FROM pizzburgdb.Orders WHERE orderID=?", id);
        } catch (DataAccessException ex) {
            logger.error("remove order fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return result == 1 ? null : DatabaseErrors.ORDER_NOT_FOUND;
    }

    public List<Status> getStatuses() {
        return query("SELECT * FROM pizzburgdb.OrderStatuses", new StatusMapper());
    }

    public Status getOrderStatus(Order o) {
        List<Status> result = query("SELECT * FROM pizzburgdb.OrderStatuses "
                + "LEFT JOIN pizzburgdb.Orders ON Orders.orderStatus = OrderStatuses.statusID WHERE orderID=?", new Object[]{o.getId()}, new StatusMapper());
        if (result.isEmpty()) {
            logger.warn("Access to status which doesn't exist");
            return null;
        } else if (result.size() != 1) {
            logger.warn("database schema error: Not unique result from query. Get " + result.size() + " statuses");
            return result.get(0);
        } else {
            return result.get(0);
        }
    }

    public Status getStatus(int id) {
        List<Status> result = query("SELECT * FROM pizzburgdb.OrderStatuses WHERE statusID=?", new Object[]{id}, new StatusMapper());
        if (result.isEmpty()) {
            logger.warn("Access to status which doesn't exist");
            return null;
        } else if (result.size() != 1) {
            logger.warn("database schema error: Not unique result from query. Get " + result.size() + " statuses");
            return result.get(0);
        } else {
            return result.get(0);
        }
    }

    public String updateStatus(Status status) {
        int result = 0;
        try {
            result = update("UPDATE pizzburgdb.OrderStatuses SET description=? WHERE statusID=?",
                    status.getDescription(), status.getId());
        } catch (DataAccessException ex) {
            logger.error("update status fails", ex);
        }
        return result == 1 ? null : DatabaseErrors.STATUS_NOT_EXIST;
    }

    public float getOrderSum(int orderId) {
        List<Float> result = query("SELECT cost FROM pizzburgdb.Orders WHERE orderID = ?",
                new Object[]{orderId},
                new SingleColumnRowMapper<Float>());
        return result == null || result.isEmpty() || result.get(0) == null ? 0 : result.get(0);
    }
}