package ru.pizzburg.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.pizzburg.constants.SortStrings;
import ru.pizzburg.utils.Converter;
import ru.pizzburg.utils.Digest;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.web.domain.WorkstationUser;
import ru.pizzburg.web.service.admin.AdminManager;
import ru.pizzburg.web.service.admin.FormUser;
import ru.pizzburg.web.service.admin.FormUserValidator;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Controller
@SessionAttributes("formUser")
@Lazy(false)
public class AdminController {

    private static final String ADMIN_VIEW_NAME = "admin";
    public static final String RESULT_COUNT_COOKIE_SUFFIX = "_admin_resultCount";
    protected final Log logger = LogFactory.getLog(getClass());
    @Autowired
    private AdminManager adminManager;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private FormUserValidator formUserValidator;

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String getPage(HttpServletRequest request, ModelMap model, Principal principal) {
        FormUser fu = new FormUser();
        model.addAttribute("formUser", fu);
        model.addAttribute("users", adminManager.getUserList(parsePagingParams(request, principal), parseSortingParams(request)));
        model.addAttribute("allRoles", adminManager.getRoleList());
        return ADMIN_VIEW_NAME;
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/admin", method = RequestMethod.POST)
    public String addUser(HttpServletRequest request, ModelMap model, @ModelAttribute("formUser") FormUser addUser,
                          BindingResult result, SessionStatus status, RedirectAttributes redirectAttributes, Principal principal) {
        formUserValidator.validate(addUser, result);

        if (!result.hasErrors()) {
            //form success
            WorkstationUser user = new WorkstationUser();
            user.setLogin(addUser.getLogin());
            user.setName(addUser.getName());
            user.setMiddleName(addUser.getMiddleName());
            user.setPasswordDigest(Digest.getSha1Hash(addUser.getPass()));
            user.setSurname(addUser.getSurname());
            user.setRole(addUser.getRole());

            String additionResult = adminManager.addUser(user);
            if (additionResult != null) {
                model.addAttribute("errorMessage", messageSource.getMessage(additionResult, null, null));
                model.addAttribute("users", adminManager.getUserList(parsePagingParams(request, principal), parseSortingParams(request)));
                model.addAttribute("allRoles", adminManager.getRoleList());
                return ADMIN_VIEW_NAME;
            } else {
                redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.user-successfully-added", null, null));
                status.setComplete();
                return "redirect:/" + ADMIN_VIEW_NAME;
            }
        } else {
            model.addAttribute("users", adminManager.getUserList(parsePagingParams(request, principal), parseSortingParams(request)));
            model.addAttribute("allRoles", adminManager.getRoleList());
            return ADMIN_VIEW_NAME;
        }
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/admin/edit/{id}", method = RequestMethod.POST)
    public String updateUser(HttpServletRequest request, ModelMap model, @ModelAttribute("formUser") FormUser editUser,
                             BindingResult result, SessionStatus status, RedirectAttributes redirectAttributes, Principal principal) {
        formUserValidator.validate(editUser, result);

        if (!result.hasErrors()) {
            //form success
            WorkstationUser user = new WorkstationUser();
            user.setId(editUser.getId());
            user.setLogin(editUser.getLogin());
            user.setName(editUser.getName());
            user.setMiddleName(editUser.getMiddleName());
            user.setSurname(editUser.getSurname());
            user.setRole(editUser.getRole());
            if (editUser.getPass() != null && !editUser.getPass().isEmpty()) {
                user.setPasswordDigest(Digest.getSha1Hash(editUser.getPass()));
            }

            String updateResult = adminManager.updateUser(user);
            if (updateResult != null) {
                model.addAttribute("errorMessage", messageSource.getMessage(updateResult, null, null));
                model.addAttribute("users", adminManager.getUserList(parsePagingParams(request, principal), parseSortingParams(request)));
                model.addAttribute("allRoles", adminManager.getRoleList());
                return ADMIN_VIEW_NAME;
            } else {
                redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.user-successfully-updated", null, null));
                status.setComplete();
                return "redirect:/" + ADMIN_VIEW_NAME;
            }
        } else {
            model.addAttribute("users", adminManager.getUserList(parsePagingParams(request, principal), parseSortingParams(request)));
            model.addAttribute("allRoles", adminManager.getRoleList());
            return ADMIN_VIEW_NAME;
        }
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/admin/edit/{id}", method = RequestMethod.GET)
    public String getEditForm(HttpServletRequest request, ModelMap model, @PathVariable int id,
                              RedirectAttributes redirectAttributes, Principal principal) {
        WorkstationUser u = adminManager.getUser(id);
        if (u == null) {
            logger.warn("Null user request");
            redirectAttributes.addFlashAttribute("errorMessage", messageSource.getMessage("error.user-not-exist", null, null));
            return "redirect:/" + ADMIN_VIEW_NAME;
        }
        FormUser fu = new FormUser();
        fu.setId(u.getId());
        fu.setLogin(u.getLogin());
        fu.setMiddleName(u.getMiddleName());
        fu.setName(u.getName());
        fu.setSurname(u.getSurname());
        fu.setRoleStr(String.valueOf(u.getRole().getId()));
        model.addAttribute("formUser", fu);
        model.addAttribute("users", adminManager.getUserList(parsePagingParams(request, principal), parseSortingParams(request)));
        model.addAttribute("allRoles", adminManager.getRoleList());
        return ADMIN_VIEW_NAME;
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/admin/remove/{id}", method = RequestMethod.GET)
    public String removeUser(HttpServletRequest request, ModelMap model, @PathVariable int id,
                             RedirectAttributes redirectAttributes, Principal principal) {
        if (model.get("formUser") == null) {
            model.addAttribute("formUser", new FormUser());
        }

        String removeResult = adminManager.removeUser(id);
        if (removeResult != null) {
            model.addAttribute("errorMessage", messageSource.getMessage(removeResult, null, null));
            model.addAttribute("users", adminManager.getUserList(parsePagingParams(request, principal), parseSortingParams(request)));
            model.addAttribute("allRoles", adminManager.getRoleList());
            return ADMIN_VIEW_NAME;
        } else {
            redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.user-successfully-removed", null, null));
            return "redirect:/" + ADMIN_VIEW_NAME;
        }
    }

    private SortingParams parseSortingParams(HttpServletRequest request) {
        String sort = request.getParameter("sort");
        String dir = request.getParameter("dir");
        if (sort == null || dir == null || !SortStrings.isDirectionCorrect(dir)
                || !SortStrings.isAdminCorrect(sort)) {
            return null;
        }
        return new SortingParams(sort, Converter.parseSortOrder(dir));
    }

    private PaginationParams parsePagingParams(HttpServletRequest request, Principal principal) {
        String pageStr = request.getParameter("page");
        String resultsStr = null;
        String cookieName = Digest.getSha1Hash(principal.getName() + RESULT_COUNT_COOKIE_SUFFIX);
        for (Cookie c : request.getCookies()) {
            if (cookieName.equals(c.getName())) {
                resultsStr = c.getValue();
                break;
            }
        }
        if (pageStr == null && resultsStr == null) {
            return null;
        }
        int page = -1;
        if (pageStr != null) {
            try {
                page = Integer.parseInt(pageStr);
            } catch (NumberFormatException ex) {
                logger.info("invalid paging params (admin page): " + pageStr);
            }
        }
        int results = -1;
        if (resultsStr != null) {
            try {
                results = Integer.parseInt(resultsStr);
            } catch (NumberFormatException ex) {
                logger.info("invalid paging params (admin page): " + pageStr);
            }
        }
        return new PaginationParams(page, results);
    }
}
