package ru.pizzburg.web.service.ingredients;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class IngredientFilter {

    private String title;
    private String description;
    private String units;
    private float priceFrom = -1;
    private float priceTo = -1;
    private float balanceFrom = -1;
    private float balanceTo = -1;
    private boolean isEmpty = true;

    public float getPriceFrom() {
        return priceFrom;
    }

    public void setPriceFrom(float priceFrom) {
        this.priceFrom = priceFrom;
    }

    public float getPriceTo() {
        return priceTo;
    }

    public void setPriceTo(float priceTo) {
        this.priceTo = priceTo;
    }

    public float getBalanceFrom() {
        return balanceFrom;
    }

    public void setBalanceFrom(float balanceFrom) {
        this.balanceFrom = balanceFrom;
    }

    public float getBalanceTo() {
        return balanceTo;
    }

    public void setBalanceTo(float balanceTo) {
        this.balanceTo = balanceTo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public boolean getIsEmpty() {
        return isEmpty;
    }

    public void setIsEmpty(boolean empty) {
        isEmpty = empty;
    }

    @Override
    public String toString() {
        return "IngredientFilter{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", units='" + units + '\'' +
                ", priceFrom=" + priceFrom +
                ", priceTo=" + priceTo +
                ", balanceFrom=" + balanceFrom +
                ", balanceTo=" + balanceTo +
                ", isEmpty=" + isEmpty +
                '}';
    }
}
