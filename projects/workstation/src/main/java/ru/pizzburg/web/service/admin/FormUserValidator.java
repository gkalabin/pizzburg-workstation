package ru.pizzburg.web.service.admin;

import org.springframework.context.annotation.Lazy;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.pizzburg.constants.FieldLength;
import ru.pizzburg.utils.StringUtils;
import ru.pizzburg.web.domain.Role;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Lazy(false)
public class FormUserValidator implements Validator {


    @Override
    public boolean supports(Class clazz) {
        return FormUser.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "error.empty-login", "Empty login");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "error.empty-name", "Empty name");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "error.empty-surname", "Empty surname");
        FormUser fu = (FormUser) obj;
        checkFieldLength(fu, errors);

        if (fu.getId() == -1) {
            // add user

            if (StringUtils.isStringEmptyOrWhitespaces(fu.getPass()) && StringUtils.isStringEmptyOrWhitespaces(fu.getPassConfirm())) {
                // password and its' confirm both empty
                errors.rejectValue("pass", "error.empty-pass", "Empty password");
                errors.rejectValue("passConfirm", "error.empty-pass", "Empty password");
            } else if (!fu.getPass().equals(fu.getPassConfirm())) {
                errors.rejectValue("pass", "error.passwords-not-equal", "Passwords aren't equals");
                errors.rejectValue("passConfirm", "error.passwords-not-equal", "Passwords aren't equals");
            }
        } else if (!"".equals(fu.getPass()) || !"".equals(fu.getPassConfirm())) {
            if (!fu.getPass().equals(fu.getPassConfirm())) {
                errors.rejectValue("pass", "error.passwords-not-equal", "Passwords aren't equals");
                errors.rejectValue("passConfirm", "error.passwords-not-equal", "Passwords aren't equals");
            }
        }

        try {
            int roleId = Integer.parseInt(fu.getRoleStr());
            if (roleId < 0) {
                errors.rejectValue("roleStr", "error.role-negative-id", "Role error");
            } else {
                final Role role = new Role();
                role.setId(roleId);
                fu.setRole(role);
            }
        } catch (NumberFormatException ex) {
            errors.rejectValue("roleStr", "error.role-not-int-id", "Role error");
        }
    }

    private void checkFieldLength(FormUser fu, Errors errors) {
        if (fu.getLogin().length() > FieldLength.ADMIN_LOGIN) {
            errors.rejectValue("login", "error.too-long-value",
                    new Object[]{FieldLength.ADMIN_LOGIN}, "Max length is " + FieldLength.ADMIN_LOGIN);
        }
        if (fu.getName().length() > FieldLength.ADMIN_NAME) {
            errors.rejectValue("name", "error.too-long-value",
                    new Object[]{FieldLength.ADMIN_NAME}, "Max length is " + FieldLength.ADMIN_NAME);
        }
        if (fu.getSurname().length() > FieldLength.ADMIN_SURNAME) {
            errors.rejectValue("surname", "error.too-long-value",
                    new Object[]{FieldLength.ADMIN_SURNAME}, "Max length is " + FieldLength.ADMIN_SURNAME);
        }
        if (fu.getMiddleName().length() > FieldLength.ADMIN_MIDDLE_NAME) {
            errors.rejectValue("middleName", "error.too-long-value",
                    new Object[]{FieldLength.ADMIN_MIDDLE_NAME}, "Max length is " + FieldLength.ADMIN_MIDDLE_NAME);
        }
        if (fu.getPass().length() > FieldLength.ADMIN_PASSWORD) {
            errors.rejectValue("pass", "error.too-long-value",
                    new Object[]{FieldLength.ADMIN_PASSWORD}, "Max length is " + FieldLength.ADMIN_PASSWORD);
        }
        if (fu.getPassConfirm().length() > FieldLength.ADMIN_PASSWORD) {
            errors.rejectValue("passConfirm", "error.too-long-value",
                    new Object[]{FieldLength.ADMIN_PASSWORD}, "Max length is " + FieldLength.ADMIN_PASSWORD);
        }
    }
}
