package ru.pizzburg.web.repo.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import ru.pizzburg.web.domain.IngredientInProduct;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class IngredientInProductMapper implements ParameterizedRowMapper<IngredientInProduct> {

    @Override
    public IngredientInProduct mapRow(ResultSet rs, int rowNum) throws SQLException {
        IngredientInProduct ingredientInProduct = new IngredientInProduct();
        ingredientInProduct.getIngredient().setId(rs.getInt("ingredientID"));
        ingredientInProduct.setCount(rs.getFloat("count"));
        ingredientInProduct.getIngredient().setTitle(rs.getString("title"));
        ingredientInProduct.getIngredient().setPrice(rs.getFloat("price"));
        ingredientInProduct.getIngredient().setDescription(rs.getString("description"));
        ingredientInProduct.getIngredient().setUnits(rs.getString("units"));
        return ingredientInProduct;
    }
}