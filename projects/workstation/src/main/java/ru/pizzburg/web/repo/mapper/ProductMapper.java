package ru.pizzburg.web.repo.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import ru.pizzburg.web.domain.Product;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class ProductMapper implements ParameterizedRowMapper<Product> {

    @Override
    public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
        Product product = new Product();
        product.setId(rs.getInt("productID"));
        product.setTitle(rs.getString("title"));
        product.setDescription(rs.getString("description"));
        return product;
    }
}