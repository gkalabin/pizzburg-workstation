package ru.pizzburg.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.pizzburg.web.repo.StreetDao;

import java.util.List;

import static java.util.Collections.EMPTY_LIST;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Service
@Lazy(false)
public class StreetsManager {

    private static final int MIN_PREFIX_LENGTH = 3;
    private static final int MAX_STREET_SUGGESTIONS_COUNT = 10;
    private StreetDao streetDao;

    public List<String> getStreetsList(String startsWith, int maxRows) {
        if (startsWith == null || startsWith.length() < MIN_PREFIX_LENGTH) {
            return EMPTY_LIST;
        }
        return streetDao.getStreetsList(startsWith, Math.min(maxRows, MAX_STREET_SUGGESTIONS_COUNT));
    }

    @Autowired
    public void setStreetDao(StreetDao streetDao) {
        this.streetDao = streetDao;
    }
}
