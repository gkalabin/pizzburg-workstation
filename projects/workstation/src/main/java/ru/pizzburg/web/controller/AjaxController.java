package ru.pizzburg.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.pizzburg.utils.StringUtils;
import ru.pizzburg.utils.NameUtils;
import ru.pizzburg.utils.PhoneUtils;
import ru.pizzburg.web.domain.Client;
import ru.pizzburg.web.domain.Ingredient;
import ru.pizzburg.web.service.StreetsManager;
import ru.pizzburg.web.service.clients.ClientsManager;
import ru.pizzburg.web.service.discount.DiscountManager;
import ru.pizzburg.web.service.ingredients.IngredientsManager;
import ru.pizzburg.web.service.orders.OrdersManager;
import ru.pizzburg.web.service.products.ProductsManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Controller
@Lazy(false)
public class AjaxController {

    protected final Log logger = LogFactory.getLog(getClass());
    @Autowired
    private ClientsManager clientsManager;
    @Autowired
    private IngredientsManager ingredientsManager;
    @Autowired
    private DiscountManager discountManager;
    @Autowired
    private ProductsManager productsManager;
    @Autowired
    private StreetsManager streetsManager;
    @Autowired
    private OrdersManager ordersManager;

    /**
     * streets autocomplete
     *
     * @return map contains list of streets
     */
	@Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/streetsAutocomplete", method = RequestMethod.GET)
    public
    @ResponseBody
    Map<String, List<String>> getStreets(@RequestParam int maxRows, @RequestParam String startsWith) {
        Map<String, List<String>> result = new HashMap<String, List<String>>();
        result.put("streets", streetsManager.getStreetsList(startsWith, maxRows));
        return result;
    }

    /**
     * Get clients autocomplete on orders page
     *
     * @return map contains list of phone numbers, converted to string
     */
	@Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/orders/clientsAutocomplete", method = RequestMethod.GET)
    public
    @ResponseBody
    Map<String, List<String>> getClients(@RequestParam int maxRows, @RequestParam String startsWith) {
        Map<String, List<String>> result = new HashMap<String, List<String>>();
        result.put("clients", clientsManager.getClientsPhones(startsWith.replaceAll("\\D", ""), maxRows));
        return result;
    }

	@Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/orders/getClientAddresses", method = RequestMethod.GET)
    public
    @ResponseBody
    List<String> getClientAddress(@RequestParam String phone) {
        return clientsManager.getClientAddresses(Long.valueOf(phone.replaceAll("\\D", "")));
    }

	@Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/orders/getPricesAndDiscount", method = RequestMethod.GET)
    public
    @ResponseBody
    DataForOrderCreation getPricesAndDiscount(@RequestParam(required = false) String phone,
                                              @RequestParam(value = "products", required = false) String[] products,
                                              @RequestParam(value = "groups", required = false) String[] groups,
                                              @RequestParam(value = "counts", required = false) String[] counts,
                                              @RequestParam String alternateDiscount,
                                              @RequestParam String alternateCard,
                                              @RequestParam(required = true) int orderId) {
        float orderSum = 0;
        float discount = -1;
        float alternateDiscountFloat = -1;
        float[] prices = null;
        DataForOrderCreation pad = new DataForOrderCreation();

        if (phone.length() == PhoneUtils.PHONE_LOCAL_DIGITS_COUNT) {
            phone = PhoneUtils.PHONE_LOCAL_PREFIX + phone;
        }

        if (prices != null || groups != null) {
            if (products.length != groups.length) {
                logger.warn("getPricesAndDiscount: not equal array lengths");
            } else {
                int arrayLength = products.length;
                prices = new float[arrayLength];
                for (int i = 0; i < arrayLength; i++) {
                    try {
                        int count = Integer.parseInt(counts[i]);
                        int pId = Integer.parseInt(products[i]);
                        int gId = Integer.parseInt(groups[i]);
                        final float productPrice = productsManager.getProductPrice(pId, gId);
                        prices[i] = productPrice;
                        orderSum += productPrice * count;
                    } catch (NumberFormatException ex) {
                        logger.warn("getPricesAndDiscount: invalid products/groups ids");
                    }
                }
            }
        }
        pad.setPrices(prices);
        pad.setOrderSum(orderSum);

        float clientSum = 0;
        if (phone != null) {
            try {
                Long phoneNumber = Long.valueOf(phone.replaceAll("\\D", ""));
                Client c = clientsManager.getClient(phoneNumber);
                if (c != null) {
                    if (c.getCard() != null) {
                        pad.setCardNumber(c.getCard().getIdFormatted());
                    }
                    pad.setFullName(NameUtils.getFullNameFormatted(c.getName(), c.getSurname(), c.getMiddleName()));
                    pad.setName(c.getName());
                    if (StringUtils.isStringEmptyOrWhitespaces(alternateCard)) {
                        clientSum = c.getOrdersSum();
                    }
                }
            } catch (NumberFormatException ex) {
                // phone not full. it's normal situation
            }
        }
        boolean isAlternateCardUsable = false;
        if (!StringUtils.isStringEmptyOrWhitespaces(alternateCard)) {
            float sumForCard = discountManager.getSumForCard(alternateCard);
            isAlternateCardUsable = sumForCard > 0;
            clientSum = sumForCard;
        }

        try {
            alternateDiscountFloat = Float.parseFloat(alternateDiscount);
            if (Float.isNaN(alternateDiscountFloat)) {
                alternateDiscountFloat = -1;
            }
        } catch (NumberFormatException ex) {
            // alternate discount has wrong format
        }

        // remove double counting of current order
        clientSum -= ordersManager.getOrderSum(orderId);
        discount = discountManager.getDiscountBySum(clientSum + orderSum);
        pad.setDiscount(Math.max(discount, alternateDiscountFloat));

        pad.setCardWillBeAssigned(!(pad.cardNumber != null &&
                !StringUtils.isStringEmptyOrWhitespaces(pad.cardNumber)) &&
                !isAlternateCardUsable &&
                pad.orderSum + clientSum >= discountManager.getMinimalSum());

        if (isAlternateCardUsable) {
            pad.setDiscountReason(DataForOrderCreation.REASON_ALTERNATE_CARD);
        } else if (pad.discount == alternateDiscountFloat) {
            pad.setDiscountReason(DataForOrderCreation.REASON_CUSTOM_DISCOUNT);
        } else if (pad.discount > 0) {
            pad.setDiscountReason(DataForOrderCreation.REASON_CLIENT_CARD);
        }

        return pad;
    }

	@Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/products/ingredientChange", method = RequestMethod.GET)
    public
    @ResponseBody
    String getIngredientUnits(@RequestParam int id) {
        final Ingredient ingredient = ingredientsManager.getIngredient(id);
        return ingredient == null ? null : ingredient.getUnits();
    }

    public static class DataForOrderCreation {
        public static final String REASON_CLIENT_CARD = "client card";
        public static final String REASON_ALTERNATE_CARD = "alternate card";
        public static final String REASON_CUSTOM_DISCOUNT = "custom";

        private float[] prices;
        private float discount;
        private float orderSum;
        private String fullName;
        private String name;
        private String cardNumber;
        private boolean cardWillBeAssigned;
        private String discountReason;

        public float getOrderSum() {
            return orderSum;
        }

        public void setOrderSum(float orderSum) {
            this.orderSum = orderSum;
        }

        public float getDiscount() {
            return discount;
        }

        public float[] getPrices() {
            return prices;
        }

        public void setDiscount(float discount) {
            this.discount = discount;
        }

        public void setPrices(float[] prices) {
            this.prices = prices;
        }

        public String getCardNumber() {
            return cardNumber;
        }

        public void setCardNumber(String cardNumber) {
            this.cardNumber = cardNumber;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public boolean getCardWillBeAssigned() {
            return cardWillBeAssigned;
        }

        public void setCardWillBeAssigned(boolean cardWillBeAssigned) {
            this.cardWillBeAssigned = cardWillBeAssigned;
        }

        public String getDiscountReason() {
            return discountReason;
        }

        public void setDiscountReason(String discountReason) {
            this.discountReason = discountReason;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
