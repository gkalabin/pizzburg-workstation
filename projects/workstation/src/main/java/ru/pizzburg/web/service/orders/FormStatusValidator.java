package ru.pizzburg.web.service.orders;

import org.springframework.context.annotation.Lazy;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.pizzburg.constants.FieldLength;
import ru.pizzburg.web.domain.Status;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Lazy(false)
public class FormStatusValidator implements Validator {

    @Override
    public boolean supports(Class clazz) {
        return FormStatus.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        FormStatus fs = (FormStatus) obj;
        checkFieldLength(fs, errors);
    }

    private void checkFieldLength(FormStatus fo, Errors errors) {
        if (fo.getDescription().length() > FieldLength.ORDER_STATUS_DESCRIPTION) {
            errors.rejectValue("description", "error.too-long-value",
                    new Object[]{FieldLength.ORDER_STATUS_DESCRIPTION}, "Max length is " + FieldLength.ORDER_STATUS_DESCRIPTION);
        }
    }

    public void validateStatus(Object obj, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "statusIdStr", "error.empty-status", "Empty status");
        FormOrder fo = (FormOrder) obj;
        if (errors.getFieldErrorCount("statusIdStr") == 0) {
            try {
                int statusId = Integer.parseInt(fo.getStatusIdStr());
                if (statusId < 0) {
                    errors.rejectValue("statusIdStr", "error.status-negative-id", "Status error");
                } else {
                    final Status status = new Status();
                    status.setId(statusId);
                    fo.setStatus(status);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("statusIdStr", "error.status-not-int-id", "Status error");
            }
        }
    }
}
