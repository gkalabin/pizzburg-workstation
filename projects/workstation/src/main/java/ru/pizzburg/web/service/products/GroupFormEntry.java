package ru.pizzburg.web.service.products;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class GroupFormEntry {

    private String id = "";
    private String price = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "GroupFormEntry{" + "id=" + id + ", price=" + price + '}';
    }
}
