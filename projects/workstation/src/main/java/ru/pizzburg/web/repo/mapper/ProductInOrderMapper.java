package ru.pizzburg.web.repo.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import ru.pizzburg.web.domain.ProductInOrder;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class ProductInOrderMapper implements ParameterizedRowMapper<ProductInOrder> {

    @Override
    public ProductInOrder mapRow(ResultSet rs, int rowNum) throws SQLException {
        ProductInOrder pio = new ProductInOrder();
        pio.getProduct().setId(rs.getInt("productID"));
        pio.getProduct().setTitle(rs.getString("productTitle"));
        pio.getProduct().setDescription(rs.getString("productDescription"));
        pio.setCount(rs.getInt("count"));
        pio.getGroup().setId(rs.getInt("groupID"));
        pio.getGroup().setTitle(rs.getString("groupTitle"));
        pio.getGroup().setDescription(rs.getString("groupDescription"));
        pio.setPrice(rs.getFloat("price"));
        return pio;
    }
}