package ru.pizzburg.web.domain;

import java.util.List;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class ProductGroup {

    private int id;
    private String title;
    private String description;
    private List<ProductInGroup> products;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ProductInGroup> getProducts() {
        return products;
    }

    public void setProducts(List<ProductInGroup> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "ProductGroup{" + "id=" + id + ", title=" + title + ", description=" + description + ", products=" + products + '}';
    }
}
