package ru.pizzburg.web.service.discount;

import org.springframework.context.annotation.Lazy;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Lazy(false)
public class FormDiscountValidator implements Validator {

    @Override
    public boolean supports(Class clazz) {
        return FormDiscount.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "sumFromStr", "error.empty-sum", "Empty sum");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "discountStr", "error.empty-discount", "Empty discount");
        FormDiscount fd = (FormDiscount) obj;
        if (errors.getFieldErrorCount("sumFromStr") == 0) {
            try {
                int sum = Integer.valueOf(fd.getSumFromStr());
                if (sum <= 0) {
                    errors.rejectValue("sumFromStr", "error.negative-sum", "Sum must be > 0");
                } else {
                    fd.setSumFrom(sum);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("sumFromStr", "typeMismatch.int", "Not convertable to number");
            }
        }
        if (errors.getFieldErrorCount("discountStr") == 0) {
            try {
                fd.setDiscountStr(fd.getDiscountStr().replace(",", "."));
                float discount = Float.valueOf(fd.getDiscountStr());
                if (discount <= 0) {
                    errors.rejectValue("discountStr", "error.negative-discount", "Discount must be > 0");
                } else {
                    fd.setDiscount(discount);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("discountStr", "typeMismatch.float", "Not convertable to number");
            }
        }
    }
}