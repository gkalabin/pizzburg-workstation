package ru.pizzburg.web.service.ingredients;

import org.displaytag.properties.SortOrderEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.pizzburg.utils.data.PageableList;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.web.domain.Ingredient;
import ru.pizzburg.web.repo.IngredientDao;

import java.util.List;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Service
@Lazy(false)
public class IngredientsManager {

    private static final PaginationParams DEFAULT_PAGINATION_PARAMS;
    private static final SortingParams DEFAULT_SORTING_PARAMS;
    private static final int MAX_OBJECTS_PER_PAGE = 100;
    private IngredientDao ingredientDao;

    static {
        DEFAULT_PAGINATION_PARAMS = new PaginationParams();
        DEFAULT_SORTING_PARAMS = new SortingParams("title", SortOrderEnum.ASCENDING);
    }

    @Autowired
    public void setIngredientDao(IngredientDao IngredientDao) {
        this.ingredientDao = IngredientDao;
    }

    public PageableList<Ingredient> getIngredients(PaginationParams pp, SortingParams sp, IngredientFilter filter) {
        if (pp == null) {
            pp = DEFAULT_PAGINATION_PARAMS;
        } else {
            int page = pp.getPageNumber() <= 0 ? DEFAULT_PAGINATION_PARAMS.getPageNumber() : pp.getPageNumber();
            int results = pp.getObjectsPerPage() <= 0 || pp.getObjectsPerPage() > MAX_OBJECTS_PER_PAGE
                    ? DEFAULT_PAGINATION_PARAMS.getObjectsPerPage() : pp.getObjectsPerPage();
            pp = new PaginationParams(page, results);
        }
        if (sp == null) {
            sp = DEFAULT_SORTING_PARAMS;
        }
        return ingredientDao.getIngredients(pp, sp, filter);
    }

    public List<Ingredient> getIngredients() {
        return ingredientDao.getIngredients();
    }

    public String addIngredient(Ingredient i) {
        return ingredientDao.addIngredient(i);
    }

    public Ingredient getIngredient(int id) {
        return ingredientDao.getIngredient(id);
    }

    public String updateIngredient(Ingredient i) {
        return ingredientDao.updateIngredient(i);
    }

    public String updateIngredientBalance(int ingredientID, float newBalance) {
        return ingredientDao.updateIngredientBalance(ingredientID, newBalance);
    }

    public String removeIngredient(int id) {
        return ingredientDao.removeIngredient(id);
    }
}
