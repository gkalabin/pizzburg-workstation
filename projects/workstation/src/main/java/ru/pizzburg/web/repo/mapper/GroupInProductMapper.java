package ru.pizzburg.web.repo.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import ru.pizzburg.web.domain.GroupInProduct;
import ru.pizzburg.web.domain.ProductGroup;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class GroupInProductMapper implements ParameterizedRowMapper<GroupInProduct> {

    @Override
    public GroupInProduct mapRow(ResultSet rs, int rowNum) throws SQLException {
        GroupInProduct gip = new GroupInProduct();
        gip.setGroup(new ProductGroup());
        gip.getGroup().setId(rs.getInt("groupID"));
        gip.getGroup().setTitle(rs.getString("title"));
        gip.getGroup().setDescription(rs.getString("description"));
        gip.setPrice(rs.getFloat("price"));
        return gip;
    }
}