package ru.pizzburg.web.repo.mapper;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import ru.pizzburg.web.domain.DiscountCard;
import ru.pizzburg.web.domain.Order;
import ru.pizzburg.web.domain.Status;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class OrderMapper implements ParameterizedRowMapper<Order> {

    @Override
    public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
        Order order = new Order();
        order.setId(rs.getInt("orderID"));
        order.setRegisterDate(rs.getTimestamp("registerDate"));
        order.setShipmentAddress(rs.getString("shipmentAddress"));
        order.setPersonsCount(rs.getInt("personsCount"));
        order.setComment(rs.getString("comment"));

        if (rs.getInt("orderStatus") == Status.CANCELLED.getId()) {
            order.setCost(0);
        } else {
            order.setCost(rs.getFloat("cost"));
        }

        order.setDiscount(rs.getFloat("discount"));

        float discount = rs.getFloat("alternateDiscount");
        if (discount != 0) {
            order.setAlternateDiscount(discount);
        }
        long card = rs.getLong("alternateDiscountCard");
        if (card != 0) {
            DiscountCard dc = new DiscountCard();
            dc.setId(card);
            order.setAlternateDiscountCard(dc);
        }
        return order;
    }
}
