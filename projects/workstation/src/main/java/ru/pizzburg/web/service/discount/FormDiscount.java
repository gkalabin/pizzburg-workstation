package ru.pizzburg.web.service.discount;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class FormDiscount {

    private String sumFromStr = "";
    private String discountStr = "";
    private int sumFrom;
    private float discount;

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public String getDiscountStr() {
        return discountStr;
    }

    public void setDiscountStr(String discountStr) {
        this.discountStr = discountStr;
    }

    public int getSumFrom() {
        return sumFrom;
    }

    public void setSumFrom(int sumFrom) {
        this.sumFrom = sumFrom;
    }

    public String getSumFromStr() {
        return sumFromStr;
    }

    public void setSumFromStr(String sumFromStr) {
        this.sumFromStr = sumFromStr;
    }

    @Override
    public String toString() {
        return "FormDiscount{" + "sumFromStr=" + sumFromStr + ", discountStr=" + discountStr + ", sumFrom=" + sumFrom + ", discount=" + discount + '}';
    }
}
