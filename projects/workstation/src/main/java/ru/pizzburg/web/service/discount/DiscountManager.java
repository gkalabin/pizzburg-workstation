package ru.pizzburg.web.service.discount;

import java.util.HashMap;
import java.util.List;
import java.util.NavigableSet;
import java.util.TreeSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.pizzburg.utils.DiscountCardUtils;
import ru.pizzburg.web.domain.DiscountEntity;
import ru.pizzburg.web.repo.DiscountDao;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Service
@Lazy(false)
public class DiscountManager {

    @Autowired
    private DiscountDao discountDao;
    private HashMap<Integer, Float> discounts;
    private NavigableSet<Integer> sums;

    public List<DiscountEntity> getDiscounts() {
        final List<DiscountEntity> discountList = discountDao.getDiscounts();
        initData(discountList);
        return discountList;
    }

    public String addDiscount(DiscountEntity d) {
        return discountDao.addDiscount(d);
    }

    public float getDiscountBySum(float sum) {
        if (discounts == null || sums == null) {
            initData(discountDao.getDiscounts());
        }
        if (sum < sums.first()) {
            return 0;
        }
        if (sum > sums.last()) {
            return discounts.get(sums.last());
        }
        return discounts.get(sums.floor(Math.round(sum)));
    }

    private void initData(List<DiscountEntity> discountList) {
        discounts = new HashMap<Integer, Float>();
        for (DiscountEntity e : discountList) {
            discounts.put(e.getSumFrom(), e.getDiscount());
        }
        sums = new TreeSet<Integer>(discounts.keySet());
        sums.add(Integer.MAX_VALUE);
        sums.remove(0);
    }

    public DiscountEntity getDiscount(int sum) {
        return discountDao.getDiscount(sum);
    }

    public String updateDiscount(DiscountEntity d, int initialSum) {
        return discountDao.updateDiscount(d, initialSum);
    }

    public String removeDiscount(int sum) {
        return discountDao.removeDiscount(sum);
    }

    public float getSumForCard(String id) {
        long cardId = DiscountCardUtils.getDiscountCardNumber(id);
        return discountDao.getSumForCard(cardId);
    }

    public int getMinimalSum() {
        initData(discountDao.getDiscounts());
        // TODO: set starts with 0. workaround.
        return sums.ceiling(0);
    }
}
