package ru.pizzburg.web.repo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.pizzburg.constants.DatabaseErrors;
import ru.pizzburg.constants.SortStrings;
import ru.pizzburg.utils.*;
import ru.pizzburg.utils.data.PageableList;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.web.domain.Order;
import ru.pizzburg.web.domain.Role;
import ru.pizzburg.web.domain.WorkstationUser;
import ru.pizzburg.web.repo.mapper.FullUserMapper;
import ru.pizzburg.web.repo.mapper.RoleMapper;
import ru.pizzburg.web.repo.mapper.UserMapper;

import javax.sql.DataSource;
import java.util.List;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Repository
@Lazy(false)
public class AdminDao extends JdbcTemplate {
    private final Log logger = LogFactory.getLog(getClass());

    @Override
    @Autowired
    public void setDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    public PageableList<WorkstationUser> getUsers(PaginationParams pp, SortingParams sp) {
        PageableList<WorkstationUser> result = new PageableList<WorkstationUser>();
        result.setListSize(queryForInt("SELECT COUNT(*) FROM pizzburgdb.WorkstationUsers"));
        String orderString;
        String query = "SELECT * FROM pizzburgdb.WorkstationUsers %s LIMIT ?, ?";
        if (SortStrings.ADMIN_NAME_SORT_CRITERION.equals(sp.getSortCriterion())) {
            orderString = String.format("ORDER BY WorkstationUsers.surname %s, WorkstationUsers.name %s, WorkstationUsers.middleName %s",
                    Converter.getStringSortOrder(sp.getSortDirection()),
                    Converter.getStringSortOrder(sp.getSortDirection()), Converter.getStringSortOrder(sp.getSortDirection()));
        } else if (SortStrings.ADMIN_ROLE_SORT_CRITERION.equals(sp.getSortCriterion())) {
            orderString = String.format("ORDER BY WorkstationUsers.roleID %s",
                    Converter.getStringSortOrder(sp.getSortDirection()));
        } else {
            orderString = String.format("ORDER BY WorkstationUsers.%s %s",
                    sp.getSortCriterion(), Converter.getStringSortOrder(sp.getSortDirection()));
        }
        query = String.format(query, orderString);
        final int from = (pp.getPageNumber() - 1) * pp.getObjectsPerPage();
        List<WorkstationUser> users = query(query,
                new Object[]{from, pp.getObjectsPerPage()},
                new UserMapper());
        result.setData(users);
        result.setPaginationParams(pp);
        result.setSortingParams(sp);
        return result;
    }

    public String addUser(WorkstationUser u) {
        if (queryForInt("SELECT COUNT(*) FROM pizzburgdb.WorkstationUsers WHERE login=?", u.getLogin()) != 0) {
            return DatabaseErrors.USER_EXISTS;
        }
        try {
            update("INSERT INTO pizzburgdb.WorkstationUsers (login, name, surname, middleName, pass, roleID) VALUES (?,?,?,?,?,?)",
                    u.getLogin(), u.getName(), u.getSurname(), u.getMiddleName(), u.getPasswordDigest(), u.getRole().getId());
        } catch (DataAccessException ex) {
            logger.error("add user fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return null;
    }

    public WorkstationUser getUser(int id) {
        List<WorkstationUser> result = query("SELECT * FROM pizzburgdb.WorkstationUsers " +
                "LEFT JOIN pizzburgdb.Roles ON WorkstationUsers.roleID = Roles.roleID " +
                "WHERE userID=?", new Object[]{id}, new FullUserMapper());
        if (result.isEmpty()) {
            logger.warn("Access to user which doesn't exist");
            return null;
        } else if (result.size() != 1) {
            logger.warn("database schema error: Not unique result from query. Get " + result.size() + " users");
            return result.get(0);
        } else {
            return result.get(0);
        }
    }

    public WorkstationUser getUser(String login) {
        List<WorkstationUser> result = query(
                "SELECT * FROM pizzburgdb.WorkstationUsers " +
                        "LEFT JOIN pizzburgdb.Roles ON WorkstationUsers.roleID = Roles.roleID " +
                        "WHERE login=?",
                new Object[]{login},
                new FullUserMapper());
        if (result.isEmpty()) {
            logger.warn("Access to user which doesn't exist");
            return null;
        } else if (result.size() != 1) {
            logger.warn("database schema error: Not unique result from query. Get " + result.size() + " users");
            return result.get(0);
        } else {
            return result.get(0);
        }
    }

    public String updateUser(WorkstationUser u) {
        int exists = queryForInt("SELECT COUNT(*) FROM pizzburgdb.WorkstationUsers WHERE login = ? AND userID != ?",
                u.getLogin(), u.getId());
        if (exists != 0) {
            logger.warn("user exist (" + exists + ")");
            return DatabaseErrors.USER_EXISTS;
        }
        int result = 0;
        try {
            if (u.getPasswordDigest() == null) {
                result = update("UPDATE pizzburgdb.WorkstationUsers SET login=?, name=?, surname=?, middleName=?, roleID=? WHERE userID=?",
                        u.getLogin(), u.getName(), u.getSurname(), u.getMiddleName(), u.getRole().getId(), u.getId());
            } else {
                result = update("UPDATE pizzburgdb.WorkstationUsers SET login=?, name=?, surname=?, middleName=?, pass=?, roleID=? WHERE userID=?",
                        u.getLogin(), u.getName(), u.getSurname(), u.getMiddleName(), u.getPasswordDigest(), u.getRole().getId(), u.getId());
            }
        } catch (DataAccessException ex) {
            logger.error("update user fails", ex);
            return DatabaseErrors.UNKNOWN;
        }

        return result > 0 ? null: DatabaseErrors.USER_NOT_EXISTS;
    }

    public String removeUser(int id) {
        int result = 0;
        try {
            result = update("DELETE FROM pizzburgdb.WorkstationUsers WHERE userID=?", id);
        } catch (DataAccessException ex) {
            logger.error("delete user fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return result == 1 ? null : DatabaseErrors.USER_NOT_EXISTS;
    }

    public List<Role> getRoleList() {
        return query("SELECT * FROM pizzburgdb.Roles", new RoleMapper());
    }

    public WorkstationUser getOrderRegistrator(Order o) {
        List<WorkstationUser> result = query("SELECT * FROM pizzburgdb.WorkstationUsers "
                + "LEFT JOIN pizzburgdb.Orders ON Orders.registrator = WorkstationUsers.userID WHERE Orders.orderID=?", new Object[]{o.getId()}, new UserMapper());
        if (result.isEmpty()) {
            logger.warn("Access to user which doesn't exist");
            return null;
        } else if (result.size() != 1) {
            logger.warn("database schema error: Not unique result from query. Get " + result.size() + " users");
            return result.get(0);
        } else {
            return result.get(0);
        }
    }

    public WorkstationUser getOrderCarrier(Order o) {
        List<WorkstationUser> result = query("SELECT * FROM pizzburgdb.WorkstationUsers "
                + "LEFT JOIN pizzburgdb.Orders ON Orders.carrier = WorkstationUsers.userID WHERE Orders.orderID=?", new Object[]{o.getId()}, new UserMapper());
        if (result.isEmpty()) {
            // it's normal not to have carrier
            return null;
        } else if (result.size() != 1) {
            logger.warn("database schema error: Not unique result from query. Get " + result.size() + " users");
            return result.get(0);
        } else {
            return result.get(0);
        }
    }

    public Role getRole(int id) {
        List<Role> result = query("SELECT * FROM pizzburgdb.Roles WHERE roleID=?", new Object[]{id}, new RoleMapper());
        if (result.isEmpty()) {
            logger.warn("Access to role which doesn't exist");
            return null;
        } else if (result.size() != 1) {
            logger.warn("database schema error: Not unique result from query. Get " + result.size() + " roles");
            return result.get(0);
        } else {
            return result.get(0);
        }
    }

    public List<WorkstationUser> getUsersForRole(Role r) {
        return query("SELECT * FROM pizzburgdb.WorkstationUsers WHERE roleID = ? ORDER BY surname, name, middleName",
                new Object[]{r.getId()},
                new UserMapper());
    }
}
