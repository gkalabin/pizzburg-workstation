package ru.pizzburg.web.controller.stats;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import ru.pizzburg.constants.SortStrings;
import ru.pizzburg.utils.Converter;
import ru.pizzburg.utils.Digest;
import ru.pizzburg.utils.NameUtils;
import ru.pizzburg.utils.data.PageableList;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.web.domain.OperatorStat;
import ru.pizzburg.web.domain.Role;
import ru.pizzburg.web.domain.WorkstationUser;
import ru.pizzburg.web.service.admin.AdminManager;
import ru.pizzburg.web.service.stats.FormEmployeeStats;
import ru.pizzburg.web.service.stats.FormEmployeeStatsValidator;
import ru.pizzburg.web.service.stats.StatsManager;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Date;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Controller
@SessionAttributes({"formOperatorsStats"})
@Lazy(false)
public class OperatorsStatsController {

    private static final String OPERATORS_VIEW_NAME = "stats-operators";
    public static final String RESULT_COUNT_COOKIE_SUFFIX = "_stats_operators_resultCount";
    protected final Log logger = LogFactory.getLog(getClass());
    @Autowired
    private StatsManager statsManager;
    @Autowired
    private AdminManager adminManager;
    @Autowired
    private FormEmployeeStatsValidator formEmployeeStatsValidator;

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/stats/operators", method = RequestMethod.GET)
    public String getOperatorsStats(ModelMap model, HttpServletRequest request, Principal principal) {
        FormEmployeeStats formOperatorsStats = (FormEmployeeStats) model.get("formOperatorsStats");
        if (formOperatorsStats == null) {
            formOperatorsStats = new FormEmployeeStats();
        }

        model.addAttribute("formOperatorsStats", formOperatorsStats);
        model.addAttribute("operators", adminManager.getOperators());

        Date fromDate = formOperatorsStats.getFromDate();
        Date toDate = formOperatorsStats.getToDate();
        int operatorId = formOperatorsStats.getEmployeeId();
        if (fromDate != null && toDate != null &&
                operatorId > 0) {

            WorkstationUser operator = adminManager.getUser(formOperatorsStats.getEmployeeId());

            if (operator != null && operator.getRole() != null &&
                    operator.getRole().getId() == Role.ROLE_OPERATOR.getId()) {
                model.addAttribute("from", ru.pizzburg.utils.DateUtils.OUT_DATE_TIME_FORMAT.format(fromDate));
                model.addAttribute("to", ru.pizzburg.utils.DateUtils.OUT_DATE_TIME_FORMAT.format(toDate));
                model.addAttribute("operator", NameUtils.getFullNameFormatted(operator));
                PageableList<OperatorStat> operatorStats = statsManager.getOperatorStats(fromDate, toDate, operator,
                        parsePagingParams(request, principal), parseOperatorsSortingParams(request));
                float price = 0;
                float cost = 0;
                for (OperatorStat os : operatorStats.getList()) {
                    price += os.getPrice();
                    cost += os.getCost() * (100 - os.getOrder().getDiscount()) / 100;
                }
                model.addAttribute("price", price);
                model.addAttribute("cost", cost);
                model.addAttribute("stats", operatorStats);
            }
        }

        return OPERATORS_VIEW_NAME;
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/stats/operators/**", method = RequestMethod.POST)
    public String getOperatorsCustomStats(ModelMap model, HttpServletRequest request,
                                          @ModelAttribute("formOperatorsStats") FormEmployeeStats formOperatorsStats,
                                          BindingResult result, Principal principal) {
        formEmployeeStatsValidator.validate(formOperatorsStats, result);
        model.addAttribute("operators", adminManager.getOperators());

        if (!result.hasErrors()) {
            Date fromDate = formOperatorsStats.getFromDate();
            Date toDate = formOperatorsStats.getToDate();
            WorkstationUser operator = adminManager.getUser(formOperatorsStats.getEmployeeId());

            if (operator != null && operator.getRole() != null &&
                    operator.getRole().getId() == Role.ROLE_OPERATOR.getId()) {
                model.addAttribute("from", ru.pizzburg.utils.DateUtils.OUT_DATE_TIME_FORMAT.format(fromDate));
                model.addAttribute("to", ru.pizzburg.utils.DateUtils.OUT_DATE_TIME_FORMAT.format(toDate));
                model.addAttribute("operator", NameUtils.getFullNameFormatted(operator));
                PageableList<OperatorStat> operatorStats = statsManager.getOperatorStats(fromDate, toDate, operator,
                        parsePagingParams(request, principal), parseOperatorsSortingParams(request));
                float price = 0;
                float cost = 0;
                for (OperatorStat os : operatorStats.getList()) {
                    price += os.getPrice();
                    cost += os.getCost() * (100 - os.getOrder().getDiscount()) / 100;
                }
                model.addAttribute("price", price);
                model.addAttribute("cost", cost);
                model.addAttribute("stats", operatorStats);
            } else {
                logger.warn("operator not found");
            }
            return OPERATORS_VIEW_NAME;
        } else {
            return OPERATORS_VIEW_NAME;
        }
    }

    private SortingParams parseOperatorsSortingParams(HttpServletRequest request) {
        String sort = request.getParameter("sort");
        String dir = request.getParameter("dir");
        if (sort == null || dir == null || !SortStrings.isDirectionCorrect(dir)
                || !SortStrings.isStatsOperatorCorrect(sort)) {
            return null;
        }
        return new SortingParams(sort, Converter.parseSortOrder(dir));
    }

    private PaginationParams parsePagingParams(HttpServletRequest request, Principal principal) {
        String pageStr = request.getParameter("page");
        String resultsStr = request.getParameter("results");
        String cookieName = Digest.getSha1Hash(principal.getName() + RESULT_COUNT_COOKIE_SUFFIX);
        for (Cookie c : request.getCookies()) {
            if (cookieName.equals(c.getName())) {
                resultsStr = c.getValue();
                break;
            }
        }
        if (pageStr == null && resultsStr == null) {
            return null;
        }
        int page = -1;
        if (pageStr != null) {
            try {
                page = Integer.parseInt(pageStr);
            } catch (NumberFormatException ex) {
                logger.info("invalid paging params (operators stats page): " + pageStr);
            }
        }
        int results = -1;
        if (resultsStr != null) {
            try {
                results = Integer.parseInt(resultsStr);
            } catch (NumberFormatException ex) {
                logger.info("invalid paging params (operators stats page): " + pageStr);
            }
        }
        return new PaginationParams(page, results);
    }
}