package ru.pizzburg.web.domain;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class OperatorStat {
    private Order order;
    private float cost;
    private float price;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "OperatorStat{" +
                "order=" + order +
                ", cost=" + cost +
                ", price=" + price +
                '}';
    }
}
