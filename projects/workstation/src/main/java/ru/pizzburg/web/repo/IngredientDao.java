package ru.pizzburg.web.repo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.pizzburg.constants.DatabaseErrors;
import ru.pizzburg.utils.*;
import ru.pizzburg.utils.data.PageableList;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.web.domain.Ingredient;
import ru.pizzburg.web.domain.IngredientInProduct;
import ru.pizzburg.web.domain.Product;
import ru.pizzburg.web.repo.mapper.IngredientInProductMapper;
import ru.pizzburg.web.repo.mapper.IngredientMapper;
import ru.pizzburg.web.service.ingredients.IngredientFilter;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Repository
@Lazy(false)
public class IngredientDao extends JdbcTemplate {

    private final Log logger = LogFactory.getLog(getClass());
    private static final String INGREDIENT_LIST_QUERY = "SELECT %s FROM pizzburgdb.Ingredients %s %s %s";
    private static final String FROM_CLAUSE_INGREDIENT_LIST = "*";
    private static final String FROM_CLAUSE_COUNT = "COUNT(*)";
    private static final String LIMIT_CLAUSE = "LIMIT ?, ?";
    private static final String ORDER_CLAUSE = "ORDER BY %s %s";

    @Override
    @Autowired
    public void setDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    public PageableList<Ingredient> getIngredients(PaginationParams pp, SortingParams sp, IngredientFilter filter) {
        PageableList<Ingredient> result = new PageableList<Ingredient>();

        List<Object> filterParams = new ArrayList<Object>();
        String whereClause = getWhereClauseByFilter(filter, filterParams);
        Object[] dataQueryParams = new Object[filterParams.size() + 2];
        Object[] countQueryParams = new Object[filterParams.size()];
        int idx = 0;
        for (Object o : filterParams) {
            countQueryParams[idx] = o;
            dataQueryParams[idx++] = o;
        }
        dataQueryParams[idx++] = (pp.getPageNumber() - 1) * pp.getObjectsPerPage();
        dataQueryParams[idx] = pp.getObjectsPerPage();

        String orderClause = String.format(ORDER_CLAUSE, sp.getSortCriterion(), Converter.getStringSortOrder(sp.getSortDirection()));
        List<Ingredient> ingredients = query(String.format(INGREDIENT_LIST_QUERY,
                FROM_CLAUSE_INGREDIENT_LIST, whereClause, orderClause, LIMIT_CLAUSE),
                dataQueryParams, new IngredientMapper());
        result.setListSize(queryForInt(String.format(INGREDIENT_LIST_QUERY,
                FROM_CLAUSE_COUNT, whereClause, "", ""), countQueryParams));
        result.setData(ingredients);
        result.setPaginationParams(pp);
        result.setSortingParams(sp);
        return result;
    }

    private String getWhereClauseByFilter(IngredientFilter filter, List<Object> params) {
        params.clear();
        if (filter == null || filter.getIsEmpty()) {
            return "";
        } else {
            String result = "";
            boolean isEmpty = true;
            if (filter.getTitle() != null) {
                result = "WHERE title LIKE ?";
                params.add(filter.getTitle());
                isEmpty = false;
            }
            if (filter.getDescription() != null) {
                result += (isEmpty ? "WHERE " : " AND ") + "description LIKE ?";
                params.add(filter.getDescription());
                isEmpty = false;
            }
            if (filter.getUnits() != null) {
                result += (isEmpty ? "WHERE " : " AND ") + "units LIKE ?";
                params.add(filter.getUnits());
                isEmpty = false;
            }
            if (filter.getPriceFrom() != -1) {
                result += (isEmpty ? "WHERE " : " AND ") + "price >= ?";
                params.add(filter.getPriceFrom());
                isEmpty = false;
            }
            if (filter.getPriceTo() != -1) {
                result += (isEmpty ? "WHERE " : " AND ") + "price <= ?";
                params.add(filter.getPriceTo());
            }
            if (filter.getBalanceFrom() != -1) {
                result += (isEmpty ? "WHERE " : " AND ") + "balance >= ?";
                params.add(filter.getBalanceFrom());
                isEmpty = false;
            }
            if (filter.getBalanceTo() != -1) {
                result += (isEmpty ? "WHERE " : " AND ") + "balance <= ?";
                params.add(filter.getBalanceTo());
            }
            return result;
        }
    }

    public List<Ingredient> getIngredients() {
        return query("SELECT * FROM pizzburgdb.Ingredients ORDER BY title ASC", new IngredientMapper());
    }

    public String addIngredient(Ingredient i) {
        if (queryForInt("SELECT COUNT(*) FROM pizzburgdb.Ingredients WHERE title=?", i.getTitle()) != 0) {
            return DatabaseErrors.INGREDIENT_EXISTS;
        }
        try {
            update("INSERT INTO pizzburgdb.Ingredients (title, description, price, units, balance) VALUES (?,?,?,?,?)",
                    i.getTitle(), i.getDescription(), i.getPrice(), i.getUnits(), i.getBalance());
        } catch (DataAccessException ex) {
            logger.error("add ingredient fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return null;
    }

    public Ingredient getIngredient(int id) {
        List<Ingredient> result = query("SELECT * FROM pizzburgdb.Ingredients WHERE ingredientID=?", new Object[]{id}, new IngredientMapper());
        if (result.isEmpty()) {
            logger.warn("Access to ingredient which doesn't exist");
            return null;
        } else if (result.size() != 1) {
            logger.warn("database schema error: Not unique result from query. Get " + result.size() + " ingredients");
            return result.get(0);
        } else {
            return result.get(0);
        }
    }

    public String updateIngredient(Ingredient i) {
        if (queryForInt("SELECT COUNT(*) FROM pizzburgdb.Ingredients WHERE title = ? AND ingredientID != ?",
                i.getTitle(), i.getId()) != 0) {
            return DatabaseErrors.INGREDIENT_EXISTS;
        }
        int result = 0;
        try {
            result = update("UPDATE pizzburgdb.Ingredients SET title=?, description=?, price=?, units=?, balance=? WHERE ingredientID=?",
                    i.getTitle(), i.getDescription(), i.getPrice(), i.getUnits(), i.getBalance(), i.getId());
        } catch (DataAccessException ex) {
            logger.error("update ingredient fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return result > 0 ? null : DatabaseErrors.INGREDIENT_NOT_EXISTS;
    }

    public String updateIngredientBalance(int ingredientID, float balanceDelta) {
        int result = 0;
        try {
            result = update("UPDATE pizzburgdb.Ingredients SET balance=balance + ? WHERE ingredientID=?",
                    balanceDelta, ingredientID);
        } catch (DataAccessException ex) {
            logger.error("update ingredient balance fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return result == 1 ? null : DatabaseErrors.INGREDIENT_NOT_EXISTS;
    }

    public String removeIngredient(int id) {
        int result = 0;
        try {
            result = update("DELETE FROM pizzburgdb.Ingredients WHERE ingredientID=?", id);
        } catch (DataAccessException ex) {
            logger.error("delete ingredient fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return result == 1 ? null : DatabaseErrors.INGREDIENT_NOT_EXISTS;
    }

    public List<IngredientInProduct> getIngredientsForProduct(Product p) {
        if (p == null) {
            return null;
        }
        return query("SELECT * FROM pizzburgdb.Ingredients,pizzburgdb.ProductIngredients "
                + "WHERE Ingredients.ingredientID = ProductIngredients.ingredientID AND ProductIngredients.productID = ?",
                new Object[]{p.getId()}, new IngredientInProductMapper());
    }
}
