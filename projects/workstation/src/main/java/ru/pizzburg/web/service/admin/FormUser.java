package ru.pizzburg.web.service.admin;

import ru.pizzburg.web.domain.Role;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class FormUser {

    private int id = -1;
    private String login;
    private String name;
    private String surname;
    private String middleName;
    private String pass;
    private String passConfirm;
    private String roleStr;
    private Role role = new Role();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPassConfirm() {
        return passConfirm;
    }

    public void setPassConfirm(String passConfirm) {
        this.passConfirm = passConfirm;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getRoleStr() {
        return roleStr;
    }

    public void setRoleStr(String roleStr) {
        this.roleStr = roleStr;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FormUser other = (FormUser) obj;
        if (this.id != other.id) {
            return false;
        }
        if ((this.login == null) ? (other.login != null) : !this.login.equals(other.login)) {
            return false;
        }
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if ((this.surname == null) ? (other.surname != null) : !this.surname.equals(other.surname)) {
            return false;
        }
        if ((this.middleName == null) ? (other.middleName != null) : !this.middleName.equals(other.middleName)) {
            return false;
        }
        if ((this.pass == null) ? (other.pass != null) : !this.pass.equals(other.pass)) {
            return false;
        }
        if ((this.passConfirm == null) ? (other.passConfirm != null) : !this.passConfirm.equals(other.passConfirm)) {
            return false;
        }
        if ((this.roleStr == null) ? (other.roleStr != null) : !this.roleStr.equals(other.roleStr)) {
            return false;
        }
        if (this.role != other.role && (this.role == null || !this.role.equals(other.role))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + this.id;
        hash = 47 * hash + (this.login != null ? this.login.hashCode() : 0);
        hash = 47 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 47 * hash + (this.surname != null ? this.surname.hashCode() : 0);
        hash = 47 * hash + (this.pass != null ? this.pass.hashCode() : 0);
        hash = 47 * hash + (this.passConfirm != null ? this.passConfirm.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "FormUser{" + "id=" + id + ", login=" + login + ", name=" + name + ", surname=" + surname + ", middleName=" + middleName + ", pass=" + pass + ", passConfirm=" + passConfirm + ", roleStr=" + roleStr + ", role=" + role + '}';
    }
}
