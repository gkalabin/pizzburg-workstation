package ru.pizzburg.web.repo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.stereotype.Repository;
import ru.pizzburg.constants.DatabaseErrors;
import ru.pizzburg.constants.SortStrings;
import ru.pizzburg.utils.*;
import ru.pizzburg.utils.DiscountCardUtils;
import ru.pizzburg.utils.PhoneUtils;
import ru.pizzburg.utils.data.PageableList;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.web.domain.Client;
import ru.pizzburg.web.domain.DiscountCard;
import ru.pizzburg.web.domain.Order;
import ru.pizzburg.web.repo.mapper.ClientMapper;
import ru.pizzburg.web.repo.mapper.DiscountCardMapper;
import ru.pizzburg.web.service.clients.ClientFilter;

import javax.sql.DataSource;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Repository
@Lazy(false)
public class ClientDao extends JdbcTemplate {

    private static final String FROM_CLAUSE_CLIENT_LIST = "*";
    private static final String FROM_CLAUSE_COUNT = "COUNT(*)";
    private static final String CLIENT_LIST_QUERY = "SELECT %s FROM pizzburgdb.Clients "
            + "LEFT JOIN pizzburgdb.DiscountCards ON DiscountCards.cardNumber = Clients.discountCardNumber %s %s %s";
    private static final String LIMIT_CLAUSE = "LIMIT ?, ?";
    private final Log logger = LogFactory.getLog(getClass());

    @Override
    @Autowired
    public void setDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    public PageableList<Client> getClients(PaginationParams pp, SortingParams sp, ClientFilter filter) {
        PageableList<Client> result = new PageableList<Client>();

        List<Object> filterParams = new ArrayList<Object>();
        String whereClause = getWhereClauseByFilter(filter, filterParams);
        Object[] dataQueryParams = new Object[filterParams.size() + 2];
        Object[] countQueryParams = new Object[filterParams.size()];
        int idx = 0;
        for (Object o : filterParams) {
            countQueryParams[idx] = o;
            dataQueryParams[idx++] = o;
        }
        dataQueryParams[idx++] = (pp.getPageNumber() - 1) * pp.getObjectsPerPage();
        dataQueryParams[idx] = pp.getObjectsPerPage();

        String orderClause = getOrderClauseByParams(sp);
        List<Client> clients = query(String.format(CLIENT_LIST_QUERY, FROM_CLAUSE_CLIENT_LIST, whereClause, orderClause, LIMIT_CLAUSE),
                dataQueryParams, new ClientMapper());
        result.setListSize(queryForInt(String.format(CLIENT_LIST_QUERY, FROM_CLAUSE_COUNT, whereClause, "", ""),
                countQueryParams));
        result.setData(clients);
        result.setPaginationParams(pp);
        result.setSortingParams(sp);
        return result;
    }

    private String getOrderClauseByParams(SortingParams sp) {
        if (SortStrings.CLIENT_NAME_SORT_CRITERION.equals(sp.getSortCriterion())) {
            return String.format("ORDER BY Clients.surname %s, Clients.name %s, Clients.middleName %s",
                    Converter.getStringSortOrder(sp.getSortDirection()),
                    Converter.getStringSortOrder(sp.getSortDirection()), Converter.getStringSortOrder(sp.getSortDirection()));
        } else {
            return String.format("ORDER BY Clients.%s %s",
                    sp.getSortCriterion(), Converter.getStringSortOrder(sp.getSortDirection()));
        }
    }

    private String getWhereClauseByFilter(ClientFilter filter, List<Object> params) {
        params.clear();
        if (filter == null || filter.getIsEmpty()) {
            return "";
        } else {
            String result = "";
            boolean isEmpty = true;
            if (filter.getName() != null) {
                result = "WHERE Clients.name LIKE ?";
                params.add(filter.getName());
                isEmpty = false;
            }
            if (filter.getSurname() != null) {
                result += (isEmpty ? "WHERE " : " AND ") + "Clients.surname LIKE ?";
                params.add(filter.getSurname());
                isEmpty = false;
            }
            if (filter.getMiddleName() != null) {
                result += (isEmpty ? "WHERE " : " AND ") + "Clients.middleName LIKE ?";
                params.add(filter.getMiddleName());
                isEmpty = false;
            }
            if (filter.getAddress() != null) {
                result += (isEmpty ? "WHERE " : " AND ") + "Clients.defaultAddress LIKE ?";
                params.add(filter.getAddress());
                isEmpty = false;
            }
            if (filter.getPhone() != null) {
                result += (isEmpty ? "WHERE " : " AND ") + "LPAD(Clients.phone, " + PhoneUtils.PHONE_FULL_DIGITS_COUNT + ", '0') LIKE ?";
                params.add(filter.getPhone());
                isEmpty = false;
            }
            if (filter.getCardNumber() != null) {
                result += (isEmpty ? "WHERE " : " AND ") + "LPAD(DiscountCards.cardNumber, " + DiscountCardUtils.DISCOUNT_CARD_DIGITS_COUNT + ", '0') LIKE ?";
                params.add(filter.getCardNumber());
                isEmpty = false;
            }
            if (filter.getSumFrom() > 0) {
                result += (isEmpty ? "WHERE " : " AND ") + "Clients.ordersSum >= ?";
                params.add(filter.getSumFrom());
                isEmpty = false;
            }
            if (filter.getSumTo() > 0) {
                result += (isEmpty ? "WHERE " : " AND ") + "(Clients.ordersSum <= ? OR Clients.ordersSum IS NULL)";
                params.add(filter.getSumTo());
            }
            return result;
        }
    }

    public String addClient(Client c) {
        if (queryForInt("SELECT COUNT(*) FROM pizzburgdb.Clients WHERE phone=?", c.getPhone()) != 0) {
            return DatabaseErrors.CLIENT_ALREADY_EXIST;
        }
        try {
            if (c.getCard() != null) {
                if (queryForInt("SELECT COUNT(*) FROM pizzburgdb.DiscountCards WHERE cardNumber=?", c.getCard().getId()) != 0) {
                    return DatabaseErrors.DISCOUNT_CARD_IN_USE;
                }
                update("INSERT INTO pizzburgdb.DiscountCards (cardNumber, emissionDate) VALUES (?,?)",
                        c.getCard().getId(), c.getCard().getEmissionDate());
            }
            update("INSERT INTO pizzburgdb.Clients (phone, name, surname, middleName, defaultAddress, discountCardNumber, ordersSum) VALUES (?,?,?,?,?,?,?)",
                    c.getPhone(), c.getName(), c.getSurname(), c.getMiddleName(), c.getDefaultAddress(), c.getCard() == null ? null : c.getCard().getId(),
                    c.getOrdersSum());
        } catch (DataAccessException ex) {
            logger.error("add client fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return null;
    }

    public Client getClient(long phone) {
        List<Client> result = query("SELECT * FROM pizzburgdb.Clients "
                + "LEFT JOIN pizzburgdb.DiscountCards ON DiscountCards.cardNumber = Clients.discountCardNumber "
                + "WHERE Clients.phone=?", new Object[]{phone}, new ClientMapper());
        if (result.isEmpty()) {
            logger.warn("Access to client which doesn't exist phone=" + phone);
            return null;
        } else if (result.size() != 1) {
            logger.warn("database schema error: Not unique result from query. Get " + result.size() + " results for client phone=" + phone);
            return result.get(0);
        } else {
            return result.get(0);
        }
    }

    public String updateClient(Client c, long phone) {
        if (phone != c.getPhone()) {
            if (queryForInt("SELECT COUNT(*) FROM pizzburgdb.Clients WHERE phone=?", c.getPhone()) != 0) {
                return DatabaseErrors.CLIENT_WITH_PHONE_EXIST;
            }
        }
        int result = 0;
        try {
            if (c.getCard() != null) {
                int isUniqueConstraintFailed = queryForInt("SELECT COUNT(*) FROM pizzburgdb.Clients WHERE discountCardNumber=? AND phone!=?", c.getCard().getId(), phone);
                if (isUniqueConstraintFailed != 0) {
                    return DatabaseErrors.DISCOUNT_CARD_IN_USE;
                }

                DiscountCard dbCard = getDiscountCard(c.getCard().getId());
                if (!c.getCard().equals(dbCard)) {
                    if (dbCard == null) {
                        update("INSERT INTO pizzburgdb.DiscountCards (cardNumber, emissionDate) VALUES (?,?)",
                                c.getCard().getId(), c.getCard().getEmissionDate());
                    } else {
                        update("UPDATE pizzburgdb.DiscountCards SET emissionDate=? WHERE cardNumber=?",
                                c.getCard().getEmissionDate(), c.getCard().getId());
                    }
                }
            }
            result = update("UPDATE pizzburgdb.Clients SET phone=?, name=?, surname=?, middleName=?, defaultAddress=?, discountCardNumber=?, ordersSum=? "
                    + "WHERE phone=?",
                    c.getPhone(), c.getName(), c.getSurname(), c.getMiddleName(), c.getDefaultAddress(), c.getCard() == null ? null : c.getCard().getId(),
                    c.getOrdersSum(), phone);
        } catch (DataAccessException ex) {
            logger.error("update client fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return result == 1 ? null : DatabaseErrors.CLIENT_NOT_EXIST;
    }

    public String removeClient(long phone) {
        int result = 0;
        try {
            result = update("DELETE FROM pizzburgdb.Clients WHERE phone=?", phone);
        } catch (DataAccessException ex) {
            logger.error("delete client fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return result == 1 ? null : DatabaseErrors.CLIENT_NOT_EXIST;
    }

    public DiscountCard getDiscountCard(long cardNumber) {
        List<DiscountCard> result = query("SELECT * FROM pizzburgdb.DiscountCards WHERE cardNumber=?", new Object[]{cardNumber}, new DiscountCardMapper());
        if (result.isEmpty()) {
            logger.info("Card with with number=" + cardNumber + " not exist");
            return null;
        } else if (result.size() != 1) {
            logger.warn("database schema error: Not unique result from query. Get " + result.size() + " discount cards");
            return result.get(0);
        } else {
            return result.get(0);
        }
    }

    public DiscountCard getDiscountCard(Client client) {
        if (client == null) {
            return null;
        }
        List<DiscountCard> result = query("SELECT DiscountCards.* " +
                "FROM pizzburgdb.Clients " +
                "LEFT JOIN pizzburgdb.DiscountCards ON Clients.discountCardNumber = DiscountCards.cardNumber " +
                "WHERE phone=?", new Object[]{client.getPhone()}, new DiscountCardMapper());
        if (result.isEmpty()) {
            logger.info("Card with for client " + client + " not exist");
            return null;
        } else if (result.size() != 1) {
            logger.warn("database schema error: Not unique result from query. Get " + result.size() + " discount cards for client " + client);
            return result.get(0);
        } else {
            return result.get(0);
        }
    }

    public Client getClientForOrder(Order o) {
        List<Client> result = query("SELECT * FROM pizzburgdb.Clients "
                + "LEFT JOIN pizzburgdb.Orders ON Orders.clientPhone = Clients.phone "
                + "LEFT JOIN pizzburgdb.DiscountCards ON DiscountCards.cardNumber = Clients.discountCardNumber "
                + "WHERE orderID=?", new Object[]{o.getId()}, new ClientMapper());
        if (result.isEmpty()) {
            logger.error("User not found for order " + o);
            return null;
        } else if (result.size() != 1) {
            logger.warn("database schema error: Not unique result from query. Get " + result.size() + " clients");
            return result.get(0);
        } else {
            return result.get(0);
        }
    }

    public List<BigInteger> getClientsPhones(String startsWith, int maxRows) {
        return query("SELECT phone FROM pizzburgdb.Clients "
                + " WHERE LPAD(Clients.phone, " + PhoneUtils.PHONE_FULL_DIGITS_COUNT + ", '0') LIKE ? LIMIT ?",
                new Object[]{"%" + startsWith + "%", maxRows},
                new SingleColumnRowMapper<BigInteger>());
    }

    public List<String> getClientAddresses(long phone) {
        return query("SELECT shipmentAddress FROM pizzburgdb.Orders "
                + "WHERE clientPhone = ? GROUP BY shipmentAddress ORDER BY COUNT(shipmentAddress) DESC",
                new Object[]{phone},
                new SingleColumnRowMapper<String>());
    }
}
