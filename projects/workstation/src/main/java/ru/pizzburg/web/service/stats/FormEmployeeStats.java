package ru.pizzburg.web.service.stats;

import java.util.Date;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class FormEmployeeStats {
    private String from="";
    private String to="";
    private String employeeIdStr ="";

    private Date fromDate;
    private Date toDate;
    private int employeeId;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getEmployeeIdStr() {
        return employeeIdStr;
    }

    public void setEmployeeIdStr(String employeeIdStr) {
        this.employeeIdStr = employeeIdStr;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public String toString() {
        return "FormEmployeeStats{" +
                "from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", employeeIdStr='" + employeeIdStr + '\'' +
                ", fromDate=" + fromDate +
                ", toDate=" + toDate +
                ", employeeId=" + employeeId +
                '}';
    }
}
