package ru.pizzburg.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Handles and retrieves the login or denied page depending on the URI template
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Controller
@Lazy(false)
public class AuthController {

	protected final Log logger = LogFactory.getLog(getClass());
    @Autowired
    private MessageSource messageSource;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String getLoginPage(@RequestParam(value = "error", required = false) boolean error,
							   ModelMap model) {
		if (error) {
			model.put("error", messageSource.getMessage("error.wrong-auth", null, null));
		} else {
			model.put("error", "");
		}
		return "login";
	}

	@RequestMapping(value = "/denied", method = RequestMethod.GET)
	public String getDeniedPage() {
		return "denied";
	}
}
