package ru.pizzburg.web.domain;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class IngredientInProduct {

    private Ingredient ingredient = new Ingredient();
    private float count;

    public float getCount() {
        return count;
    }

    public void setCount(float count) {
        this.count = count;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IngredientInProduct other = (IngredientInProduct) obj;
        if (this.ingredient != other.ingredient && (this.ingredient == null || !this.ingredient.equals(other.ingredient))) {
            return false;
        }
        if (Float.floatToIntBits(this.count) != Float.floatToIntBits(other.count)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (this.ingredient != null ? this.ingredient.hashCode() : 0);
        hash = 47 * hash + Float.floatToIntBits(this.count);
        return hash;
    }

    @Override
    public String toString() {
        return "IngredientInProduct{" + "ingredient=" + ingredient + ", count=" + count + '}';
    }
}
