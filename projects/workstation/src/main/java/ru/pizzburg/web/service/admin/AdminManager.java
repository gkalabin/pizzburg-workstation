package ru.pizzburg.web.service.admin;

import org.displaytag.properties.SortOrderEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.pizzburg.utils.data.PageableList;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.web.domain.Order;
import ru.pizzburg.web.domain.Role;
import ru.pizzburg.web.domain.WorkstationUser;
import ru.pizzburg.web.repo.AdminDao;

import java.util.List;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Service
@Lazy(false)
public class AdminManager implements UserDetailsService {

    private static final PaginationParams DEFAULT_PAGINATION_PARAMS;
    private static final SortingParams DEFAULT_SORTING_PARAMS;
    private static final int MAX_OBJECTS_PER_PAGE = 100;
    @Autowired
    private AdminDao adminDao;

    static {
        DEFAULT_PAGINATION_PARAMS = new PaginationParams();
        DEFAULT_SORTING_PARAMS = new SortingParams("login", SortOrderEnum.ASCENDING);
    }

    public PageableList<WorkstationUser> getUserList(PaginationParams pp, SortingParams sp) {
        if (pp == null) {
            pp = DEFAULT_PAGINATION_PARAMS;
        } else {
            int page = pp.getPageNumber() <= 0 ? DEFAULT_PAGINATION_PARAMS.getPageNumber() : pp.getPageNumber();
            int results = pp.getObjectsPerPage() <= 0 || pp.getObjectsPerPage() > MAX_OBJECTS_PER_PAGE
                    ? DEFAULT_PAGINATION_PARAMS.getObjectsPerPage() : pp.getObjectsPerPage();
            pp = new PaginationParams(page, results);
        }
        if (sp == null) {
            sp = DEFAULT_SORTING_PARAMS;
        }
        final PageableList<WorkstationUser> userList = adminDao.getUsers(pp, sp);
        if (userList.getList() == null) {
            return null;
        }
        for (WorkstationUser u : userList.getList()) {
            u.setRole(adminDao.getRole(u.getRole().getId()));
        }
        return userList;
    }

    public String addUser(WorkstationUser u) {
        return adminDao.addUser(u);
    }

    public WorkstationUser getUser(int id) {
        WorkstationUser user = adminDao.getUser(id);
        if (user == null) {
            return null;
        }
        return user;
    }

    public String updateUser(WorkstationUser u) {
        return adminDao.updateUser(u);
    }

    public String removeUser(int id) {
        return adminDao.removeUser(id);
    }

    public List<Role> getRoleList() {
        return adminDao.getRoleList();
    }

    public List<WorkstationUser> getCarriers() {
        return adminDao.getUsersForRole(Role.ROLE_CARRIER);
    }

    public List<WorkstationUser> getOperators() {
        return adminDao.getUsersForRole(Role.ROLE_OPERATOR);
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        WorkstationUser user = adminDao.getUser(s);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return new User(user.getLogin(), user.getPasswordDigest(), user.getAuthorities());
    }

    public WorkstationUser getOrderRegistrator(Order o) {
        WorkstationUser user = adminDao.getOrderRegistrator(o);
        if (user == null) {
            return null;
        }
        user.setRole(adminDao.getRole(user.getRole().getId()));
        return user;
    }

    public WorkstationUser getOrderCarrier(Order o) {
        WorkstationUser user = adminDao.getOrderCarrier(o);
        if (user == null) {
            return null;
        }
        user.setRole(adminDao.getRole(user.getRole().getId()));
        return user;
    }
}
