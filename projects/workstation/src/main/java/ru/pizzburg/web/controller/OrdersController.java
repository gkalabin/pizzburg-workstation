package ru.pizzburg.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.pizzburg.constants.SortStrings;
import ru.pizzburg.utils.*;
import ru.pizzburg.utils.PhoneUtils;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.web.domain.*;
import ru.pizzburg.web.service.admin.AdminManager;
import ru.pizzburg.web.service.groups.ProductGroupsManager;
import ru.pizzburg.web.service.orders.*;
import ru.pizzburg.web.service.products.ProductsManager;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Controller
@SessionAttributes({"formOrder", "orderSearch", "orderFilter"})
@Lazy(false)
public class OrdersController{

    private static final String ORDERS_VIEW_NAME = "orders";
    public static final String RESULT_COUNT_COOKIE_SUFFIX = "_orders_resultCount";
    protected final Log logger = LogFactory.getLog(getClass());
    @Autowired
    private OrdersManager ordersManager;
    @Autowired
    private ProductsManager productsManager;
    @Autowired
    private ProductGroupsManager groupsManager;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private FormOrderValidator formOrderValidator;
    @Autowired
    private OrderSearchValidator orderSearchValidator;
    @Autowired
    private FormStatusValidator formStatusValidator;
    @Autowired
    private AdminManager adminManager;

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/orders/search", method = RequestMethod.GET)
    public String searchPage(HttpServletRequest request, ModelMap model, Principal principal) {
        OrderSearch os = (OrderSearch) model.get("orderSearch");
        if (os == null) {
            model.addAttribute("orderSearch", getNewOrderSearch());
        }

        FormOrder fo = (FormOrder) model.get("formOrder");
        if (fo == null) {
            model.addAttribute("formOrder", new FormOrder());
        }

        model.addAttribute("orders", ordersManager.getOrders(parsePagingParams(request, principal),
                parseSortingParams(request), (OrderFilter) model.get("orderFilter")));
        model.addAttribute("allGroups", groupsManager.getGroups());
        model.addAttribute("allStatuses", ordersManager.getStatuses());
        return ORDERS_VIEW_NAME;
    }

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/orders/search/cancel", method = RequestMethod.GET)
    public String cancelSearch(ModelMap model) {
        model.addAttribute("orderSearch", getNewOrderSearch());
        model.addAttribute("orderFilter", new OrderFilter());

        FormOrder fo = (FormOrder) model.get("formOrder");
        if (fo == null) {
            fo = new FormOrder();
            model.addAttribute("formOrder", fo);
        }
        return "redirect:/orders/";
    }

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/orders/search", method = RequestMethod.POST)
    public String search(HttpServletRequest request, ModelMap model, @ModelAttribute("orderSearch") OrderSearch os,
                         BindingResult result, Principal principal) {
        orderSearchValidator.validate(os, result);

        if (os == null) {
            os = getNewOrderSearch();
            model.addAttribute("orderSearch", os);
        } else {
            if (!result.hasErrors() && os.isRegistratorMe() && principal != null) {
                os.setRegistratorLogin(principal.getName());
            }
        }

        FormOrder fo = (FormOrder) model.get("formOrder");
        if (fo == null) {
            fo = new FormOrder();
            model.addAttribute("formOrder", fo);
        }

        OrderFilter filter = os.getOrderFilter();
        model.addAttribute("orderFilter", filter);
        if (result.hasErrors()) {
            model.addAttribute("orders", ordersManager.getOrders(parsePagingParams(request,principal),
                    parseSortingParams(request), filter));
            model.addAttribute("allGroups", groupsManager.getGroups());
            model.addAttribute("allStatuses", ordersManager.getStatuses());
            return ORDERS_VIEW_NAME;
        } else {
            return "redirect:/orders/";
        }
    }

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public String getOrdersPage(HttpServletRequest request, ModelMap model, Principal principal) {
        FormOrder fo = new FormOrder();
        model.addAttribute("formOrder", fo);

        OrderSearch os = (OrderSearch) model.get("orderSearch");
        if (os == null) {
            model.addAttribute("orderSearch", getNewOrderSearch());
        }

        model.addAttribute("orders", ordersManager.getOrders(parsePagingParams(request, principal), parseSortingParams(request),
                (OrderFilter) model.get("orderFilter")));
        model.addAttribute("allGroups", groupsManager.getGroups());
        model.addAttribute("allStatuses", ordersManager.getStatuses());
        return ORDERS_VIEW_NAME;
    }

    private OrderSearch getNewOrderSearch() {
        OrderSearch os = new OrderSearch();
        final ArrayList<Integer> orderSearchList = new ArrayList<Integer>();
        List<Status> statuses = ordersManager.getStatuses();
        for (Status s : statuses) {
            orderSearchList.add(s.getId());
        }
        os.setStatus(orderSearchList);
        return os;
    }

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/orders", method = RequestMethod.POST)
    public String addOrder(HttpServletRequest request, ModelMap model, @ModelAttribute("formOrder") FormOrder addOrder,
                           BindingResult result, SessionStatus status, RedirectAttributes redirectAttributes,
                           Principal principal) {
        formOrderValidator.validate(addOrder, result);

        if (!result.hasErrors()) {
            //form success
            Order o = new Order();
            o.setRegisterDate(new Date());
            WorkstationUser registrator = new WorkstationUser();
            registrator.setLogin(principal.getName());
            o.setRegistrator(registrator);
            o.setShipmentAddress(addOrder.getShipmentAddress());
            o.setComment(addOrder.getComment());
            o.setPersonsCount(addOrder.getPersonsCount());
            Client c = new Client();
            c.setPhone(PhoneUtils.parsePhone(addOrder.getClientPhoneStr()));
            c.setName(addOrder.getClientName());
            o.setClient(c);
            o.setProducts(addOrder.getProducts());
            o.setAlternateDiscount(addOrder.getAlternateDiscountFloat());
            o.setAlternateDiscountCard(addOrder.getAlternateDiscountCardObject());

            String additionResult = ordersManager.addOrder(o);
            if (additionResult != null) {
                model.addAttribute("errorMessage", messageSource.getMessage(additionResult, null, null));
                model.addAttribute("orders", ordersManager.getOrders(parsePagingParams(request,principal), parseSortingParams(request),
                        (OrderFilter) model.get("orderFilter")));
                model.addAttribute("allGroups", groupsManager.getGroups());
                model.addAttribute("allStatuses", ordersManager.getStatuses());
                model.addAttribute("allProducts", getProductsForOrderProduct(addOrder));
                return ORDERS_VIEW_NAME;
            } else {
                redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.order-successfully-added", null, null));
                status.setComplete();
                return "redirect:/orders/";
            }
        } else {
            model.addAttribute("orders", ordersManager.getOrders(parsePagingParams(request,principal), parseSortingParams(request),
                    (OrderFilter) model.get("orderFilter")));
            model.addAttribute("allGroups", groupsManager.getGroups());
            model.addAttribute("allStatuses", ordersManager.getStatuses());
            model.addAttribute("allProducts", getProductsForOrderProduct(addOrder));
            return ORDERS_VIEW_NAME;
        }
    }

    private List<List<ProductInGroup>> getProductsForOrderProduct(FormOrder fo) {
        List<List<ProductInGroup>> result = new ArrayList<List<ProductInGroup>>();
        for (ProductFormEntry pfe : fo.getProductsStr()) {
            int groupId;
            // TODO: wtf?
            List<ProductInGroup> products = new ArrayList<ProductInGroup>();
            try {
                groupId = Integer.parseInt(pfe.getGroupId());
            } catch (NumberFormatException ex) {
                result.add(new ArrayList<ProductInGroup>());
                continue;
            }
            result.add(productsManager.getProductsForGroupId(groupId));
        }
        return result;
    }

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/orders/edit/{id}", method = RequestMethod.POST)
    public String updateOrder(HttpServletRequest request, ModelMap model, @ModelAttribute("formOrder") FormOrder editOrder,
                              BindingResult result, SessionStatus status, RedirectAttributes redirectAttributes, Principal principal) {
        formOrderValidator.validate(editOrder, result);
        formStatusValidator.validateStatus(editOrder, result);
        if (editOrder.getStatus() != null &&
                (editOrder.getStatus().getId() == Status.SHIPPED.getId() ||
                        editOrder.getStatus().getId() == Status.PAID.getId() ||
                        editOrder.getStatus().getId() == Status.CANCELLED.getId())) {
            formOrderValidator.validateCarrier(editOrder, result);
        }

        if (!result.hasErrors()) {
            //form success
            Order o = new Order();
            o.setRegisterDate(new Date());
            o.setShipmentAddress(editOrder.getShipmentAddress());
            o.setComment(editOrder.getComment());
            o.setPersonsCount(editOrder.getPersonsCount());
            o.setStatus(editOrder.getStatus());
            Client c = new Client();
            c.setPhone(PhoneUtils.parsePhone(editOrder.getClientPhoneStr()));
            c.setName(editOrder.getClientName());
            o.setClient(c);
            o.setId(editOrder.getId());
            o.setProducts(editOrder.getProducts());
            o.setCarrier(editOrder.getCarrier());
            if (editOrder.getAlternateDiscountFloat() > 0) {
                o.setAlternateDiscount(editOrder.getAlternateDiscountFloat());
            }
            if (editOrder.getAlternateDiscountCardObject() != null) {
                o.setAlternateDiscountCard(editOrder.getAlternateDiscountCardObject());
            }

            String updateResult = ordersManager.updateOrder(o);
            if (updateResult != null) {
                model.addAttribute("errorMessage", messageSource.getMessage(updateResult, null, null));
                model.addAttribute("orders", ordersManager.getOrders(parsePagingParams(request,principal), parseSortingParams(request),
                        (OrderFilter) model.get("orderFilter")));
                model.addAttribute("allGroups", groupsManager.getGroups());
                model.addAttribute("allStatuses", ordersManager.getStatuses());
                model.addAttribute("allProducts", getProductsForOrderProduct(editOrder));
                return ORDERS_VIEW_NAME;
            } else {
                redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.order-successfully-updated", null, null));
                status.setComplete();
                return "redirect:/" + ORDERS_VIEW_NAME;
            }
        } else {
            model.addAttribute("orders", ordersManager.getOrders(parsePagingParams(request,principal), parseSortingParams(request),
                    (OrderFilter) model.get("orderFilter")));
            model.addAttribute("allGroups", groupsManager.getGroups());
            model.addAttribute("allStatuses", ordersManager.getStatuses());
            model.addAttribute("allProducts", getProductsForOrderProduct(editOrder));
            return ORDERS_VIEW_NAME;
        }
    }

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/orders/edit/{id}", method = RequestMethod.GET)
    public String getEditForm(HttpServletRequest request, ModelMap model, @PathVariable int id,
                              RedirectAttributes redirectAttributes, Principal principal) {
        OrderSearch os = (OrderSearch) model.get("orderSearch");
        if (os == null) {
            model.addAttribute("orderSearch", getNewOrderSearch());
        }

        Order o = ordersManager.getOrder(id);
        if (o == null) {
            logger.warn("Null order request");
            redirectAttributes.addFlashAttribute("errorMessage", messageSource.getMessage("error.order-not-exist", null, null));
            return "redirect:/" + ORDERS_VIEW_NAME;
        }

        if (!request.isUserInRole("ROLE_ADMIN") && !principal.getName().equals(o.getRegistrator().getLogin())) {
            logger.warn("Trying to edit not own order by " + principal.getName());
            redirectAttributes.addFlashAttribute("errorMessage", messageSource.getMessage("error.not-accessed", null, null));
            return "redirect:/" + ORDERS_VIEW_NAME;
        }

        FormOrder fo = new FormOrder();
        fo.setId(o.getId());
        fo.setClientPhoneStr(o.getClient().getPhoneFormatted());
        fo.setClientName(o.getClient().getName());
        fo.setShipmentAddress(o.getShipmentAddress());
        fo.setPersonsCountStr(String.valueOf(o.getPersonsCount()));
        fo.setComment(o.getComment());
        fo.setStatusIdStr(String.valueOf(o.getStatus().getId()));
        if (o.getCarrier() != null) {
            fo.setCarrierIdStr(String.valueOf(o.getCarrier().getId()));
        }
        if (o.getAlternateDiscount() > 0) fo.setAlternateDiscount(String.valueOf(o.getAlternateDiscount()));
        if (o.getAlternateDiscountCard() != null)
            fo.setAlternateDiscountCard(String.valueOf(o.getAlternateDiscountCard().getIdFormatted()));
        for (ProductInOrder pio : o.getProducts()) {
            ProductFormEntry pfe = new ProductFormEntry();
            pfe.setCount(String.valueOf(pio.getCount()));
            pfe.setProductId(String.valueOf(pio.getProduct().getId()));
            pfe.setGroupId(String.valueOf(pio.getGroup().getId()));
            fo.getProductsStr().add(pfe);
        }
        model.addAttribute("formOrder", fo);
        model.addAttribute("orders", ordersManager.getOrders(parsePagingParams(request, principal), parseSortingParams(request),
                (OrderFilter) model.get("orderFilter")));
        model.addAttribute("allGroups", groupsManager.getGroups());
        model.addAttribute("allStatuses", ordersManager.getStatuses());
        model.addAttribute("allProducts", getProductsForOrderProduct(fo));
        return ORDERS_VIEW_NAME;
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/orders/remove/{id}", method = RequestMethod.GET)
    public String removeOrder(HttpServletRequest request, ModelMap model, @PathVariable int id,
                              RedirectAttributes redirectAttributes, Principal principal) {
        if (model.get("formOrder") == null) {
            model.addAttribute("formOrder", new FormOrder());
        }

        String removeResult = ordersManager.removeOrder(id);
        if (removeResult != null) {
            model.addAttribute("errorMessage", messageSource.getMessage(removeResult, null, null));
            model.addAttribute("orders", ordersManager.getOrders(parsePagingParams(request,principal), parseSortingParams(request),
                    (OrderFilter) model.get("orderFilter")));
            model.addAttribute("allGroups", groupsManager.getGroups());
            model.addAttribute("allStatuses", ordersManager.getStatuses());
            return ORDERS_VIEW_NAME;
        } else {
            redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.order-successfully-removed", null, null));
            return "redirect:/orders/";
        }
    }

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(method = RequestMethod.GET, value = "/orders/addProduct")
    protected String appendProductField(@RequestParam Integer fieldId, ModelMap model, @ModelAttribute("formOrder") FormOrder addOrder) {
        model.addAttribute("productNumber", fieldId);
        final List<ProductGroup> groups = groupsManager.getGroups();
        model.addAttribute("allGroups", groups);
        List<ProductInGroup> products = new ArrayList<ProductInGroup>();
        if (groups != null && !groups.isEmpty()) {
            products = productsManager.getProductsForGroupId(groups.get(0).getId());
        }
        model.addAttribute("allProducts", products);

        return "addProductToOrder";
    }

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(method = RequestMethod.GET, value = "/orders/groupChanged")
    protected String onGroupChanged(@RequestParam Integer fieldId, @RequestParam Integer selectedGroup,
                                    ModelMap model, @ModelAttribute("formOrder") FormOrder addOrder) {
        model.addAttribute("productNumber", fieldId);
        final List<ProductInGroup> products = productsManager.getProductsForGroupId(selectedGroup);
        model.addAttribute("allProducts", products);
        if (addOrder != null) {
            String productId = products != null && !products.isEmpty() ? String.valueOf(products.get(0).getProduct().getId()) : "";
            addOrder.getProductsStr().get(fieldId).setProductId(productId);
        }
        return "productInputForGroup";
    }

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(value = "/orders/statusChange", method = RequestMethod.GET)
    protected String onOrderStatusChange(ModelMap model, @RequestParam int statusId,
                                         @ModelAttribute("formOrder") FormOrder addOrder) {
        if (statusId == Status.SHIPPED.getId() ||
                statusId == Status.PAID.getId() ||
                statusId == Status.CANCELLED.getId()) {
            model.addAttribute("carriers", adminManager.getCarriers());
            model.addAttribute("formOrder", addOrder);
            model.addAttribute("selectedId", addOrder.getCarrierIdStr());
            return "carrierSelector";
        }
        return "empty";
    }

    @Secured({"ROLE_ADMIN", "ROLE_OPERATOR"})
    @RequestMapping(method = RequestMethod.GET, value = "/orders/deleteProduct")
    protected String removeProductField(@RequestParam Integer fieldId, @ModelAttribute("formOrder") FormOrder addOrder) {
        addOrder.getDeletedProducts().add(fieldId);
        // TODO: not elegant
        return "empty";
    }

    private SortingParams parseSortingParams(HttpServletRequest request) {
        String sort = request.getParameter("sort");
        String dir = request.getParameter("dir");
        if (sort == null || dir == null || !SortStrings.isDirectionCorrect(dir)
                || !SortStrings.isOrderCorrect(sort)) {
            return null;
        }
        return new SortingParams(sort, Converter.parseSortOrder(dir));
    }

    private PaginationParams parsePagingParams(HttpServletRequest request, Principal principal) {
        String pageStr = request.getParameter("page");
        String resultsStr = request.getParameter("results");
        String cookieName = Digest.getSha1Hash(principal.getName() + RESULT_COUNT_COOKIE_SUFFIX);
        for (Cookie c: request.getCookies()) {
            if (cookieName.equals(c.getName())) {
                resultsStr = c.getValue();
                break;
            }
        }
        if (pageStr == null && resultsStr == null) {
            return null;
        }
        int page = -1;
        if (pageStr != null) {
            try {
                page = Integer.parseInt(pageStr);
            } catch (NumberFormatException ex) {
                logger.info("invalid paging params (orders page): " + pageStr);
            }
        }
        int results = -1;
        if (resultsStr != null) {
            try {
                results = Integer.parseInt(resultsStr);
            } catch (NumberFormatException ex) {
                logger.info("invalid paging params (orders page): " + pageStr);
            }
        }
        return new PaginationParams(page, results);
    }
}