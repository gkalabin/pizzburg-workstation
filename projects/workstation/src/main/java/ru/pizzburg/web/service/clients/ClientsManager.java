package ru.pizzburg.web.service.clients;

import org.displaytag.properties.SortOrderEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.pizzburg.utils.data.PageableList;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.utils.PhoneUtils;
import ru.pizzburg.web.domain.Client;
import ru.pizzburg.web.repo.ClientDao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Service
@Lazy(false)
public class ClientsManager {

    private static final PaginationParams DEFAULT_PAGINATION_PARAMS;
    private static final SortingParams DEFAULT_SORTING_PARAMS;
    private static final int MAX_CLIENT_SUGGESTIONS_COUNT = 10;
    private static final int MAX_OBJECTS_PER_PAGE = 100;
    private static final int MIN_PREFIX_LENGTH = 3;
    private ClientDao clientDao;

    static {
        DEFAULT_PAGINATION_PARAMS = new PaginationParams();
        DEFAULT_SORTING_PARAMS = new SortingParams("phone", SortOrderEnum.ASCENDING);
    }

    @Autowired
    public void setClientDao(ClientDao clientDao) {
        this.clientDao = clientDao;
    }

    public PageableList<Client> getClients(PaginationParams pp, SortingParams sp, ClientFilter filter) {
        if (pp == null) {
            pp = DEFAULT_PAGINATION_PARAMS;
        } else {
            int page = pp.getPageNumber() <= 0 ? DEFAULT_PAGINATION_PARAMS.getPageNumber() : pp.getPageNumber();
            int results = pp.getObjectsPerPage() <= 0 || pp.getObjectsPerPage() > MAX_OBJECTS_PER_PAGE
                    ? DEFAULT_PAGINATION_PARAMS.getObjectsPerPage() : pp.getObjectsPerPage();
            pp = new PaginationParams(page, results);
        }
        if (sp == null) {
            sp = DEFAULT_SORTING_PARAMS;
        }
        return clientDao.getClients(pp, sp, filter);
    }

    public String addClient(Client c) {
        return clientDao.addClient(c);
    }

    public Client getClient(long phone) {
        return clientDao.getClient(phone);
    }

    public String updateClient(Client c, long phone) {
        return clientDao.updateClient(c, phone);
    }

    public String removeClient(long phone) {
        return clientDao.removeClient(phone);
    }

    @SuppressWarnings("unchecked")
    public List<String> getClientsPhones(String startsWith, int maxRows) {
        if (startsWith == null || startsWith.length() < MIN_PREFIX_LENGTH) {
            return Collections.EMPTY_LIST;
        }
        final List<BigInteger> phones = clientDao.getClientsPhones(startsWith, Math.min(maxRows, MAX_CLIENT_SUGGESTIONS_COUNT));
        List<String> result = new ArrayList<String>(phones.size());
        for (BigInteger i : phones) {
            Long phone = i.longValue();
            result.add(PhoneUtils.getPhoneNumberFormatted(phone));
        }
        return result;
    }

    public List<String> getClientAddresses(long phone) {
        List<String> clientAddresses = clientDao.getClientAddresses(phone);
        if (clientAddresses == null) {
            return null;
        }
        Client client = clientDao.getClient(phone);
        if (client != null) {
            String defaultAddress = client.getDefaultAddress();
            if (!clientAddresses.contains(defaultAddress)) {
                clientAddresses.add(defaultAddress);
            }
        }
        return clientAddresses;
    }
}
