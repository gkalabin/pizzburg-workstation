/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pizzburg.web.domain;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class Status {
    public static final StatusImmutable ACCEPTED;
    public static final StatusImmutable PROCESSING;
    public static final StatusImmutable SHIPPED;
    public static final StatusImmutable PAID;
    public static final StatusImmutable CANCELLED;

    static {
        Status status = new Status();
        status.setId(1);
        status.setTitle("Получен");
        ACCEPTED = new StatusImmutable(status);
        status.setId(2);
        status.setTitle("Обрабатывается");
        PROCESSING = new StatusImmutable(status);
        status.setId(3);
        status.setTitle("Отправлен");
        SHIPPED = new StatusImmutable(status);
        status.setId(4);
        status.setTitle("Оплачен");
        PAID = new StatusImmutable(status);
        status.setId(5);
        status.setTitle("Отменён");
        CANCELLED = new StatusImmutable(status);
    }

    private int id;
    private String title;
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Status{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public static class StatusImmutable extends Status {
        private final Status status;

        public StatusImmutable(Status status) {
            this.status = new Status();
            this.status.setId(status.getId());
            this.status.setTitle(status.getTitle());
            this.status.setDescription(status.getDescription());
        }

        @Override
        public void setDescription(String description) {
            throw new UnsupportedOperationException("Trying to change immutable object");
        }

        @Override
        public void setId(int id) {
            throw new UnsupportedOperationException("Trying to change immutable object");
        }

        @Override
        public void setTitle(String title) {
            throw new UnsupportedOperationException("Trying to change immutable object");
        }

        @Override
        public String getDescription() {
            return status.getDescription();
        }

        @Override
        public int getId() {
            return status.getId();
        }

        @Override
        public String getTitle() {
            return status.getTitle();
        }
    }
}
