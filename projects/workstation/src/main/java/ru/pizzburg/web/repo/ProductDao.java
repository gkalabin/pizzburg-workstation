package ru.pizzburg.web.repo;

import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.stereotype.Repository;
import ru.pizzburg.constants.DatabaseErrors;
import ru.pizzburg.utils.*;
import ru.pizzburg.utils.data.PageableList;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.web.domain.*;
import ru.pizzburg.web.repo.mapper.ProductInGroupMapper;
import ru.pizzburg.web.repo.mapper.ProductInOrderMapper;
import ru.pizzburg.web.repo.mapper.ProductMapper;

import javax.sql.DataSource;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Repository
@Lazy(false)
public class ProductDao extends JdbcTemplate {

    private final Log logger = LogFactory.getLog(getClass());

    @Override
    @Autowired
    public void setDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    public List<Product> getProducts() {
        return query("SELECT * FROM pizzburgdb.Products ORDER BY title ASC", new ProductMapper());
    }

    public PageableList<Product> getProducts(PaginationParams pp, SortingParams sp) {
        PageableList<Product> result = new PageableList<Product>();
        result.setListSize(queryForInt("SELECT COUNT(*) FROM pizzburgdb.Products"));
        String query = "SELECT * FROM pizzburgdb.Products ORDER BY %s %s LIMIT ?, ?";
        query = String.format(query, sp.getSortCriterion(), Converter.getStringSortOrder(sp.getSortDirection()));
        final int from = (pp.getPageNumber() - 1) * pp.getObjectsPerPage();
        List<Product> products = query(query,
                new Object[]{from, pp.getObjectsPerPage()},
                new ProductMapper());
        result.setData(products);
        result.setPaginationParams(pp);
        result.setSortingParams(sp);
        return result;
    }

    public String addProduct(Product p) {
        if (queryForInt("SELECT COUNT(*) FROM pizzburgdb.Products WHERE title=?", p.getTitle()) != 0) {
            return DatabaseErrors.PRODUCT_EXISTS;
        }
        try {
            update("INSERT INTO pizzburgdb.Products (title, description) VALUES (?,?)", p.getTitle(), p.getDescription());
            int id = queryForInt("SELECT last_insert_id()");
            for (IngredientInProduct iip : p.getIngredients()) {
                update("INSERT INTO pizzburgdb.ProductIngredients (productID, ingredientID, count) VALUES (?,?,?)", id, iip.getIngredient().getId(), iip.getCount());
            }
            for (GroupInProduct gip : p.getGroups()) {
                update("INSERT INTO pizzburgdb.ProductsGroups (productID, groupID, price) VALUES (?,?,?)", id, gip.getGroup().getId(), gip.getPrice());
            }
        } catch (DataAccessException ex) {
            logger.error("add product fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return null;
    }

    public Product getProduct(int id) {
        List<Product> result = query("SELECT * FROM pizzburgdb.Products WHERE productID=?", new Object[]{id}, new ProductMapper());
        if (result.isEmpty()) {
            logger.warn("Access to product which doesn't exist");
            return null;
        } else if (result.size() != 1) {
            logger.warn("database schema error: Not unique result from query. Get " + result.size() + " products");
            return result.get(0);
        } else {
            return result.get(0);
        }
    }

    public String updateProduct(Product p, List<IngredientInProduct> initialIngredients, List<GroupInProduct> initialGroups) {
        if (queryForInt("SELECT COUNT(*) FROM pizzburgdb.Products WHERE title=? AND productID != ?", p.getTitle(), p.getId()) != 0) {
            return DatabaseErrors.PRODUCT_EXISTS;
        }
        int result = 0;
        try {
            result = update("UPDATE pizzburgdb.Products SET title=?, description=? WHERE productID=?",
                    p.getTitle(), p.getDescription(), p.getId());

            if (result == 0) {
                return DatabaseErrors.PRODUCT_NOT_EXISTS;
            }

            // adding new and updating modified ingredients
            for (IngredientInProduct iipNew : p.getIngredients()) {
                boolean contains = false;
                boolean changed = false;
                for (IngredientInProduct iipOld : initialIngredients) {
                    if (iipNew.getIngredient().getId() == iipOld.getIngredient().getId()) {
                        contains = true;
                        if (iipNew.getCount() != iipOld.getCount()) {
                            changed = true;
                        }
                        break;
                    }
                }
                if (contains && changed) {
                    update("UPDATE pizzburgdb.ProductIngredients SET count=? WHERE productID=? AND ingredientID=?",
                            iipNew.getCount(), p.getId(), iipNew.getIngredient().getId());
                } else if (!contains) {
                    update("INSERT INTO pizzburgdb.ProductIngredients (productID, ingredientID, count) VALUES (?,?,?)", p.getId(), iipNew.getIngredient().getId(), iipNew.getCount());
                }
            }

            // delete removed ingredients
            for (IngredientInProduct iipOld : initialIngredients) {
                boolean removed = true;
                for (IngredientInProduct iipNew : p.getIngredients()) {
                    if (iipNew.getIngredient().getId() == iipOld.getIngredient().getId()) {
                        removed = false;
                        break;
                    }
                }
                if (removed) {
                    update("DELETE FROM pizzburgdb.ProductIngredients WHERE productID=? AND ingredientID=?", p.getId(), iipOld.getIngredient().getId());
                }
            }

            // adding new and updating modified groups
            for (GroupInProduct gipNew : p.getGroups()) {
                boolean contains = false;
                boolean changed = false;
                for (GroupInProduct gipOld : initialGroups) {
                    if (gipNew.getGroup().getId() == gipOld.getGroup().getId()) {
                        contains = true;
                        if (gipNew.getPrice() != gipOld.getPrice()) {
                            changed = true;
                        }
                        break;
                    }
                }
                if (contains && changed) {
                    update("UPDATE pizzburgdb.ProductsGroups SET price=? WHERE productID=? AND groupID=?",
                            gipNew.getPrice(), p.getId(), gipNew.getGroup().getId());
                } else if (!contains) {
                    update("INSERT INTO pizzburgdb.ProductsGroups (productID, groupID, price) VALUES (?,?,?)", p.getId(), gipNew.getGroup().getId(), gipNew.getPrice());
                }
            }

            // delete removed ingredients
            for (GroupInProduct gipOld : initialGroups) {
                boolean removed = true;
                for (GroupInProduct gipNew : p.getGroups()) {
                    if (gipNew.getGroup().getId() == gipOld.getGroup().getId()) {
                        removed = false;
                        break;
                    }
                }
                if (removed) {
                    update("DELETE FROM pizzburgdb.ProductsGroups WHERE productID=? AND groupID=?", p.getId(), gipOld.getGroup().getId());
                }
            }
        } catch (DataAccessException ex) {
            logger.error("update product fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return null;
    }

    public String removeProduct(int id) {
        int result = 0;
        try {
            result = update("DELETE FROM pizzburgdb.Products WHERE productID=?", id);
        } catch (DataAccessException ex) {
            logger.error("delete product fails", ex);
            return DatabaseErrors.UNKNOWN;
        }
        return result == 1 ? null : DatabaseErrors.PRODUCT_NOT_EXISTS;
    }

    public List<ProductInOrder> getProductsForOrder(Order o) {
        if (o == null) {
            return null;
        }
        return query("SELECT Products.productID, Products.title AS productTitle, Products.description AS productDescription, "
                + "OrdersProducts.count, ProductGroups.groupID, ProductGroups.title AS groupTitle, ProductGroups.description AS groupDescription, ProductsGroups.price "
                + "FROM pizzburgdb.Products "
                + "LEFT JOIN pizzburgdb.ProductsGroups ON Products.productID=ProductsGroups.productID "
                + "LEFT JOIN pizzburgdb.ProductGroups ON ProductGroups.groupID=ProductsGroups.groupID "
                + "LEFT JOIN pizzburgdb.OrdersProducts ON Products.productID=OrdersProducts.productID AND OrdersProducts.groupID=ProductsGroups.groupID "
                + "WHERE OrdersProducts.orderID=?",
                new Object[]{o.getId()},
                new ProductInOrderMapper());
    }

    public List<ProductInGroup> getProductsForGroup(ProductGroup group) {
        if (group == null) {
            return null;
        }
        return query("SELECT * FROM pizzburgdb.Products " +
                "LEFT JOIN pizzburgdb.ProductsGroups ON Products.productID=ProductsGroups.productID " +
                "WHERE ProductsGroups.groupID = ? " +
                "ORDER BY title ASC",
                new Object[]{group.getId()},
                new ProductInGroupMapper());
    }

    public float getProductPrice(int productId, int groupId) {
        List<Float> result = query("SELECT price FROM pizzburgdb.ProductsGroups WHERE productID=? AND groupID=?",
                new Object[]{productId, groupId}, new SingleColumnRowMapper<Float>());
        if (result.isEmpty()) {
            logger.warn("Access to product which doesn't exist");
            return -1;
        } else if (result.size() != 1) {
            logger.warn("database schema error: Not unique result from query. Get " + result.size() + " products");
            return result.get(0);
        } else {
            return result.get(0);
        }
    }
}
