package ru.pizzburg.web.service;

import org.springframework.context.annotation.Lazy;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.pizzburg.constants.Const;
import ru.pizzburg.utils.DateUtils;

import java.text.ParseException;
import java.util.Date;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Lazy(false)
public class FormDateTimeRangeValidator implements Validator {


    @Override
    public boolean supports(Class clazz) {
        return FormDateTimeRange.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "from", "error.empty-date", "Empty date");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "to", "error.empty-date", "Empty date");
        FormDateTimeRange fo = (FormDateTimeRange) obj;
        fo.setFromDate(null);
        fo.setToDate(null);
        try {
            Date from = DateUtils.IN_DATE_TIME_FORMAT.parse(fo.getFrom());
            fo.setFromDate(from);
        } catch (ParseException ex) {
            errors.rejectValue("from", "error.invalid-date-time-format", new Object[]{Const.IN_DATE_TIME_FORMAT_STRING}, "Invalid datetime format");
        }
        try {
            Date to = DateUtils.IN_DATE_TIME_FORMAT.parse(fo.getTo());
            fo.setToDate(to);
        } catch (ParseException ex) {
            errors.rejectValue("to", "error.invalid-date-time-format", new Object[]{Const.IN_DATE_TIME_FORMAT_STRING}, "Invalid datetime format");
        }
        if (fo.getFromDate() != null && fo.getToDate() != null && fo.getFromDate().compareTo(fo.getToDate()) >= 0) {
            errors.rejectValue("from", "error.reverse-dates", "Before must be really before, ok?");
        }
    }
}