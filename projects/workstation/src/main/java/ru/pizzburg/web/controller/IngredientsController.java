package ru.pizzburg.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.pizzburg.constants.SortStrings;
import ru.pizzburg.utils.*;
import ru.pizzburg.utils.data.PageableList;
import ru.pizzburg.utils.data.PaginationParams;
import ru.pizzburg.utils.data.SortingParams;
import ru.pizzburg.web.domain.Ingredient;
import ru.pizzburg.web.service.ingredients.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Controller
@SessionAttributes({"ingredientSearch", "ingredientFilter"})
@Lazy(false)
public class IngredientsController {

    private static final String INGREDIENTS_VIEW_NAME = "ingredients";
    public static final String RESULT_COUNT_COOKIE_SUFFIX = "_ingredients_resultCount";
    protected final Log logger = LogFactory.getLog(getClass());
    @Autowired
    private IngredientsManager ingredientsManager;
    @Autowired
    private FormIngredientValidator formIngredientValidator;
    @Autowired
    private IngredientSearchValidator ingredientSearchValidator;
    @Autowired
    private MessageSource messageSource;

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/ingredients", method = RequestMethod.GET)
    public String getPage(HttpServletRequest request, ModelMap model, Principal principal) {
        IngredientSearch is = (IngredientSearch) model.get("ingredientSearch");
        if (is == null) {
            is = new IngredientSearch();
            model.addAttribute("ingredientSearch", is);
        }

        FormIngredient fi = new FormIngredient();
        model.addAttribute("formIngredient", fi);
        final PageableList ingredients = ingredientsManager.getIngredients(parsePagingParams(request, principal),
                parseSortingParams(request), (IngredientFilter) model.get("ingredientFilter"));

        model.addAttribute("ingredients", ingredients);
        return INGREDIENTS_VIEW_NAME;
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/ingredients", method = RequestMethod.POST)
    public String addIngredient(HttpServletRequest request, ModelMap model, @ModelAttribute("formIngredient") FormIngredient addIngredient,
                                BindingResult result, RedirectAttributes redirectAttributes, Principal principal) {
        formIngredientValidator.validate(addIngredient, result);

        if (!result.hasErrors()) {
            //form success
            final Ingredient ingredient = new Ingredient();
            ingredient.setTitle(addIngredient.getTitle());
            ingredient.setDescription(addIngredient.getDescription());
            ingredient.setUnits(addIngredient.getUnits());
            ingredient.setPrice(addIngredient.getPrice());
            ingredient.setBalance(addIngredient.getBalance());

            String additionResult = ingredientsManager.addIngredient(ingredient);
            if (additionResult != null) {
                model.addAttribute("errorMessage", messageSource.getMessage(additionResult, null, null));
                model.addAttribute("ingredients", ingredientsManager.getIngredients(parsePagingParams(request, principal),
                        parseSortingParams(request), (IngredientFilter) model.get("ingredientFilter")));
                return INGREDIENTS_VIEW_NAME;
            } else {
                redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.ingredient-successfully-added", null, null));
                redirectAttributes.addFlashAttribute("ingredientSearch", model.get("ingredientSearch"));
                return "redirect:/" + INGREDIENTS_VIEW_NAME;
            }
        } else {
            model.addAttribute("ingredients", ingredientsManager.getIngredients(parsePagingParams(request, principal),
                    parseSortingParams(request), (IngredientFilter) model.get("ingredientFilter")));
            return INGREDIENTS_VIEW_NAME;
        }
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/ingredients/search", method = RequestMethod.GET)
    public String searchPage(HttpServletRequest request, ModelMap model, Principal principal) {
        IngredientSearch is = (IngredientSearch) model.get("ingredientSearch");
        if (is == null) {
            is = new IngredientSearch();
            model.addAttribute("ingredientSearch", is);
        }

        FormIngredient fi = (FormIngredient) model.get("formIngredient");
        if (fi == null) {
            fi = new FormIngredient();
            model.addAttribute("formIngredient", fi);
        }

        final PageableList ingredients = ingredientsManager.getIngredients(parsePagingParams(request, principal),
                parseSortingParams(request), (IngredientFilter) model.get("ingredientFilter"));
        model.addAttribute("ingredients", ingredients);
        return INGREDIENTS_VIEW_NAME;
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/ingredients/search/cancel", method = RequestMethod.GET)
    public String cancelSearch(ModelMap model) {
        FormIngredient fi = (FormIngredient) model.get("formIngredient");
        if (fi == null) {
            fi = new FormIngredient();
            model.addAttribute("formIngredient", fi);
        }
        model.addAttribute("ingredientSearch", new IngredientSearch());
        model.addAttribute("ingredientFilter", new IngredientFilter());
        return "redirect:/ingredients/";
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/ingredients/search", method = RequestMethod.POST)
    public String search(HttpServletRequest request, ModelMap model, @ModelAttribute("ingredientSearch") IngredientSearch is,
                         BindingResult result, Principal principal) {
        ingredientSearchValidator.validate(is, result);

        if (is == null) {
            is = new IngredientSearch();
            model.addAttribute("ingredientSearch", is);
        }

        FormIngredient fi = (FormIngredient) model.get("formIngredient");
        if (fi == null) {
            fi = new FormIngredient();
            model.addAttribute("formIngredient", fi);
        }

        IngredientFilter filter = is.getIngredientFilter();
        model.addAttribute("ingredientFilter", filter);
        if (result.hasErrors()) {
            model.addAttribute("ingredients", ingredientsManager.getIngredients(parsePagingParams(request, principal),
                    parseSortingParams(request), filter));
            return INGREDIENTS_VIEW_NAME;
        } else {
            return "redirect:/ingredients/";
        }
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/ingredients/edit/{id}", method = RequestMethod.GET)
    public String getEditForm(HttpServletRequest request, ModelMap model, @PathVariable int id,
                              RedirectAttributes redirectAttributes, Principal principal) {
        IngredientSearch is = (IngredientSearch) model.get("ingredientSearch");
        if (is == null) {
            is = new IngredientSearch();
            model.addAttribute("ingredientSearch", is);
        }
        Ingredient i = ingredientsManager.getIngredient(id);
        if (i == null) {
            logger.warn("Null ingredient request");
            redirectAttributes.addFlashAttribute("errorMessage", messageSource.getMessage("error.ingredient-not-exist", null, null));
            return "redirect:/" + INGREDIENTS_VIEW_NAME;
        }
        FormIngredient fi = new FormIngredient();
        fi.setTitle(i.getTitle());
        fi.setPriceStr(String.valueOf(i.getPrice()));
        fi.setBalanceStr(String.valueOf(i.getBalance()));
        fi.setDescription(i.getDescription());
        fi.setUnits(i.getUnits());
        fi.setId(i.getId());
        model.addAttribute("formIngredient", fi);
        model.addAttribute("ingredients", ingredientsManager.getIngredients(parsePagingParams(request, principal),
                parseSortingParams(request), (IngredientFilter) model.get("ingredientFilter")));
        return INGREDIENTS_VIEW_NAME;
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/ingredients/add/{id}", method = RequestMethod.GET)
    public String getAddForm(HttpServletRequest request, ModelMap model, @PathVariable int id,
                             RedirectAttributes redirectAttributes, Principal principal) {
        return prepareStockForm(request, model, id, redirectAttributes, principal);
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/ingredients/subtract/{id}", method = RequestMethod.GET)
    public String getSubtractForm(HttpServletRequest request, ModelMap model, @PathVariable int id,
                                  RedirectAttributes redirectAttributes, Principal principal) {
        return prepareStockForm(request, model, id, redirectAttributes, principal);
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/ingredients/add/{id}", method = RequestMethod.POST)
    public String addIngredientToStock(HttpServletRequest request, ModelMap model, @ModelAttribute("formIngredient") FormIngredient editIngredient,
                                       BindingResult result, RedirectAttributes redirectAttributes, Principal principal) {
        return processStockEdition(request, model, editIngredient, result, redirectAttributes, 1, principal);
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/ingredients/subtract/{id}", method = RequestMethod.POST)
    public String subtractIngredientFromStock(HttpServletRequest request, ModelMap model, @ModelAttribute("formIngredient") FormIngredient editIngredient,
                                              BindingResult result, RedirectAttributes redirectAttributes,
                                              Principal principal) {
        return processStockEdition(request, model, editIngredient, result, redirectAttributes, -1, principal);
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/ingredients/edit/{id}", method = RequestMethod.POST)
    public String editIngredient(HttpServletRequest request, ModelMap model, @ModelAttribute("formIngredient") FormIngredient editIngredient,
                                 BindingResult result, RedirectAttributes redirectAttributes, Principal principal) {
        formIngredientValidator.validate(editIngredient, result);

        if (!result.hasErrors()) {
            //form success
            final Ingredient ingredient = new Ingredient();
            ingredient.setTitle(editIngredient.getTitle());
            ingredient.setDescription(editIngredient.getDescription());
            ingredient.setUnits(editIngredient.getUnits());
            ingredient.setPrice(editIngredient.getPrice());
            ingredient.setBalance(editIngredient.getBalance());
            ingredient.setId(editIngredient.getId());

            String updateResult = ingredientsManager.updateIngredient(ingredient);
            if (updateResult != null) {
                model.addAttribute("errorMessage", messageSource.getMessage(updateResult, null, null));
                model.addAttribute("ingredients", ingredientsManager.getIngredients(parsePagingParams(request, principal),
                        parseSortingParams(request), (IngredientFilter) model.get("ingredientFilter")));
                return INGREDIENTS_VIEW_NAME;
            } else {
                redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.ingredient-successfully-updated", null, null));
                redirectAttributes.addFlashAttribute("ingredientSearch", model.get("ingredientSearch"));
                return "redirect:/" + INGREDIENTS_VIEW_NAME;
            }
        } else {
            model.addAttribute("ingredients", ingredientsManager.getIngredients(parsePagingParams(request, principal),
                    parseSortingParams(request), (IngredientFilter) model.get("ingredientFilter")));
            return INGREDIENTS_VIEW_NAME;
        }
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/ingredients/remove/{id}", method = RequestMethod.GET)
    public String removeIngredient(HttpServletRequest request, ModelMap model, @PathVariable int id,
                                   RedirectAttributes redirectAttributes, Principal principal) {
        IngredientSearch is = (IngredientSearch) model.get("ingredientSearch");
        if (is == null) {
            is = new IngredientSearch();
            model.addAttribute("ingredientSearch", is);
        }

        String removeResult = ingredientsManager.removeIngredient(id);
        if (removeResult != null) {
            model.addAttribute("errorMessage", messageSource.getMessage(removeResult, null, null));
            model.addAttribute("ingredients", ingredientsManager.getIngredients(parsePagingParams(request, principal),
                    parseSortingParams(request), (IngredientFilter) model.get("ingredientFilter")));
            return INGREDIENTS_VIEW_NAME;
        } else {
            redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.ingredient-successfully-removed", null, null));
            return "redirect:/" + INGREDIENTS_VIEW_NAME;
        }
    }

    private SortingParams parseSortingParams(HttpServletRequest request) {
        String sort = request.getParameter("sort");
        String dir = request.getParameter("dir");
        if (sort == null || dir == null || !SortStrings.isDirectionCorrect(dir)
                || !SortStrings.isIngredientCorrect(sort)) {
            return null;
        }
        return new SortingParams(sort, Converter.parseSortOrder(dir));
    }

    private String prepareStockForm(HttpServletRequest request, ModelMap model, int id,
                                    RedirectAttributes redirectAttributes, Principal principal) {
        IngredientSearch is = (IngredientSearch) model.get("ingredientSearch");
        if (is == null) {
            is = new IngredientSearch();
            model.addAttribute("ingredientSearch", is);
        }
        Ingredient i = ingredientsManager.getIngredient(id);
        if (i == null) {
            logger.warn("Null ingredient request");
            redirectAttributes.addFlashAttribute("errorMessage", messageSource.getMessage("error.ingredient-not-exist", null, null));
            return "redirect:/" + INGREDIENTS_VIEW_NAME;
        }
        FormIngredient fi = new FormIngredient();
        fi.setTitle(i.getTitle());
        fi.setPriceStr(String.valueOf(i.getPrice()));
        fi.setBalanceStr("0");
        fi.setDescription(i.getDescription());
        fi.setUnits(i.getUnits());
        fi.setId(i.getId());
        model.addAttribute("formIngredient", fi);
        model.addAttribute("ingredients", ingredientsManager.getIngredients(parsePagingParams(request, principal),
                parseSortingParams(request), (IngredientFilter) model.get("ingredientFilter")));
        return INGREDIENTS_VIEW_NAME;
    }

    private String processStockEdition(HttpServletRequest request, ModelMap model, FormIngredient editIngredient,
                                       BindingResult result, RedirectAttributes redirectAttributes, int sgn,
                                       Principal principal) {
        formIngredientValidator.validateBalance(editIngredient, result);

        if (!result.hasErrors()) {
            //form success
            String updateResult = ingredientsManager.updateIngredientBalance(editIngredient.getId(), Math.signum(sgn) * editIngredient.getBalance());
            if (updateResult != null) {
                model.addAttribute("errorMessage", messageSource.getMessage(updateResult, null, null));
                model.addAttribute("ingredients", ingredientsManager.getIngredients(parsePagingParams(request, principal),
                        parseSortingParams(request), (IngredientFilter) model.get("ingredientFilter")));
                return INGREDIENTS_VIEW_NAME;
            } else {
                redirectAttributes.addFlashAttribute("infoMessage", messageSource.getMessage("info.ingredient-successfully-updated", null, null));
                redirectAttributes.addFlashAttribute("ingredientSearch", model.get("ingredientSearch"));
                return "redirect:/" + INGREDIENTS_VIEW_NAME;
            }
        } else {
            model.addAttribute("ingredients", ingredientsManager.getIngredients(parsePagingParams(request, principal),
                    parseSortingParams(request), (IngredientFilter) model.get("ingredientFilter")));
            return INGREDIENTS_VIEW_NAME;
        }
    }

    private PaginationParams parsePagingParams(HttpServletRequest request, Principal principal) {
        String pageStr = request.getParameter("page");
        String resultsStr = request.getParameter("results");
        String cookieName = Digest.getSha1Hash(principal.getName() + RESULT_COUNT_COOKIE_SUFFIX);
        for (Cookie c: request.getCookies()) {
            if (cookieName.equals(c.getName())) {
                resultsStr = c.getValue();
                break;
            }
        }
        if (pageStr == null && resultsStr == null) {
            return null;
        }
        int page = -1;
        if (pageStr != null) {
            try {
                page = Integer.parseInt(pageStr);
            } catch (NumberFormatException ex) {
                logger.info("invalid paging params (ingredients page): " + pageStr);
            }
        }
        int results = -1;
        if (resultsStr != null) {
            try {
                results = Integer.parseInt(resultsStr);
            } catch (NumberFormatException ex) {
                logger.info("invalid paging params (ingredients page): " + pageStr);
            }
        }
        return new PaginationParams(page, results);
    }
}
