package ru.pizzburg.web.service.groups;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.pizzburg.web.domain.ProductGroup;
import ru.pizzburg.web.repo.ProductDao;
import ru.pizzburg.web.repo.ProductGroupDao;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Service
@Lazy(false)
public class ProductGroupsManager {
    @Autowired
    private ProductGroupDao productGroupDao;
    @Autowired
    private ProductDao productDao;

    public List<ProductGroup> getGroups() {
        final List<ProductGroup> groupList = productGroupDao.getGroups();
        if (groupList == null) {
            return null;
        }
        for (ProductGroup pg : groupList) {
            pg.setProducts(productDao.getProductsForGroup(pg));
        }
        return groupList;
    }

    public String addGroup(ProductGroup group) {
        return productGroupDao.addGroup(group);
    }

    public ProductGroup getGroup(int id) {
        final ProductGroup group = productGroupDao.getGroup(id);
        group.setProducts(productDao.getProductsForGroup(group));
        return group;
    }

    public String updateGroup(ProductGroup group) {
        return productGroupDao.updateGroup(group, productDao.getProductsForGroup(group));
    }

    public String removeGroup(int id) {
        return productGroupDao.removeGroup(id);
    }
}
