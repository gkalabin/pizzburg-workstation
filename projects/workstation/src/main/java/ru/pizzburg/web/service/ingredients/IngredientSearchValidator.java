package ru.pizzburg.web.service.ingredients;

import org.springframework.context.annotation.Lazy;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.pizzburg.constants.FieldLength;
import ru.pizzburg.utils.StringUtils;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Lazy(false)
public class IngredientSearchValidator implements Validator {

    @Override
    public boolean supports(Class clazz) {
        return IngredientSearch.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        IngredientSearch is = (IngredientSearch) obj;
        checkFieldLength(is, errors);
        is.setIngredientFilter(new IngredientFilter());

        if (!StringUtils.isStringEmptyOrWhitespaces(is.getTitle())) {
            is.getIngredientFilter().setTitle(is.getTitle().replace("*", "%").replace("?", "_"));
            is.getIngredientFilter().setIsEmpty(false);
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(is.getDescription())) {
            is.getIngredientFilter().setDescription(is.getDescription().replace("*", "%").replace("?", "_"));
            is.getIngredientFilter().setIsEmpty(false);
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(is.getUnits())) {
            is.getIngredientFilter().setUnits(is.getUnits().replace("*", "%").replace("?", "_"));
            is.getIngredientFilter().setIsEmpty(false);
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(is.getPriceFromStr())) {
            try {
                is.setPriceFromStr(is.getPriceFromStr().replace(",", "."));
                float price = Float.valueOf(is.getPriceFromStr());
                if (price <= 0) {
                    errors.rejectValue("priceFromStr", "error.negative-price", "Price must be > 0");
                } else {
                    is.getIngredientFilter().setPriceFrom(price);
                    is.getIngredientFilter().setIsEmpty(false);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("priceFromStr", "typeMismatch.float", "Not convertable to number");
            }
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(is.getPriceToStr())) {
            try {
                is.setPriceToStr(is.getPriceToStr().replace(",", "."));
                float price = Float.valueOf(is.getPriceToStr());
                if (price <= 0) {
                    errors.rejectValue("priceToStr", "error.negative-price", "Price must be > 0");
                } else {
                    is.getIngredientFilter().setPriceTo(price);
                    is.getIngredientFilter().setIsEmpty(false);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("priceToStr", "typeMismatch.float", "Not convertable to number");
            }
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(is.getBalanceFromStr())) {
            try {
                is.setBalanceFromStr(is.getBalanceFromStr().replace(",", "."));
                float balance = Float.valueOf(is.getBalanceFromStr());
                is.getIngredientFilter().setBalanceFrom(balance);
                is.getIngredientFilter().setIsEmpty(false);
            } catch (NumberFormatException ex) {
                errors.rejectValue("balanceFromStr", "typeMismatch.float", "Not convertable to number");
            }
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(is.getBalanceToStr())) {
            try {
                is.setBalanceToStr(is.getBalanceToStr().replace(",", "."));
                float balance = Float.valueOf(is.getBalanceToStr());
                is.getIngredientFilter().setBalanceTo(balance);
                is.getIngredientFilter().setIsEmpty(false);
            } catch (NumberFormatException ex) {
                errors.rejectValue("balanceToStr", "typeMismatch.float", "Not convertable to number");
            }
        }

        if (errors.hasErrors()) {
            is.setIngredientFilter(new IngredientFilter());
        }
    }

    private void checkFieldLength(IngredientSearch ai, Errors errors) {
        if (ai.getTitle().length() > FieldLength.INGREDIENT_TITLE) {
            errors.rejectValue("title", "error.too-long-value",
                    new Object[]{FieldLength.INGREDIENT_TITLE}, "Max length is " + FieldLength.INGREDIENT_TITLE);
        }
        if (ai.getDescription().length() > FieldLength.INGREDIENT_DESCRIPTION) {
            errors.rejectValue("description", "error.too-long-value",
                    new Object[]{FieldLength.INGREDIENT_DESCRIPTION}, "Max length is " + FieldLength.INGREDIENT_DESCRIPTION);
        }
        if (ai.getUnits().length() > FieldLength.INGREDIENT_UNITS) {
            errors.rejectValue("units", "error.too-long-value",
                    new Object[]{FieldLength.INGREDIENT_UNITS}, "Max length is " + FieldLength.INGREDIENT_UNITS);
        }
    }
}