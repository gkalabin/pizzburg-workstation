package ru.pizzburg.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.pizzburg.web.domain.Client;
import ru.pizzburg.web.domain.Ingredient;
import ru.pizzburg.web.domain.Order;
import ru.pizzburg.web.domain.Product;
import ru.pizzburg.web.service.clients.ClientsManager;
import ru.pizzburg.web.service.ingredients.IngredientsManager;
import ru.pizzburg.web.service.orders.OrdersManager;
import ru.pizzburg.web.service.products.ProductsManager;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Controller
@Lazy(false)
public class DetailsController {

    @Autowired
    private OrdersManager ordersManager;
    @Autowired
    private IngredientsManager ingredientsManager;
    @Autowired
    private ProductsManager productsManager;
    @Autowired
    private ClientsManager clientsManager;

	@Secured({"ROLE_ADMIN","ROLE_OPERATOR"})
    @RequestMapping(value = "/details/order", method = RequestMethod.GET)
    public
    @ResponseBody
    Order getOrderDetails(@RequestParam int id) {
        return ordersManager.getOrder(id);
    }

    @Secured({"ROLE_ADMIN","ROLE_OPERATOR"})
    @RequestMapping(value = "/details/ingredient", method = RequestMethod.GET)
    public
    @ResponseBody
    Ingredient getIngredientDetails(@RequestParam int id) {
        return ingredientsManager.getIngredient(id);
    }

    @Secured({"ROLE_ADMIN","ROLE_OPERATOR"})
    @RequestMapping(value = "/details/product", method = RequestMethod.GET)
    public
    @ResponseBody
    Product getProductDetails(@RequestParam int id) {
        return productsManager.getProduct(id);
    }

    @Secured({"ROLE_ADMIN","ROLE_OPERATOR"})
    @RequestMapping(value = "/details/client", method = RequestMethod.GET)
    public
    @ResponseBody
    Client getClientDetails(@RequestParam String phone) {
        return clientsManager.getClient(Long.valueOf(phone.replaceAll("\\D","")));
    }
}
