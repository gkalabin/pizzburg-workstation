package ru.pizzburg.web.service.products;

import java.util.ArrayList;
import java.util.List;
import org.springframework.util.AutoPopulatingList;
import ru.pizzburg.web.domain.GroupInProduct;
import ru.pizzburg.web.domain.IngredientInProduct;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class FormProduct {

    private int id;
    private String title;
    private String description;
    private AutoPopulatingList<IngredientFormEntry> ingredientsStr = new AutoPopulatingList<IngredientFormEntry>(IngredientFormEntry.class);
    private List<IngredientInProduct> ingredients = new ArrayList<IngredientInProduct>();
    private List<Integer> deletedIngredients = new ArrayList<Integer>();
    private AutoPopulatingList<GroupFormEntry> groupsStr = new AutoPopulatingList<GroupFormEntry>(GroupFormEntry.class);
    private List<GroupInProduct> groups = new ArrayList<GroupInProduct>();
    private List<Integer> deletedGroups = new ArrayList<Integer>();

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AutoPopulatingList<IngredientFormEntry> getIngredientsStr() {
        return ingredientsStr;
    }

    public void setIngredientsStr(AutoPopulatingList<IngredientFormEntry> ingredientsStr) {
        this.ingredientsStr = ingredientsStr;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<IngredientInProduct> getIngredients() {
        return ingredients;
    }

    public List<Integer> getDeletedIngredients() {
        return deletedIngredients;
    }

    public List<Integer> getDeletedGroups() {
        return deletedGroups;
    }

    public void setDeletedGroups(List<Integer> deletedGroups) {
        this.deletedGroups = deletedGroups;
    }

    public List<GroupInProduct> getGroups() {
        return groups;
    }

    public void setGroups(List<GroupInProduct> groups) {
        this.groups = groups;
    }

    public AutoPopulatingList<GroupFormEntry> getGroupsStr() {
        return groupsStr;
    }

    public void setGroupsStr(AutoPopulatingList<GroupFormEntry> groupsStr) {
        this.groupsStr = groupsStr;
    }

    @Override
    public String toString() {
        return "FormProduct{" + "id=" + id + ", title=" + title + ", description=" + description + ", ingredientsStr=" + ingredientsStr + ", ingredients=" + ingredients + ", deletedIngredients=" + deletedIngredients + ", groupsStr=" + groupsStr + ", groups=" + groups + ", deletedGroups=" + deletedGroups + '}';
    }
}
