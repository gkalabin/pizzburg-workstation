package ru.pizzburg.web.service.orders;

import org.springframework.context.annotation.Lazy;
import org.springframework.util.AutoPopulatingList;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.pizzburg.constants.Const;
import ru.pizzburg.constants.FieldLength;
import ru.pizzburg.utils.StringUtils;
import ru.pizzburg.utils.DiscountCardUtils;
import ru.pizzburg.utils.PhoneUtils;
import ru.pizzburg.web.domain.DiscountCard;
import ru.pizzburg.web.domain.ProductInOrder;
import ru.pizzburg.web.domain.WorkstationUser;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
@Lazy(false)
public class FormOrderValidator implements Validator {

    @Override
    public boolean supports(Class clazz) {
        return FormOrder.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "clientPhoneStr", "error.empty-phone", "Empty phone");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shipmentAddress", "error.empty-address", "Empty address");
        FormOrder fo = (FormOrder) obj;
        checkFieldLength(fo, errors);
        if (errors.getFieldErrorCount("clientPhoneStr") == 0) {
            if (!PhoneUtils.isPhoneValid(fo.getClientPhoneStr())) {
                errors.rejectValue("clientPhoneStr", "error.invalid-phone", new Object[]{PhoneUtils.PHONE_FULL_DIGITS_COUNT, PhoneUtils.PHONE_LOCAL_DIGITS_COUNT},
                        "Phone number must contain " + PhoneUtils.PHONE_FULL_DIGITS_COUNT + " or " + PhoneUtils.PHONE_LOCAL_DIGITS_COUNT + " digits");
            }
        }

        AutoPopulatingList<ProductFormEntry> productsStr = fo.getProductsStr();
        AutoPopulatingList<ProductFormEntry> productsStrNew = new AutoPopulatingList<ProductFormEntry>(ProductFormEntry.class);
        productsStrNew.addAll(productsStr);
        productsStr.clear();
        fo.getProducts().clear();

        // remove removed items
        // Copy not deleted products
        for (int i = 0; i < productsStrNew.size(); i++) {
            if (!fo.getDeletedProducts().contains(i)) {
                final ProductFormEntry product = productsStrNew.get(i);
                productsStr.add(product);
            }
        }
        fo.getDeletedProducts().clear();

        // checking products
        for (int i = 0; i < productsStr.size(); i++) {
            final ProductInOrder product = new ProductInOrder();
            try {
                int productId = Integer.parseInt(productsStr.get(i).getProductId());
                if (productId < 0) {
                    errors.rejectValue("productsStr[" + i + "]", "error.product-negative-id", "Product error");
                } else {
                    fo.getProducts().add(product);
                    product.getProduct().setId(productId);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("productsStr[" + i + "]", "error.product-not-int-id", "Product error");
                continue;
            }
            try {
                int groupId = Integer.parseInt(productsStr.get(i).getGroupId());
                if (groupId < 0) {
                    errors.rejectValue("productsStr[" + i + "]", "error.group-negative-id", "Group error");
                } else {
                    product.getGroup().setId(groupId);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("productsStr[" + i + "]", "error.group-not-int-id", "Group error");
                continue;
            }
            try {
                int count = Integer.parseInt(productsStr.get(i).getCount());
                if (count <= 0) {
                    errors.rejectValue("productsStr[" + i + "]", "error.negative-count", "Count must be >0");
                } else {
                    product.setCount(count);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("productsStr[" + i + "]", "typeMismatch.int", "Count must be a number");
            }
        }

        // checking duplicates
        for (int i = 0; i < productsStr.size(); i++) {
            for (int j = i + 1; j < productsStr.size(); j++) {
                if (productsStr.get(i).getProductId().equals(productsStr.get(j).getProductId()) && !productsStr.get(i).getProductId().isEmpty()
                        && productsStr.get(i).getGroupId().equals(productsStr.get(j).getGroupId()) && !productsStr.get(i).getGroupId().isEmpty()) {
                    if (errors.getFieldErrorCount("productsStr[" + i + "]") == 0) {
                        errors.rejectValue("productsStr[" + i + "]", "error.not-unique-product", "Duplicate product");
                    }
                    if (errors.getFieldErrorCount("productsStr[" + j + "]") == 0) {
                        errors.rejectValue("productsStr[" + j + "]", "error.not-unique-product", "Duplicate product");
                    }
                }
            }
        }

        // alternate discounts
        if (!StringUtils.isStringEmptyOrWhitespaces(fo.getAlternateDiscount())) {
            try {
                float discount = Float.parseFloat(fo.getAlternateDiscount().replace(",", "."));
                if (discount <= 0) {
                    errors.rejectValue("alternateDiscount", "error.negative-discount", "Discount must be >0");
                } else if (discount >= Const.MAX_DISCOUNT_VALUE) {
                    errors.rejectValue("alternateDiscount", "error.too-big-discount",
                            new Object[]{Const.MAX_DISCOUNT_VALUE},
                            "Discount must be <" + Const.MAX_DISCOUNT_VALUE);
                    fo.setAlternateDiscountFloat(-1);
                } else {
                    fo.setAlternateDiscountFloat(discount);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("alternateDiscount", "typeMismatch.float", "Discount must be a number");
            }
        } else {
            fo.setAlternateDiscountFloat(-1);
        }
        if (!StringUtils.isStringEmptyOrWhitespaces(fo.getAlternateDiscountCard())) {
            if (!DiscountCardUtils.isDiscountCardNumberValid(fo.getAlternateDiscountCard())) {
                errors.rejectValue("alternateDiscountCard", "error.invalid-card",
                        new Object[]{DiscountCardUtils.DISCOUNT_CARD_DIGITS_COUNT},
                        "Card number must contain " + DiscountCardUtils.DISCOUNT_CARD_DIGITS_COUNT + " digits");
                fo.setAlternateDiscountCardObject(null);
            } else {
                DiscountCard dc = new DiscountCard();
                dc.setId(DiscountCardUtils.getDiscountCardNumber(fo.getAlternateDiscountCard()));
                fo.setAlternateDiscountCardObject(dc);
            }
        } else {
            fo.setAlternateDiscountCardObject(null);
        }

        try {
            int personsCount = Integer.parseInt(fo.getPersonsCountStr());
            if (personsCount <= 0) {
                errors.rejectValue("personsCountStr", "error.negative-persons-count", "Persons count must be >0");
            }
            fo.setPersonsCount(personsCount);
        } catch (NumberFormatException ex) {
            errors.rejectValue("personsCountStr", "typeMismatch.int", "Persons count must be a number");
        }
    }

    private void checkFieldLength(FormOrder fo, Errors errors) {
        if (fo.getShipmentAddress().length() > FieldLength.CLIENT_DEFAULT_ADDRESS) {
            errors.rejectValue("shipmentAddress", "error.too-long-value",
                    new Object[]{FieldLength.CLIENT_DEFAULT_ADDRESS}, "Max length is " + FieldLength.CLIENT_DEFAULT_ADDRESS);
        }
        if (fo.getClientName().length() > FieldLength.CLIENT_NAME) {
            errors.rejectValue("clientName", "error.too-long-value",
                    new Object[]{FieldLength.CLIENT_NAME}, "Max length is " + FieldLength.CLIENT_NAME);
        }
        if (fo.getComment().length() > FieldLength.ORDER_COMMENT) {
            errors.rejectValue("comment", "error.too-long-value",
                    new Object[]{FieldLength.ORDER_COMMENT}, "Max length is " + FieldLength.ORDER_COMMENT);
        }
    }

    public void validateCarrier(Object obj, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "carrierIdStr", "error.empty-carrier", "Empty carrier");
        FormOrder fo = (FormOrder) obj;
        if (errors.getFieldErrorCount("carrierIdStr") == 0) {
            try {
                int carrierId = Integer.parseInt(fo.getCarrierIdStr());
                if (carrierId < 0) {
                    errors.rejectValue("carrierIdStr", "error.negative-carrier-id", "Carrier error");
                } else {
                    final WorkstationUser carrier = new WorkstationUser();
                    carrier.setId(carrierId);
                    fo.setCarrier(carrier);
                }
            } catch (NumberFormatException ex) {
                errors.rejectValue("carrierIdStr", "error.not-int-carrier-id", "Carrier error");
            }
        }
    }
}
