package ru.pizzburg.web.domain;

import ru.pizzburg.utils.NameUtils;
import ru.pizzburg.utils.PhoneUtils;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class Client {

    private long phone;
    private String name;
    private String surname;
    private String middleName;
    private String defaultAddress;
    private DiscountCard card;
    private float ordersSum;

    public DiscountCard getCard() {
        return card;
    }

    public void setCard(DiscountCard card) {
        this.card = card;
    }

    public String getDefaultAddress() {
        return defaultAddress;
    }

    public void setDefaultAddress(String defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhoneFormatted() {
        return PhoneUtils.getPhoneNumberFormatted(phone);
    }

    public String getFullName() {
        return NameUtils.getFullNameFormatted(name, surname, middleName);
    }

    public float getOrdersSum() {
        return ordersSum;
    }

    public void setOrdersSum(float ordersSum) {
        this.ordersSum = ordersSum;
    }

    @Override
    public String toString() {
        return "Client{" +
                "phone=" + phone +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", middleName='" + middleName + '\'' +
                ", defaultAddress='" + defaultAddress + '\'' +
                ", card=" + card +
                ", ordersSum=" + ordersSum +
                '}';
    }
}
