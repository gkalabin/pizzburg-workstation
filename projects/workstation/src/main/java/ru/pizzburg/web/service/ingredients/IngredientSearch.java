package ru.pizzburg.web.service.ingredients;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class IngredientSearch {

    private String title = "";
    private String description = "";
    private String units = "";
    private String priceFromStr = "";
    private String priceToStr = "";
    private String balanceFromStr = "";
    private String balanceToStr = "";
    private IngredientFilter ingredientFilter;

    public IngredientFilter getIngredientFilter() {
        return ingredientFilter;
    }

    public void setIngredientFilter(IngredientFilter ingredientFilter) {
        this.ingredientFilter = ingredientFilter;
    }

    public String getPriceFromStr() {
        return priceFromStr;
    }

    public void setPriceFromStr(String priceFromStr) {
        this.priceFromStr = priceFromStr;
    }

    public String getPriceToStr() {
        return priceToStr;
    }

    public void setPriceToStr(String priceToStr) {
        this.priceToStr = priceToStr;
    }

    public String getBalanceFromStr() {
        return balanceFromStr;
    }

    public void setBalanceFromStr(String balanceFromStr) {
        this.balanceFromStr = balanceFromStr;
    }

    public String getBalanceToStr() {
        return balanceToStr;
    }

    public void setBalanceToStr(String balanceToStr) {
        this.balanceToStr = balanceToStr;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    @Override
    public String toString() {
        return "IngredientSearch{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", units='" + units + '\'' +
                ", priceFromStr='" + priceFromStr + '\'' +
                ", priceToStr='" + priceToStr + '\'' +
                ", balanceFromStr='" + balanceFromStr + '\'' +
                ", balanceToStr='" + balanceToStr + '\'' +
                ", ingredientFilter=" + ingredientFilter +
                '}';
    }
}
