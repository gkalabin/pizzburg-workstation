package ru.pizzburg.constants;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class Const {
    public static final float MAX_DISCOUNT_VALUE = 100; // in percents
    public static final String DATE_FORMAT_STRING = "dd.MM.yyyy";
    public static final String OUT_DATE_TIME_FORMAT_STRING = "HH:mm dd.MM.yyyy";
    public static final String IN_DATE_TIME_FORMAT_STRING = "dd.MM.yyyy HH:mm";

    private Const() {}
}
