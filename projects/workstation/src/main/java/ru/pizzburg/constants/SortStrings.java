package ru.pizzburg.constants;

import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class SortStrings {

    public static final String ADMIN_NAME_SORT_CRITERION = "name";
    public static final String ADMIN_ROLE_SORT_CRITERION = "role";
    public static final String CLIENT_NAME_SORT_CRITERION = "name";
    public static final String ORDER_REGISTRATOR_SORT_CRITERION = "registrator";
    public static final String ORDER_CARRIER_SORT_CRITERION = "carrier";
    public static final String ORDER_STATUS_SORT_CRITERION = "status";
    public static final String ORDER_DISCOUNT_CARD_SORT_CRITERION = "discountcard";
    public static final String ORDER_COST_SORT_CRITERION = "cost";
    public static final String STATS_PRODUCT_SORT_CRITERION = "product";
    public static final String STATS_INGREDIENT_SORT_CRITERION = "ingredient";
    private static final Collection<String> directions = new HashSet<String>();
    private static final Collection<String> ingredients = new HashSet<String>();
    private static final Collection<String> products = new HashSet<String>();
    private static final Collection<String> clients = new HashSet<String>();
    private static final Collection<String> admin = new HashSet<String>();
    private static final Collection<String> statsProducts = new HashSet<String>();
    private static final Collection<String> statsIngredients = new HashSet<String>();
    private static final Collection<String> statsOperators = new HashSet<String>();
    private static final Collection<String> statsCarriers = new HashSet<String>();
    private static final Collection<String> orders = new HashSet<String>();

    static {
        // directions
        directions.add("asc");
        directions.add("desc");
        // ingredients
        ingredients.add("title");
        ingredients.add("description");
        ingredients.add("price");
        ingredients.add("units");
        ingredients.add("balance");
        // products
        products.add("title");
        products.add("description");
        products.add("price");
        // products
        clients.add("phone");
        clients.add(CLIENT_NAME_SORT_CRITERION);
        clients.add("discountcardnumber");
        clients.add("defaultaddress");
        clients.add("orderssum");
        // admin
        admin.add(ADMIN_NAME_SORT_CRITERION);
        admin.add("login");
        admin.add(ADMIN_ROLE_SORT_CRITERION);
        // stats
        //------ products
        statsProducts.add(STATS_PRODUCT_SORT_CRITERION);
        statsProducts.add("count");
        //------ ingredients
        statsIngredients.add(STATS_INGREDIENT_SORT_CRITERION);
        statsIngredients.add("count");
        statsIngredients.add("cost");
        //------ operators
        statsOperators.add("orderid");
        statsOperators.add("ordercost");
        statsOperators.add("orderprice");
        statsOperators.add("registerdate");
        //------ carriers
        statsCarriers.add("orderid");
        statsCarriers.add("cost");
        statsCarriers.add("registerdate");
        // orders
        orders.add("orderid");
        orders.add("clientphone");
        orders.add("registerdate");
        orders.add("shipmentaddress");
        orders.add("discount");
        orders.add("comment");
        orders.add("personscount");
        orders.add(ORDER_COST_SORT_CRITERION);
        orders.add(ORDER_DISCOUNT_CARD_SORT_CRITERION);
        orders.add(ORDER_STATUS_SORT_CRITERION);
        orders.add(ORDER_REGISTRATOR_SORT_CRITERION);
        orders.add(ORDER_CARRIER_SORT_CRITERION);

    }

    public static boolean isDirectionCorrect(String direction) {
        return direction != null && directions.contains(direction.toLowerCase());
    }

    public static boolean isIngredientCorrect(String ingredient) {
        return ingredient != null && ingredients.contains(ingredient.toLowerCase());
    }

    public static boolean isProductCorrect(String product) {
        return product != null && products.contains(product.toLowerCase());
    }

    public static boolean isClientCorrect(String client) {
        return client != null && clients.contains(client.toLowerCase());
    }

    public static boolean isAdminCorrect(String field) {
        return field != null && admin.contains(field.toLowerCase());
    }

    public static boolean isStatsProductCorrect(String field) {
        return field != null && statsProducts.contains(field.toLowerCase());
    }

    public static boolean isStatsIngredientCorrect(String field) {
        return field != null && statsIngredients.contains(field.toLowerCase());
    }

    public static boolean isStatsOperatorCorrect(String field) {
        return field != null && statsOperators.contains(field.toLowerCase());
    }

    public static boolean isStatsCarrierCorrect(String field) {
        return field != null && statsCarriers.contains(field.toLowerCase());
    }

    public static boolean isOrderCorrect(String field) {
        return field != null && orders.contains(field.toLowerCase());
    }
}
