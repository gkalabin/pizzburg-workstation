package ru.pizzburg.constants;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class FieldLength {

    //--------------------/admin-------------------
    public static final int ADMIN_LOGIN = 45;
    public static final int ADMIN_NAME = 45;
    public static final int ADMIN_SURNAME = 45;
    public static final int ADMIN_MIDDLE_NAME = 45;
    public static final int ADMIN_PASSWORD = 45;
    //--------------------/client-------------------
    public static final int CLIENT_NAME = 30;
    public static final int CLIENT_SURNAME = 30;
    public static final int CLIENT_MIDDLE_NAME = 30;
    public static final int CLIENT_DEFAULT_ADDRESS = 45;
    //--------------------/ingredient-------------------
    public static final int INGREDIENT_TITLE = 45;
    public static final int INGREDIENT_DESCRIPTION = 100;
    public static final int INGREDIENT_UNITS = 45;
    //--------------------/product-------------------
    public static final int PRODUCT_TITLE = 45;
    public static final int PRODUCT_DESCRIPTION = 250;
    //--------------------/order-------------------
    public static final int ORDER_STATUS_DESCRIPTION = 150;
    public static final int ORDER_COMMENT = 300;
    //--------------------/groups-------------------
    public static final int PRODUCT_GROUP_TITLE = 45;
    public static final int PRODUCT_GROUP_DESCRIPTION = 150;

    private FieldLength() { }
}
