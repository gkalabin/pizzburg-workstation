package ru.pizzburg.constants;

/**
 * @author Grigory Kalabin grigory.kalabin@gmail.com
 */
public class DatabaseErrors {
    public static final String UNKNOWN = "error.database-error";
    public static final String DISCOUNT_CARD_NOT_FOUND = "error.discount-card-not-found";
    public static final String ORDER_NOT_FOUND = "error.order-not-found";
    public static final String STATUS_NOT_EXIST = "error.status-not-exist";

    public static final String CLIENT_ALREADY_EXIST = "error.client-already-exist";
    public static final String DISCOUNT_CARD_IN_USE = "error.discount-card-already-in-use";
    public static final String CLIENT_WITH_PHONE_EXIST = "error.client-update-phone-exist";
    public static final String CLIENT_NOT_EXIST = "error.client-not-exist";

    public static final String PRODUCT_EXISTS = "error.product-already-exist";
    public static final String PRODUCT_NOT_EXISTS = "error.product-not-exist";

    public static final String USER_EXISTS = "error.user-already-exist";
    public static final String USER_NOT_EXISTS = "error.user-not-exist";

    public static final String DISCOUNT_EXISTS = "error.discount-already-exist";
    public static final String DISCOUNT_NOT_EXISTS = "error.discount-not-exist";

    public static final String INGREDIENT_EXISTS = "error.ingredient-already-exist";
    public static final String INGREDIENT_NOT_EXISTS = "error.ingredient-not-exist";

    public static final String PRODUCT_GROUP_EXISTS = "error.group-already-exist";
    public static final String PRODUCT_GROUP_NOT_EXISTS = "error.group-not-exist";

    private DatabaseErrors(){}
}
