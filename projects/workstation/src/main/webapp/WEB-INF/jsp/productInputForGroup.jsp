<%-- 
    Document   : productInputForGroup
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>
<span id="productSelector${productNumber}">
    <c:choose>
        <c:when test="${not empty allProducts}">
            <form:select id="productId${productNumber}" path="formOrder.productsStr[${productNumber}].productId"
                         cssClass="basic-input orders-product-selector select-input">
                <form:options items="${allProducts}" itemValue="product.id" itemLabel="product.title"/>
            </form:select>

            <form:input id="productCount${productNumber}" path="formOrder.productsStr[${productNumber}].count"
                        cssClass="basic-input number-input"/>

        </c:when>
        <c:otherwise>Товары не найдены. Выберите другую категорию</c:otherwise>
    </c:choose>
    <span id="productCost${productNumber}"></span>
</span>
