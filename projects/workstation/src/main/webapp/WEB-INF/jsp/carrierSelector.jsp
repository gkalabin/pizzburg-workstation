<%-- 
    Created on : 5/15/12
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>
Курьер:
<form:select path="formOrder.carrierIdStr" cssClass="basic-input select-input">
    <c:forEach var="carrier" items="${carriers}">
        <option value="${carrier.id}" <c:if test="${selectedId != null && carrier.id == selectedId}">selected="true"</c:if>>
            <c:out value="${carrier.name}"/> <c:out value="${carrier.surname}"/>
        </option>
    </c:forEach>
</form:select>