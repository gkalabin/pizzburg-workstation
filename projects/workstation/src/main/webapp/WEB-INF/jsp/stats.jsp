<%-- 
    Document   : stats
    Created on : Apr 9, 2012, 4:38:48 PM
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="StatsTitle"/></title>

    <%@ include file="/WEB-INF/jsp/include-head.jsp" %>

    <script type="text/javascript">
        $(function() {
            $( "a#submenu").button();
            $('#from').datetimepicker({
                onClose: function(dateText, inst) {
                    var endDateTextBox = $('#to');
                    if (endDateTextBox.val() != '') {
                        var testStartDate = new Date(dateText);
                        var testEndDate = new Date(endDateTextBox.val());
                        if (testStartDate > testEndDate)
                            endDateTextBox.val(dateText);
                    }
                    else {
                        endDateTextBox.val(dateText);
                    }
                },
                onSelect: function (selectedDateTime){
                    var start = $(this).datetimepicker('getDate');
                    $('#to').datetimepicker('option', 'minDate', new Date(start.getTime()));
                },
                timeFormat: 'hh:mm',
                dateFormat: 'dd.mm.yy'
            });
            $('#to').datetimepicker({
                onClose: function(dateText, inst) {
                    var startDateTextBox = $('#from');
                    if (startDateTextBox.val() != '') {
                        var testStartDate = new Date(startDateTextBox.val());
                        var testEndDate = new Date(dateText);
                        if (testStartDate > testEndDate)
                            startDateTextBox.val(dateText);
                    }
                    else {
                        startDateTextBox.val(dateText);
                    }
                },
                onSelect: function (selectedDateTime){
                    var end = $(this).datetimepicker('getDate');
                    $('#from').datetimepicker('option', 'maxDate', new Date(end.getTime()) );
                },
                timeFormat: 'hh:mm',
                dateFormat: 'dd.mm.yy'
            });
        });
    </script>
</head>
<body>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<table class="submenu">
    <tr>
        <td>
            <a href="${pageContext.request.contextPath}/stats/products-flow">Расход товаров</a>
        </td>
        <td>
            <a href="${pageContext.request.contextPath}/stats/ingredients-flow">Расход ингредиентов</a>
        </td>
        <td>
            <a href="${pageContext.request.contextPath}/stats/operators">Операторы</a>
        </td>
        <td>
            <a href="${pageContext.request.contextPath}/stats/carriers">Курьеры</a>
        </td>
    </tr>
</table>

<br/>
<a id="submenu" href="${pageContext.request.contextPath}/stats/hour">Час</a>
<a id="submenu" href="${pageContext.request.contextPath}/stats/day">День</a>
<a id="submenu" href="${pageContext.request.contextPath}/stats/week">Неделя</a>
<a id="submenu" href="${pageContext.request.contextPath}/stats/month">Месяц</a>
<a id="submenu" href="${pageContext.request.contextPath}/stats/year">Год</a>
<form:form method="post" commandName="formDate">
    <form:input path="from"/>
    <form:errors path="from" cssClass="error"/>
    <form:input path="to"/>
    <form:errors path="to" cssClass="error"/>
    <input type="submit" align="center" value="Показать">
</form:form>
<br/>

<c:choose>
    <c:when test="${!empty infoMessage}">
        <div class="ui-widget">
            <div class="info-block ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                        ${infoMessage}</p>
            </div>
        </div>
    </c:when>
    <c:when test="${!empty errorMessage}"><div class="ui-widget">
        <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                    ${errorMessage}</p>
        </div>
    </div>
    </c:when>
</c:choose>

<h3 class="h3table">Общая статистика с ${from} до ${to}:</h3><br/>
<p class="stats-common-details">
    Себестоимость заказов: <i><fmt:formatNumber value="${summaryPrice}" type="number" maxFractionDigits="2"/></i> р.<br/>
    Общая сумма заказов: <i><fmt:formatNumber value="${summaryCost}" type="number" maxFractionDigits="2"/></i> р.<br/>
    Выручка: <i><fmt:formatNumber value="${summaryCost - summaryPrice}" type="number" maxFractionDigits="2"/></i> р.<br/>
    Дней: <i><fmt:formatNumber value="${daysCount}" type="number" maxFractionDigits="2"/></i><br/>
    Заказов: <i><fmt:formatNumber value="${ordersCount}" type="number" maxFractionDigits="2"/></i><br/>
    Заказов в день: <i><fmt:formatNumber value="${ordersCount/daysCount}" type="number" maxFractionDigits="2"/></i><br/>
</p>

</body>
</html>
