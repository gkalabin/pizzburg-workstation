<%--
    Document   : detailed info about entities javascript
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>

<!DOCTYPE html>
<script type="text/javascript">
    var dialogParams = {
        modal:true,
        width:'auto',
        height:'auto'
    };

    function showOrderDetails(id) {
        $.ajax({
            url:"${pageContext.request.contextPath}/details/order/",
            data:{ id:id },
            success:function (data) {
                if (data == null) {
                    return;
                }
                var $dialog = $('<div class="#order-info"></div>');
                var productsDetails = "";
                for (var i = 0, n = data.products.length; i < n; i += 1) {
                    var item = data.products[i];
                    var cost = item.price * (100 - data.discount) / 100;
                    productsDetails += "<span class='productTitleInDetails'>" + item.product.title + "</span> " +
                            "<span class='productCostInDetails'>" +
                            cost + "x" + item.count + " = " + cost * item.count +
                            " р.</span> " +
                            "<span class='productDiscountInDetails'>(Скидка " + item.price * data.discount / 100 + " р.)</span>" +
                            "<br/>";
                }
                $dialog.attr('title', 'Заказ №' + data.id);
                $dialog.html("<table>" +
                        "<tr><td class='detailsTitle'>Поступил:</td><td class='detailsValue'>" + data.registerDateFormatted + "</td></tr>" +
                        "<tr><td class='detailsTitle'>Клиент:</td><td class='detailsValue'>" + data.client.phoneFormatted + "<br/>" + data.client.fullName + "</td></tr>" +
                        "<tr><td class='detailsTitle'>Адрес:</td><td class='detailsValue'>" + data.shipmentAddress + "</td></tr>" +
                        "<tr><td class='detailsTitle'>Статус:</td><td class='detailsValue'>" + data.status.title + "</td></tr>" +
                        "<tr><td class='detailsTitle'>Товары:</td><td class='detailsValue'>" + productsDetails + "</td></tr>" +
                        "<tr><td class='detailsTitle'>Сумма:</td><td class='detailsValue'>" + data.cost * (100 - data.discount) / 100 + " р.</td></tr>" +
                        "<tr><td class='detailsTitle'>Скидка:</td><td class='detailsValue'>" + data.discount + " % (" + data.cost * data.discount / 100 + " р.)</td></tr>" +

                        "<tr><td class='detailsTitle'>Дисконтная карта:</td><td class='detailsValue'>"
                        + (data.alternateDiscountCard != null ? data.alternateDiscountCard.idFormatted :
                        data.client.card != null ? data.client.card.idFormatted : "нет") +
                        "</td></tr>" +

                        "<tr><td class='detailsTitle'>Оператор:</td><td class='detailsValue'>" + data.registrator.fullName + "</td></tr>" +
                        "<tr><td class='detailsTitle'>Курьер:</td><td class='detailsValue'>" + (data.carrier == null ? "нет" : data.carrier.fullName) + "</td></tr>" +
                        "<tr><td class='detailsTitle'>Количество персон:</td><td class='detailsValue'>" + data.personsCount + "</td></tr>" +
                        "<tr><td class='detailsTitle'>Комментарий:</td><td class='detailsValue'>" + (data.comment == null ? "" : data.comment) + "</td></tr>" +
                        "</table>");
                $dialog.dialog(dialogParams);
            }
        });
    }

    function showIngredientDetails(id) {
        $.ajax({
            url:"${pageContext.request.contextPath}/details/ingredient/",
            data:{ id:id },
            success:function (data) {
                if (data == null) {
                    return;
                }
                var $dialog = $('<div class="#ingredient-info"></div>');
                $dialog.attr('title', data.title);
                $dialog.html("<table>" +
                        "<tr><td class='detailsTitle'>Название:</td><td class='detailsValue'>" + data.title + "</td></tr>" +
                        "<tr><td class='detailsTitle'>Описание:</td><td class='detailsValue'>" + data.description + "</td></tr>" +
                        "<tr><td class='detailsTitle'>Стоимость:</td><td class='detailsValue'>" + data.price + " р. за "+data.units+"</td></tr>" +
                        "<tr><td class='detailsTitle'>Остаток на складе:</td><td class='detailsValue'>" + data.balance+" "+data.units + "</td></tr>" +
                        "</table>");
                $dialog.dialog(dialogParams);
            }
        });
    }

    function showProductDetails(id) {
        $.ajax({
            url:"${pageContext.request.contextPath}/details/product/",
            data:{ id:id },
            success:function (data) {
                if (data == null) {
                    return;
                }
                var ingredientDetails = "";
                for (var i = 0, n = data.ingredients.length; i < n; i += 1) {
                    var item = data.ingredients[i];
                    ingredientDetails += "<span class='ingredientTitleInDetails'>" + item.ingredient.title + "</span> - " +
                            "<span class='ingredientCountInDetails'>" +
                            item.count + " " + item.ingredient.units +
                            "</span> " +
                            "<br/>";
                }
                var groupDetails = "";
                for (var i = 0, n = data.groups.length; i < n; i += 1) {
                    var item = data.groups[i];
                    groupDetails += "<span class='groupTitleInDetails'>" + item.group.title + "</span> - " +
                            "<span class='groupPriceInDetails'>" +
                            item.price + " р."+
                            "</span> " +
                            "<br/>";
                }
                var $dialog = $('<div class="#product-info"></div>');
                $dialog.attr('title', data.title);
                $dialog.html("<table>" +
                        "<tr><td class='detailsTitle'>Название:</td><td class='detailsValue'>" + data.title + "</td></tr>" +
                        "<tr><td class='detailsTitle'>Описание:</td><td class='detailsValue'>" + data.description + "</td></tr>" +
                        "<tr><td class='detailsTitle'>Ингредиенты:</td><td class='detailsValue'>" + ingredientDetails +"</td></tr>" +
                        "<tr><td class='detailsTitle'>Группы:</td><td class='detailsValue'>" + groupDetails + "</td></tr>" +
                        "</table>");
                $dialog.dialog(dialogParams);
            }
        });
    }

    function showClientDetails(phone) {
        $.ajax({
            url:"${pageContext.request.contextPath}/details/client/",
            data:{ phone:phone },
            success:function (data) {
                if (data == null) {
                    return;
                }
                var cardDetails = "";
                if (data.card != null) {
                    cardDetails = data.card.idFormatted +"<br/>Выдана: "+data.card.emissionDateFormatted;
                } else {
                    cardDetails = "нет";
                }
                var $dialog = $('<div class="#client-info"></div>');
                $dialog.attr('title', data.fullName);
                $dialog.html("<table>" +
                        "<tr><td class='detailsTitle'>Телефон:</td><td class='detailsValue'>" + data.phone + "</td></tr>" +
                        "<tr><td class='detailsTitle'>Фамилия:</td><td class='detailsValue'>" +
                        (data.surname==null?"":data.surname) + "</td></tr>" +
                        "<tr><td class='detailsTitle'>Имя:</td><td class='detailsValue'>" +
                        (data.name==null?"":data.name) +"</td></tr>" +
                        "<tr><td class='detailsTitle'>Отчество:</td><td class='detailsValue'>" +
                        (data.middleName==null?"":data.middleName) + "</td></tr>" +
                        "<tr><td class='detailsTitle'>Адрес:</td><td class='detailsValue'>" + data.defaultAddress + "</td></tr>" +
                        "<tr><td class='detailsTitle'>Накопления на счёте:</td><td class='detailsValue'>" + data.ordersSum + " р.</td></tr>" +
                        "<tr><td class='detailsTitle'>Дисконтная карта:</td><td class='detailsValue'>" + cardDetails + "</td></tr>" +
                        "</table>");
                $dialog.dialog(dialogParams);
            }
        });
    }
</script>