<%-- 
    Document   : addProductToGroup
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>
<div id="product${productNumber}" class="row">

    <form:select path="formProductGroup.productsStr[${productNumber}].id" cssClass="basic-input select-input">
        <form:options items="${allProducts}" itemValue="id" itemLabel="title"/>
    </form:select>

    <span>
        <form:input path="formProductGroup.productsStr[${productNumber}].price" cssClass="basic-input number-input"/>
    </span>

    <span>
        <span class="delete-item-button button ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only delButton"
                id="del_${productNumber}" role="button" aria-disabled="false" title="Удалить товар">
            <span class="ui-button-icon-primary ui-icon ui-icon-close"></span>
            <span class="ui-button-text">Удалить товар</span>
        </span>
    </span>
    <form:errors path="productsStr[${productNumber}]" cssClass="error"/>
</div>
