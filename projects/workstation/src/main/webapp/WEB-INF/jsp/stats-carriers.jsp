<%@ page import="ru.pizzburg.utils.Digest" %>
<%@ page import="ru.pizzburg.web.controller.stats.CarriersStatsController" %>
<%--
    Document   : stats-carriers
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>
<!DOCTYPE html>

<html>
<head>
    <title><fmt:message key="StatsTitle"/></title>

    <%@ include file="/WEB-INF/jsp/include-head.jsp" %>

    <script type="text/javascript">
        function setResultsCount(count) {
            // setup cookie
            $.cookie('<%= Digest.getSha1Hash(request.getRemoteUser() + CarriersStatsController.RESULT_COUNT_COOKIE_SUFFIX) %>', count);
            // reload page to changes take effect
            window.location.reload();
        }

        $(function () {
            $("a#submenu").button();
            $('#from').datetimepicker({
                onClose:function (dateText, inst) {
                    var endDateTextBox = $('#to');
                    if (endDateTextBox.val() != '') {
                        var testStartDate = new Date(dateText);
                        var testEndDate = new Date(endDateTextBox.val());
                        if (testStartDate > testEndDate)
                            endDateTextBox.val(dateText);
                    }
                    else {
                        endDateTextBox.val(dateText);
                    }
                },
                onSelect:function (selectedDateTime) {
                    var start = $(this).datetimepicker('getDate');
                    $('#to').datetimepicker('option', 'minDate', new Date(start.getTime()));
                },
                timeFormat:'hh:mm',
                dateFormat:'dd.mm.yy'
            });
            $('#to').datetimepicker({
                onClose:function (dateText, inst) {
                    var startDateTextBox = $('#from');
                    if (startDateTextBox.val() != '') {
                        var testStartDate = new Date(startDateTextBox.val());
                        var testEndDate = new Date(dateText);
                        if (testStartDate > testEndDate)
                            startDateTextBox.val(dateText);
                    }
                    else {
                        startDateTextBox.val(dateText);
                    }
                },
                onSelect:function (selectedDateTime) {
                    var end = $(this).datetimepicker('getDate');
                    $('#from').datetimepicker('option', 'maxDate', new Date(end.getTime()));
                },
                timeFormat:'hh:mm',
                dateFormat:'dd.mm.yy'
            });

            $('.linkToOrder').live('click', function () {
                showOrderDetails($(this).text());
            });
        });
    </script>
</head>

<body>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<table class="submenu">
    <tr>
        <td>
            <a href="${pageContext.request.contextPath}/stats/products-flow">Расход товаров</a>
        </td>
        <td>
            <a href="${pageContext.request.contextPath}/stats/ingredients-flow">Расход ингредиентов</a>
        </td>
        <td>
            <a href="${pageContext.request.contextPath}/stats/operators">Операторы</a>
        </td>
        <td>
            <a href="${pageContext.request.contextPath}/stats/carriers">Курьеры</a>
        </td>
    </tr>
</table>

<br/>

<c:choose>
    <c:when test="${empty carriers}">
        <div class="ui-widget" style="margin: 3em;">
            <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                    Не найдено ни одного курьера.
                    Вы можете создать пользователей системы на
                    <a href="${pageContext.request.contextPath}/admin/">странице администрирования</a>.</p>
            </div>
        </div>
    </c:when>
    <c:otherwise>

        <form:form method="post" commandName="formCarriersStats">
            <form:input path="from"/>
            <form:errors path="from" cssClass="error"/>
            <form:input path="to"/>
            <form:errors path="to" cssClass="error"/>

            <form:select path="employeeIdStr">
                <c:forEach var="carrier" items="${carriers}">
                    <option value="${carrier.id}"
                            <c:if test="${selectedId != null && carrier.id == selectedId}">selected="true"</c:if>>
                        <c:out value="${carrier.fullName}"/>
                    </option>
                </c:forEach>
            </form:select>
            <form:errors path="employeeIdStr" cssClass="error"/>

            <input type="submit" align="center" value="Показать">
        </form:form>
        <br/>

        <c:choose>
            <c:when test="${!empty infoMessage}">
                <div class="ui-widget">
                    <div class="info-block ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                        <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                                ${infoMessage}</p>
                    </div>
                </div>
            </c:when>
            <c:when test="${!empty errorMessage}">
                <div class="ui-widget">
                    <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
                        <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                                ${errorMessage}</p>
                    </div>
                </div>
            </c:when>
        </c:choose>

        <c:if test="${not empty stats}">
            <h3 class="h3table">Доставки с <i>${from}</i> до <i>${to}</i>. Курьер <i>${carrier}</i>.</h3><br/>
            <p class="stats-common-details">
                Отправлено заказов: <i>${shipped}</i><br/>
                Оплачено заказов: <i>${paid}</i><br/>
                Отменено заказов: <i>${cancelled}</i><br/>
            </p>

            <p class="reztable">Количество результатов на страницу:
                <a onclick="javascript:setResultsCount(10)">10</a>
                <a onclick="javascript:setResultsCount(25)">25</a>
                <a onclick="javascript:setResultsCount(50)">50</a></p>
            <display:table id="row" name="stats"
                           requestURI="${pageContext.request.getAttribute('javax.servlet.forward.request_uri')}">
                <display:column sortProperty="orderID" sortable="true" title="# заказа">
                    ${row.id}
                </display:column>
                <display:column sortProperty="registerDate" property="registerDateFormatted" sortable="true"
                                title="Дата"/>
                <display:column sortProperty="cost" sortable="true" title="Цена">
                    <fmt:formatNumber value="${row.cost*(100-row.discount)/100}" type="number" maxFractionDigits="2"/> р.
                </display:column>
                <display:column sortable="false">
                    <a href="javascript:showOrderDetails(${row.id});">
                        <span class="ui-state-default ui-corner-all ui-icon ui-icon-info">&nbsp;</span>
                    </a>
                </display:column>
            </display:table>
        </c:if>

    </c:otherwise>
</c:choose>
</body>
</html>