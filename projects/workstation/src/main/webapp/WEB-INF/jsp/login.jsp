<%-- 
    Created on : 5/23/12
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="LoginTitle"/></title>

    <link type="text/css" href="${pageContext.request.contextPath}/resources/css/default/jquery-ui-1.8.18.custom.css"
          rel="stylesheet"/>
    <link type="text/css" href="${pageContext.request.contextPath}/resources/css/pizzburg.css" rel="stylesheet"/>

    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-1.7.2.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.8.18.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pizzburg.js"></script>

</head>

<body>

<div style="width: 75%; position: relative; z-index: 1; margin: auto;">
    <img style="width: 100%; height: 100%;" src="${pageContext.request.contextPath}/resources/images/pw_logo.png" alt="ПиццБург"/>
</div>

<div style="width: 25%; position: absolute; z-index: 2; top: 55%; left: 35%; text-align: right; color: #fff; font-weight: bold;">
    <form action="${pageContext.request.contextPath}/j_spring_security_check" method="post" autocomplete="off">

        <p>
            <label for="j_username">Логин:</label>
            <input id="j_username" name="j_username" type="text" style="padding:3px; border:2px solid #ff0;"/>
        </p>

        <p>
            <label for="j_password">Пароль:</label>
            <input id="j_password" name="j_password" type="password" style="padding:3px;border:2px solid #ff0;"/>
        </p>

        <p>
            <input type="submit" style="border: 2px solid #ff0; background: none; color: #fff; font-weight: bold;" value="Войти"/>
        </p>
    </form>

    <c:if test="${!empty error}">
        <div class="ui-state-error ui-corner-all" style="padding: 0 .7em; text-align: center;">
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                    ${error}
            </p>
        </div>
    </c:if>

</div>

</body>
</html>