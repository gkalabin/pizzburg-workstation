<%-- 
    Document   : statuses
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="StatusesTitle"/></title>
    <%@ include file="/WEB-INF/jsp/include-head.jsp" %>
</head>
<body>

<%@ include file="/WEB-INF/jsp/header.jsp" %>

<c:if test="${formStatus != null}">
    <form:form method="post" commandName="formStatus">
        <div class="row">
            Редактирование статуса «<c:out value="${formStatus.title}"/>»:
        </div>

        <div class="row <form:errors path='description'>row-error</form:errors>">Описание:

            <form:textarea path="description" cssClass="basic-input description-input"/>

            <br/><form:errors path="description" cssClass="error"/>
        </div>
        <form:hidden path="id"/>
        <div class="row clear-row">
            <input type="submit" class="button" value="Обновить">
        </div>
    </form:form>
    <c:choose>
        <c:when test="${!empty infoMessage}">
            <div class="ui-widget">
                <div class="info-block ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                            ${infoMessage}</p>
                </div>
            </div>
        </c:when>
        <c:when test="${!empty errorMessage}">
            <div class="ui-widget">
                <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                            ${errorMessage}</p>
                </div>
            </div>
        </c:when>
    </c:choose>
</c:if>

<h3 class="h3table">Статусы:</h3>
<display:table class="reztable" id="row" name="statuses"
               requestURI="${pageContext.request.getAttribute('javax.servlet.forward.request_uri')}">
    <display:column sortProperty="title" sortable="true" title="Название">
        <c:out value="${row.title}"/>
    </display:column>
    <display:column sortProperty="description" sortable="true" title="Описание">
        <c:out value="${row.description}"/>
    </display:column>
    <display:column sortable="false">
        <a href="${pageContext.request.contextPath}/orders/statuses/edit/${row.id}">
            <span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil">&nbsp;</span>
        </a>
    </display:column>
</display:table>
</body>
</html>