<%-- 
    Document   : productGroups
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>

<%@ page import="ru.pizzburg.web.service.groups.FormProductGroup" %>
<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="ProductGroupsTitle"/></title>

    <%@ include file="/WEB-INF/jsp/include-head.jsp" %>

    <script type="text/javascript">
        $(function () {
            var productPosition = <%= ((FormProductGroup) request.getAttribute("formProductGroup")).getProductsStr().size() - 1%>;

            var deleteFunction = function () {
                var $id = parseInt(this.id.substring(4));
                $("#product" + $id).remove();
                $.get("${pageContext.request.contextPath}/groups/deleteProduct", { fieldId:$id}, null);
            };

            $("#addProductButton").click(function () {
                productPosition++;
                $.get("${pageContext.request.contextPath}/groups/addProduct", { fieldId:productPosition},
                        function (data) {
                            $("#addProductButtonRow").before(data);
                            $(".delButton").click(null);
                            $(".delButton").click(deleteFunction);
                        });
            });

            $(".delButton").click(deleteFunction);

            <% String url = request.getAttribute("javax.servlet.forward.servlet_path").toString(); %>
            $("#formsTabs").tabs({
                collapsible:true,
                cookie:{ expires:365, name:"<sec:authentication property="principal.username"/>productGroupsTabs" },
                <% if (url.contains("/edit")) { %>selected:0<% } %>
            });
        });
    </script>

</head>
<body>

<%@ include file="/WEB-INF/jsp/header.jsp" %>

<sec:authorize ifAllGranted="ROLE_ADMIN">
    <div id="formsTabs">
        <ul>
            <li><a href="#editionFormTab">Добавить/редактировать</a></li>
        </ul>
        <div id="editionFormTab">
            <form:form method="post" commandName="formProductGroup" cssClass="content">

                <div class="row <form:errors path='title'>row-error</form:errors>">
                    <span>Название:</span>

                    <form:input path="title" cssClass="basic-input"/>

                    <br/><form:errors path="title" cssClass="error"/>
                </div>

                <div class="row <form:errors path='description'>row-error</form:errors>">
                    <span>Описание:</span>

                    <form:textarea path="description" cssClass="basic-input description-input"/>

                    <br/><form:errors path="description" cssClass="error"/>
                </div>

                <div class="row form-section-caption">
                    <span>Товары:&nbsp;</span>
                    <!-- spacer -->
                    <span class="spacer">&nbsp;</span>
                </div>

                <%
                    FormProductGroup fpg = (FormProductGroup) request.getAttribute("formProductGroup");
                    for (int i = 0; i < fpg.getProductsStr().size(); i++) {
                        pageContext.setAttribute("i", i);
                %>
                <div class="row <form:errors path='productsStr[${i}]'>row-error</form:errors>" id="product${i}">
                    <form:select path="productsStr[${i}].id" cssClass="basic-input select-input">
                        <form:options items="${allProducts}" itemValue="id" itemLabel="title"/>
                    </form:select>
                    <span>
                        <form:input path="productsStr[${i}].price" cssClass="basic-input number-input"/>
                    </span>
                    <span>
                        <button class="delete-item-button button ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only delButton"
                                id="del_${i}" role="button" aria-disabled="false" title="Удалить товар">
                            <span class="ui-button-icon-primary ui-icon ui-icon-close"></span>
                            <span class="ui-button-text">Удалить товар</span>
                        </button>
                    </span>
                    <br/><form:errors path="productsStr[${i}]" cssClass="error"/>
                </div>
                <%}%>

                <div class="row" id="addProductButtonRow">
                    <input type="button" class="button" id="addProductButton" value="Добавить товар"/>
                </div>

                <form:hidden path="id"/>
                <%
                    if (url.contains("/edit")) {
                %>

                <input type="submit" class="basic-input" value="Обновить группу">
                <% } else {%>
                <input type="submit" class="basic-input" value="Добавить группу ">


                <%}%>
            </form:form>
        </div>
    </div>

    <br><br/>
    <c:choose>
        <c:when test="${!empty infoMessage}">
            <div class="ui-widget">
                <div class="info-block ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                            ${infoMessage}</p>
                </div>
            </div>
        </c:when>
        <c:when test="${!empty errorMessage}">
            <div class="ui-widget">
                <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                            ${errorMessage}</p>
                </div>
            </div>
        </c:when>
    </c:choose>
</sec:authorize>

<h3 class="h3table">Группы:</h3>
<display:table class="reztable" id="row" name="groups"
               requestURI='${request.getAttribute("javax.servlet.forward.servlet_path")}' pagesize="25">
    <display:column sortProperty="title" sortable="true" title="Название">
        <c:out value="${row.title}"/>
    </display:column>
    <display:column sortProperty="description" sortable="true" title="Описание">
        <c:out value="${row.description}"/>
    </display:column>
    <display:column sortable="false" title="Товары">
        <nobr>
            <c:forEach items="${row.products}" var="p" varStatus="status">
                <a class="detailsDialogLink" href="javascript:showProductDetails(${p.product.id})"><c:out
                        value="${p.product.title}"/></a> - <c:out value="${p.price}"/> руб.

                <% // remove extra line break %>
                <c:if test="${status.count != 3}"><br/></c:if>

                <% // hide extra content by default %>
                <c:if test="${status.count == 3 && !status.last}">
                    <div class="collapsed-block">
                    <div class="collapsed-content">
                </c:if>

                <% // close opened tags used for content hiding %>
                <c:if test="${status.last && status.count > 3}">
                    </div>
                    <a class="toggle-collapsed-content">Показать всё</a>
                    </div>
                </c:if>
            </c:forEach>
        </nobr>
    </display:column>

    <sec:authorize ifAllGranted="ROLE_ADMIN">
        <display:column sortable="false">
            <a href="${pageContext.request.contextPath}/groups/edit/${row.id}">
                <span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil">&nbsp;</span>
            </a>
        </display:column>
        <display:column sortable="false">
            <a href="${pageContext.request.contextPath}/groups/remove/${row.id}">
                <span class="ui-state-default ui-corner-all ui-icon ui-icon-closethick">&nbsp;</span>
            </a>
        </display:column>
    </sec:authorize>

</display:table>
</body>
</html>
