<%-- 
    Document   : addProductToOrder
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>
<div id="product${productNumber}"
     class="row <form:errors path='productsStr[${productNumber}]'>row-error</form:errors>">

    <form:select id="groupId${productNumber}" path="formOrder.productsStr[${productNumber}].groupId"
            cssClass="basic-input orders-group-selector select-input">
        <form:options items="${allGroups}" itemValue="id" itemLabel="title" />
    </form:select>

    <%@ include file="/WEB-INF/jsp/productInputForGroup.jsp" %>

    <span class="delete-item-button button ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only delButton" id="del_${productNumber}"
            role="button" aria-disabled="false" title="Удалить товар">
        <span class="ui-button-icon-primary ui-icon ui-icon-close"></span>
        <span class="ui-button-text">Удалить товар</span>
    </span>

    <br/><form:errors path="productsStr[${productNumber}]" cssClass="error"/>
</div>
