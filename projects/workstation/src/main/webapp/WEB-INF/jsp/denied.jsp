<%-- 
    Created on : 5/23/12
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="DeniedTitle"/></title>

    <link type="text/css" href="${pageContext.request.contextPath}/resources/css/default/jquery-ui-1.8.18.custom.css"
          rel="stylesheet"/>
    <link type="text/css" href="${pageContext.request.contextPath}/resources/css/pizzburg.css" rel="stylesheet"/>

    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-1.7.2.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.8.18.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pizzburg.js"></script>

</head>
<body>

<%@ include file="/WEB-INF/jsp/header.jsp" %>

<div class="ui-widget">
    <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
        <h3>Доступ запрещён</h3>
        <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
            Недостаточно прав для просмотра данной страницы.
        </p>
    </div>
</div>

</body>
</html>