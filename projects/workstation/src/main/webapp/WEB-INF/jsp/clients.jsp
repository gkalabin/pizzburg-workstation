<%-- 
    Document   : clients
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>
<%@ page import="ru.pizzburg.utils.Digest" %>
<%@ page import="ru.pizzburg.web.controller.ClientsController" %>
<%@ page import="ru.pizzburg.web.service.clients.FormClient" %>
<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="ClientsTitle"/></title>

    <%@ include file="/WEB-INF/jsp/include-head.jsp" %>

    <script type="text/javascript">
        function setResultsCount(count) {
            // setup cookie
            $.cookie('<%= Digest.getSha1Hash(request.getRemoteUser() + ClientsController.RESULT_COUNT_COOKIE_SUFFIX) %>', count);
            // reload page to changes take effect
            window.location.reload();
        }

        $(function () {
            $("#emissionDatePicker").datepicker({
                showButtonPanel:true,
                dateFormat:"dd.mm.yy"
            });
            $("#addDiscountCard").click(function () {
                $.get("${pageContext.request.contextPath}/clients/addDiscountCard", null, null);
                $("#discountCard").css({visibility:"visible", display:"inline"});
                $("#addDiscountCard").css({visibility:"hidden", display:"none"});
            });

            $("#removeDiscountCard").click(function () {
                $.get("${pageContext.request.contextPath}/clients/removeDiscountCard", null, null);
                $("#discountCard").css({visibility:"hidden", display:"none"});
                $("#addDiscountCard").css({visibility:"visible", display:"inline"});
            });

            <%
                String url = request.getAttribute("javax.servlet.forward.servlet_path").toString();
                if (url.contains("/edit")) {
            %>
            $("#formClient").get(0).setAttribute('action', "${pageContext.request.getAttribute('javax.servlet.forward.request_uri')}");
            <% } else {%>
            $("#formClient").get(0).setAttribute('action', "${pageContext.request.contextPath}/clients");
            <%}%>

            <%  FormClient fc = ((FormClient) request.getAttribute("formClient"));
                if (fc.isWithCard()) {
            %>
            $("#discountCard").css({visibility:"visible", display:"inline"});
            $("#addDiscountCard").css({visibility:"hidden", display:"none"});
            <%  } else {%>
            $("#discountCard").css({visibility:"hidden", display:"none"});
            $("#addDiscountCard").css({visibility:"visible", display:"inline"});
            <%  }%>

            $("#formsTabs").tabs({
                collapsible:true,
                cookie:{ expires:365, name:"<sec:authentication property="principal.username"/>clientsTabs" },
                <% if (url.contains("/edit")) { %>selected:0<% } %>
            });
        });
    </script>
</head>
<body>

<%@ include file="/WEB-INF/jsp/header.jsp" %>
<div id="formsTabs">
    <ul>
        <li><a href="#editionFormTab">Добавить/редактировать</a></li>
        <li><a href="#searchFormTab">Искать</a></li>
    </ul>
    <div id="editionFormTab">
        <form:form method="post" commandName="formClient" cssClass="content">

            <div class="row <form:errors path='phoneStr'>row-error</form:errors>">
                <span>Телефон:</span>

                <form:input path="phoneStr" cssClass="basic-input"/>

                <br/><form:errors path="phoneStr" cssClass="error"/>
            </div>

            <div class="row <form:errors path='defaultAddress'>row-error</form:errors>">
                <span>Адрес:</span>

                <form:input path="defaultAddress" cssClass="basic-input"/>

                <br/><form:errors path="defaultAddress" cssClass="error"/>
            </div>

            <div class="row <form:errors path='surname'>row-error</form:errors>">
                <span>Фамилия:</span>

                <form:input path="surname" cssClass="basic-input"/>

                <br/><form:errors path="surname" cssClass="error"/>
            </div>

            <div class="row <form:errors path='name'>row-error</form:errors>">
                <span>Имя:</span>

                <form:input path="name" cssClass="basic-input"/>

                <br/><form:errors path="name" cssClass="error"/>
            </div>

            <div class="row <form:errors path='middleName'>row-error</form:errors>">
                <span>Отчество:</span>

                <form:input path="middleName" cssClass="basic-input"/>

                <br/><form:errors path="middleName" cssClass="error"/>
            </div>

            <sec:authorize ifAllGranted="ROLE_ADMIN">
                <div class="row <form:errors path='ordersSumStr'>row-error</form:errors>">

                    <span>Накопления:</span>

                    <form:input path="ordersSumStr" cssClass="basic-input"/>

                    <br/><form:errors path="ordersSumStr" cssClass="error"/>
                </div>
            </sec:authorize>

            <div class="row form-section-caption">
                <span>Дисконтная карта:</span>
                <input type="button" class="button" id="addDiscountCard" value="Создать карту"/>
                <!-- spacer -->
                <span class="spacer">&nbsp;</span>
            </div>

            <div id="discountCard">

                <div class="row <form:errors path='card.id'>row-error</form:errors>">

                    <span>Номер карты:</span>

                    <form:input path="card.id" cssClass="basic-input"/>

                    <br/><form:errors path="card.id" cssClass="error"/>
                </div>

                <div class="row <form:errors path='card.emissionDate'>row-error</form:errors>">
                    <span>Дата выдачи:</span>

                    <form:input path="card.emissionDate" cssClass="basic-input" id="emissionDatePicker"/>

                    <br/><form:errors path="card.emissionDate" cssClass="error"/>
                </div>

                <div class="row">
                    <input type="button" class="button" id="removeDiscountCard" value="Удалить карту"/>
                </div>
            </div>

            <div class="row clear-row">
                <%
                    if (url.contains("/edit")) {
                %>
                <input type="submit" class="basic-input" value="Обновить данные">
                <% } else {%>
                <input type="submit" class="basic-input" value="Добавить клиента">
                <%}%>
            </div>

        </form:form>
        <br><br/>
    </div>
    <div id="searchFormTab">
        <form:form method="post" commandName="clientSearch" cssClass="content"
                   action="${pageContext.request.contextPath}/clients/search">
            <div class="ui-widget">
                <div class="info-block ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                        В текстовых полях символ «*» означает любые символы, «?» - любой одинарный символ</p>
                </div>
            </div>

            <div class="row <form:errors path='phone'>row-error</form:errors>">
                <span>Телефон:</span>

                <form:input path="phone" cssClass="basic-input"/>

                <br/><form:errors path="phone" cssClass="error"/>
            </div>

            <div class="row <form:errors path='address'>row-error</form:errors>">
                <span>Адрес:</span>

                <form:input path="address" cssClass="basic-input"/>

                <br/><form:errors path="address" cssClass="error"/>
            </div>

            <div class="row <form:errors path='surname'>row-error</form:errors>">
                <span>Фамилия:</span>

                <form:input path="surname" cssClass="basic-input"/>

                <br/><form:errors path="surname" cssClass="error"/>
            </div>

            <div class="row <form:errors path='name'>row-error</form:errors>">
                <span>Имя:</span>

                <form:input path="name" cssClass="basic-input"/>

                <br/><form:errors path="name" cssClass="error"/>
            </div>

            <div class="row <form:errors path='middleName'>row-error</form:errors>">
                <span>Отчество:</span>

                <form:input path="middleName" cssClass="basic-input"/>

                <br/><form:errors path="middleName" cssClass="error"/>
            </div>

            <div class="row <form:errors path='cardNumber'>row-error</form:errors>">
                <span>Номер карты:</span>

                <form:input path="cardNumber" cssClass="basic-input"/>

                <br/><form:errors path="cardNumber" cssClass="error"/>
            </div>

            <div class="row <form:errors path='sumFrom'>row-error</form:errors>">
                <span>Сумма на карте (от):</span>

                <form:input path="sumFrom" cssClass="basic-input"/>

                <br/><form:errors path="sumFrom" cssClass="error"/>
            </div>

            <div class="row <form:errors path='sumTo'>row-error</form:errors>">
                <span>Сумма на карте (до):</span>

                <form:input path="sumTo" cssClass="basic-input"/>

                <br/><form:errors path="sumTo" cssClass="error"/>
            </div>

            <div class="row clear-row">
                <input type="submit" class="button" align="center" value="Искать клиента">
            </div>

        </form:form>
    </div>
</div>
<c:choose>
    <c:when test="${!empty infoMessage}">
        <div class="ui-widget">
            <div class="info-block ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                        ${infoMessage}</p>
            </div>
        </div>
    </c:when>
    <c:when test="${!empty errorMessage}">
        <div class="ui-widget">
            <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                        ${errorMessage}</p>
            </div>
        </div>
    </c:when>
</c:choose>

<c:if test="${clientFilter != null && !clientFilter.isEmpty}">
    <div class="ui-widget">
        <div class="info-block ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                Данные фильтруются. <a href="${pageContext.request.contextPath}/clients/search/cancel">Сбросить
                    фильтр.</a></p>
        </div>
    </div>
</c:if>

<h3 class="h3table">Клиенты:</h3>

<p class="reztable">Количество результатов на страницу:
    <a onclick="javascript:setResultsCount(10)">10</a>
    <a onclick="javascript:setResultsCount(25)">25</a>
    <a onclick="javascript:setResultsCount(50)">50</a></p>
<display:table id="row" name="clients"
               requestURI="${pageContext.request.getAttribute('javax.servlet.forward.request_uri')}">
    <display:column sortProperty="phone" property="phoneFormatted" sortable="true" title="Телефон"/>
    <display:column sortProperty="name" sortable="true" title="ФИО">
        <c:out value="${row.fullName}"/>
    </display:column>
    <display:column sortProperty="discountCardNumber" sortable="true" title="Номер дисконтной карты">
        <c:choose>
            <c:when test="${row.card != null}">${row.card.idFormatted}</c:when>
            <c:otherwise>---</c:otherwise>
        </c:choose>
    </display:column>
    <display:column sortProperty="ordersSum" sortable="true" title="Накопления">
        <fmt:formatNumber value="${row.ordersSum}" type="number" maxFractionDigits="2"/> р.
    </display:column>
    <display:column sortable="false" title="Скидка">
        <c:choose>
            <c:when test="${row.card != null}">${discountManager.getDiscountBySum(row.ordersSum)} %</c:when>
            <c:otherwise>---</c:otherwise>
        </c:choose>
    </display:column>
    <display:column sortProperty="defaultAddress" sortable="true" title="Адрес">
        <c:out value="${row.defaultAddress}"/>
    </display:column>
    <display:column sortable="false">
        <a href="${pageContext.request.contextPath}/clients/edit/${row.phone}">
            <span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil">&nbsp;</span>
        </a>
    </display:column>
    <display:column sortable="false">
        <a href="${pageContext.request.contextPath}/clients/remove/${row.phone}">
            <span class="ui-state-default ui-corner-all ui-icon ui-icon-closethick">&nbsp;</span>
        </a>
    </display:column>
</display:table>
</body>
</html>
