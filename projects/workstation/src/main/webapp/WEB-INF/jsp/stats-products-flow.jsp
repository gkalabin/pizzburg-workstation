<%@ page import="ru.pizzburg.utils.Digest" %>
<%@ page import="ru.pizzburg.web.controller.stats.FlowStatsController" %>
<%--
    Document   : stats-products-flow
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>
<!DOCTYPE html>

<html>
<head>
    <title><fmt:message key="StatsTitle"/></title>

    <%@ include file="/WEB-INF/jsp/include-head.jsp" %>

    <script type="text/javascript">
        function setResultsCount(count) {
            // setup cookie
            $.cookie('<%= Digest.getSha1Hash(request.getRemoteUser() + FlowStatsController.PRODUCTS_RESULT_COUNT_COOKIE_SUFFIX) %>', count);
            // reload page to changes take effect
            window.location.reload();
        }

        $(function() {
            $( "a#submenu").button();
            $('#from').datetimepicker({
                onClose: function(dateText, inst) {
                    var endDateTextBox = $('#to');
                    if (endDateTextBox.val() != '') {
                        var testStartDate = new Date(dateText);
                        var testEndDate = new Date(endDateTextBox.val());
                        if (testStartDate > testEndDate)
                            endDateTextBox.val(dateText);
                    }
                    else {
                        endDateTextBox.val(dateText);
                    }
                },
                onSelect: function (selectedDateTime){
                    var start = $(this).datetimepicker('getDate');
                    $('#to').datetimepicker('option', 'minDate', new Date(start.getTime()));
                },
                timeFormat: 'hh:mm',
                dateFormat: 'dd.mm.yy'
            });
            $('#to').datetimepicker({
                onClose: function(dateText, inst) {
                    var startDateTextBox = $('#from');
                    if (startDateTextBox.val() != '') {
                        var testStartDate = new Date(startDateTextBox.val());
                        var testEndDate = new Date(dateText);
                        if (testStartDate > testEndDate)
                            startDateTextBox.val(dateText);
                    }
                    else {
                        startDateTextBox.val(dateText);
                    }
                },
                onSelect: function (selectedDateTime){
                    var end = $(this).datetimepicker('getDate');
                    $('#from').datetimepicker('option', 'maxDate', new Date(end.getTime()) );
                },
                timeFormat: 'hh:mm',
                dateFormat: 'dd.mm.yy'
            });
        });
    </script>
</head>

<body>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<table class="submenu">
    <tr>
        <td>
            <a href="${pageContext.request.contextPath}/stats/products-flow">Расход товаров</a>
        </td>
        <td>
            <a href="${pageContext.request.contextPath}/stats/ingredients-flow">Расход ингредиентов</a>
        </td>
        <td>
            <a href="${pageContext.request.contextPath}/stats/operators">Операторы</a>
        </td>
        <td>
            <a href="${pageContext.request.contextPath}/stats/carriers">Курьеры</a>
        </td>
    </tr>
</table>

<br/>
<a id="submenu" href="${pageContext.request.contextPath}/stats/products-flow/hour">Час</a>
<a id="submenu" href="${pageContext.request.contextPath}/stats/products-flow/day">День</a>
<a id="submenu" href="${pageContext.request.contextPath}/stats/products-flow/week">Неделя</a>
<a id="submenu" href="${pageContext.request.contextPath}/stats/products-flow/month">Месяц</a>
<a id="submenu" href="${pageContext.request.contextPath}/stats/products-flow/year">Год</a>
<form:form method="post" commandName="formDate">
    <form:input path="from"/>
    <form:errors path="from" cssClass="error"/>
    <form:input path="to"/>
    <form:errors path="to" cssClass="error"/>
    <input type="submit" align="center" value="Показать">
</form:form>
<br/>

<c:choose>
    <c:when test="${!empty infoMessage}">
        <div class="ui-widget">
            <div class="info-block ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                        ${infoMessage}</p>
            </div>
        </div>
    </c:when>
    <c:when test="${!empty errorMessage}"><div class="ui-widget">
        <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                    ${errorMessage}</p>
        </div>
    </div>
    </c:when>
</c:choose>

<h3 class="h3table">Продажи товаров с ${from} до ${to}</h3>
<p class="reztable">Количество результатов на страницу:
    <a onclick="javascript:setResultsCount(10)">10</a>
    <a onclick="javascript:setResultsCount(25)">25</a>
    <a onclick="javascript:setResultsCount(50)">50</a></p>
<display:table id="row" name="productsFlow" requestURI="${pageContext.request.getAttribute('javax.servlet.forward.request_uri')}">
    <display:column sortProperty="product" sortable="true" title="Товар">
        <a class="detailsDialogLink" href="javascript:showProductDetails(${row.product.id})"><c:out value="${row.product.title}"/></a>
    </display:column>
    <display:column sortProperty="count" sortable="true" title="Количество">
        ${row.count} шт.
    </display:column>
</display:table>

</body>
</html>