<%-- 
    Document   : admin
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>
<%@ page import="ru.pizzburg.utils.Digest" %>
<%@ page import="ru.pizzburg.web.controller.AdminController" %>
<%@ page import="ru.pizzburg.web.domain.Role" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="AdminTitle"/></title>

    <%@ include file="/WEB-INF/jsp/include-head.jsp" %>

    <script type="text/javascript">
        function setResultsCount(count) {
            // setup cookie
            $.cookie('<%= Digest.getSha1Hash(request.getRemoteUser() + AdminController.RESULT_COUNT_COOKIE_SUFFIX) %>', count);
            // reload page to changes take effect
            window.location.reload();
        }

        $(function () {
            $("#formsTabs").tabs({
                collapsible:true,
                cookie:{ expires:365, name:"<sec:authentication property="principal.username"/>adminTabs" },
                <% if (request.getAttribute("javax.servlet.forward.servlet_path").toString().contains("/edit")) { %>selected:0<% } %>
            });

            $('#passInputLayout, #passConfirmInputLayout').live('focus',
                    function () {
                        $('#passInputLayout, #passConfirmInputLayout').addClass("over");
                    });
            $('#passInputLayout, #passConfirmInputLayout').live('blur',
                    function () {
                        $('#passInputLayout, #passConfirmInputLayout').removeClass("over");
                    });

            <%
                List<Role> roles = (List<Role>) request.getAttribute("allRoles");
                for (int i = 0; i < roles.size(); i++) {
                    pageContext.setAttribute("i", i);
            %>
            $('.<%= roles.get(i).getName()%>').qtip({
                content:'<%= roles.get(i).getDescription()%>'
            });
            <% }%>
        });
    </script>
</head>
<body>

<%@ include file="/WEB-INF/jsp/header.jsp" %>

<div id="formsTabs">
    <ul>
        <li><a href="#editionFormTab">Добавить/редактировать</a></li>
    </ul>
    <div id="editionFormTab">
        <form:form method="post" commandName="formUser" cssClass="content">

            <div class="row <form:errors path='login'>row-error</form:errors>">
                Логин:

                <form:input path="login" cssClass="basic-input"/>

                <br/><form:errors path="login" cssClass="error"/>
            </div>


            <div class="row <form:errors path='name'>row-error</form:errors>">
                Имя:

                <form:input path="name" cssClass="basic-input"/>

                <br/><form:errors path="name" cssClass="error"/>
            </div>


            <div class="row <form:errors path='surname'>row-error</form:errors>">
                Фамилия:

                <form:input path="surname" cssClass="basic-input"/>

                <br/><form:errors path="surname" cssClass="error"/>
            </div>


            <div class="row <form:errors path='middleName'>row-error</form:errors>">
                Отчество:

                <form:input path="middleName" cssClass="basic-input"/>

                <br/><form:errors path="middleName" cssClass="error"/>
            </div>


            <div id="passInputLayout" class="row <form:errors path='pass'>row-error</form:errors>">
                Пароль:
                <form:password path="pass" cssClass="basic-input"/>

                <br/><form:errors path="pass" cssClass="error"/>
            </div>

            <div id="passConfirmInputLayout" class="row <form:errors path='passConfirm'>row-error</form:errors>">
                Повтор пароля:
                <form:password path="passConfirm" cssClass="basic-input"/>

                <br/><form:errors path="passConfirm" cssClass="error"/>
            </div>


            <div class="row <form:errors path='roleStr'>row-error</form:errors>">
                Роль:

                <form:select path="roleStr" cssClass="basic-input select-input">
                    <form:options items="${allRoles}" itemValue="id" itemLabel="name"/>
                </form:select>

                <br/><form:errors path="roleStr" cssClass="error"/>
            </div>

            <form:hidden path="id"/>

            <div class="row clear-row">
                <%
                    String url = request.getAttribute("javax.servlet.forward.servlet_path").toString();
                    if (url.contains("/edit")) {
                %>
                <input type="submit" class="button" value="Обновить">

                <div class="ui-widget">
                    <div class="info-block ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                        <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                            Оставьте поля "пароль" и "повтор пароля" пустыми, если не хотите его менять.</p>
                    </div>
                </div>
                <% } else {%>
                <input type="submit" class="button" value="Добавить">
                <%}%>
            </div>
        </form:form>
    </div>
</div>
<c:choose>
    <c:when test="${!empty infoMessage}">
        <div class="ui-widget">
            <div class="info-block ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                        ${infoMessage}</p>
            </div>
        </div>
    </c:when>
    <c:when test="${!empty errorMessage}">
        <div class="ui-widget">
            <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                        ${errorMessage}</p>
            </div>
        </div>
    </c:when>
</c:choose>

<div class="info-block ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
        <strong>Описание ролей:</strong><br/>
        <c:forEach items="${allRoles}" var="role">
            <br/><i>${role.name}</i> - ${role.description}
        </c:forEach>
    </p>
</div>

<h3 class="h3table">Пользователи:</h3>

<p class="reztable">Количество результатов на страницу:
    <a onclick="javascript:setResultsCount(10)">10</a>
    <a onclick="javascript:setResultsCount(25)">25</a>
    <a onclick="javascript:setResultsCount(50)">50</a></p>
<display:table id="row" name="users"
               requestURI="${pageContext.request.getAttribute('javax.servlet.forward.request_uri')}">
    <display:column property="login" sortable="true" title="Логин"/>
    <display:column sortProperty="name" property="fullName" sortable="true" title="ФИО"/>
    <display:column sortProperty="role" sortable="true" title="Роль">
        <span class="${row.role.name}">${row.role.name}</span>
    </display:column>
    <display:column sortable="false">
        <a href="${pageContext.request.contextPath}/admin/edit/${row.id}">
            <span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil">&nbsp;</span>
        </a>
    </display:column>
    <display:column sortable="false">
        <a href="${pageContext.request.contextPath}/admin/remove/${row.id}">
            <span class="ui-state-default ui-corner-all ui-icon ui-icon-closethick">&nbsp;</span>
        </a>
    </display:column>
</display:table>
</body>
</html>
