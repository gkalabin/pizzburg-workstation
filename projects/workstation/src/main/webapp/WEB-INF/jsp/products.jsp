<%-- 
    Document   : products
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>

<%@ page import="ru.pizzburg.utils.Digest" %>
<%@ page import="ru.pizzburg.web.controller.ProductsController" %>
<%@ page import="ru.pizzburg.web.service.products.FormProduct" %>
<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="ProductsTitle"/></title>

    <%@ include file="/WEB-INF/jsp/include-head.jsp" %>

    <script type="text/javascript">
        function setResultsCount(count) {
            // setup cookie
            $.cookie('<%= Digest.getSha1Hash(request.getRemoteUser() + ProductsController.RESULT_COUNT_COOKIE_SUFFIX) %>', count);
            // reload page to changes take effect
            window.location.reload();
        }

        $(function () {
            var ingredientPosition = <%= ((FormProduct) request.getAttribute("formProduct")).getIngredientsStr().size() - 1%>;
            var groupPosition = <%= ((FormProduct) request.getAttribute("formProduct")).getGroupsStr().size() - 1%>;

            var deleteIngredientFunction = function () {
                var $id = parseInt(this.id.substring("del_ingredient_".length));
                $("#ingredient" + $id).remove();
                $.get("${pageContext.request.contextPath}/products/deleteIngredient", { fieldId:$id}, null);
            };

            var deleteGroupFunction = function () {
                var $id = parseInt(this.id.substring("del_group_".length));
                $("#group" + $id).remove();
                $.get("${pageContext.request.contextPath}/products/deleteGroup", { fieldId:$id}, null);
            };

            var ingredientChangeFunction = function () {
                var $id = parseInt(this.id.substring("ingredientSelector".length));
                var unitsElement = $("#ingredientUnits" + $id);
                $.ajax({
                    url:"${pageContext.request.contextPath}/products/ingredientChange",
                    data:{
                        id:$(this).attr('value')
                    },
                    success:function (data) {
                        unitsElement.text(data);
                    }
                });
            };

            $("#addIngredientButton").click(function () {
                ingredientPosition++;
                $.get("${pageContext.request.contextPath}/products/addIngredient", { fieldId:ingredientPosition},
                        function (data) {
                            $("#addIngredientButtonRow").before(data);

                        });
            });

            $("#addGroupButton").click(function () {
                groupPosition++;
                $.get("${pageContext.request.contextPath}/products/addGroup", { fieldId:groupPosition},
                        function (data) {
                            $("#addGroupButtonRow").before(data);
                        });
            });

            $(".delIngredientButton").live('click', deleteIngredientFunction);
            $(".delGroupButton").live('click', deleteGroupFunction);

            $(".ingredientSelector").live('change', ingredientChangeFunction);
            $(".ingredientSelector").change(ingredientChangeFunction);
            $(".ingredientSelector").trigger('change');

            <% String url = request.getAttribute("javax.servlet.forward.servlet_path").toString(); %>
            $("#formsTabs").tabs({
                collapsible:true,
                cookie:{ expires:365, name:"<sec:authentication property="principal.username"/>productTabs" },
                <% if (url.contains("/edit")) { %>selected:0<% } %>
            });
        });
    </script>

</head>
<body>

<%@ include file="/WEB-INF/jsp/header.jsp" %>

<sec:authorize ifAllGranted="ROLE_ADMIN">
    <div id="formsTabs">
        <ul>
            <li><a href="#editionFormTab">Добавить/редактировать</a></li>
        </ul>
        <div id="editionFormTab">
            <form:form method="post" commandName="formProduct" cssClass="content">

                <div class="row <form:errors path='title'>row-error</form:errors>">
                    Название:

                    <form:input path="title" cssClass="basic-input"/>

                    <br/><form:errors path="title" cssClass="error"/>
                </div>

                <div class="row <form:errors path='description'>row-error</form:errors>">
                    Описание:

                    <form:textarea path="description" cssClass="basic-input description-input"/>

                    <br/><form:errors path="description" cssClass="error"/>
                </div>

                <div class="row form-section-caption">
                    <span>Состав:&nbsp;</span>
                    <!-- spacer -->
                    <span class="spacer">&nbsp;</span>
                </div>

                <%
                    FormProduct fp = (FormProduct) request.getAttribute("formProduct");
                    for (int i = 0; i < fp.getIngredientsStr().size(); i++) {
                        pageContext.setAttribute("i", i);
                %>

                <div id="ingredient${i}" class="row <form:errors path='ingredientsStr[${i}]'>row-error</form:errors>">

                    <form:select id="ingredientSelector${i}"
                                 path="ingredientsStr[${i}].id" cssClass="ingredientSelector basic-input select-input">
                        <form:options items="${allIngredients}" itemValue="id" itemLabel="title"/>
                    </form:select>

                    <form:input path="ingredientsStr[${i}].count" cssClass="basic-input number-input"/>
                    <span id="ingredientUnits${i}"></span>

                    <button class="delete-item-button button ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only delIngredientButton"
                            id="del_ingredient_${i}" role="button" aria-disabled="false" title="Удалить ингредиент">
                        <span class="ui-button-icon-primary ui-icon ui-icon-close"></span>
                        <span class="ui-button-text">Удалить ингредиент</span>
                    </button>

                    <br/><form:errors path="ingredientsStr[${i}]" cssClass="error"/>
                </div>

                <%}%>

                <div class="row" id="addIngredientButtonRow">
                    <input type="button" class="button" id="addIngredientButton" value="Добавить ингредиент"/>
                </div>


                <div class="row form-section-caption">
                    <span>Группы:&nbsp;</span>
                    <!-- spacer -->
                    <span class="spacer">&nbsp;</span>
                </div>

                <%
                    for (int i = 0; i < fp.getGroupsStr().size(); i++) {
                        pageContext.setAttribute("i", i);%>
                <div id="group${i}" class="row <form:errors path='groupsStr[${i}]'>row-error</form:errors>">
                    <form:select id="groupSelect${i}" path="groupsStr[${i}].id" cssClass="basic-input select-input">
                        <form:options items="${allGroups}" itemValue="id" itemLabel="title"/>
                    </form:select>

                    <form:input path="groupsStr[${i}].price" cssClass="basic-input number-input"/>

                    <button class="delete-item-button button ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only delGroupButton"
                            id="del_group_${i}" role="button" aria-disabled="false" title="Удалить группу">
                        <span class="ui-button-icon-primary ui-icon ui-icon-close"></span>
                        <span class="ui-button-text">Удалить группу</span>
                    </button>

                    <br/><form:errors path="groupsStr[${i}]" cssClass="error"/>

                </div>
                <%}%>

                <div class="row" id="addGroupButtonRow">
                    <input type="button" class="button" id="addGroupButton" value="Добавить группу"/>
                </div>

                <form:hidden path="id"/>

                <div class="clear-row">
                    <%
                        if (url.contains("/edit")) {
                    %>
                    <input type="submit" class="basic-input" value="Обновить">
                    <% } else {%>
                    <input type="submit" class="basic-input" value="Добавить">
                    <%}%>
                </div>
            </form:form>
        </div>
    </div>

    <c:choose>
        <c:when test="${!empty infoMessage}">
            <div class="ui-widget">
                <div class="info-block ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                            ${infoMessage}</p>
                </div>
            </div>
        </c:when>
        <c:when test="${!empty errorMessage}">
            <div class="ui-widget">
                <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                            ${errorMessage}</p>
                </div>
            </div>
        </c:when>
    </c:choose>
</sec:authorize>

<h3 class="h3table">Товары:</h3>

<p class="reztable">Количество результатов на страницу:
    <a onclick="javascript:setResultsCount(10)">10</a>
    <a onclick="javascript:setResultsCount(25)">25</a>
    <a onclick="javascript:setResultsCount(50)">50</a></p>
<display:table id="row" name="products"
               requestURI="${pageContext.request.getAttribute('javax.servlet.forward.request_uri')}">
    <display:column sortProperty="title" sortable="true" title="Название">
        <c:out value="${row.title}"/>
    </display:column>
    <display:column sortProperty="description" sortable="true" title="Описание">
        <c:out value="${row.description}"/>
    </display:column>
    <display:column sortable="false" title="Состав">
        <nobr>
            <c:forEach items="${row.ingredients}" var="i" varStatus="status">
                <span class="detailsDialogLink" onclick="javascript:showIngredientDetails(${i.ingredient.id})"><c:out
                        value="${i.ingredient.title}"/></span> - <i>${i.count}
                <c:out value="${i.ingredient.units}"/></i>

                <% // remove extra line break %>
                <c:if test="${status.count != 3}"><br/></c:if>

                <% // hide extra content by default %>
                <c:if test="${status.count == 3 && !status.last}">
                    <div class="collapsed-block">
                    <div class="collapsed-content">
                </c:if>

                <% // close opened tags used for content hiding %>
                <c:if test="${status.last && status.count > 3}">
                    </div>
                    <a class="toggle-collapsed-content">Показать всё</a>
                    </div>
                </c:if>
            </c:forEach>
        </nobr>
    </display:column>
    <display:column sortable="false" title="Группы">
        <c:forEach items="${row.groups}" var="g">
            <c:out value="${g.group.title}"/> - ${g.price} р.<br/>
        </c:forEach>
    </display:column>

    <sec:authorize ifAllGranted="ROLE_ADMIN">
        <display:column sortable="false">
            <a href="${pageContext.request.contextPath}/products/edit/${row.id}">
                <span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil">&nbsp;</span>
            </a>
        </display:column>
        <display:column sortable="false">
            <a href="${pageContext.request.contextPath}/products/remove/${row.id}">
                <span class="ui-state-default ui-corner-all ui-icon ui-icon-closethick">&nbsp;</span>
            </a>
        </display:column>
    </sec:authorize>

</display:table>
</body>
</html>
