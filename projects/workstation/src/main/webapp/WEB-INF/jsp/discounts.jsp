<%-- 
    Document   : discounts
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="DiscountsTitle"/></title>

    <%@ include file="/WEB-INF/jsp/include-head.jsp" %>

    <script type="text/javascript">
        $(function () {
            <% String url = request.getAttribute("javax.servlet.forward.servlet_path").toString(); %>
            $("#formsTabs").tabs({
                collapsible:true,
                cookie:{ expires:365, name:"<sec:authentication property="principal.username"/>discountTabs" },
                <% if (url.contains("/edit")) { %>selected:0<% } %>
            });
        });
    </script>
</head>
<body>

<%@ include file="/WEB-INF/jsp/header.jsp" %>

<sec:authorize ifAllGranted="ROLE_ADMIN">
    <div id="formsTabs">
        <ul>
            <li><a href="#editionFormTab">Добавить/редактировать</a></li>
        </ul>
        <div id="editionFormTab">
            <div class="row">
                <h3>Добавить скидку:</h3>
            </div>

            <form:form method="post" commandName="formDiscount" cssClass="content">

                <div class="row <form:errors path='sumFromStr'>row-error</form:errors>">

                    <span>Сумма (от):</span>

                    <form:input path="sumFromStr" cssClass="basic-input"/>

                    <br/><form:errors path="sumFromStr" cssClass="error"/>
                </div>

                <div class="row <form:errors path='discountStr'>row-error</form:errors>">

                    <span>Скидка:</span>

                    <form:input path="discountStr" cssClass="basic-input"/>

                    <br/><form:errors path="discountStr" cssClass="error"/>
                </div>

                <div class="row clear-row">
                    <%
                        if (url.contains("/edit")) {
                    %>
                    <input type="submit" class="button" value="Обновить">
                    <% } else {%>
                    <input type="submit" class="button" value="Добавить">
                    <%}%>
                </div>

            </form:form>
        </div>
    </div>
    <br/>

    <c:choose>
        <c:when test="${!empty infoMessage}">
            <div class="ui-widget">
                <div class="info-block ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                            ${infoMessage}</p>
                </div>
            </div>
        </c:when>
        <c:when test="${!empty errorMessage}">
            <div class="ui-widget">
                <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                            ${errorMessage}</p>
                </div>
            </div>
        </c:when>
    </c:choose>
</sec:authorize>

<% pageContext.setAttribute("MAX_INT", Integer.MAX_VALUE); %>

<h3 class="h3table">Скидки:</h3>
<display:table class="reztable" id="row" name="discounts"
               requestURI="${pageContext.request.getAttribute('javax.servlet.forward.request_uri')}">
    <display:column property="sumFrom" sortable="true" title="От"/>
    <display:column sortProperty="sumTo" sortable="true" title="До">
        <c:choose>
            <c:when test="${row.sumTo == MAX_INT}">
                &#8734;
            </c:when>
            <c:otherwise>
                <c:out value="${row.sumTo}"/>
            </c:otherwise>
        </c:choose>
    </display:column>
    <display:column property="discount" sortable="true" title="Скидка"/>

    <sec:authorize ifAllGranted="ROLE_ADMIN">
        <display:column sortable="false">
            <c:if test="${row.sumFrom != 0}">
                <a href="${pageContext.request.contextPath}/discounts/edit/${row.sumFrom}">
                    <span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil">&nbsp;</span>
                </a>
            </c:if>
        </display:column>
        <display:column sortable="false">
            <c:if test="${row.sumFrom != 0}">
                <a href="${pageContext.request.contextPath}/discounts/remove/${row.sumFrom}">
                    <span class="ui-state-default ui-corner-all ui-icon ui-icon-closethick">&nbsp;</span>
                </a>
            </c:if>
        </display:column>
    </sec:authorize>

</display:table>
</body>
</html>