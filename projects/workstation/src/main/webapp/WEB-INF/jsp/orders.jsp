<%--
    Document   : orders
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>

<%
    String url = request.getAttribute("javax.servlet.forward.servlet_path").toString();
%>

<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="OrdersTitle"/></title>
    <%@ include file="/WEB-INF/jsp/include-head.jsp" %>
    <%@ include file="/WEB-INF/jsp/orders.js.jsp" %>
</head>

<body>
<%@ include file="/WEB-INF/jsp/header.jsp" %>

<table class="submenu">
    <tr>
        <td>
            <sec:authorize ifAllGranted="ROLE_ADMIN">
                <a href="${pageContext.request.contextPath}/orders/statuses">Статусы заказов</a>
            </sec:authorize>
        </td>
    </tr>
</table>

<div id="formsTabs">
<ul>
    <li><a href="#editionFormTab">Добавить/редактировать</a></li>
    <li><a href="#searchFormTab">Искать</a></li>
</ul>

<div id="editionFormTab">
    <form:form method="post" commandName="formOrder" cssClass="content">

    <div class="row <form:errors path='clientPhoneStr'>row-error</form:errors>">
        Телефон: <form:input id="clientPhone" path="clientPhoneStr" cssClass="basic-input"/>
        <p><form:errors path="clientPhoneStr" cssClass="error"/></p>
    </div>

    <div class="row">
        <p id="clientFullName"></p>

        <p style="font-weight: bold" id="clientDiscount"></p>

        <p id="clientDiscountCard"></p>
    </div>

    <div class="row <form:errors path='clientName'>row-error</form:errors>">
        Имя: <form:input path="clientName" cssClass="basic-input"/>
        <p><form:errors path="clientName" cssClass="error"/></p>
    </div>

    <div class="row <form:errors path='personsCountStr'>row-error</form:errors>">
        Количество персон: <form:input path="personsCountStr" cssClass="basic-input number-input"/>
        <p><form:errors path="personsCountStr" cssClass="error"/></p>
    </div>

    <div class="row <form:errors path='comment'>row-error</form:errors>">
        Комментарий: <form:textarea path="comment" cssClass="basic-input description-input"/>
        <p><form:errors path="comment" cssClass="error"/></p>
    </div>

    <div class="row">
        <span class='alternateDiscountOption' id="alternateDiscountCardOption">Указать другую дисконтную карту</span>
        <span class='alternateDiscountOption' id="alternateDiscountOption">Указать скидку вручную</span>
    </div>

    <div class="row <form:errors path='alternateDiscountCard'>row-error</form:errors>" id="alternateDiscountCardInput">
        Номер карты: <form:input path="alternateDiscountCard" cssClass="basic-input"/>
        <p><form:errors path="alternateDiscountCard" cssClass="error"/></p>
    </div>

    <div class="row <form:errors path='alternateDiscount'>row-error</form:errors>" id="alternateDiscountInput">
        Скидка (%): <form:input path="alternateDiscount" cssClass="basic-input"/>
        <p><form:errors path="alternateDiscount" cssClass="error"/></p>
    </div>

    <div class="row <form:errors path='shipmentAddress'>row-error</form:errors>">
        Адрес: <form:input path="shipmentAddress" cssClass="basic-input"/>
        <br/>

        <div id="addressesHint"></div>
        <p><form:errors path="shipmentAddress" cssClass="error"/></p>
    </div>

    <% FormOrder fo = (FormOrder) request.getAttribute("formOrder");
        if (fo.getId() != -1) {
    %>

    <div class="row <form:errors path='statusIdStr'>row-error</form:errors> <form:errors path='carrierIdStr'>row-error</form:errors>">
        Статус: <form:select path="statusIdStr" cssClass="basic-input select-input">
        <form:options items="${allStatuses}" itemValue="id" itemLabel="title"/>
    </form:select>
        <div id="statusData"></div>
        <p><form:errors path="statusIdStr" cssClass="error"/></p>

        <p><form:errors path="carrierIdStr" cssClass="error"/></p>
    </div>

    <% }%>
    <div class="row form-section-caption" style="text-align:right;">
        Товары:
    </div>

    <% for (int i = 0; i < fo.getProductsStr().size(); i++) {
        pageContext.setAttribute("i", i); %>

    <div id="product${i}" class="row <form:errors path='productsStr[${i}]'>row-error</form:errors>">

        <form:select id="groupId${i}" path="productsStr[${i}].groupId"
                     cssClass="basic-input orders-group-selector select-input">
            <form:options items="${allGroups}" itemValue="id" itemLabel="title"/>
        </form:select>

        <span id="productSelector${i}">
            <c:choose>
                <c:when test="${not empty allProducts[i]}">
                    <form:select id="productId${i}" path="productsStr[${i}].productId"
                                 cssClass="basic-input orders-product-selector select-input">
                        <form:options items="${allProducts[i]}" itemValue="product.id" itemLabel="product.title"/>
                    </form:select>
                    <form:input id="productCount${i}" path="productsStr[${i}].count"
                                cssClass="basic-input number-input"/>
                </c:when>
                <c:otherwise>
                    <% fo.getProductsStr().get(i).setProductId(""); %>
                    Товары не найдены. Выберите другую категорию
                </c:otherwise>
            </c:choose>
        </span>

        <span id="productCost${i}"></span>
        <span class="delete-item-button button ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only delButton"
              id="del_${i}" role="button" aria-disabled="false" title="Удалить товар">
            <span class="ui-button-icon-primary ui-icon ui-icon-close"></span>
            <span class="ui-button-text">Удалить товар</span>
        </span>

        <p><form:errors path="productsStr[${i}]" cssClass="error"/></p>
    </div>

    <%}%>

    <div class="row" id="addProductButtonRow" style="float:right; margin-right:52.5%">
        <input type="button" id="addProductButton" class="button" value="Добавить товар"/>
    </div>

    <div class="row"><p>
            <form:hidden path="id"/>
        <br/><br/>
                <%
            if (url.contains("/edit")) {
        %>
        <input type="submit" class="button" value="ОБНОВИТЬ">
                <% } else {%>
        <input type="submit" class="button" value="ДОБАВИТЬ">
                <%}%>

        <div id="orderCostBlock" style="font-weight: bold;">
            <p>Сумма заказа: <span id="orderCost"></span> р.</p>
        </div>
        </form:form>
    </div>
</div>
<div id="searchFormTab">
    <form:form method="post" commandName="orderSearch" action="${pageContext.request.contextPath}/orders/search"
               cssClass="content">
        <div class="ui-widget">
            <div class="info-block ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                    В текстовых полях символ «*» означает любые символы, «?» - любой одинарный символ</p>
            </div>
        </div>
        <div class="row <form:errors path='phone'>row-error</form:errors>">
            Телефон:

            <form:input path="phone" cssClass="basic-input"/>

            <br/><form:errors path="phone" cssClass="error"/>
        </div>

        <div class="row <form:errors path='address'>row-error</form:errors>">
            Адрес:

            <form:input path="address" cssClass="basic-input"/>

            <br/><form:errors path="address" cssClass="error"/>
        </div>

        <div class="row <form:errors path='card'>row-error</form:errors>">
            Дисконтная карта:

            <form:input path="card" cssClass="basic-input"/>

            <br/><form:errors path="card" cssClass="error"/>
        </div>

        <div class="row form-section-caption">
            <span>Дата:&nbsp;</span>
            <!-- spacer -->
            <span class="spacer">&nbsp;</span>
        </div>

        <div class="row <form:errors path='dateFrom'>row-error</form:errors> <form:errors path='dateTo'>row-error</form:errors>">

            От: <form:input path="dateFrom" cssClass="basic-input date-input"/> <br/>
            До:<form:input path="dateTo" cssClass="basic-input date-input"/>

            <br/> <form:errors path="dateFrom" cssClass="error"/>
            <br/><form:errors path="dateTo" cssClass="error"/>
        </div>

        <div class="row form-section-caption">
            <span>Оператор:&nbsp;</span>
            <!-- spacer -->
            <span class="spacer">&nbsp;</span>
        </div>

        <div class="row <form:errors path='registratorName'>row-error</form:errors>
            <form:errors path='registratorSurname'>row-error</form:errors>
            <form:errors path='registratorMe'>row-error</form:errors>">

            <form:checkbox id="onlyMyOrders" path="registratorMe"/> Только внесённые мной <br/>

            <div id="operatorSearchForm">
                Логин: <form:input path="registratorLogin" cssClass="basic-input"/><br/>
                Имя: <form:input path="registratorName" cssClass="basic-input"/><br/>
                Фамилия: <form:input path="registratorSurname" cssClass="basic-input"/>
            </div>

            <br/><form:errors path="registratorName" cssClass="error"/>
            <br/><form:errors path="registratorSurname" cssClass="error"/>
            <br/><form:errors path="registratorMe" cssClass="error"/>

        </div>

        <div class="row form-section-caption">
            <span>Статус:&nbsp;</span>
            <!-- spacer -->
            <span class="spacer">&nbsp;</span>
        </div>

        <div class="row reversed-gravity">
            <!-- spacer -->
            <span class="spacer-left">&nbsp;</span>
            <form:checkboxes items="${allStatuses}" path="status"
                             itemLabel="title" itemValue="id" delimiter="<br/><span class='spacer-left'>&nbsp;</span>"/>
        </div>

        <div class="row <form:errors path='costFrom'>row-error</form:errors> <form:errors path='costTo'>row-error</form:errors>">
            Сумма заказа:

            От <form:input path="costFrom" cssClass="basic-input number-input"/>
            до <form:input path="costTo" cssClass="basic-input number-input"/>

            <br/><form:errors path="costFrom" cssClass="error"/>
            <br/><form:errors path="costTo" cssClass="error"/>
        </div>

        <div class="row <form:errors path='discountFrom'>row-error</form:errors> <form:errors path='discountTo'>row-error</form:errors>">
            Скидка:

            От <form:input path="discountFrom" cssClass="basic-input number-input"/>
            до <form:input path="discountTo" cssClass="basic-input number-input"/>

            <br/><form:errors path="discountFrom" cssClass="error"/>
            <br/><form:errors path="discountTo" cssClass="error"/>
        </div>

        <input type="submit" class="basic-input" value="Искать">
    </form:form>
</div>
</div>
<c:choose>
    <c:when test="${!empty infoMessage}">
        <div class="ui-widget">
            <div class="info-block ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                        ${infoMessage}</p>
            </div>
        </div>
    </c:when>
    <c:when test="${!empty errorMessage}">
        <div class="ui-widget">
            <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                        ${errorMessage}</p>
            </div>
        </div>
    </c:when>
</c:choose>

<c:if test="${orderFilter != null && !orderFilter.isEmpty}">
    <div class="ui-widget">
        <div class="info-block ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                Данные фильтруются. <a href="${pageContext.request.contextPath}/orders/search/cancel">Сбросить
                    фильтр.</a></p>
        </div>
    </div>
</c:if>

<h3 class="h3table">Заказы:</h3>

<p class="reztable">Количество результатов на страницу:
    <a onclick="javascript:setResultsCount(10)">10</a>
    <a onclick="javascript:setResultsCount(25)">25</a>
    <a onclick="javascript:setResultsCount(50)">50</a></p>

<%
    Long epsTime = 60000L;
    pageContext.setAttribute("epsTime", epsTime);
%>

<display:table id="row" name="orders"
               requestURI="${pageContext.request.getAttribute('javax.servlet.forward.request_uri')}">
    <display:column sortProperty="orderID" property="id" sortable="true" title="#"/>
    <display:column sortProperty="registerDate" property="registerDateFormatted" sortable="true" title="Дата"
                    style="width: 1px;"/>
    <display:column sortProperty="clientPhone" sortable="true" title="Клиент">
        <nobr><a class="detailsDialogLink"
                 href="javascript:showClientDetails(${row.client.phone})">${row.client.phoneFormatted}</a></nobr>
    </display:column>
    <display:column sortProperty="shipmentAddress" sortable="true" title="Адрес">
        <c:out value="${row.shipmentAddress}"/>
    </display:column>
    <display:column sortProperty="status" sortable="true" title="Статус">
        <c:out value="${row.status.title}"/>
    </display:column>
    <display:column sortProperty="personsCount" sortable="true" title="Персон">
        <c:out value="${row.personsCount}"/>
    </display:column>
    <display:column sortable="false" title="Товары">
        <nobr>
            <c:forEach items="${row.products}" var="p" varStatus="status">
                <a class="detailsDialogLink" href="javascript:showProductDetails(${p.product.id})"><c:out
                        value="${p.product.title}"/></a> ${p.count} шт.


                <% // remove extra line break %>
                <c:if test="${status.count != 3}"><br/></c:if>

                <% // hide extra content by default %>
                <c:if test="${status.count == 3 && !status.last}">
                    <div class="collapsed-block">
                    <div class="collapsed-content">
                </c:if>

                <% // close opened tags used for content hiding %>
                <c:if test="${status.last && status.count > 3}">
                    </div>
                    <a class="toggle-collapsed-content">Показать всё</a>
                    </div>
                </c:if>
            </c:forEach>
        </nobr>
    </display:column>
    <display:column sortProperty="cost" sortable="true" title="Сумма">
        <fmt:formatNumber value="${row.cost * (100 - row.discount)/100}" type="number" maxFractionDigits="2"/> р.
    </display:column>
    <display:column sortProperty="discount" sortable="true" title="Скидка">
        <c:choose>
            <c:when test="${row.discount == 0}">---</c:when>
            <c:otherwise>
                <nobr><fmt:formatNumber value="${row.discount}" type="number" maxFractionDigits="2"/>%
                    (<fmt:formatNumber value="${row.cost * row.discount/100}" type="number" maxFractionDigits="2"/> р.)
                </nobr>
                <br/>
                <nobr>Карта:
                    <c:choose>
                        <c:when test="${row.client.card != null &&
                        row.client.card.emissionDate.time < row.registerDate.time + epsTime &&
                        row.alternateDiscountCard == null}">
                            ${row.client.card.idFormatted}
                        </c:when>
                        <c:when test="${row.alternateDiscountCard != null}">${row.alternateDiscountCard.idFormatted}</c:when>
                        <c:otherwise>нет</c:otherwise>
                    </c:choose>
                </nobr>
            </c:otherwise>
        </c:choose>
    </display:column>
    <display:column sortProperty="registrator" sortable="true" title="Оператор">
        <c:out value="${row.registrator.name}"/> <c:out value="${row.registrator.surname}"/>
    </display:column>
    <display:column sortProperty="carrier" sortable="true" title="Курьер">
        <c:choose>
            <c:when test="${row.carrier == null}">---</c:when>
            <c:otherwise>
                <c:out value="${row.carrier.name}"/> <c:out value="${row.carrier.surname}"/>
            </c:otherwise>
        </c:choose>
    </display:column>
    <display:column sortProperty="comment" sortable="true" title="Комментарий" style="width: 7%;">
        <c:out value="${row.comment}"/>
    </display:column>

    <sec:authorize ifAnyGranted="ROLE_ADMIN">
        <display:column sortable="false">
            <a href="${pageContext.request.contextPath}/orders/edit/${row.id}">
                <span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil">&nbsp;</span>
            </a>
        </display:column>
        <display:column sortable="false">
            <a href="${pageContext.request.contextPath}/orders/remove/${row.id}">
                <span class="ui-state-default ui-corner-all ui-icon ui-icon-closethick">&nbsp;</span>
            </a>
        </display:column>
    </sec:authorize>
    <sec:authorize access="not hasRole('ROLE_ADMIN')">
        <display:column sortable="false">
            <c:if test="${pageContext.request.userPrincipal.name != null && pageContext.request.userPrincipal.name.equals(row.registrator.login)}">
                <a href="${pageContext.request.contextPath}/orders/edit/${row.id}">
                    <span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil">&nbsp;</span>
                </a>
            </c:if>
        </display:column>
    </sec:authorize>

    <display:column sortable="false">
        <a href='javascript:openInNewWindow("${pageContext.request.contextPath}/orders/print/${row.id}")'>
            <span class="ui-state-default ui-corner-all ui-icon ui-icon-print">&nbsp;</span>
        </a>
    </display:column>

</display:table>
</body>
</html>