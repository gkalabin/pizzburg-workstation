<%-- 
    Created on : 5/23/12
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <title>Чек №${order.id}</title>
</head>
<c:choose>
    <c:when test="${order != null}">
        <body onload="window.print();" style="width: 3.0in;">
        <div id="check" style="width: 3.0in; height: 3.7in; font-size: 10px; font-family: Arial, sans-serif">
            <b>Товарный чек №${order.id} от ${order.registerDateOnly}<br/> ИП Замалеева Е. Л. <br/>ИНН 590612484486 ОГРН 307590616000017</b><br/><br/>

            <table border="0" style="width: 100%; border-spacing: 0;">
                <tr>
                    <td style="width: 15%;"><b>Получатель</b></td>
                    <td style="width: 70%;">${order.client.fullName}</td>
                    <td style="width: 15%;" rowspan="5" valign="top">
                        <img style="width: 60px; height: 60px;" src="${pageContext.request.contextPath}/resources/images/check-logo.jpg"/>
                    </td>
                </tr>
                <tr>
                    <td><b>Телефон</b></td>
                    <td>${order.client.phoneFormatted}</td>
                </tr>
                <tr>
                    <td><b>Адрес</b></td>
                    <td>${order.shipmentAddress}</td>
                </tr>
                <tr>
                    <td><b>Персон</b></td>
                    <td>${order.personsCount}</td>
                </tr>
                <tr>
                    <td valign="top"><b>Комментарий</b></td>
                    <td colspan="2">${order.comment}</td>
                </tr>
            </table>


            <table cellspacing=0 rowspacing=0 style="border-spacing: 0; width: 100%;">
                <thead>
                <td style="border-left: 1px solid; border-top: 1px solid; width: 4%;"><b>№</b></td>
                <td style="border-left: 1px solid; border-top: 1px solid; width: 54%;"><b>Описание</b></td>
                <td style="border-left: 1px solid; border-top: 1px solid; width: 4%;"><b>Кол.</b></td>
                <td style="border-left: 1px solid; border-top: 1px solid; width: 17%;"><b>За ед.</b></td>
                <td style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid; width:21%;"><b>Всего</b></td>
                </thead>
                <c:forEach items="${order.products}" var="item" varStatus="status">
                    <tr>
                        <td style="border-left: 1px solid; border-top: 1px solid;">${status.count}</td>
                        <td style="border-left: 1px solid; border-top: 1px solid;">${item.product.title}</td>
                        <td style="border-left: 1px solid; border-top: 1px solid;">${item.count}</td>
                        <td style="border-left: 1px solid; border-top: 1px solid;">
                            <nobr><fmt:formatNumber value="${item.price * (1 - order.discount/100)}" type="number" maxFractionDigits="2"/> р.</nobr>
                        </td>
                        <td style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid">
                            <nobr><fmt:formatNumber value="${item.price * item.count * (1 - order.discount/100)}" type="number" maxFractionDigits="2"/> р.</nobr>
                        </td>
                    </tr>
                </c:forEach>
                <tr>
                    <td colspan="4" style="border-left: 1px solid; border-top: 1px solid;"><b>Сумма скидки
                        <c:if test="${row.discount > 0}">(<fmt:formatNumber value="${row.discount}" type="number" maxFractionDigits="2"/>%)</c:if>
                    </b></td>
                    <td style="border-left: 1px solid; border-top: 1px solid; border-right: 1px solid">
                        <nobr><fmt:formatNumber value="${order.cost * order.discount/100}" type="number" maxFractionDigits="2"/> р.</nobr>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="border-bottom: 0; border-left: 0; border-top: 1px solid;">&nbsp;</td>
                    <td style="border-left: 1px solid; border-top: 1px solid; border-bottom: 1px solid;"><b>Итого</b></td>
                    <td style="border: 1px solid;"><b>
                        <nobr><fmt:formatNumber value="${order.cost * (1 - order.discount/100)}" type="number" maxFractionDigits="2"/> р.</nobr>
                    </b></td>
                </tr>
            </table>
        </div>

        <div id="check-cook" style="width: 3.0in; height: 3.7in; font-size: 10px; font-family: Arial, SANS-SERIF; page-break-before: always;">
            <b>Товарный чек №${order.id} от ${order.registerDateOnly}</b><br/><br/>

            <table cellspacing=0 rowspacing=0 style="border-spacing: 0; width: 100%;">
                <thead>
                <td style="border-left: 1px solid; border-bottom: 1px solid; border-top: 1px solid; width: 5%;"><b>№</b></td>
                <td style="border-left: 1px solid; border-bottom: 1px solid; border-top: 1px solid; width: 60%;"><b>Описание</b></td>
                <td style="border-left: 1px solid; border-bottom: 1px solid; border-top: 1px solid; border-right:  1px solid; width: 5%;"><b>Кол-во</b></td>
                </thead>
                <c:forEach items="${order.products}" var="item" varStatus="status">
                    <tr>
                        <td style="border-left: 1px solid; border-bottom: 1px solid;">${status.count}</td>
                        <td style="border-left: 1px solid; border-bottom: 1px solid;">${item.product.title}</td>
                        <td style="border-left: 1px solid; border-bottom: 1px solid; border-right:  1px solid;">${item.count}</td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        </body>
    </c:when>
    <c:otherwise>
        <body>Ошибка: заказ не найден.</body>
    </c:otherwise>
</c:choose>
</html>