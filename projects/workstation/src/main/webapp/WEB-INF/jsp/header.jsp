<%-- 
    Document   : header
    Created on : Mar 30, 2012, 2:38:35 PM
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>
<!DOCTYPE html>

<table class="mainmenu">
    <tr>
        <td>
            <a
                    <c:if
                            test="${pageContext.request.getAttribute('javax.servlet.forward.request_uri').contains(pageContext.request.contextPath.concat('/orders'))}">class="selected"</c:if>
                    href="${pageContext.request.contextPath}/orders">Заказы</a></td>

        <sec:authorize ifAllGranted="ROLE_ADMIN">
            <td>
                <a
                        <c:if
                                test="${pageContext.request.getAttribute('javax.servlet.forward.request_uri').contains(pageContext.request.contextPath.concat('/stats'))}">class="selected"</c:if>
                        href="${pageContext.request.contextPath}/stats">Статистика</a></td>
        </sec:authorize>

        <td>
            <a
                    <c:if
                            test="${pageContext.request.getAttribute('javax.servlet.forward.request_uri').contains(pageContext.request.contextPath.concat('/clients'))}">class="selected"</c:if>
                    href="${pageContext.request.contextPath}/clients">Клиенты</a></td>
        <td>
            <a
                    <c:if
                            test="${pageContext.request.getAttribute('javax.servlet.forward.request_uri').contains(pageContext.request.contextPath.concat('/discounts'))}">class="selected"</c:if>
                    href="${pageContext.request.contextPath}/discounts">Скидки</a></td>
        <td>
            <a
                    <c:if
                            test="${pageContext.request.getAttribute('javax.servlet.forward.request_uri').contains(pageContext.request.contextPath.concat('/groups'))}">class="selected"</c:if>
                    href="${pageContext.request.contextPath}/groups">Группы товаров</a></td>
        <td>
            <a
                    <c:if
                            test="${pageContext.request.getAttribute('javax.servlet.forward.request_uri').contains(pageContext.request.contextPath.concat('/products'))}">class="selected"</c:if>
                    href="${pageContext.request.contextPath}/products">Товары</a></td>

        <sec:authorize ifAllGranted="ROLE_ADMIN">
            <td>
                <a
                        <c:if
                                test="${pageContext.request.getAttribute('javax.servlet.forward.request_uri').contains(pageContext.request.contextPath.concat('/ingredients'))}">class="selected"</c:if>
                        href="${pageContext.request.contextPath}/ingredients">Ингредиенты</a></td>
        </sec:authorize>

        <sec:authorize ifAllGranted="ROLE_ADMIN">
            <td>
                <a
                        <c:if
                                test="${pageContext.request.getAttribute('javax.servlet.forward.request_uri').contains(pageContext.request.contextPath.concat('/admin'))}">class="selected"</c:if>
                        href="${pageContext.request.contextPath}/admin">Администрирование</a></td>
        </sec:authorize>

        <sec:authorize access="isAuthenticated()">
            <td class="UserInfo">Вы вошли как
                <span class="username"><sec:authentication property="principal.username"/></span>.
                <a href="${pageContext.request.contextPath}/logout" class="logout">Выйти</a></td>
        </sec:authorize>
    </tr>
</table>