<%-- 
    Document   : addGroupToProduct
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>
<div id="group${groupNumber}" class="row">

    <form:select id="groupSelect${groupNumber}" path="formProduct.groupsStr[${groupNumber}].id"
                 cssClass="basic-input select-input">
        <form:options items="${allGroups}" itemValue="id" itemLabel="title"/>
    </form:select>

    <form:input path="formProduct.groupsStr[${groupNumber}].price" cssClass="basic-input number-input"/>

    <span class="delete-item-button button ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only delGroupButton"
            id="del_group_${groupNumber}" role="button" aria-disabled="false" title="Удалить группу">
        <span class="ui-button-icon-primary ui-icon ui-icon-close"></span>
        <span class="ui-button-text">Удалить группу</span>
    </span>

    <br/><form:errors path="groupsStr[${groupNumber}]" cssClass="error"/>

</div>
