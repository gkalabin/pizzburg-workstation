<%-- 
    Document   : addIngredientToProduct
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>

<div id="ingredient${ingredientNumber}" class="row">

    <form:select id="ingredientSelector${ingredientNumber}" path="formProduct.ingredientsStr[${ingredientNumber}].id"
                 cssClass="ingredientSelector basic-input select-input">
        <form:options items="${allIngredients}" itemValue="id" itemLabel="title"/>
    </form:select>

    <form:input path="formProduct.ingredientsStr[${ingredientNumber}].count" cssClass="basic-input number-input"/>

    <span id="ingredientUnits${ingredientNumber}">
        <c:if test="${not empty allIngredients}">
            <c:out value="${allIngredients[0].units}"/>
        </c:if>
    </span>

    <span class="delete-item-button button ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only delIngredientButton"
            id="del_ingredient_${ingredientNumber}" role="button" aria-disabled="false" title="Удалить ингредиент">
        <span class="ui-button-icon-primary ui-icon ui-icon-close"></span>
        <span class="ui-button-text">Удалить ингредиент</span>
    </span>

    <br/><form:errors path="ingredientsStr[${ingredientNumber}]" cssClass="error"/>
</div>