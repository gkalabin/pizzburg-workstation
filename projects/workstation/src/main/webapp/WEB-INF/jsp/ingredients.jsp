<%@ page import="ru.pizzburg.utils.Digest" %>
<%@ page import="ru.pizzburg.web.controller.IngredientsController" %>
<%--
    Document   : ingredients
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="IngredientsTitle"/></title>

    <%@ include file="/WEB-INF/jsp/include-head.jsp" %>

    <script type="text/javascript">
        function setResultsCount(count) {
            // setup cookie
            $.cookie('<%= Digest.getSha1Hash(request.getRemoteUser() + IngredientsController.RESULT_COUNT_COOKIE_SUFFIX) %>', count);
            // reload page to changes take effect
            window.location.reload();
        }

        $(function () {
            <%
                String url = request.getAttribute("javax.servlet.forward.servlet_path").toString();
                if (url.contains("/edit") || url.contains("/add/") || url.contains("/subtract/")) {
            %>
            $("#formIngredient").get(0).setAttribute('action', "${pageContext.request.getAttribute('javax.servlet.forward.request_uri')}");
            <% } else { %>
            $("#formIngredient").get(0).setAttribute('action', "${pageContext.request.contextPath}/ingredients");
            <% } %>

            $("#formsTabs").tabs({
                collapsible:true,
                cookie:{ expires:365, name:"<sec:authentication property="principal.username"/>ingredientTabs" },
                <% if (url.contains("/edit") || url.contains("/add/") || url.contains("/subtract/")) { %>selected:0<% } %>
            });
        });
    </script>
</head>
<body>

<%@ include file="/WEB-INF/jsp/header.jsp" %>

<div id="formsTabs">
    <ul>
        <li><a href="#editionFormTab">Добавить/редактировать</a></li>
        <li><a href="#searchFormTab">Искать</a></li>
    </ul>
    <div id="editionFormTab">
        <%
            if (url.contains("/add/") || url.contains("/subtract/")) {
        %>

        <!------------------------------------------------------------------------------------------------------- Stock form -->
        <h3>
            <% if (url.contains("/add/")) { %>
            Добавить ингредиент «<c:out value="${formIngredient.title}"/>» на склад:
            <% } else { %>
            Списать ингредиент «<c:out value="${formIngredient.title}"/>» со склада:
            <% } %>
        </h3>
        <form:form method="post" commandName="formIngredient" cssClass="content">
            <div class="row <form:errors path='balanceStr'>row-error</form:errors>">Количество:
                <form:input path="balanceStr" cssClass="basic-input"/>
                <br/><form:errors path="balanceStr" cssClass="error"/>
            </div>

            <form:hidden path="id"/>

            <div class="row">
                <% if (url.contains("/add/")) { %>
                <input type="submit" class="button" value="Добавить">
                <% } else { %>
                <input type="submit" class="button" value="Списать">
                <% } %>
            </div>

        </form:form>

        <% } else {%>

        <!--------------------------------------------------------------------------------- Ingredient addition/edition form -->
        <h3>Добавить ингредиент:</h3>
        <form:form method="post" commandName="formIngredient" cssClass="content">
            <div class="row <form:errors path='title'>row-error</form:errors>">
                Название:

                <form:input path="title" cssClass="basic-input"/>

                <br/><form:errors path="title" cssClass="error"/>
            </div>

            <div class="row <form:errors path='description'>row-error</form:errors>">
                Описание:

                <form:textarea path="description" cssClass="basic-input description-input"/>

                <br/><form:errors path="description" cssClass="error"/>
            </div>

            <div class="row <form:errors path='priceStr'>row-error</form:errors>">
                Цена:

                <form:input path="priceStr" cssClass="basic-input"/>

                <br/><form:errors path="priceStr" cssClass="error"/>
            </div>

            <div class="row <form:errors path='balanceStr'>row-error</form:errors>">
                Остаток:

                <form:input path="balanceStr" cssClass="basic-input"/>

                <br/><form:errors path="balanceStr" cssClass="error"/>
            </div>

            <div class="row <form:errors path='units'>row-error</form:errors>">
                Единицы измерения:

                <form:input path="units" cssClass="basic-input"/>

                <br/><form:errors path="units" cssClass="error"/>
            </div>

            <form:hidden path="id"/>

            <div class="row clear-row">
                <%
                    if (url.contains("/edit")) {
                %>
                <input type="submit" class="button" value="Обновить">
                <% } else {%>
                <input type="submit" class="button" value="Добавить">
                <%}%>
            </div>
        </form:form>
        <%}%>
    </div>
    <div id="searchFormTab">
        <div class="ui-widget">
            <div class="info-block ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                    В текстовых полях символ «*» означает любые символы, «?» - любой одинарный символ</p>
            </div>
        </div>
        <form:form method="post" commandName="ingredientSearch" cssClass="content"
                   action="${pageContext.request.contextPath}/ingredients/search">
            <div class="row <form:errors path='title'>row-error</form:errors>">
                Название:

                <form:input path="title" cssClass="basic-input"/>

                <br/><form:errors path="title" cssClass="error"/>
            </div>

            <div class="row <form:errors path='description'>row-error</form:errors>">
                Описание:

                <form:input path="description" cssClass="basic-input"/>

                <br/><form:errors path="description" cssClass="error"/>
            </div>

            <div class="row <form:errors path='priceFromStr'>row-error</form:errors> <form:errors path='priceToStr'>row-error</form:errors>">
                Цена:

                От <form:input path="priceFromStr" cssClass="basic-input number-input"/>
                до <form:input path="priceToStr" cssClass="basic-input number-input"/>

                <br/><form:errors path="priceFromStr" cssClass="error"/><br/>
                <form:errors path="priceToStr" cssClass="error"/>
            </div>

            <div class="row <form:errors path='balanceFromStr'>row-error</form:errors> <form:errors path='balanceToStr'>row-error</form:errors>">
                Остаток:
                От <form:input path="balanceFromStr" cssClass="basic-input number-input"/>
                до <form:input path="balanceToStr" cssClass="basic-input number-input"/>

                <br/><form:errors path="balanceFromStr" cssClass="error"/><br/>
                <form:errors path="balanceToStr" cssClass="error"/>
            </div>

            <div class="row <form:errors path='units'>row-error</form:errors>">
                Единицы измерения:

                <form:input path="units" cssClass="basic-input"/>

                <br/><form:errors path="units" cssClass="error"/>
            </div>

            <div class="row clear-row">
                <input type="submit" class="button" value="Искать">
            </div>
        </form:form>
    </div>
</div>

<c:choose>
    <c:when test="${!empty infoMessage}">
        <div class="ui-widget">
            <div class="info-block ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                        ${infoMessage}</p>
            </div>
        </div>
    </c:when>
    <c:when test="${!empty errorMessage}">
        <div class="ui-widget">
            <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                        ${errorMessage}</p>
            </div>
        </div>
    </c:when>
</c:choose>

<c:if test="${ingredientFilter != null && !ingredientFilter.isEmpty}">
    <div class="ui-widget">
        <div class="info-block ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                Данные фильтруются. <a href="${pageContext.request.contextPath}/ingredients/search/cancel">Сбросить
                    фильтр.</a></p>
        </div>
    </div>
</c:if>
<h3 class="h3table">Ингредиенты:</h3>
<p class="reztable">Количество результатов на страницу:
    <a onclick="javascript:setResultsCount(10)">10</a>
    <a onclick="javascript:setResultsCount(25)">25</a>
    <a onclick="javascript:setResultsCount(50)">50</a></p>
<display:table id="row" name="ingredients"
               requestURI="${pageContext.request.getAttribute('javax.servlet.forward.request_uri')}">
    <display:column sortProperty="title" sortable="true" title="Название">
        <c:out value="${row.title}"/>
    </display:column>
    <display:column sortProperty="description" sortable="true" title="Описание">
        <c:out value="${row.description}"/>
    </display:column>
    <display:column sortProperty="price" sortable="true" title="Цена">
        <fmt:formatNumber value="${row.price}" type="number" maxFractionDigits="2"/> р.
    </display:column>
    <display:column sortProperty="balance" sortable="true" title="Остаток">
        <fmt:formatNumber value="${row.balance}" type="number" maxFractionDigits="2"/>
        <c:out value="${row.units}"/>
    </display:column>
    <display:column sortProperty="units" sortable="true" title="Единицы измерения">
        <c:out value="${row.units}"/>
    </display:column>
    <display:column sortable="false">
        <a href="${pageContext.request.contextPath}/ingredients/add/${row.id}">
            <span class="ui-state-default ui-corner-all ui-icon ui-icon-plusthick">&nbsp;</span>
        </a>
    </display:column>
    <display:column sortable="false">
        <a href="${pageContext.request.contextPath}/ingredients/subtract/${row.id}">
            <span class="ui-state-default ui-corner-all ui-icon ui-icon-minusthick">&nbsp;</span>
        </a>
    </display:column>
    <display:column sortable="false">
        <a href="${pageContext.request.contextPath}/ingredients/edit/${row.id}">
            <span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil">&nbsp;</span>
        </a>
    </display:column>
    <display:column sortable="false">
        <a href="${pageContext.request.contextPath}/ingredients/remove/${row.id}">
            <span class="ui-state-default ui-corner-all ui-icon ui-icon-closethick">&nbsp;</span>
        </a>
    </display:column>
</display:table>
</body>
</html>