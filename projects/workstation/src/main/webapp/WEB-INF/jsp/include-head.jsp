<%--
    Document   : inclusions from head section
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<link type="text/css" href="${pageContext.request.contextPath}/resources/css/default/jquery-ui-1.8.18.custom.css"
      rel="stylesheet"/>
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/jquery.qtip.css" rel="stylesheet"/>
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet"/>
<link type="text/css" href="${pageContext.request.contextPath}/resources/css/pizzburg.css" rel="stylesheet"/>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-1.7.2.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.8.18.custom.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/js/jquery.cookie.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/js/jquery.scrollto.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.qtip.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pizzburg.js"></script>

<%@ include file="/WEB-INF/jsp/details.js.jsp" %>