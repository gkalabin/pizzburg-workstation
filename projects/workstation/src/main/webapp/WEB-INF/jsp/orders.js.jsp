<%--
    Document   : orders javascript
    Author     : Grigory Kalabin grigory.kalabin@gmail.com
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/jsp/include-taglibs.jsp" %>
<%@ page import="ru.pizzburg.web.service.orders.FormOrder" %>
<%@ page import="ru.pizzburg.web.controller.AjaxController" %>
<%@ page import="ru.pizzburg.utils.Digest" %>
<%@ page import="ru.pizzburg.web.controller.OrdersController" %>

<!DOCTYPE html>
<script type="text/javascript">
var streetsAutocompleteParams = {
    source:function (request, response) {
        $.ajax({
            url:"${pageContext.request.contextPath}/streetsAutocomplete",
            dataType:"json",
            data:{
                maxRows:6,
                startsWith:request.term
            },
            success:function (data) {
                response($.map(data.streets, function (item) {
                    return {
                        label:item,
                        value:item + ", "
                    }
                }));
            }
        });
    },
    minLength:3
};

// replace first '8' with '7'
function setDefaultCountryCode(phone) {
    if (phone.substring(0,1) === "8") {
        phone = "7"+phone.substring(1);
    }
    return phone;
}

function setResultsCount(count) {
    // setup cookie
    $.cookie('<%= Digest.getSha1Hash(request.getRemoteUser() + OrdersController.RESULT_COUNT_COOKIE_SUFFIX) %>', count);
    // reload page to changes take effect
    window.location.reload();
}

function updatePricesInfo(clientChanged, phone) {
    if (clientChanged) { clearUserData(); }

    var products = [];
    var groups = [];
    var counts = [];
    var ids = [];
    var idx = 0;
    var alternateDiscount = "";
    var alternateCard = "";
    var clientPhone = setDefaultCountryCode(phone ? phone : $('#clientPhone').attr('value'));
    var orderId = $('#id').attr('value');

    $('div[id*="product"]:visible').each(function () {
        ids[idx] = parseInt(this.id.substring("product".length));
        products[idx] = parseInt($('#productId' + ids[idx]).attr('value'));
        groups[idx] = parseInt($('#groupId' + ids[idx]).attr('value'));
        counts[idx] = parseInt($('#productCount' + ids[idx]).attr('value'));
        idx++;
    });

    if ((clientPhone == "" || clientPhone == null) && ids.length == 0) {
        $("#clientFullName, #clientDiscount, #orderCostBlock, span[id*='productCost']:visible").css({visibility:"hidden", display:"none"});
    } else {
        $("#clientFullName, #clientDiscount, #orderCostBlock").css({visibility:"visible", display:"block"});
        $("span[id*='productCost']").css({visibility:"visible", display:"inline"});
    }

    if ($("#alternateDiscount").is(":visible")) {
        alternateDiscount = parseInt($('#alternateDiscount').attr('value'));
    }
    if ($("#alternateDiscountCard").is(":visible")) {
        alternateCard = parseInt($('#alternateDiscountCard').attr('value'));
    }

    $.ajax({
        url:"${pageContext.request.contextPath}/orders/getPricesAndDiscount",
        data:{
            phone:clientPhone,
            products:products,
            groups:groups,
            alternateDiscount:alternateDiscount,
            alternateCard:alternateCard,
            counts:counts,
            orderId: orderId
        },
        success:function (data) {
            if (data == null || data.discount == null || data.orderSum == null) {
                return;
            }

            if (clientChanged && data.fullName != null) {
                $('#clientFullName').css({visibility:"visible", display:"block"});
                $('#clientFullName').text(data.fullName);
                addressesHint();
            }

            if (clientChanged && data.name != null) {
                $('#clientName').attr('value', data.name);
            }

            var reason = "";
            var dataReason = data.discountReason;
            if (dataReason != null && dataReason != "") {
                if (dataReason == "<%= AjaxController.DataForOrderCreation.REASON_ALTERNATE_CARD %>") {
                    reason = " по карте #" + $('#alternateDiscountCard').attr('value');
                } else if (dataReason == "<%= AjaxController.DataForOrderCreation.REASON_CUSTOM_DISCOUNT %>") {
                    reason = " указана вручную";
                } else if (dataReason == "<%= AjaxController.DataForOrderCreation.REASON_CLIENT_CARD %>")
                {
                    reason = " по карте клиента";
                }
            }

            $('#clientDiscount').text("Скидка " + data.discount + "% (" + (data.orderSum * data.discount / 100).toFixed(2) + " р.)" + reason);

            if (data.cardNumber != null) {
                $('#clientDiscountCard').css({visibility:"visible", display:"block"});
                $('#clientDiscountCard').text("Дисконтная карта клиента #" + data.cardNumber);
            }

            if (data.cardWillBeAssigned) {
                $('#clientDiscountCard').css({visibility:"visible", display:"block"});
                $('#clientDiscountCard').html("<div style='margin-right: 0px;' class='info-block ui-state-highlight ui-corner-all'>" +
                        "<p style='margin-left: 20px; margin-right: 20px;'><span class='ui-icon ui-icon-info' style='float: left; margin-right: .3em;'></span>" +
                        "Клиенту будет выдана дисконтная карта</p>" +
                        "</div>");
            } else {
                $('#clientDiscountCard').css({visibility:"hidden", display:"none"});
                $('#clientDiscountCard').html("");
            }

            $('#orderCost').text((data.orderSum * (1 - data.discount / 100)).toFixed(2));
            idx = 0;
            $('div[id*="product"]:visible').each(function () {
                var cost = data.prices[idx] * (1 - data.discount / 100);
                if (isInt(counts[idx])) {
                    $('#productCost' + ids[idx]).removeClass("error");
                    $('#productCost' + ids[idx]).text("x" + cost.toFixed(2) + " р. = " + (cost * counts[idx]).toFixed(2) + " р.");
                } else {
                    if ($("#productId" + ids[idx]).length > 0) {
                        $('#productCost' + ids[idx]).addClass("error");
                        $('#productCost' + ids[idx]).text("Количество должно быть целым");
                    }
                }
                idx++;
            });
        }
    });
};

function addressesHint() {
    $.ajax({
        url:"${pageContext.request.contextPath}/orders/getClientAddresses",
        data:{ phone : setDefaultCountryCode($('#clientPhone').attr('value')) },
        success:function (data) {
            if (data == null) {
                return;
            }
            $("#addressesHint").css({visibility:"visible", display:"block"});
            $("#addressesHint").html("");
            for (item in data) {
                $("#addressesHint").append("<span class='addressHint'>" + data[item] + "</span>");
            }
        }
    });
}

function clearUserData() {
    $("#addressesHint").css({visibility:"hidden", display:"none"}).html("");
    $('#clientDiscountCard').css({visibility:"hidden", display:"none"}).html("");
    $('#clientFullName').css({visibility:"hidden", display:"none"}).html("");
}

var onGroupSelect = function () {
    var fieldId = parseInt(this.id.substring("groupId".length));
    var selectedGroup = parseInt($(this).attr('value'));
    var fieldIdStr = this.id;
    $.get("${pageContext.request.contextPath}/orders/groupChanged", {fieldId:fieldId, selectedGroup:selectedGroup},
            function (data) {
                $("#productSelector" + fieldId).remove();
                $("#" + fieldIdStr).after(data);
                updatePricesInfo(false);
            });
};

$(function () {
    $('#dateFrom').datetimepicker({
        onClose:function (dateText, inst) {
            var endDateTextBox = $('#dateTo');
            if (endDateTextBox.val() != '') {
                var testStartDate = new Date(dateText);
                var testEndDate = new Date(endDateTextBox.val());
                if (testStartDate > testEndDate)
                    endDateTextBox.val(dateText);
            }
            else {
                endDateTextBox.val(dateText);
            }
        },
        onSelect:function (selectedDateTime) {
            var start = $(this).datetimepicker('getDate');
            $('#dateTo').datetimepicker('option', 'minDate', new Date(start.getTime()));
        },
        timeFormat:'hh:mm',
        dateFormat:'dd.mm.yy'
    });
    $('#dateTo').datetimepicker({
        onClose:function (dateText, inst) {
            var startDateTextBox = $('#dateFrom');
            if (startDateTextBox.val() != '') {
                var testStartDate = new Date(startDateTextBox.val());
                var testEndDate = new Date(dateText);
                if (testStartDate > testEndDate)
                    startDateTextBox.val(dateText);
            }
            else {
                startDateTextBox.val(dateText);
            }
        },
        onSelect:function (selectedDateTime) {
            var end = $(this).datetimepicker('getDate');
            $('#dateFrom').datetimepicker('option', 'maxDate', new Date(end.getTime()));
        },
        timeFormat:'hh:mm',
        dateFormat:'dd.mm.yy'
    });

    $("#clientPhone").autocomplete({
        source:function (request, response) {
            $.ajax({
                url:"${pageContext.request.contextPath}/orders/clientsAutocomplete",
                dataType:"json",
                data:{
                    maxRows:6,
                    startsWith:setDefaultCountryCode(request.term)
                },
                success:function (data) {
                    response($.map(data.clients, function (item) {
                        return {
                            label:item,
                            value:item
                        }
                    }));
                }
            });
        },
        minLength:3,
        select:function (event, ui) {updatePricesInfo(true, ui.item ? ui.item.value : null)}
    });

    $("#shipmentAddress, #address").autocomplete(streetsAutocompleteParams);

    var productPosition = <%= ((FormOrder) request.getAttribute("formOrder")).getProductsStr().size() - 1%>;

    var deleteFunction = function () {
        var $id = parseInt(this.id.substring(4));
        $("#product" + $id).remove();
        $.get("${pageContext.request.contextPath}/orders/deleteProduct", { fieldId:$id}, null);
        updatePricesInfo(false);
    };

    $("#addProductButton").click(function () {
        productPosition++;
        $.get("${pageContext.request.contextPath}/orders/addProduct", { fieldId:productPosition},
                function (data) {
                    $("#addProductButtonRow").before(data);
                    updatePricesInfo(false);
                });
    });

    $('#onlyMyOrders').change(function () {
        if ($('#onlyMyOrders').is(':checked')) {
            $("#operatorSearchForm").css({visibility:"hidden", display:"none"});
        } else {
            $("#operatorSearchForm").css({visibility:"visible", display:"block"});
        }
    });

    if ($('#onlyMyOrders').is(':checked')) {
        $("#operatorSearchForm").css({visibility:"hidden", display:"none"});
    } else {
        $("#operatorSearchForm").css({visibility:"visible", display:"block"});
    }

    $('*[id*="groupId"]:visible').live('change', onGroupSelect);
    $(".delButton").live('click', deleteFunction);
    $('*[id*="productId"]:visible').live('change', function () {updatePricesInfo(false)});
    $('*[id*="productCount"]:visible').live('change', function () {updatePricesInfo(false)});
    $('#clientPhone').keyup(function (event) {
        if (event.which <= 40 && event.which >= 37) {
            return;
        }

        var phoneDigitsPattern = /\D/g;
        var phoneDigits = $(this).attr('value').toString().replace(phoneDigitsPattern, "").length;

        if ((event.which == 8 || event.which == 46) && phoneDigits == 10 ||
                phoneDigits == 11 ||
                (event.which != 8 || event.which != 46) && phoneDigits == 12) {
            updatePricesInfo(true);
        }
        if ((event.which == 8 || event.which == 46) && phoneDigits == 6 ||
                phoneDigits == 7 ||
                (event.which != 8 || event.which != 46) && phoneDigits == 8) {
            updatePricesInfo(true);
        }
    });
    $('.addressHint').live('click', function () {
        $('#shipmentAddress').val($(this).text());
    });
    $("#addressesHint").css({visibility:"hidden", display:"none"});

    if ($("#statusIdStr").length > 0) {
        $("#statusIdStr").live('change', function () {
            $.ajax({
                url:"${pageContext.request.contextPath}/orders/statusChange",
                data:{
                    statusId:$("#statusIdStr").attr('value')
                },
                success:function (data) {
                    $("#statusData").html(data);
                }
            });
        });
        $("#statusIdStr").trigger('change');
    }

    $('#alternateDiscountCardOption').live('click', function () {
        $('#alternateDiscountCardInput').toggle(100, function() {
            $('#alternateDiscountInput').hide();
            updatePricesInfo(false);
        });
    });
    $('#alternateDiscountOption').live('click', function () {
        $('#alternateDiscountInput').toggle(100, function() {
            $('#alternateDiscountCardInput').hide();
            updatePricesInfo(false);
        });
    });
    $('#alternateDiscountInput').hide();
    $('#alternateDiscountCardInput').hide();
    <c:if test="${not empty formOrder.alternateDiscount}">
    $('#alternateDiscountInput').show();
    </c:if>
    <c:if test="${not empty formOrder.alternateDiscountCard}">
    $('#alternateDiscountCardInput').show();
    </c:if>

    $('#alternateDiscount').keyup(function (event) {
        updatePricesInfo(false);
    });

    $('#alternateDiscountCard').keyup(function (event) {
        updatePricesInfo(false);
    });

    $("#formOrder").submit(function () {
        if (!$('#alternateDiscountInput').is(":visible")) {
            $('#alternateDiscount').val("");
        }
        if (!$('#alternateDiscountCardInput').is(":visible")) {
            $('#alternateDiscountCard').val("");
        }
        return true;
    });

    <%
        if (url.contains("/edit")) {
    %>
    $("#formOrder").get(0).setAttribute('action', "${pageContext.request.getAttribute('javax.servlet.forward.request_uri')}");
    updatePricesInfo(true);
    <% } else {%>
    $("#formOrder").get(0).setAttribute('action', "${pageContext.request.contextPath}/orders");
    <c:if test="${not empty errorMessage}">updatePricesInfo(true);
    </c:if>
    <spring:hasBindErrors name="formOrder">
    updatePricesInfo(true);
    </spring:hasBindErrors>
    <%}%>

    $("#formsTabs").tabs({
        collapsible:true,
        cookie:{ expires:365, name:"<sec:authentication property="principal.username"/>ordersFormsTabs" },
        <% if (url.contains("/edit")) { %>selected: 0<% } %>
    });
});
</script>