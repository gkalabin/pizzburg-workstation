function isInt(n) {
    return parseFloat(n) == parseInt(n) && !isNaN(n);
}

function openInNewWindow(url) {
    window.open(url);
}

// it's extremly slow and have some bugs
//$.ui.autocomplete.prototype._renderItem = function( ul, item){
//  var term = this.term.split(' ').join('|');
//  var re = new RegExp("(" + term + ")", "gi") ;
//  var t = item.label.replace(re,"<b>$1</b>");
//  return $( "<li></li>" )
//     .data( "item.autocomplete", item )
//     .append( "<a>" + t + "</a>" )
//     .appendTo( ul );
//};

$.ajaxSetup({
    traditional:true
});

$(function () {
    $("input:submit, input:button").button();

    $('.content .left, .content input, .content textarea, .content select').live('focus',
        function () {
            $(this).parents('.row').addClass("over");
        });
    $('.content .left, .content input, .content textarea, .content select').live('blur',
        function () {
            $(this).parents('.row').removeClass("over");
        });

    $('.toggle-collapsed-content').live('click',
        function () {
            var button = $(this);
            var content = $(this).parent(".collapsed-block").children(".collapsed-content");
            $(this).parent(".collapsed-block").parent().ScrollTo({offset:-5});
            content.toggle(100, function () {
                if (content.is(":visible")) {
                    button.text("Скрыть");
                } else {
                    button.text("Показать всё");
                }
            });
        });

    $('form').submit(function () {
        // On submit disable its submit button
        $('input[type=submit]', this).attr('disabled', 'disabled');
    });
});